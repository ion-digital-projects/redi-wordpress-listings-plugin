
# REDi WordPress Listings Plugin

Provides access to live sales information & capturing of lead interest.


## Getting Started

Make sure that PHP 7.2+, Composer is installed - if not, they are available from the following places:

* [PHP](https://php.net/ "php.net").
* [Composer](https://getcomposer.org/ "getcomposer.org").


###For development:

Execute the following from the package's root directory, in order to update all dependencies:

> php composer.phar update


Or, on Linux and Windows:

> composer update


###For deployment:

Follow the steps for development, then execute the following from the package's root directory, in order to create a .ZIP file of the current version:


On Linux:

> ./make archive


Or, on Windows:

> make archive


A deployable .ZIP file of the current version and the branch it was generated from, should be available in the sub-directory /archives, along with the uncompressed project build in a directory corresponding to its build version.


### Prerequisites

* GIT
* Composer
* PHP 7.2+ (for development)
* PHP 5.6+ (for deployment)


## Built With

* [PHP](https://php.net/) - PHP 7.2+
* [Composer](https://getcomposer.org/) - Dependency Management
* [Phing](https://www.phing.info) - Used to generate custom builds for various target PHP versions (5.6, 7.0, 7.1)
* [NetBeans](https://www.netbeans.org) - IDE
* [WordPress Helper](https://bitbucket.org/ion-digital-projects/wordpress-helper/) - WordPress functionality


## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://bitbucket.org/ion-digital-projects/redi-wordpress-listings-plugin/downloads/?tab=tags "bitbucket.org"). 


## Authors

* **Justus Meyer** - *Initial work* - [Justus Meyer (WordPress plugin development)](https://justusmeyer.com/wordpress-plugin-development)


## License

This project is licensed under a proprietary License - see the [LICENSE.md](LICENSE.md) file for details, if available.
