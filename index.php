<?php

/*
  Plugin Name: REDi Listings
  Version: 0.6.2 (main)
  Plugin URI: http://www.red-i.co.za
  Author: REDi
  Author URI: http://justusmeyer.com
  Description: Provides access to live sales information & capturing of lead interest.
  Text Domain: REDi
  Domain Path: /languages
 */

if(!defined('ABSPATH')) {

    http_response_code(404);
    exit;
}

require_once(__DIR__ . '/autoload.php');

$package = \ion\Package::getInstance("redi", "redi-plugin");

if($package === null) {
    
    throw new \Exception("No REDi plugin package has been defined?");
}

new ion\Viewport\RedI\RedIFeedPlugIn($package);

