
$redI = function (data) {

    this.data = null;

    this.init = function (data) {

        var self = this;

        this.data = data;

        jQuery(document).find("a[plantype]").each(function (index, element) {

            var planType = jQuery(element).attr("plantype");

            if (planType) {
                jQuery(element).click(function () {
                    self.updateFields(planType);
                    return false;
                });
            }

        });

    };

    this.updateFields = function (planType) {



    };

    this.init(data);
};



var phoneClicked = false;
var emailClicked = false;



function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

jQuery(document).ready(function () {
    $redI();

    /*! Magnific Popup - v1.1.0 - 2016-02-20
     * http://dimsemenov.com/plugins/magnific-popup/
     * Copyright (c) 2016 Dmitry Semenov; */
    !function (a) {
        "function" == typeof define && define.amd ? define(["jquery"], a) : a("object" == typeof exports ? require("jquery") : window.jQuery || window.Zepto)
    }(function (a) {
        var b, c, d, e, f, g, h = "Close", i = "BeforeClose", j = "AfterClose", k = "BeforeAppend", l = "MarkupParse", m = "Open", n = "Change", o = "mfp", p = "." + o, q = "mfp-ready", r = "mfp-removing", s = "mfp-prevent-close", t = function () {}, u = !!window.jQuery, v = a(window), w = function (a, c) {
            b.ev.on(o + a + p, c)
        }, x = function (b, c, d, e) {
            var f = document.createElement("div");
            return f.className = "mfp-" + b, d && (f.innerHTML = d), e ? c && c.appendChild(f) : (f = a(f), c && f.appendTo(c)), f
        }, y = function (c, d) {
            b.ev.triggerHandler(o + c, d), b.st.callbacks && (c = c.charAt(0).toLowerCase() + c.slice(1), b.st.callbacks[c] && b.st.callbacks[c].apply(b, a.isArray(d) ? d : [d]))
        }, z = function (c) {
            return c === g && b.currTemplate.closeBtn || (b.currTemplate.closeBtn = a(b.st.closeMarkup.replace("%title%", b.st.tClose)), g = c), b.currTemplate.closeBtn
        }, A = function () {
            a.magnificPopup.instance || (b = new t, b.init(), a.magnificPopup.instance = b)
        }, B = function () {
            var a = document.createElement("p").style, b = ["ms", "O", "Moz", "Webkit"];
            if (void 0 !== a.transition)
                return!0;
            for (; b.length; )
                if (b.pop() + "Transition"in a)
                    return!0;
            return!1
        };
        t.prototype = {constructor: t, init: function () {
                var c = navigator.appVersion;
                b.isLowIE = b.isIE8 = document.all && !document.addEventListener, b.isAndroid = /android/gi.test(c), b.isIOS = /iphone|ipad|ipod/gi.test(c), b.supportsTransition = B(), b.probablyMobile = b.isAndroid || b.isIOS || /(Opera Mini)|Kindle|webOS|BlackBerry|(Opera Mobi)|(Windows Phone)|IEMobile/i.test(navigator.userAgent), d = a(document), b.popupsCache = {}
            }, open: function (c) {
                var e;
                if (c.isObj === !1) {
                    b.items = c.items.toArray(), b.index = 0;
                    var g, h = c.items;
                    for (e = 0; e < h.length; e++)
                        if (g = h[e], g.parsed && (g = g.el[0]), g === c.el[0]) {
                            b.index = e;
                            break
                        }
                } else
                    b.items = a.isArray(c.items) ? c.items : [c.items], b.index = c.index || 0;
                if (b.isOpen)
                    return void b.updateItemHTML();
                b.types = [], f = "", c.mainEl && c.mainEl.length ? b.ev = c.mainEl.eq(0) : b.ev = d, c.key ? (b.popupsCache[c.key] || (b.popupsCache[c.key] = {}), b.currTemplate = b.popupsCache[c.key]) : b.currTemplate = {}, b.st = a.extend(!0, {}, a.magnificPopup.defaults, c), b.fixedContentPos = "auto" === b.st.fixedContentPos ? !b.probablyMobile : b.st.fixedContentPos, b.st.modal && (b.st.closeOnContentClick = !1, b.st.closeOnBgClick = !1, b.st.showCloseBtn = !1, b.st.enableEscapeKey = !1), b.bgOverlay || (b.bgOverlay = x("bg").on("click" + p, function () {
                    b.close()
                }), b.wrap = x("wrap").attr("tabindex", -1).on("click" + p, function (a) {
                    b._checkIfClose(a.target) && b.close()
                }), b.container = x("container", b.wrap)), b.contentContainer = x("content"), b.st.preloader && (b.preloader = x("preloader", b.container, b.st.tLoading));
                var i = a.magnificPopup.modules;
                for (e = 0; e < i.length; e++) {
                    var j = i[e];
                    j = j.charAt(0).toUpperCase() + j.slice(1), b["init" + j].call(b)
                }
                y("BeforeOpen"), b.st.showCloseBtn && (b.st.closeBtnInside ? (w(l, function (a, b, c, d) {
                    c.close_replaceWith = z(d.type)
                }), f += " mfp-close-btn-in") : b.wrap.append(z())), b.st.alignTop && (f += " mfp-align-top"), b.fixedContentPos ? b.wrap.css({overflow: b.st.overflowY, overflowX: "hidden", overflowY: b.st.overflowY}) : b.wrap.css({top: v.scrollTop(), position: "absolute"}), (b.st.fixedBgPos === !1 || "auto" === b.st.fixedBgPos && !b.fixedContentPos) && b.bgOverlay.css({height: d.height(), position: "absolute"}), b.st.enableEscapeKey && d.on("keyup" + p, function (a) {
                    27 === a.keyCode && b.close()
                }), v.on("resize" + p, function () {
                    b.updateSize()
                }), b.st.closeOnContentClick || (f += " mfp-auto-cursor"), f && b.wrap.addClass(f);
                var k = b.wH = v.height(), n = {};
                if (b.fixedContentPos && b._hasScrollBar(k)) {
                    var o = b._getScrollbarSize();
                    o && (n.marginRight = o)
                }
                b.fixedContentPos && (b.isIE7 ? a("body, html").css("overflow", "hidden") : n.overflow = "hidden");
                var r = b.st.mainClass;
                return b.isIE7 && (r += " mfp-ie7"), r && b._addClassToMFP(r), b.updateItemHTML(), y("BuildControls"), a("html").css(n), b.bgOverlay.add(b.wrap).prependTo(b.st.prependTo || a(document.body)), b._lastFocusedEl = document.activeElement, setTimeout(function () {
                    b.content ? (b._addClassToMFP(q), b._setFocus()) : b.bgOverlay.addClass(q), d.on("focusin" + p, b._onFocusIn)
                }, 16), b.isOpen = !0, b.updateSize(k), y(m), c
            }, close: function () {
                b.isOpen && (y(i), b.isOpen = !1, b.st.removalDelay && !b.isLowIE && b.supportsTransition ? (b._addClassToMFP(r), setTimeout(function () {
                    b._close()
                }, b.st.removalDelay)) : b._close())
            }, _close: function () {
                y(h);
                var c = r + " " + q + " ";
                if (b.bgOverlay.detach(), b.wrap.detach(), b.container.empty(), b.st.mainClass && (c += b.st.mainClass + " "), b._removeClassFromMFP(c), b.fixedContentPos) {
                    var e = {marginRight: ""};
                    b.isIE7 ? a("body, html").css("overflow", "") : e.overflow = "", a("html").css(e)
                }
                d.off("keyup" + p + " focusin" + p), b.ev.off(p), b.wrap.attr("class", "mfp-wrap").removeAttr("style"), b.bgOverlay.attr("class", "mfp-bg"), b.container.attr("class", "mfp-container"), !b.st.showCloseBtn || b.st.closeBtnInside && b.currTemplate[b.currItem.type] !== !0 || b.currTemplate.closeBtn && b.currTemplate.closeBtn.detach(), b.st.autoFocusLast && b._lastFocusedEl && a(b._lastFocusedEl).focus(), b.currItem = null, b.content = null, b.currTemplate = null, b.prevHeight = 0, y(j)
            }, updateSize: function (a) {
                if (b.isIOS) {
                    var c = document.documentElement.clientWidth / window.innerWidth, d = window.innerHeight * c;
                    b.wrap.css("height", d), b.wH = d
                } else
                    b.wH = a || v.height();
                b.fixedContentPos || b.wrap.css("height", b.wH), y("Resize")
            }, updateItemHTML: function () {
                var c = b.items[b.index];
                b.contentContainer.detach(), b.content && b.content.detach(), c.parsed || (c = b.parseEl(b.index));
                var d = c.type;
                if (y("BeforeChange", [b.currItem ? b.currItem.type : "", d]), b.currItem = c, !b.currTemplate[d]) {
                    var f = b.st[d] ? b.st[d].markup : !1;
                    y("FirstMarkupParse", f), f ? b.currTemplate[d] = a(f) : b.currTemplate[d] = !0
                }
                e && e !== c.type && b.container.removeClass("mfp-" + e + "-holder");
                var g = b["get" + d.charAt(0).toUpperCase() + d.slice(1)](c, b.currTemplate[d]);
                b.appendContent(g, d), c.preloaded = !0, y(n, c), e = c.type, b.container.prepend(b.contentContainer), y("AfterChange")
            }, appendContent: function (a, c) {
                b.content = a, a ? b.st.showCloseBtn && b.st.closeBtnInside && b.currTemplate[c] === !0 ? b.content.find(".mfp-close").length || b.content.append(z()) : b.content = a : b.content = "", y(k), b.container.addClass("mfp-" + c + "-holder"), b.contentContainer.append(b.content)
            }, parseEl: function (c) {
                var d, e = b.items[c];
                if (e.tagName ? e = {el: a(e)} : (d = e.type, e = {data: e, src: e.src}), e.el) {
                    for (var f = b.types, g = 0; g < f.length; g++)
                        if (e.el.hasClass("mfp-" + f[g])) {
                            d = f[g];
                            break
                        }
                    e.src = e.el.attr("data-mfp-src"), e.src || (e.src = e.el.attr("href"))
                }
                return e.type = d || b.st.type || "inline", e.index = c, e.parsed = !0, b.items[c] = e, y("ElementParse", e), b.items[c]
            }, addGroup: function (a, c) {
                var d = function (d) {
                    d.mfpEl = this, b._openClick(d, a, c)
                };
                c || (c = {});
                var e = "click.magnificPopup";
                c.mainEl = a, c.items ? (c.isObj = !0, a.off(e).on(e, d)) : (c.isObj = !1, c.delegate ? a.off(e).on(e, c.delegate, d) : (c.items = a, a.off(e).on(e, d)))
            }, _openClick: function (c, d, e) {
                var f = void 0 !== e.midClick ? e.midClick : a.magnificPopup.defaults.midClick;
                if (f || !(2 === c.which || c.ctrlKey || c.metaKey || c.altKey || c.shiftKey)) {
                    var g = void 0 !== e.disableOn ? e.disableOn : a.magnificPopup.defaults.disableOn;
                    if (g)
                        if (a.isFunction(g)) {
                            if (!g.call(b))
                                return!0
                        } else if (v.width() < g)
                            return!0;
                    c.type && (c.preventDefault(), b.isOpen && c.stopPropagation()), e.el = a(c.mfpEl), e.delegate && (e.items = d.find(e.delegate)), b.open(e)
                }
            }, updateStatus: function (a, d) {
                if (b.preloader) {
                    c !== a && b.container.removeClass("mfp-s-" + c), d || "loading" !== a || (d = b.st.tLoading);
                    var e = {status: a, text: d};
                    y("UpdateStatus", e), a = e.status, d = e.text, b.preloader.html(d), b.preloader.find("a").on("click", function (a) {
                        a.stopImmediatePropagation()
                    }), b.container.addClass("mfp-s-" + a), c = a
                }
            }, _checkIfClose: function (c) {
                if (!a(c).hasClass(s)) {
                    var d = b.st.closeOnContentClick, e = b.st.closeOnBgClick;
                    if (d && e)
                        return!0;
                    if (!b.content || a(c).hasClass("mfp-close") || b.preloader && c === b.preloader[0])
                        return!0;
                    if (c === b.content[0] || a.contains(b.content[0], c)) {
                        if (d)
                            return!0
                    } else if (e && a.contains(document, c))
                        return!0;
                    return!1
                }
            }, _addClassToMFP: function (a) {
                b.bgOverlay.addClass(a), b.wrap.addClass(a)
            }, _removeClassFromMFP: function (a) {
                this.bgOverlay.removeClass(a), b.wrap.removeClass(a)
            }, _hasScrollBar: function (a) {
                return(b.isIE7 ? d.height() : document.body.scrollHeight) > (a || v.height())
            }, _setFocus: function () {
                (b.st.focus ? b.content.find(b.st.focus).eq(0) : b.wrap).focus()
            }, _onFocusIn: function (c) {
                return c.target === b.wrap[0] || a.contains(b.wrap[0], c.target) ? void 0 : (b._setFocus(), !1)
            }, _parseMarkup: function (b, c, d) {
                var e;
                d.data && (c = a.extend(d.data, c)), y(l, [b, c, d]), a.each(c, function (c, d) {
                    if (void 0 === d || d === !1)
                        return!0;
                    if (e = c.split("_"), e.length > 1) {
                        var f = b.find(p + "-" + e[0]);
                        if (f.length > 0) {
                            var g = e[1];
                            "replaceWith" === g ? f[0] !== d[0] && f.replaceWith(d) : "img" === g ? f.is("img") ? f.attr("src", d) : f.replaceWith(a("<img>").attr("src", d).attr("class", f.attr("class"))) : f.attr(e[1], d)
                        }
                    } else
                        b.find(p + "-" + c).html(d)
                })
            }, _getScrollbarSize: function () {
                if (void 0 === b.scrollbarSize) {
                    var a = document.createElement("div");
                    a.style.cssText = "width: 99px; height: 99px; overflow: scroll; position: absolute; top: -9999px;", document.body.appendChild(a), b.scrollbarSize = a.offsetWidth - a.clientWidth, document.body.removeChild(a)
                }
                return b.scrollbarSize
            }}, a.magnificPopup = {instance: null, proto: t.prototype, modules: [], open: function (b, c) {
                return A(), b = b ? a.extend(!0, {}, b) : {}, b.isObj = !0, b.index = c || 0, this.instance.open(b)
            }, close: function () {
                return a.magnificPopup.instance && a.magnificPopup.instance.close()
            }, registerModule: function (b, c) {
                c.options && (a.magnificPopup.defaults[b] = c.options), a.extend(this.proto, c.proto), this.modules.push(b)
            }, defaults: {disableOn: 0, key: null, midClick: !1, mainClass: "", preloader: !0, focus: "", closeOnContentClick: !1, closeOnBgClick: !0, closeBtnInside: !0, showCloseBtn: !0, enableEscapeKey: !0, modal: !1, alignTop: !1, removalDelay: 0, prependTo: null, fixedContentPos: "auto", fixedBgPos: "auto", overflowY: "auto", closeMarkup: '<button title="%title%" type="button" class="mfp-close">&#215;</button>', tClose: "Close (Esc)", tLoading: "Loading...", autoFocusLast: !0}}, a.fn.magnificPopup = function (c) {
            A();
            var d = a(this);
            if ("string" == typeof c)
                if ("open" === c) {
                    var e, f = u ? d.data("magnificPopup") : d[0].magnificPopup, g = parseInt(arguments[1], 10) || 0;
                    f.items ? e = f.items[g] : (e = d, f.delegate && (e = e.find(f.delegate)), e = e.eq(g)), b._openClick({mfpEl: e}, d, f)
                } else
                    b.isOpen && b[c].apply(b, Array.prototype.slice.call(arguments, 1));
            else
                c = a.extend(!0, {}, c), u ? d.data("magnificPopup", c) : d[0].magnificPopup = c, b.addGroup(d, c);
            return d
        };
        var C, D, E, F = "inline", G = function () {
            E && (D.after(E.addClass(C)).detach(), E = null)
        };
        a.magnificPopup.registerModule(F, {options: {hiddenClass: "hide", markup: "", tNotFound: "Content not found"}, proto: {initInline: function () {
                    b.types.push(F), w(h + "." + F, function () {
                        G()
                    })
                }, getInline: function (c, d) {
                    if (G(), c.src) {
                        var e = b.st.inline, f = a(c.src);
                        if (f.length) {
                            var g = f[0].parentNode;
                            g && g.tagName && (D || (C = e.hiddenClass, D = x(C), C = "mfp-" + C), E = f.after(D).detach().removeClass(C)), b.updateStatus("ready")
                        } else
                            b.updateStatus("error", e.tNotFound), f = a("<div>");
                        return c.inlineElement = f, f
                    }
                    return b.updateStatus("ready"), b._parseMarkup(d, {}, c), d
                }}});
        var H, I = "ajax", J = function () {
            H && a(document.body).removeClass(H)
        }, K = function () {
            J(), b.req && b.req.abort()
        };
        a.magnificPopup.registerModule(I, {options: {settings: null, cursor: "mfp-ajax-cur", tError: '<a href="%url%">The content</a> could not be loaded.'}, proto: {initAjax: function () {
                    b.types.push(I), H = b.st.ajax.cursor, w(h + "." + I, K), w("BeforeChange." + I, K)
                }, getAjax: function (c) {
                    H && a(document.body).addClass(H), b.updateStatus("loading");
                    var d = a.extend({url: c.src, success: function (d, e, f) {
                            var g = {data: d, xhr: f};
                            y("ParseAjax", g), b.appendContent(a(g.data), I), c.finished = !0, J(), b._setFocus(), setTimeout(function () {
                                b.wrap.addClass(q)
                            }, 16), b.updateStatus("ready"), y("AjaxContentAdded")
                        }, error: function () {
                            J(), c.finished = c.loadError = !0, b.updateStatus("error", b.st.ajax.tError.replace("%url%", c.src))
                        }}, b.st.ajax.settings);
                    return b.req = a.ajax(d), ""
                }}});
        var L, M = function (c) {
            if (c.data && void 0 !== c.data.title)
                return c.data.title;
            var d = b.st.image.titleSrc;
            if (d) {
                if (a.isFunction(d))
                    return d.call(b, c);
                if (c.el)
                    return c.el.attr(d) || ""
            }
            return""
        };
        a.magnificPopup.registerModule("image", {options: {markup: '<div class="mfp-figure"><div class="mfp-close"></div><figure><div class="mfp-img"></div><figcaption><div class="mfp-bottom-bar"><div class="mfp-title"></div><div class="mfp-counter"></div></div></figcaption></figure></div>', cursor: "mfp-zoom-out-cur", titleSrc: "title", verticalFit: !0, tError: '<a href="%url%">The image</a> could not be loaded.'}, proto: {initImage: function () {
                    var c = b.st.image, d = ".image";
                    b.types.push("image"), w(m + d, function () {
                        "image" === b.currItem.type && c.cursor && a(document.body).addClass(c.cursor)
                    }), w(h + d, function () {
                        c.cursor && a(document.body).removeClass(c.cursor), v.off("resize" + p)
                    }), w("Resize" + d, b.resizeImage), b.isLowIE && w("AfterChange", b.resizeImage)
                }, resizeImage: function () {
                    var a = b.currItem;
                    if (a && a.img && b.st.image.verticalFit) {
                        var c = 0;
                        b.isLowIE && (c = parseInt(a.img.css("padding-top"), 10) + parseInt(a.img.css("padding-bottom"), 10)), a.img.css("max-height", b.wH - c)
                    }
                }, _onImageHasSize: function (a) {
                    a.img && (a.hasSize = !0, L && clearInterval(L), a.isCheckingImgSize = !1, y("ImageHasSize", a), a.imgHidden && (b.content && b.content.removeClass("mfp-loading"), a.imgHidden = !1))
                }, findImageSize: function (a) {
                    var c = 0, d = a.img[0], e = function (f) {
                        L && clearInterval(L), L = setInterval(function () {
                            return d.naturalWidth > 0 ? void b._onImageHasSize(a) : (c > 200 && clearInterval(L), c++, void(3 === c ? e(10) : 40 === c ? e(50) : 100 === c && e(500)))
                        }, f)
                    };
                    e(1)
                }, getImage: function (c, d) {
                    var e = 0, f = function () {
                        c && (c.img[0].complete ? (c.img.off(".mfploader"), c === b.currItem && (b._onImageHasSize(c), b.updateStatus("ready")), c.hasSize = !0, c.loaded = !0, y("ImageLoadComplete")) : (e++, 200 > e ? setTimeout(f, 100) : g()))
                    }, g = function () {
                        c && (c.img.off(".mfploader"), c === b.currItem && (b._onImageHasSize(c), b.updateStatus("error", h.tError.replace("%url%", c.src))), c.hasSize = !0, c.loaded = !0, c.loadError = !0)
                    }, h = b.st.image, i = d.find(".mfp-img");
                    if (i.length) {
                        var j = document.createElement("img");
                        j.className = "mfp-img", c.el && c.el.find("img").length && (j.alt = c.el.find("img").attr("alt")), c.img = a(j).on("load.mfploader", f).on("error.mfploader", g), j.src = c.src, i.is("img") && (c.img = c.img.clone()), j = c.img[0], j.naturalWidth > 0 ? c.hasSize = !0 : j.width || (c.hasSize = !1)
                    }
                    return b._parseMarkup(d, {title: M(c), img_replaceWith: c.img}, c), b.resizeImage(), c.hasSize ? (L && clearInterval(L), c.loadError ? (d.addClass("mfp-loading"), b.updateStatus("error", h.tError.replace("%url%", c.src))) : (d.removeClass("mfp-loading"), b.updateStatus("ready")), d) : (b.updateStatus("loading"), c.loading = !0, c.hasSize || (c.imgHidden = !0, d.addClass("mfp-loading"), b.findImageSize(c)), d)
                }}});
        var N, O = function () {
            return void 0 === N && (N = void 0 !== document.createElement("p").style.MozTransform), N
        };
        a.magnificPopup.registerModule("zoom", {options: {enabled: !1, easing: "ease-in-out", duration: 300, opener: function (a) {
                    return a.is("img") ? a : a.find("img")
                }}, proto: {initZoom: function () {
                    var a, c = b.st.zoom, d = ".zoom";
                    if (c.enabled && b.supportsTransition) {
                        var e, f, g = c.duration, j = function (a) {
                            var b = a.clone().removeAttr("style").removeAttr("class").addClass("mfp-animated-image"), d = "all " + c.duration / 1e3 + "s " + c.easing, e = {position: "fixed", zIndex: 9999, left: 0, top: 0, "-webkit-backface-visibility": "hidden"}, f = "transition";
                            return e["-webkit-" + f] = e["-moz-" + f] = e["-o-" + f] = e[f] = d, b.css(e), b
                        }, k = function () {
                            b.content.css("visibility", "visible")
                        };
                        w("BuildControls" + d, function () {
                            if (b._allowZoom()) {
                                if (clearTimeout(e), b.content.css("visibility", "hidden"), a = b._getItemToZoom(), !a)
                                    return void k();
                                f = j(a), f.css(b._getOffset()), b.wrap.append(f), e = setTimeout(function () {
                                    f.css(b._getOffset(!0)), e = setTimeout(function () {
                                        k(), setTimeout(function () {
                                            f.remove(), a = f = null, y("ZoomAnimationEnded")
                                        }, 16)
                                    }, g)
                                }, 16)
                            }
                        }), w(i + d, function () {
                            if (b._allowZoom()) {
                                if (clearTimeout(e), b.st.removalDelay = g, !a) {
                                    if (a = b._getItemToZoom(), !a)
                                        return;
                                    f = j(a)
                                }
                                f.css(b._getOffset(!0)), b.wrap.append(f), b.content.css("visibility", "hidden"), setTimeout(function () {
                                    f.css(b._getOffset())
                                }, 16)
                            }
                        }), w(h + d, function () {
                            b._allowZoom() && (k(), f && f.remove(), a = null)
                        })
                    }
                }, _allowZoom: function () {
                    return"image" === b.currItem.type
                }, _getItemToZoom: function () {
                    return b.currItem.hasSize ? b.currItem.img : !1
                }, _getOffset: function (c) {
                    var d;
                    d = c ? b.currItem.img : b.st.zoom.opener(b.currItem.el || b.currItem);
                    var e = d.offset(), f = parseInt(d.css("padding-top"), 10), g = parseInt(d.css("padding-bottom"), 10);
                    e.top -= a(window).scrollTop() - f;
                    var h = {width: d.width(), height: (u ? d.innerHeight() : d[0].offsetHeight) - g - f};
                    return O() ? h["-moz-transform"] = h.transform = "translate(" + e.left + "px," + e.top + "px)" : (h.left = e.left, h.top = e.top), h
                }}});
        var P = "iframe", Q = "//about:blank", R = function (a) {
            if (b.currTemplate[P]) {
                var c = b.currTemplate[P].find("iframe");
                c.length && (a || (c[0].src = Q), b.isIE8 && c.css("display", a ? "block" : "none"))
            }
        };
        a.magnificPopup.registerModule(P, {options: {markup: '<div class="mfp-iframe-scaler"><div class="mfp-close"></div><iframe class="mfp-iframe" src="//about:blank" frameborder="0" allowfullscreen></iframe></div>', srcAction: "iframe_src", patterns: {youtube: {index: "youtube.com", id: "v=", src: "//www.youtube.com/embed/%id%?autoplay=1"}, vimeo: {index: "vimeo.com/", id: "/", src: "//player.vimeo.com/video/%id%?autoplay=1"}, gmaps: {index: "//maps.google.", src: "%id%&output=embed"}}}, proto: {initIframe: function () {
                    b.types.push(P), w("BeforeChange", function (a, b, c) {
                        b !== c && (b === P ? R() : c === P && R(!0))
                    }), w(h + "." + P, function () {
                        R()
                    })
                }, getIframe: function (c, d) {
                    var e = c.src, f = b.st.iframe;
                    a.each(f.patterns, function () {
                        return e.indexOf(this.index) > -1 ? (this.id && (e = "string" == typeof this.id ? e.substr(e.lastIndexOf(this.id) + this.id.length, e.length) : this.id.call(this, e)), e = this.src.replace("%id%", e), !1) : void 0
                    });
                    var g = {};
                    return f.srcAction && (g[f.srcAction] = e), b._parseMarkup(d, g, c), b.updateStatus("ready"), d
                }}});
        var S = function (a) {
            var c = b.items.length;
            return a > c - 1 ? a - c : 0 > a ? c + a : a
        }, T = function (a, b, c) {
            return a.replace(/%curr%/gi, b + 1).replace(/%total%/gi, c)
        };
        a.magnificPopup.registerModule("gallery", {options: {enabled: !1, arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>', preload: [0, 2], navigateByImgClick: !0, arrows: !0, tPrev: "Previous (Left arrow key)", tNext: "Next (Right arrow key)", tCounter: "%curr% of %total%"}, proto: {initGallery: function () {
                    var c = b.st.gallery, e = ".mfp-gallery";
                    return b.direction = !0, c && c.enabled ? (f += " mfp-gallery", w(m + e, function () {
                        c.navigateByImgClick && b.wrap.on("click" + e, ".mfp-img", function () {
                            return b.items.length > 1 ? (b.next(), !1) : void 0
                        }), d.on("keydown" + e, function (a) {
                            37 === a.keyCode ? b.prev() : 39 === a.keyCode && b.next()
                        })
                    }), w("UpdateStatus" + e, function (a, c) {
                        c.text && (c.text = T(c.text, b.currItem.index, b.items.length))
                    }), w(l + e, function (a, d, e, f) {
                        var g = b.items.length;
                        e.counter = g > 1 ? T(c.tCounter, f.index, g) : ""
                    }), w("BuildControls" + e, function () {
                        if (b.items.length > 1 && c.arrows && !b.arrowLeft) {
                            var d = c.arrowMarkup, e = b.arrowLeft = a(d.replace(/%title%/gi, c.tPrev).replace(/%dir%/gi, "left")).addClass(s), f = b.arrowRight = a(d.replace(/%title%/gi, c.tNext).replace(/%dir%/gi, "right")).addClass(s);
                            e.click(function () {
                                b.prev()
                            }), f.click(function () {
                                b.next()
                            }), b.container.append(e.add(f))
                        }
                    }), w(n + e, function () {
                        b._preloadTimeout && clearTimeout(b._preloadTimeout), b._preloadTimeout = setTimeout(function () {
                            b.preloadNearbyImages(), b._preloadTimeout = null
                        }, 16)
                    }), void w(h + e, function () {
                        d.off(e), b.wrap.off("click" + e), b.arrowRight = b.arrowLeft = null
                    })) : !1
                }, next: function () {
                    b.direction = !0, b.index = S(b.index + 1), b.updateItemHTML()
                }, prev: function () {
                    b.direction = !1, b.index = S(b.index - 1), b.updateItemHTML()
                }, goTo: function (a) {
                    b.direction = a >= b.index, b.index = a, b.updateItemHTML()
                }, preloadNearbyImages: function () {
                    var a, c = b.st.gallery.preload, d = Math.min(c[0], b.items.length), e = Math.min(c[1], b.items.length);
                    for (a = 1; a <= (b.direction?e:d); a++)
                        b._preloadItem(b.index + a);
                    for (a = 1; a <= (b.direction?d:e); a++)
                        b._preloadItem(b.index - a)
                }, _preloadItem: function (c) {
                    if (c = S(c), !b.items[c].preloaded) {
                        var d = b.items[c];
                        d.parsed || (d = b.parseEl(c)), y("LazyLoad", d), "image" === d.type && (d.img = a('<img class="mfp-img" />').on("load.mfploader", function () {
                            d.hasSize = !0
                        }).on("error.mfploader", function () {
                            d.hasSize = !0, d.loadError = !0, y("LazyLoadError", d)
                        }).attr("src", d.src)), d.preloaded = !0
                    }
                }}});
        var U = "retina";
        a.magnificPopup.registerModule(U, {options: {replaceSrc: function (a) {
                    return a.src.replace(/\.\w+$/, function (a) {
                        return"@2x" + a
                    })
                }, ratio: 1}, proto: {initRetina: function () {
                    if (window.devicePixelRatio > 1) {
                        var a = b.st.retina, c = a.ratio;
                        c = isNaN(c) ? c() : c, c > 1 && (w("ImageHasSize." + U, function (a, b) {
                            b.img.css({"max-width": b.img[0].naturalWidth / c, width: "100%"})
                        }), w("ElementParse." + U, function (b, d) {
                            d.src = a.replaceSrc(d, c)
                        }))
                    }
                }}}), A()
    });
    jQuery('.with-caption').magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        closeBtnInside: false,
        mainClass: 'mfp-with-zoom mfp-img-mobile',
        image: {
            verticalFit: true
        },
        gallery: {
            enabled: true
        },
        zoom: {
            enabled: true
        }
    });

    console.log('Initializing FlexSlider (from RedIFeedPlugin.js).')

    /* jQuery FlexSlider v2.1 */
    !function (a) {
        var b = !0;
        a.flexslider = function (c, d) {
            var e = a(c);
            e.vars = a.extend({}, a.flexslider.defaults, d);
            var k, f = e.vars.namespace, g = window.navigator && window.navigator.msPointerEnabled && window.MSGesture, h = ("ontouchstart"in window || g || window.DocumentTouch && document instanceof DocumentTouch) && e.vars.touch, i = "click touchend MSPointerUp keyup", j = "", l = "vertical" === e.vars.direction, m = e.vars.reverse, n = e.vars.itemWidth > 0, o = "fade" === e.vars.animation, p = "" !== e.vars.asNavFor, q = {};
            a.data(c, "flexslider", e), q = {init: function () {
                    e.animating = !1, e.currentSlide = parseInt(e.vars.startAt ? e.vars.startAt : 0, 10), isNaN(e.currentSlide) && (e.currentSlide = 0), e.animatingTo = e.currentSlide, e.atEnd = 0 === e.currentSlide || e.currentSlide === e.last, e.containerSelector = e.vars.selector.substr(0, e.vars.selector.search(" ")), e.slides = a(e.vars.selector, e), e.container = a(e.containerSelector, e), e.count = e.slides.length, e.syncExists = a(e.vars.sync).length > 0, "slide" === e.vars.animation && (e.vars.animation = "swing"), e.prop = l ? "top" : "marginLeft", e.args = {}, e.manualPause = !1, e.stopped = !1, e.started = !1, e.startTimeout = null, e.transitions = !e.vars.video && !o && e.vars.useCSS && function () {
                        var a = document.createElement("div"), b = ["perspectiveProperty", "WebkitPerspective", "MozPerspective", "OPerspective", "msPerspective"];
                        for (var c in b)
                            if (void 0 !== a.style[b[c]])
                                return e.pfx = b[c].replace("Perspective", "").toLowerCase(), e.prop = "-" + e.pfx + "-transform", !0;
                        return!1
                    }(), e.ensureAnimationEnd = "", "" !== e.vars.controlsContainer && (e.controlsContainer = a(e.vars.controlsContainer).length > 0 && a(e.vars.controlsContainer)), "" !== e.vars.manualControls && (e.manualControls = a(e.vars.manualControls).length > 0 && a(e.vars.manualControls)), "" !== e.vars.customDirectionNav && (e.customDirectionNav = 2 === a(e.vars.customDirectionNav).length && a(e.vars.customDirectionNav)), e.vars.randomize && (e.slides.sort(function () {
                        return Math.round(Math.random()) - .5
                    }), e.container.empty().append(e.slides)), e.doMath(), e.setup("init"), e.vars.controlNav && q.controlNav.setup(), e.vars.directionNav && q.directionNav.setup(), e.vars.keyboard && (1 === a(e.containerSelector).length || e.vars.multipleKeyboard) && a(document).bind("keyup", function (a) {
                        var b = a.keyCode;
                        if (!e.animating && (39 === b || 37 === b)) {
                            var c = 39 === b ? e.getTarget("next") : 37 === b && e.getTarget("prev");
                            e.flexAnimate(c, e.vars.pauseOnAction)
                        }
                    }), e.vars.mousewheel && e.bind("mousewheel", function (a, b, c, d) {
                        a.preventDefault();
                        var f = b < 0 ? e.getTarget("next") : e.getTarget("prev");
                        e.flexAnimate(f, e.vars.pauseOnAction)
                    }), e.vars.pausePlay && q.pausePlay.setup(), e.vars.slideshow && e.vars.pauseInvisible && q.pauseInvisible.init(), e.vars.slideshow && (e.vars.pauseOnHover && e.hover(function () {
                        e.manualPlay || e.manualPause || e.pause()
                    }, function () {
                        e.manualPause || e.manualPlay || e.stopped || e.play()
                    }), e.vars.pauseInvisible && q.pauseInvisible.isHidden() || (e.vars.initDelay > 0 ? e.startTimeout = setTimeout(e.play, e.vars.initDelay) : e.play())), p && q.asNav.setup(), h && e.vars.touch && q.touch(), (!o || o && e.vars.smoothHeight) && a(window).bind("resize orientationchange focus", q.resize), e.find("img").attr("draggable", "false"), setTimeout(function () {
                        e.vars.start(e)
                    }, 200)
                }, asNav: {setup: function () {
                        e.asNav = !0, e.animatingTo = Math.floor(e.currentSlide / e.move), e.currentItem = e.currentSlide, e.slides.removeClass(f + "active-slide").eq(e.currentItem).addClass(f + "active-slide"), g ? (c._slider = e, e.slides.each(function () {
                            var b = this;
                            b._gesture = new MSGesture, b._gesture.target = b, b.addEventListener("MSPointerDown", function (a) {
                                a.preventDefault(), a.currentTarget._gesture && a.currentTarget._gesture.addPointer(a.pointerId)
                            }, !1), b.addEventListener("MSGestureTap", function (b) {
                                b.preventDefault();
                                var c = a(this), d = c.index();
                                a(e.vars.asNavFor).data("flexslider").animating || c.hasClass("active") || (e.direction = e.currentItem < d ? "next" : "prev", e.flexAnimate(d, e.vars.pauseOnAction, !1, !0, !0))
                            })
                        })) : e.slides.on(i, function (b) {
                            b.preventDefault();
                            var c = a(this), d = c.index(), g = c.offset().left - a(e).scrollLeft();
                            g <= 0 && c.hasClass(f + "active-slide") ? e.flexAnimate(e.getTarget("prev"), !0) : a(e.vars.asNavFor).data("flexslider").animating || c.hasClass(f + "active-slide") || (e.direction = e.currentItem < d ? "next" : "prev", e.flexAnimate(d, e.vars.pauseOnAction, !1, !0, !0))
                        })
                    }}, controlNav: {setup: function () {
                        e.manualControls ? q.controlNav.setupManual() : q.controlNav.setupPaging()
                    }, setupPaging: function () {
                        var d, g, b = "thumbnails" === e.vars.controlNav ? "control-thumbs" : "control-paging", c = 1;
                        if (e.controlNavScaffold = a('<ol class="' + f + "control-nav " + f + b + '"></ol>'), e.pagingCount > 1)
                            for (var h = 0; h < e.pagingCount; h++) {
                                g = e.slides.eq(h), void 0 === g.attr("data-thumb-alt") && g.attr("data-thumb-alt", "");
                                var k = "" !== g.attr("data-thumb-alt") ? k = ' alt="' + g.attr("data-thumb-alt") + '"' : "";
                                if (d = "thumbnails" === e.vars.controlNav ? '<img src="' + g.attr("data-thumb") + '"' + k + "/>" : '<a href="#">' + c + "</a>", "thumbnails" === e.vars.controlNav && !0 === e.vars.thumbCaptions) {
                                    var l = g.attr("data-thumbcaption");
                                    "" !== l && void 0 !== l && (d += '<span class="' + f + 'caption">' + l + "</span>")
                                }
                                e.controlNavScaffold.append("<li>" + d + "</li>"), c++
                            }
                        e.controlsContainer ? a(e.controlsContainer).append(e.controlNavScaffold) : e.append(e.controlNavScaffold), q.controlNav.set(), q.controlNav.active(), e.controlNavScaffold.delegate("a, img", i, function (b) {
                            if (b.preventDefault(), "" === j || j === b.type) {
                                var c = a(this), d = e.controlNav.index(c);
                                c.hasClass(f + "active") || (e.direction = d > e.currentSlide ? "next" : "prev", e.flexAnimate(d, e.vars.pauseOnAction))
                            }
                            "" === j && (j = b.type), q.setToClearWatchedEvent()
                        })
                    }, setupManual: function () {
                        e.controlNav = e.manualControls, q.controlNav.active(), e.controlNav.bind(i, function (b) {
                            if (b.preventDefault(), "" === j || j === b.type) {
                                var c = a(this), d = e.controlNav.index(c);
                                c.hasClass(f + "active") || (d > e.currentSlide ? e.direction = "next" : e.direction = "prev", e.flexAnimate(d, e.vars.pauseOnAction))
                            }
                            "" === j && (j = b.type), q.setToClearWatchedEvent()
                        })
                    }, set: function () {
                        var b = "thumbnails" === e.vars.controlNav ? "img" : "a";
                        e.controlNav = a("." + f + "control-nav li " + b, e.controlsContainer ? e.controlsContainer : e)
                    }, active: function () {
                        e.controlNav.removeClass(f + "active").eq(e.animatingTo).addClass(f + "active")
                    }, update: function (b, c) {
                        e.pagingCount > 1 && "add" === b ? e.controlNavScaffold.append(a('<li><a href="#">' + e.count + "</a></li>")) : 1 === e.pagingCount ? e.controlNavScaffold.find("li").remove() : e.controlNav.eq(c).closest("li").remove(), q.controlNav.set(), e.pagingCount > 1 && e.pagingCount !== e.controlNav.length ? e.update(c, b) : q.controlNav.active()
                    }}, directionNav: {setup: function () {
                        var b = a('<ul class="' + f + 'direction-nav"><li class="' + f + 'nav-prev"><a class="' + f + 'prev" href="#">' + e.vars.prevText + '</a></li><li class="' + f + 'nav-next"><a class="' + f + 'next" href="#">' + e.vars.nextText + "</a></li></ul>");
                        e.customDirectionNav ? e.directionNav = e.customDirectionNav : e.controlsContainer ? (a(e.controlsContainer).append(b), e.directionNav = a("." + f + "direction-nav li a", e.controlsContainer)) : (e.append(b), e.directionNav = a("." + f + "direction-nav li a", e)), q.directionNav.update(), e.directionNav.bind(i, function (b) {
                            b.preventDefault();
                            var c;
                            "" !== j && j !== b.type || (c = a(this).hasClass(f + "next") ? e.getTarget("next") : e.getTarget("prev"), e.flexAnimate(c, e.vars.pauseOnAction)), "" === j && (j = b.type), q.setToClearWatchedEvent()
                        })
                    }, update: function () {
                        var a = f + "disabled";
                        1 === e.pagingCount ? e.directionNav.addClass(a).attr("tabindex", "-1") : e.vars.animationLoop ? e.directionNav.removeClass(a).removeAttr("tabindex") : 0 === e.animatingTo ? e.directionNav.removeClass(a).filter("." + f + "prev").addClass(a).attr("tabindex", "-1") : e.animatingTo === e.last ? e.directionNav.removeClass(a).filter("." + f + "next").addClass(a).attr("tabindex", "-1") : e.directionNav.removeClass(a).removeAttr("tabindex")
                    }}, pausePlay: {setup: function () {
                        var b = a('<div class="' + f + 'pauseplay"><a href="#"></a></div>');
                        e.controlsContainer ? (e.controlsContainer.append(b), e.pausePlay = a("." + f + "pauseplay a", e.controlsContainer)) : (e.append(b), e.pausePlay = a("." + f + "pauseplay a", e)), q.pausePlay.update(e.vars.slideshow ? f + "pause" : f + "play"), e.pausePlay.bind(i, function (b) {
                            b.preventDefault(), "" !== j && j !== b.type || (a(this).hasClass(f + "pause") ? (e.manualPause = !0, e.manualPlay = !1, e.pause()) : (e.manualPause = !1, e.manualPlay = !0, e.play())), "" === j && (j = b.type), q.setToClearWatchedEvent()
                        })
                    }, update: function (a) {
                        "play" === a ? e.pausePlay.removeClass(f + "pause").addClass(f + "play").html(e.vars.playText) : e.pausePlay.removeClass(f + "play").addClass(f + "pause").html(e.vars.pauseText)
                    }}, touch: function () {
                    function u(a) {
                        a.stopPropagation(), e.animating ? a.preventDefault() : (e.pause(), c._gesture.addPointer(a.pointerId), t = 0, f = l ? e.h : e.w, i = Number(new Date), d = n && m && e.animatingTo === e.last ? 0 : n && m ? e.limit - (e.itemW + e.vars.itemMargin) * e.move * e.animatingTo : n && e.currentSlide === e.last ? e.limit : n ? (e.itemW + e.vars.itemMargin) * e.move * e.currentSlide : m ? (e.last - e.currentSlide + e.cloneOffset) * f : (e.currentSlide + e.cloneOffset) * f)
                    }
                    function v(a) {
                        a.stopPropagation();
                        var b = a.target._slider;
                        if (b) {
                            var e = -a.translationX, g = -a.translationY;
                            return t += l ? g : e, h = t, q = l ? Math.abs(t) < Math.abs(-e) : Math.abs(t) < Math.abs(-g), a.detail === a.MSGESTURE_FLAG_INERTIA ? void setImmediate(function () {
                                c._gesture.stop()
                            }) : void((!q || Number(new Date) - i > 500) && (a.preventDefault(), !o && b.transitions && (b.vars.animationLoop || (h = t / (0 === b.currentSlide && t < 0 || b.currentSlide === b.last && t > 0 ? Math.abs(t) / f + 2 : 1)), b.setProps(d + h, "setTouch"))))
                        }
                    }
                    function w(c) {
                        c.stopPropagation();
                        var e = c.target._slider;
                        if (e) {
                            if (e.animatingTo === e.currentSlide && !q && null !== h) {
                                var g = m ? -h : h, j = g > 0 ? e.getTarget("next") : e.getTarget("prev");
                                e.canAdvance(j) && (Number(new Date) - i < 550 && Math.abs(g) > 50 || Math.abs(g) > f / 2) ? e.flexAnimate(j, e.vars.pauseOnAction) : o || e.flexAnimate(e.currentSlide, e.vars.pauseOnAction, !0)
                            }
                            a = null, b = null, h = null, d = null, t = 0
                        }
                    }
                    var a, b, d, f, h, i, j, k, p, q = !1, r = 0, s = 0, t = 0;
                    g ? (c.style.msTouchAction = "none", c._gesture = new MSGesture, c._gesture.target = c, c.addEventListener("MSPointerDown", u, !1), c._slider = e, c.addEventListener("MSGestureChange", v, !1), c.addEventListener("MSGestureEnd", w, !1)) : (j = function (g) {
                        e.animating ? g.preventDefault() : (window.navigator.msPointerEnabled || 1 === g.touches.length) && (e.pause(), f = l ? e.h : e.w, i = Number(new Date), r = g.touches[0].pageX, s = g.touches[0].pageY, d = n && m && e.animatingTo === e.last ? 0 : n && m ? e.limit - (e.itemW + e.vars.itemMargin) * e.move * e.animatingTo : n && e.currentSlide === e.last ? e.limit : n ? (e.itemW + e.vars.itemMargin) * e.move * e.currentSlide : m ? (e.last - e.currentSlide + e.cloneOffset) * f : (e.currentSlide + e.cloneOffset) * f, a = l ? s : r, b = l ? r : s, c.addEventListener("touchmove", k, !1), c.addEventListener("touchend", p, !1))
                    }, k = function (c) {
                        r = c.touches[0].pageX, s = c.touches[0].pageY, h = l ? a - s : a - r, q = l ? Math.abs(h) < Math.abs(r - b) : Math.abs(h) < Math.abs(s - b);
                        var g = 500;
                        (!q || Number(new Date) - i > g) && (c.preventDefault(), !o && e.transitions && (e.vars.animationLoop || (h /= 0 === e.currentSlide && h < 0 || e.currentSlide === e.last && h > 0 ? Math.abs(h) / f + 2 : 1), e.setProps(d + h, "setTouch")))
                    }, p = function (g) {
                        if (c.removeEventListener("touchmove", k, !1), e.animatingTo === e.currentSlide && !q && null !== h) {
                            var j = m ? -h : h, l = j > 0 ? e.getTarget("next") : e.getTarget("prev");
                            e.canAdvance(l) && (Number(new Date) - i < 550 && Math.abs(j) > 50 || Math.abs(j) > f / 2) ? e.flexAnimate(l, e.vars.pauseOnAction) : o || e.flexAnimate(e.currentSlide, e.vars.pauseOnAction, !0)
                        }
                        c.removeEventListener("touchend", p, !1), a = null, b = null, h = null, d = null
                    }, c.addEventListener("touchstart", j, !1))
                }, resize: function () {
                    !e.animating && e.is(":visible") && (n || e.doMath(), o ? q.smoothHeight() : n ? (e.slides.width(e.computedW), e.update(e.pagingCount), e.setProps()) : l ? (e.viewport.height(e.h), e.setProps(e.h, "setTotal")) : (e.vars.smoothHeight && q.smoothHeight(), e.newSlides.width(e.computedW), e.setProps(e.computedW, "setTotal")))
                }, smoothHeight: function (a) {
                    if (!l || o) {
                        var b = o ? e : e.viewport;
                        a ? b.animate({height: e.slides.eq(e.animatingTo).innerHeight()}, a) : b.innerHeight(e.slides.eq(e.animatingTo).innerHeight())
                    }
                }, sync: function (b) {
                    var c = a(e.vars.sync).data("flexslider"), d = e.animatingTo;
                    switch (b) {
                        case"animate":
                            c.flexAnimate(d, e.vars.pauseOnAction, !1, !0);
                            break;
                            case"play":
                            c.playing || c.asNav || c.play();
                            break;
                            case"pause":
                            c.pause()
                            }
                }, uniqueID: function (b) {
                    return b.filter("[id]").add(b.find("[id]")).each(function () {
                        var b = a(this);
                        b.attr("id", b.attr("id") + "_clone")
                    }), b
                }, pauseInvisible: {visProp: null, init: function () {
                        var a = q.pauseInvisible.getHiddenProp();
                        if (a) {
                            var b = a.replace(/[H|h]idden/, "") + "visibilitychange";
                            document.addEventListener(b, function () {
                                q.pauseInvisible.isHidden() ? e.startTimeout ? clearTimeout(e.startTimeout) : e.pause() : e.started ? e.play() : e.vars.initDelay > 0 ? setTimeout(e.play, e.vars.initDelay) : e.play()
                            })
                        }
                    }, isHidden: function () {
                        var a = q.pauseInvisible.getHiddenProp();
                        return!!a && document[a]
                    }, getHiddenProp: function () {
                        var a = ["webkit", "moz", "ms", "o"];
                        if ("hidden"in document)
                            return"hidden";
                        for (var b = 0; b < a.length; b++)
                            if (a[b] + "Hidden"in document)
                                return a[b] + "Hidden";
                        return null
                    }}, setToClearWatchedEvent: function () {
                    clearTimeout(k), k = setTimeout(function () {
                        j = ""
                    }, 3e3)
                }}, e.flexAnimate = function (b, c, d, g, i) {
                if (e.vars.animationLoop || b === e.currentSlide || (e.direction = b > e.currentSlide ? "next" : "prev"), p && 1 === e.pagingCount && (e.direction = e.currentItem < b ? "next" : "prev"), !e.animating && (e.canAdvance(b, i) || d) && e.is(":visible")) {
                    if (p && g) {
                        var j = a(e.vars.asNavFor).data("flexslider");
                        if (e.atEnd = 0 === b || b === e.count - 1, j.flexAnimate(b, !0, !1, !0, i), e.direction = e.currentItem < b ? "next" : "prev", j.direction = e.direction, Math.ceil((b + 1) / e.visible) - 1 === e.currentSlide || 0 === b)
                            return e.currentItem = b, e.slides.removeClass(f + "active-slide").eq(b).addClass(f + "active-slide"), !1;
                        e.currentItem = b, e.slides.removeClass(f + "active-slide").eq(b).addClass(f + "active-slide"), b = Math.floor(b / e.visible)
                    }
                    if (e.animating = !0, e.animatingTo = b, c && e.pause(), e.vars.before(e), e.syncExists && !i && q.sync("animate"), e.vars.controlNav && q.controlNav.active(), n || e.slides.removeClass(f + "active-slide").eq(b).addClass(f + "active-slide"), e.atEnd = 0 === b || b === e.last, e.vars.directionNav && q.directionNav.update(), b === e.last && (e.vars.end(e), e.vars.animationLoop || e.pause()), o)
                        h ? (e.slides.eq(e.currentSlide).css({opacity: 0, zIndex: 1}), e.slides.eq(b).css({opacity: 1, zIndex: 2}), e.wrapup(k)):(e.slides.eq(e.currentSlide).css({zIndex : 1}).animate({opacity: 0}, e.vars.animationSpeed, e.vars.easing), e.slides.eq(b).css({zIndex: 2}).animate({opacity: 1}, e.vars.animationSpeed, e.vars.easing, e.wrapup));
                    else {
                        var r, s, t, k = l ? e.slides.filter(":first").height() : e.computedW;
                        n ? (r = e.vars.itemMargin, t = (e.itemW + r) * e.move * e.animatingTo, s = t > e.limit && 1 !== e.visible ? e.limit : t) : s = 0 === e.currentSlide && b === e.count - 1 && e.vars.animationLoop && "next" !== e.direction ? m ? (e.count + e.cloneOffset) * k : 0 : e.currentSlide === e.last && 0 === b && e.vars.animationLoop && "prev" !== e.direction ? m ? 0 : (e.count + 1) * k : m ? (e.count - 1 - b + e.cloneOffset) * k : (b + e.cloneOffset) * k, e.setProps(s, "", e.vars.animationSpeed), e.transitions ? (e.vars.animationLoop && e.atEnd || (e.animating = !1, e.currentSlide = e.animatingTo), e.container.unbind("webkitTransitionEnd transitionend"), e.container.bind("webkitTransitionEnd transitionend", function () {
                            clearTimeout(e.ensureAnimationEnd), e.wrapup(k)
                        }), clearTimeout(e.ensureAnimationEnd), e.ensureAnimationEnd = setTimeout(function () {
                            e.wrapup(k)
                        }, e.vars.animationSpeed + 100)) : e.container.animate(e.args, e.vars.animationSpeed, e.vars.easing, function () {
                            e.wrapup(k)
                        })
                    }
                    e.vars.smoothHeight && q.smoothHeight(e.vars.animationSpeed)
                }
            }, e.wrapup = function (a) {
                o || n || (0 === e.currentSlide && e.animatingTo === e.last && e.vars.animationLoop ? e.setProps(a, "jumpEnd") : e.currentSlide === e.last && 0 === e.animatingTo && e.vars.animationLoop && e.setProps(a, "jumpStart")), e.animating = !1, e.currentSlide = e.animatingTo, e.vars.after(e)
            }, e.animateSlides = function () {
                !e.animating && b && e.flexAnimate(e.getTarget("next"))
            }, e.pause = function () {
                clearInterval(e.animatedSlides), e.animatedSlides = null, e.playing = !1, e.vars.pausePlay && q.pausePlay.update("play"), e.syncExists && q.sync("pause")
            }, e.play = function () {
                e.playing && clearInterval(e.animatedSlides), e.animatedSlides = e.animatedSlides || setInterval(e.animateSlides, e.vars.slideshowSpeed), e.started = e.playing = !0, e.vars.pausePlay && q.pausePlay.update("pause"), e.syncExists && q.sync("play")
            }, e.stop = function () {
                e.pause(), e.stopped = !0
            }, e.canAdvance = function (a, b) {
                var c = p ? e.pagingCount - 1 : e.last;
                return!!b || (!(!p || e.currentItem !== e.count - 1 || 0 !== a || "prev" !== e.direction) || (!p || 0 !== e.currentItem || a !== e.pagingCount - 1 || "next" === e.direction) && (!(a === e.currentSlide && !p) && (!!e.vars.animationLoop || (!e.atEnd || 0 !== e.currentSlide || a !== c || "next" === e.direction) && (!e.atEnd || e.currentSlide !== c || 0 !== a || "next" !== e.direction))))
            }, e.getTarget = function (a) {
                return e.direction = a, "next" === a ? e.currentSlide === e.last ? 0 : e.currentSlide + 1 : 0 === e.currentSlide ? e.last : e.currentSlide - 1
            }, e.setProps = function (a, b, c) {
                var d = function () {
                    var c = a ? a : (e.itemW + e.vars.itemMargin) * e.move * e.animatingTo, d = function () {
                        if (n)
                            return"setTouch" === b ? a : m && e.animatingTo === e.last ? 0 : m ? e.limit - (e.itemW + e.vars.itemMargin) * e.move * e.animatingTo : e.animatingTo === e.last ? e.limit : c;
                        switch (b) {
                            case"setTotal":
                                return m ? (e.count - 1 - e.currentSlide + e.cloneOffset) * a : (e.currentSlide + e.cloneOffset) * a;
                                case"setTouch":
                                return m ? a : a;
                                case"jumpEnd":
                                return m ? a : e.count * a;
                                case"jumpStart":
                                return m ? e.count * a : a;
                                default:
                                return a
                                }
                    }();
                    return d * -1 + "px"
                }();
                e.transitions && (d = l ? "translate3d(0," + d + ",0)" : "translate3d(" + d + ",0,0)", c = void 0 !== c ? c / 1e3 + "s" : "0s", e.container.css("-" + e.pfx + "-transition-duration", c), e.container.css("transition-duration", c)), e.args[e.prop] = d, (e.transitions || void 0 === c) && e.container.css(e.args), e.container.css("transform", d)
            }, e.setup = function (b) {
                if (o)
                    e.slides.css({width: "100%", float: "left", marginRight: "-100%", position: "relative"}), "init" === b && (h ? e.slides.css({opacity: 0, display: "block", webkitTransition: "opacity " + e.vars.animationSpeed / 1e3 + "s ease", zIndex: 1}).eq(e.currentSlide).css({opacity: 1, zIndex: 2}) : 0 == e.vars.fadeFirstSlide ? e.slides.css({opacity: 0, display: "block", zIndex: 1}).eq(e.currentSlide).css({zIndex: 2}).css({opacity: 1}):e.slides.css({opacity: 0, display: "block", zIndex: 1}).eq(e.currentSlide).css({zIndex : 2}).animate({opacity: 1}, e.vars.animationSpeed, e.vars.easing)), e.vars.smoothHeight && q.smoothHeight();
                else {
                    var c, d;
                    "init" === b && (e.viewport = a('<div class="' + f + 'viewport"></div>').css({overflow: "hidden", position: "relative"}).appendTo(e).append(e.container), e.cloneCount = 0, e.cloneOffset = 0, m && (d = a.makeArray(e.slides).reverse(), e.slides = a(d), e.container.empty().append(e.slides))), e.vars.animationLoop && !n && (e.cloneCount = 2, e.cloneOffset = 1, "init" !== b && e.container.find(".clone").remove(), e.container.append(q.uniqueID(e.slides.first().clone().addClass("clone")).attr("aria-hidden", "true")).prepend(q.uniqueID(e.slides.last().clone().addClass("clone")).attr("aria-hidden", "true"))), e.newSlides = a(e.vars.selector, e), c = m ? e.count - 1 - e.currentSlide + e.cloneOffset : e.currentSlide + e.cloneOffset, l && !n ? (e.container.height(200 * (e.count + e.cloneCount) + "%").css("position", "absolute").width("100%"), setTimeout(function () {
                        e.newSlides.css({display: "block"}), e.doMath(), e.viewport.height(e.h), e.setProps(c * e.h, "init")
                    }, "init" === b ? 100 : 0)) : (e.container.width(200 * (e.count + e.cloneCount) + "%"), e.setProps(c * e.computedW, "init"), setTimeout(function () {
                        e.doMath(), e.newSlides.css({width: e.computedW, marginRight: e.computedM, float: "left", display: "block"}), e.vars.smoothHeight && q.smoothHeight()
                    }, "init" === b ? 100 : 0))
                }
                n || e.slides.removeClass(f + "active-slide").eq(e.currentSlide).addClass(f + "active-slide"), e.vars.init(e)
            }, e.doMath = function () {
                var a = e.slides.first(), b = e.vars.itemMargin, c = e.vars.minItems, d = e.vars.maxItems;
                e.w = void 0 === e.viewport ? e.width() : e.viewport.width(), e.h = a.height(), e.boxPadding = a.outerWidth() - a.width(), n ? (e.itemT = e.vars.itemWidth + b, e.itemM = b, e.minW = c ? c * e.itemT : e.w, e.maxW = d ? d * e.itemT - b : e.w, e.itemW = e.minW > e.w ? (e.w - b * (c - 1)) / c : e.maxW < e.w ? (e.w - b * (d - 1)) / d : e.vars.itemWidth > e.w ? e.w : e.vars.itemWidth, e.visible = Math.floor(e.w / e.itemW), e.move = e.vars.move > 0 && e.vars.move < e.visible ? e.vars.move : e.visible, e.pagingCount = Math.ceil((e.count - e.visible) / e.move + 1), e.last = e.pagingCount - 1, e.limit = 1 === e.pagingCount ? 0 : e.vars.itemWidth > e.w ? e.itemW * (e.count - 1) + b * (e.count - 1) : (e.itemW + b) * e.count - e.w - b) : (e.itemW = e.w, e.itemM = b, e.pagingCount = e.count, e.last = e.count - 1), e.computedW = e.itemW - e.boxPadding, e.computedM = e.itemM
            }, e.update = function (a, b) {
                e.doMath(), n || (a < e.currentSlide ? e.currentSlide += 1 : a <= e.currentSlide && 0 !== a && (e.currentSlide -= 1), e.animatingTo = e.currentSlide), e.vars.controlNav && !e.manualControls && ("add" === b && !n || e.pagingCount > e.controlNav.length ? q.controlNav.update("add") : ("remove" === b && !n || e.pagingCount < e.controlNav.length) && (n && e.currentSlide > e.last && (e.currentSlide -= 1, e.animatingTo -= 1), q.controlNav.update("remove", e.last))), e.vars.directionNav && q.directionNav.update()
            }, e.addSlide = function (b, c) {
                var d = a(b);
                e.count += 1, e.last = e.count - 1, l && m ? void 0 !== c ? e.slides.eq(e.count - c).after(d) : e.container.prepend(d) : void 0 !== c ? e.slides.eq(c).before(d) : e.container.append(d), e.update(c, "add"), e.slides = a(e.vars.selector + ":not(.clone)", e), e.setup(), e.vars.added(e)
            }, e.removeSlide = function (b) {
                var c = isNaN(b) ? e.slides.index(a(b)) : b;
                e.count -= 1, e.last = e.count - 1, isNaN(b) ? a(b, e.slides).remove() : l && m ? e.slides.eq(e.last).remove() : e.slides.eq(b).remove(), e.doMath(), e.update(c, "remove"), e.slides = a(e.vars.selector + ":not(.clone)", e), e.setup(), e.vars.removed(e)
            }, q.init()
        }, a(window).blur(function (a) {
            b = !1
        }).focus(function (a) {
            b = !0
        }), a.flexslider.defaults = {namespace: "flex-", selector: ".slides > li", animation: "fade", easing: "swing", direction: "horizontal", reverse: !1, animationLoop: !0, smoothHeight: !1, startAt: 0, slideshow: !0, slideshowSpeed: 7e3, animationSpeed: 600, initDelay: 0, randomize: !1, fadeFirstSlide: !0, thumbCaptions: !1, pauseOnAction: !0, pauseOnHover: !1, pauseInvisible: !0, useCSS: !0, touch: !0, video: !1, controlNav: !0, directionNav: !0, prevText: "Previous", nextText: "Next", keyboard: !0, multipleKeyboard: !1, mousewheel: !1, pausePlay: !1, pauseText: "Pause", playText: "Play", controlsContainer: "", manualControls: "", customDirectionNav: "", sync: "", asNavFor: "", itemWidth: 0, itemMargin: 0, minItems: 1, maxItems: 0, move: 0, allowOneSlide: !0, start: function () {}, before: function () {}, after: function () {}, end: function () {}, added: function () {}, removed: function () {}, init: function () {}}, a.fn.flexslider = function (b) {
            if (void 0 === b && (b = {}), "object" == typeof b)
                return this.each(function () {
                    var c = a(this), d = b.selector ? b.selector : ".slides > li", e = c.find(d);
                    1 === e.length && b.allowOneSlide === !1 || 0 === e.length ? (e.fadeIn(400), b.start && b.start(c)) : void 0 === c.data("flexslider") && new a.flexslider(this, b)
                });
            var c = a(this).data("flexslider");
            switch (b) {
                case"play":
                    c.play();
                    break;
                    case"pause":
                    c.pause();
                    break;
                    case"stop":
                    c.stop();
                    break;
                    case"next":
                    c.flexAnimate(c.getTarget("next"), !0);
                    break;
                    case"prev":
                case"previous":
                    c.flexAnimate(c.getTarget("prev"), !0);
                    break;
                    default:
                    "number" == typeof b && c.flexAnimate(b, !0)
                    }
        }
    }(jQuery);
    
    jQuery(window).load(function () {
        
        console.log('Loading sliders');
        
        jQuery('#homeslider').flexslider({
            animation: "fade",
            start: function (slider) {
                jQuery('body').removeClass('loading');
            }
        });
        jQuery('#carousel').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            itemWidth: 120,
            itemMargin: 10,
            asNavFor: '#slider'
        });
        jQuery('#slider').flexslider({
            animation: "fade",
            animationLoop: false,
            slideshow: false,
            sync: "#carousel",
            start: function (slider) {
                jQuery('body').removeClass('loading');
                console.log('Starting slider');
            }
        });
    });

    /*! Agent Phone Number/Email address Toggle */
    jQuery('.agent #phonenumber').click(function () {

        if (!phoneClicked) {
            jQuery('#phonenumber').find('span').toggle();
            phoneClicked = true;
        }
    });
    jQuery('.agent #emailaddress').click(function () {
        if (!emailClicked) {
            jQuery('#emailaddress').find('span').toggle();
            emailClicked = true;
        }
    });

    jQuery('select#agents').change(function () {

        if (phoneClicked) {
            jQuery('#phonenumber').find('span').toggle();
            phoneClicked = false;
        }

        if (emailClicked) {
            jQuery('#emailaddress').find('span').toggle();
            emailClicked = false;
        }
    });
});

jQuery(window).load(function () {

    console.log('Loading sliders');

    jQuery('#homeslider').flexslider({
        animation: "fade",
        start: function (slider) {
            jQuery('body').removeClass('loading');
        }
    });
    jQuery('#carousel').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        itemWidth: 120,
        itemMargin: 10,
        asNavFor: '#slider'
    });
    jQuery('#slider').flexslider({
        animation: "fade",
        animationLoop: false,
        slideshow: false,
        sync: "#carousel",
        start: function (slider) {
            jQuery('body').removeClass('loading');
            console.log('Starting slider');
        }
    });
});