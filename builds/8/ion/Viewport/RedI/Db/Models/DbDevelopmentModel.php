<?php

namespace ion\Viewport\RedI\Db\Models;

/**
 * Description of DbProperties
 *
 * @author Justus
 */
use \ion\Viewport\RedI\Model;
use \ion\Viewport\RedI\Db\DbModel;
use \ion\WordPress\WordPressHelper as WP;
use \ion\Viewport\RedI\ViewModels\PropertyViewModel;

class DbDevelopmentModel extends DbModel {

    protected static function GetDefaultTableName() {
        global $wpdb;
        return $wpdb->prefix . 'redi_developments';
    }    
    
    public static function GetSchema() {
        return [
            'import_id' => '%s',
            'batch_id' => '%d',
            'development' => '%s',            
            //'import_slug' => '%s',
            'hide_prices' => '%d',
            'hide_plan_prices' => '%d',
            'label' => '%s',
            'import_start_time' => '%s',
            'import_end_time' => '%s',
            'agencies' => '%s',
            'errors' => '%s'
        ];        
    }
    
 
    
    private $development;

    public function __construct($development, $tableSuffix = null) {

        //var_dump($data);

        global $wpdb;
        
        parent::__construct($wpdb->prefix . 'redi_developments' . ($tableSuffix === null ? '' : '_' . $tableSuffix), $development->ToArray(), 'id');

        $this->development = $development;
       
    }
    
    public function GetDevelopmentModel() {
        return $this->development;
    }

    


    public function Insert() {
        
        $this->Set('import_id', md5($this->GetTableName() . '_' . $this->GetDevelopmentModel()->GetName()));
        $this->Set('batch_id', $this->GetBatchId());
        $this->Set('development', $this->GetDevelopmentModel()->GetName());
        $this->Set('hide_prices', ($this->GetDevelopmentModel()->GetHidePrices() ? 1 : 0));
        $this->Set('hide_plan_prices', ($this->GetDevelopmentModel()->GetHidePlanPrices() ? 1 : 0));        
//        $this->Set('hide_prices', 0);
//        $this->Set('hide_plan_prices', 0);            
        $this->Set('label', $this->GetDevelopmentModel()->GetLabel());
        $this->Set('import_start_time', date('Y-m-d H:i:s'));
        $this->Set('import_end_time', null);                
        $this->Set('agencies', json_encode($this->GetDevelopmentModel()->GetAgencies()->toArray()));
        $this->Set('errors', null);
        
        return parent::Insert();

    }

    public function GetId() {
        return $this->Get('id');
    }

    public function SetId(/* int */ $id) {
        $this->Set('id', $id);
    }
    
    public function GetBatchId() {
        return $this->Get('batch_id');
    }
    
    public function SetBatchId($batchId) {
        $this->Set('batch_id', $batchId);
    }

    public function GetImportId() {
        //return $this->Get('import_id');

        return (string) md5($this->GetTableName() . '_' . $this->GetDevelopment() . ($this->GetLabel() !== null ? '_' . $this->GetLabel() : ""));
    }

//    public function SetImportId(/* int */ $importId) {
//        $this->Set('import_id', $importId);
//    }

    public function GetDevelopment() {
        return $this->Get('development');
    }

    public function SetDevelopment(/* string */ $development) {
        $this->Set('development', $development);
    }

    public function GetLabel() {
        return $this->Get('label');
    }

    public function SetLabel(/* string */ $label) {
        $this->Set('label', $label);
    }

    public function SetHidePrices($hidePrices) {
        $this->Set('hide_prices', $hidePrices);
    }
    
    public function SetHidePlanPrices($hidePlanPrices) {
        $this->Set('hide_plan_prices', $hidePlanPrices);
    }    
    
    public function GetHidePrices() {
        return $this->Get('hide_prices');
    }
    
    public function GetHidePlanPrices() {
        return $this->Get('hide_plan_prices');
    }
    
    public function GetAgencies() {
        return $this->Get('agencies');
    }
    
//    public function GetData() {
//        return $this->Get('data');
//    }
//
//    public function SetData(/* string */ $data = null) {
//        $this->Set('data', $data);
//    }
//
//    public function GetTimeImported() {
//        return $this->Get('time_imported');
//    }
//
//    public function SetTimeImported(/* DateTime? */ $time = null) {
//        $this->Set('time_imported', $time);
//    }
    
//    public abstract static function finalize($targetTableName, array $targetSchema) {
//        
//        
//    }    
    
}
