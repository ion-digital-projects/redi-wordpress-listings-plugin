<?php

namespace ion\Viewport\RedI\Db;

/**
 * Description of DbModel
 *
 * @author Justus
 */
use \ion\WordPress\WordPressHelper as WP;
use \ion\Viewport\RedI\Model;

abstract class DbModel extends Model implements IDbModel {

    protected static $tableName = [];
    
    protected static function SetTableName($tableName) {
        static::$tableName[static::class] = $tableName;
    }
    
    protected static function GetDefaultTableName() {
        // empty!
    }
    
    public static function GetTableName() {
        
        $tmp = null;
        
        if(in_array(static::class, static::$tableName)) {
            $tmp = static::$tableName[static::class];
        }
        
        if(empty($tmp)) {
            return static::GetDefaultTableName();
        }
        
        return $tmp;
    }
    
    public static function GetInsertFieldSql() {
        
        $fields = [];
        
        foreach(static::GetSchema() as $field => $parm) {
            //if(array_key_exists(strtolower($field), array_change_key_case($values, CASE_LOWER))) {
                
                $fields[] = $field;
            //}
        }
        
        return  join(', ', $fields);
    }   

    
    
    public static function Truncate($tableName = null, $developmentName = null) {
        
        if($tableName === null) {
            
            $tableName = static::GetTableName();
        }
        
        if($developmentName === null) {
            
            WP::DbQuery("DELETE FROM " . $tableName . ";");
            return;
        }
        WP::DbQuery("DELETE FROM `$tableName` WHERE `development` LIKE ('$developmentName');");
        return;
    }    
    
    
    private $parameters; 
    private $primaryKeyName;

    public function __construct($tableName, $data, $primaryKeyName = null) {
        parent::__construct($data);

        static::SetTableName($tableName);
        
        $this->primaryKeyName = $primaryKeyName;
        
        
    }
 
    public function Exists() {
        $result = WP::DbQuery("SELECT COUNT(`import_id`) AS cnt FROM " . $this->GetTableName() . " WHERE `import_id` = %d", [$this->GetImportId()]);
        return (bool) intval($result[0]['cnt']) > 0;
    }
    
    public function GetParameters() {
        return $this->parameters;
    }    
    
    public function SetParameters(array $parameters) {
        $this->parameters = array_change_key_case($parameters, CASE_LOWER);
    }


    public function Insert() {
        //$tableName = static::GetTableName();

        $values = [];

        foreach(static::GetSchema() as $field => $parm) {
                       
            $val = null;
            
            if(array_key_exists(strtolower($field), array_change_key_case($this->ToArray(), CASE_LOWER))) {

                $val = $this->ToArray()[$field];
            }

            if($val !== null) {

                if($parm == '%s') {
                    $val = str_replace("'", "\\'", $val);
                } 
            }       
            
            $values[] = ($val === null ? 'NULL' : ($parm == '%s' ? "'$val'" : $val));
            
        }

        return join(',', $values);        
    }

    public static function copy($sourceTableName, $targetTableName, array $targetSchema) {
        
//INSERT INTO courses (name, location, gid)
//SELECT name, location, 1
//FROM   courses
//WHERE  cid = 2        
        
        
        $fields = "`" . join("`, `", array_keys($targetSchema)) . "`";
        
        $sql = "INSERT INTO `{$targetTableName}` ({$fields}) SELECT ${fields} FROM `{$sourceTableName}`;" ;

        WP::DbQuery($sql);
    }

}
