<?php


namespace ion\Viewport\RedI;

/**
 * Description of RedIFeedFilterWidget
 *
 * @author Justus
 */

use \ion\WordPress\WordPressHelper as WP;
use \ion\WordPress\Helper\WordPressWidget;
use \ion\Viewport\RedI\RedIFeedPlugIn as RedI;

class RedIFeedFilterWidget extends RedIWidget {

    public function __construct() {
        parent::__construct("Red-I Search Widget");
    }

    protected function RenderFrontEnd(array $values = null) {
        
        $view = WP::getContext('redi/redi-plugin')->getView("filtering-widget-front");
        $view();
    }

    //protected function RenderBackEndForm(array $values = null) {
        
    //}

    //protected function ProcessBackEndForm(array $oldValues = null, array $newValues = null) {
        
    //}
    
    
}
