<?php

namespace ion\Viewport\RedI;

/**
 * Description of FeedSettings
 *
 * @author Justus
 */
class FeedSettings {

    public static function ApplyTemplate( /* string */ $template, array $tags) {
        foreach(array_keys($tags) as $tag) {                        
            $template = preg_replace("/{\\s*$tag\s*}/", ($tags[$tag] !== null ? $tags[$tag] : ''), $template);
        }
        return $template;
    }    
    
    private $delay;
    private $cacheDirectory;
    private $username;
    private $password;
    private $baseUri;
    private $uriTemplates;
    private $fakeFeed;
    private $import;
    private $db;
    
    public function __construct( /* string */ $baseUri, /* int */ $delay, array $uriTemplates,/* string */$cacheDirectory = null, /* bool */ $fakeFeed = false, /* string */$username = null,/* string */$password = null, /* bool */ $import = false, /* bool */ $db = false) {
        $this->delay = $delay;
        $this->cacheDirectory = $cacheDirectory;
        $this->username = $username;
        $this->password = $password;
        $this->baseUri = $baseUri;
        $this->uriTemplates = $uriTemplates;
        $this->fakeFeed = $fakeFeed;
        $this->import = $import;
        $this->db = $db;
    }
    
    public function GetDelay() {
        return (int) $this->delay;
    }
    
    public function GetCacheDirectory() {
        return $this->cacheDirectory;
    }
    
    public function GetUsername() {
        return $this->username;
    }
    
    public function GetPassword() {
        return $this->password;
    }
    
    public function GetBaseUri() {
        return $this->baseUri;
    }
    
    public function GetUriTemplates() {
        return $this->uriTemplates;
    }
    
    public function GetUriTemplate( /* string */ $name) {
        if(array_key_exists($name, $this->uriTemplates)) {
            return $this->uriTemplates[$name];
        }
        
        return null;
    }
    
    public function GetFakeFeed() {
        return $this->fakeFeed;
    }
    
    public function GetImport() {
        return $this->import;
    }
    
    public function GetDb() {
        return $this->db;
    }
    
}
