<?php


namespace ion\Viewport\RedI\Feeds\Models;

/**
 * Description of Gallery
 *
 * @author Justus
 */

use \ion\Viewport\RedI\Model;
use \ion\Viewport\RedI\FeedSettings;

class Gallery extends Model {

    private $items;
    
    public function __construct(FeedSettings $feedSettings, array $data, $fetchNow = false) {
        parent::__construct($data);
        
        $this->items = [];
        
        foreach($data as $obj) {
            $this->items[] = new Image($obj);
        }        
    }    

    public function GetItems() {
        return $this->items;
    }
    
    public function Filter(array $filter) {
        
    }
    
}
