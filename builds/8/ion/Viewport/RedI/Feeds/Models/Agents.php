<?php


namespace ion\Viewport\RedI\Feeds\Models;

/**
 * Description of Gallery
 *
 * @author Justus
 */

use \ion\Viewport\RedI\Model;
use \ion\Viewport\RedI\FeedSettings;
use \ion\Viewport\RedI\Feeds\Models\PlanTypes;

class Agents extends Model {

    
    private $items;
    
    public function __construct(FeedSettings $feedSettings, array $data) {
        parent::__construct($data);
        
        $this->items = [];
        
        foreach($data as $obj) {            
            $this->items[] = new Agent($obj);
        }
        
    }    
    
    
    public function GetItems() {
        return $this->items;
    }


    
    
    
}
