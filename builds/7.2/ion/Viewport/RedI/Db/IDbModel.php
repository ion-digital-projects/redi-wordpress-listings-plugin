<?php
namespace ion\Viewport\RedI\Db;

/**
 *
 * @author Justus
 */
interface IDbmodel
{
    static function GetSchema();
    static function GetTableName();
    static function GetInsertFieldSql();
    function Exists();
    function Insert();
}