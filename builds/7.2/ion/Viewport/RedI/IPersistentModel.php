<?php
namespace ion\Viewport\RedI;

/**
 *
 * @author Justus
 */
interface IPersistentModel
{
    static function GetTableFields();
    static function GetTableName();
}