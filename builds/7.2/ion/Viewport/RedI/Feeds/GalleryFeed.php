<?php
namespace ion\Viewport\RedI\Feeds;

/**
 * Description of FeedGallery
 *
 * @author Justus
 */
use ion\Viewport\RedI\SalesMapFeed;
use ion\Viewport\RedI\FeedSettings;
use ion\Viewport\RedI\Feeds\Models\Gallery;
class GalleryFeed extends SalesMapFeed
{
    public function __construct(FeedSettings $feedSettings, $estateName, $developmentName)
    {
        parent::__construct($feedSettings, "gallery", ["estate" => $estateName, "development" => $developmentName]);
    }
    protected function Process(array $json, $now = false)
    {
        return new Gallery($this->GetSettings(), $json, $now);
    }
}