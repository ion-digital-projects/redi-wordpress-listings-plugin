<?php
namespace ion\Viewport\RedI\Feeds;

/**
 * Description of FeedProperties
 *
 * @author Justus
 */
use ion\Viewport\RedI\SalesMapFeed;
use ion\Viewport\RedI\FeedSettings;
use ion\Viewport\RedI\Feeds\Models\Development;
use ion\Viewport\RedI\Feeds\Models\Properties;
use ion\Viewport\RedI\Feeds\Models\PlanTypes;
use ion\Viewport\RedI\Feeds\Models\Agents;
class PropertiesFeed extends SalesMapFeed
{
    private $planTypes;
    private $agents;
    public function __construct(FeedSettings $feedSettings, $estateName, $developmentName, PlanTypes $planTypes = null, Agents $agents = null, $fetchNow = false)
    {
        parent::__construct($feedSettings, "properties", ["estate" => $estateName, "development" => $developmentName]);
        $this->planTypes = $planTypes;
        $this->agents = $agents;
    }
    public function GetPlanTypes()
    {
        return $this->planTypes;
    }
    public function GetAgents()
    {
        return $this->agents;
    }
    protected function Process(array $json, $now = false)
    {
        return new Properties($this->GetSettings(), $json, $this->GetPlanTypes(), $this->GetAgents());
    }
}