<?php
namespace ion\Viewport\RedI\Feeds\Models;

/**
 * Description of Agent
 *
 * @author Justus
 */
use ion\Viewport\RedI\Model;
class Agent extends Model
{
    public function __construct(array $data)
    {
        parent::__construct($data);
    }
    // string
    public function GetFirstName()
    {
        return $this->Get("firstName");
    }
    // string
    public function GetLastName()
    {
        return $this->Get("lastName");
    }
    // string
    public function GetEmailAddress()
    {
        return $this->Get("emailAddress");
    }
    // string
    public function GetMobileNumber()
    {
        return $this->Get("mobileNumber");
    }
    // string
    public function GetPhotoUrl()
    {
        return $this->Get("photoUrl");
    }
    public function GetAgencies()
    {
        return $this->Get("agencies");
    }
}