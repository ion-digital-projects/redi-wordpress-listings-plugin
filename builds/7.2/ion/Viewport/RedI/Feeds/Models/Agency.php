<?php
namespace ion\Viewport\RedI\Feeds\Models;

/**
 * Description of Agent
 *
 * @author Justus
 */
use ion\Viewport\RedI\Model;
class Agency extends Model
{
    public function __construct(array $data)
    {
        parent::__construct($data);
    }
    // string
    public function GetName()
    {
        return $this->Get("name");
    }
    // string
    public function GetDescription()
    {
        return $this->Get("description");
    }
    // string
    public function GetEmailAddress()
    {
        return $this->Get("emailAddress");
    }
    // string
    public function GetOfficeNumber()
    {
        return $this->Get("officeNumber");
    }
    // string
    public function GetPhotoUrl()
    {
        return $this->Get("photoUrl");
    }
}