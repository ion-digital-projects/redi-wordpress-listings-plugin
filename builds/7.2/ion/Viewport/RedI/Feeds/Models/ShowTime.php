<?php
namespace ion\Viewport\RedI\Feeds\Models;

/**
 * Description of ShowTime
 *
 * @author Justus
 */
use ion\Viewport\RedI\Model;
class ShowTime extends Model
{
    public function __construct(array $data)
    {
        parent::__construct($data);
    }
    // string
    public function GetAgency()
    {
        return $this->Get("agency");
    }
    // string
    public function GetAgent()
    {
        return $this->Get("agent");
    }
    // int
    public function GetStartTime()
    {
        return $this->Get("startTime");
    }
    // int
    public function GetEndTime()
    {
        return $this->Get("endTime");
    }
}