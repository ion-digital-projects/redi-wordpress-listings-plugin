<?php
namespace ion\Types;

// This is a polyfill for the actual ion\Types\StringObject class, to fix
// Waterfall
use ion\PhpHelper as PHP;
class StringObject
{
    private $string;
    public static function create(string $string = null)
    {
        return new static($string);
    }
    public function __construct(string $string = null)
    {
        $this->string = $string;
    }
    public function replace(string $search, string $replacement, bool $recursive = false)
    {
        return static::create(str_replace($search, $replacement, $this->string));
    }
    function stripWhiteSpace(string $replacement = ' ')
    {
        return static::create(PHP::strStripWhiteSpace($this->string, $replacement));
    }
    public function __toString()
    {
        return (string) $this->string;
    }
}