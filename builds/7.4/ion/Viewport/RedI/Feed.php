<?php
namespace ion\Viewport\RedI;

/**
 * Description of Feed
 *
 * @author Justus
 */
use Exception;
use ion\Viewport\RedI\FeedSettings;
use ion\WordPress\WordPressHelper as WP;
use ion\PhpHelper as PHP;
use ion\WordPress\Helper\LogLevel;
abstract class Feed
{
    public static function GetCacheFilename($url)
    {
        return md5($url) . ".php";
    }
    public static function WrapCacheFile($content, $debugComment = null)
    {
        $tmp = "";
        if ($debugComment !== null) {
            $tmp = "/* " . $debugComment . " */";
        }
        return "<?php header('HTTP/1.1 403 Unauthorized'); exit; ?>{$tmp}\n" . $content;
    }
    private $uri;
    private $settings;
    public function __construct(FeedSettings $feedSettings, $uri)
    {
        $this->settings = $feedSettings;
        $this->uri = $uri;
    }
    public function GetSettings()
    {
        return $this->settings;
    }
    public function GetUri()
    {
        return $this->uri;
    }
    protected abstract function Process(array $json, $now = false);
    public function Fetch($now = false)
    {
        if ($this->settings->GetFakeFeed() === true) {
            $lastSlash = strrpos($this->uri, "/") + 1;
            $fileName = substr($this->uri, $lastSlash, strlen($this->uri) - $lastSlash);
            $path = WP::context()->getWorkingDirectory() . "/debug/" . $fileName . ".json";
            //echo $path . "<br />";
            if (file_exists($path)) {
                $content = file_get_contents($path);
                $json = json_decode($content, true);
                if ($json !== null) {
                    return $this->Process($json);
                }
            }
            return $this->Process([]);
        }
        $cachePath = $this->GetSettings()->GetCacheDirectory() . static::GetCacheFilename($this->uri);
        $fetch = false;
        if ($now === true) {
            $fetch = true;
        } else {
            $time = time();
            if (file_exists($cachePath)) {
                $modified = filemtime($cachePath);
                if ($time >= $modified + 60 * $this->GetSettings()->GetDelay()) {
                    $fetch = true;
                }
            } else {
                $fetch = true;
            }
        }
        if ($fetch === true) {
            $handle = curl_init($this->uri);
            if ($handle !== false) {
                $opts = array(
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_AUTOREFERER => true,
                    CURLOPT_FAILONERROR => true,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_MAXREDIRS => 3,
                    CURLOPT_TIMEOUT => 28800,
                    // set this to 8 hours so we dont timeout on big files
                    CURLOPT_PROTOCOLS => CURLPROTO_HTTP | CURLPROTO_HTTPS,
                    CURLOPT_HTTPAUTH => CURLAUTH_ANY,
                    CURLOPT_USERAGENT => "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322)",
                );
                if ($this->GetSettings()->GetUsername() !== null) {
                    $opts[CURLOPT_USERPWD] = $this->GetSettings()->GetUsername() . ":" . $this->GetSettings()->GetPassword();
                }
                curl_setopt_array($handle, $opts);
                $result = curl_exec($handle);
                if ($result === false) {
                    $uri = PHP::toString(curl_getinfo($handle, CURLINFO_EFFECTIVE_URL));
                    $error = curl_error($handle);
                    $errorCode = PHP::toInt(curl_getinfo($handle, CURLINFO_RESPONSE_CODE));
                    $opts[CURLOPT_FAILONERROR] = false;
                    $opts[CURLOPT_HEADER] = true;
                    $opts[CURLINFO_HEADER_OUT] = true;
                    $opts[CURLOPT_HTTP200ALIASES] = array($errorCode);
                    curl_setopt_array($handle, $opts);
                    $errorResponse = curl_exec($handle);
                    $errorRequest = PHP::toString(curl_getinfo($handle, CURLINFO_HEADER_OUT));
                    WP::log("Import failed for URI with response code *{$errorCode}*: **{$uri}**<br /><br />**cURL said**: " . $error . "<br /><br />Request headers:<br /><br /><pre style=\"display: block; box-sizing: border-box; border: 1px dotted black; width: 100%; background-color: white; padding: 5px;\">" . htmlentities($errorRequest) . "</pre><br /><br />Response w/headers:<br /><br /><pre style=\"display: block; box-sizing: border-box; border: 1px dotted black; width: 100%; background-color: white; padding: 5px;\">" . htmlentities($errorResponse) . "</pre>", LogLevel::ERROR, 'redi-cron');
                    //throw new Exception("URI: <strong>{$this->uri}</strong><br /><br />" . curl_error($handle));
                    //return null;
                }
                //curl_getinfo($handle, CURLINFO_HTTP_CODE );
                curl_close($handle);
                if ($result !== false) {
                    file_put_contents($cachePath, static::WrapCacheFile($result, $this->uri . " (" . date('m/d/Y h:i:s a', time()) . ")"));
                    $json = json_decode($result, true);
                    if ($json !== null) {
                        return $this->Process($json, $now);
                    }
                }
            }
        } else {
            $content = file_get_contents($cachePath);
            $json = json_decode(substr($content, strpos($content, "\n")), true);
            if ($json !== null) {
                return $this->Process($json, $now);
            }
        }
        return null;
    }
}