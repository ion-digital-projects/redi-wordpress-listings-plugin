<?php
namespace ion\Viewport\RedI;

/**
 * Description of TTemplate
 *
 * @author Justus
 */
use Exception;
use ion\WordPress\WordPressHelper as WP;
use ion\PhpHelper as PHP;
use ion\Viewport\RedI\Feeds\PropertiesFeed;
use ion\Viewport\RedI\FeedMarshal;
use ion\Viewport\RedI\Model;
use ion\Viewport\RedI\Marshal;
use ion\Viewport\RedI\Feeds\Models\EnquiryFormField;
use ion\Viewport\RedI\ViewModels\PlanTypeViewModel;
use ion\Viewport\RedI\Feeds\Models\PlanType;
trait TemplateTagsTrait
{
    protected static function IsProperty()
    {
        return static::GetInstance()->GetState()->IsProperty();
    }
    public static function PrintDebugModels(array $models = null)
    {
        //if (static::IsDebugMode()) {
        echo "<h1>ViewModel Debug Information</h1>";
        echo "<pre>";
        print_r($models);
        echo "</pre>";
        exit;
        //}
    }
    public static function GetInvalidImage($height = null)
    {
        $h = "";
        if ($height !== null) {
            $h = "height:{$height};";
        }
        return "<img style=\"cursor:help;{$h}\" src=\"" . WP::getContext('redi/redi-plugin')->getWorkingUri() . "/images/placeholder.gif\" title=\"\" alt=\"\" />";
    }
    private static function CheckValue($value, $alternativeValue = null)
    {
        if ($value === null || $value === "") {
            if (static::IsDebugMode()) {
                return "<span style=\"red\">" . static::GetInvalidImage("1em") . "</span>";
            } else {
                return $alternativeValue;
            }
        }
        return $value;
    }
    public static function DevelopmentLabel($plural = false, $echo = true)
    {
        $str = $plural ? WP::getOption('redi-development-string-plural', 'Developments') : WP::getOption('redi-development-string-singular', 'Development');
        if ($echo) {
            echo $str;
        }
        return $str;
    }
    public static function PropertyLabel()
    {
        $property = static::GetCurrentViewModel();
        $value = null;
        if ($property !== null) {
            $value = $property->GetLabel();
        }
        echo $value === null ? "?" : "{$value}";
    }
    public static function PropertyPhase($echo = true)
    {
        $property = static::GetCurrentViewModel();
        if ($echo) {
            echo $property->getPhase();
        }
        return $property->GetPhase();
    }
    public static function PropertyTitle($echo = true)
    {
        //if(!WP::getOption('redi-show-unit-number', true) && static::IsProperty()) {
        //
        //    return;
        //}
        $unitNumber = null;
        $development = null;
        $phase = null;
        if (WP::getOption('redi-show-development', true)) {
            $development = static::DevelopmentName(false);
        }
        if (WP::getOption('redi-show-phase', true)) {
            $phase = static::PropertyPhase(false);
        }
        if (WP::getOption('redi-show-unit-number', true)) {
            $property = static::GetCurrentViewModel();
            if ($property !== null) {
                $unitNumber = $property->GetTitle();
            }
            if (PHP::isEmpty($unitNumber)) {
                $unitNumber = $property->GetLabel();
            } else {
                $unitNumber = "Unit #: {$unitNumber}";
            }
        }
        $result = [$development, strtoupper($phase) != strtoupper($development) ? $phase : null, $unitNumber];
        $output = implode(" / ", array_values(array_filter($result)));
        if ($echo) {
            echo $output;
        }
        return $output;
        //        if ($unitNumber === null || $unitNumber === '') {
        //
        //            $unitNumber = $property->GetUnitNumber();
        //
        //            if (PHP::isEmpty($property->GetUnitNumber())) {
        //
        //                $unitNumber = ($property->GetLabel() !== null && $property->GetLabel() ? $property->GetLabel() : "Details");
        //
        //            } else {
        //
        //                echo "Unit # ";
        //            }
        //
        //            $estateName = WP::GetOption("redi-feed-estate");
        //
        //            if ($estateName !== null) {
        //
        //                $state = new State($estateName);
        //
        //                if($state->isProperty()) {
        //
        //                    $unitNumber = "{$unitNumber} / " . static::DevelopmentName(false, false);
        //                }
        //            }
        //
        //
        //        }
        //
        //        if($echo) {
        //
        //            if (!PHP::isEmpty($property->GetUnitNumber())) {
        //
        //                echo "Unit # {$unitNumber}";
        //
        //            } else {
        //
        //                echo $unitNumber;
        //            }
        //        }
        //
        //        return $unitNumber;
    }
    public static function PropertySubTitle()
    {
        $property = static::GetCurrentViewModel();
        $value = null;
        if ($property !== null) {
            $value = $property->GetSubTitle();
        }
        echo static::CheckValue($value);
    }
    public static function PropertyUnitNumber()
    {
        $property = static::GetCurrentViewModel();
        $value = null;
        if ($property !== null) {
            $value = $property->GetUnitNumber();
            if ($value === null || $value === 0) {
                $value = $property->GetLabel() !== null ? $property->GetLabel() : null;
            }
        }
        echo static::CheckValue($value, "N/A");
    }
    private static function getCurrentPlan($name = null, $default = null)
    {
        $result = null;
        $property = static::GetCurrentViewModel();
        if (count($property->GetPlans()) === 0) {
            return null;
        }
        if ($name !== null) {
            foreach ($property->GetPlans() as $plan) {
                //echo $plan->GetLabel() . " = " . $name . "<br />";
                if ($plan->GetLabel() == $name) {
                    $result = $plan;
                    break;
                }
            }
            return $result;
        }
        if ($default !== null) {
            foreach ($property->GetPlans() as $plan) {
                //echo $plan->GetLabel() . " = " . $name . "<br />";
                if ($plan->GetLabel() == $default) {
                    $result = $plan;
                    break;
                }
            }
            return $result;
        }
        $result = $property->GetPlans()[0];
        return $result;
    }
    public static function PropertyPrice($echo = true)
    {
        $estateName = WP::GetOption("redi-feed-estate");
        $value = null;
        if ($estateName !== null) {
            $state = new State($estateName);
            $property = static::GetCurrentViewModel();
            if ($property !== null) {
                $dev = $property->GetDevelopment();
                //var_Dump($dev);
                if (!$property->GetHidePrices()) {
                    $plan = static::getCurrentPlan($state->GetPlan(), $property->GetSelectedPlanType());
                    //                    echo "<pre>";
                    //                    print_r($plan);
                    //                    echo "</pre>";
                    $value = '';
                    $plotPrice = 0;
                    $planPrice = 0;
                    $totalPrice = 0;
                    $label = "Total";
                    if (strtolower($property->GetPropertyStatus()) === 'available for rental') {
                        $totalPrice = $property->GetRentalAmount();
                        $label = "Rental";
                    } else {
                        //                        var_dump($property->GetHidePrices());
                        //                        die("HERE");
                        if ($property->GetPlotPrice() !== null && $property->GetPlotPrice() > 0) {
                            $plotPrice = floatval($property->GetPlotPrice());
                        }
                        if ($plan !== null) {
                            if ($plan->GetPlanPrice() !== null && $plan->GetPlanPrice() > 0) {
                                $planPrice = floatval($plan->GetPlanPrice());
                            }
                        }
                        if ($plan !== null && $plan->getLabel() != 'Plot only') {
                            if (!$property->GetHidePlanPrices()) {
                                if ($property->GetPlotPrice() !== null && $property->GetPlotPrice() > 0) {
                                    $value .= "<span class=\"price\"><span class=\"plotPrice\">Plot</span><span class=\"plotPriceValue\"> " . static::FormatCurrency($plotPrice) . "</span></span><br />";
                                }
                                if ($plan->GetPlanPrice() !== null && $plan->GetPlanPrice() > 0) {
                                    $planPrice = floatval($plan->GetPlanPrice());
                                    $value .= "<span class=\"price\"><span class=\"planPrice\">Plan</span><span class=\"planPriceValue\"> " . static::FormatCurrency($planPrice) . "</span></span><br />";
                                }
                            }
                            $totalPrice = $plotPrice + $planPrice;
                        } else {
                            $totalPrice = $plotPrice;
                        }
                    }
                    $value .= "<span class=\"price\"><span class=\"totalPrice\">{$label}</span><span class=\"totalPriceValue\"> " . static::FormatCurrency(floatval($totalPrice)) . "</span></span><br />";
                }
            }
            $output = static::CheckValue($value, $property->GetHidePrices() ? '' : "No price available");
        }
        if ($echo === true) {
            echo $output;
        }
        return $value;
    }
    public static function PlanSelection($echo = true)
    {
        $output = '';
        $estateName = WP::GetOption("redi-feed-estate");
        if ($estateName !== null) {
            $state = new State($estateName);
            $property = static::GetCurrentViewModel();
            if ($property !== null) {
                if (count($property->GetPlans()) > 1) {
                    //                    echo "<pre>";
                    //                    var_dump($property->GetPlans());
                    //                    die("</pre>");
                    $output = "<small>Plan Options</small><br /><select id=\"plan-select\">";
                    foreach ($property->GetPlans() as $plan) {
                        $selected = '';
                        if ($state->GetPlan() === null && $property->GetSelectedPlanType() !== null) {
                            if ($plan->GetLabel() == $property->GetSelectedPlanType()) {
                                $selected = ' selected';
                            }
                        } else {
                            if ($plan->GetLabel() == $state->GetPlan()) {
                                $selected = ' selected';
                            }
                        }
                        $output .= "<option value\"" . rawurlencode($plan->GetLabel()) . "\"{$selected}>" . $plan->GetLabel() . "</option>";
                    }
                    $output .= "</select>";
                    $output .= <<<JS
<script type="text/javascript">
jQuery('#plan-select').change(function() {
    var url = window.location.href.split('?')[0] + '?plan=' + encodeURIComponent(jQuery(this).val());                        
    window.location = url;
});
</script>
JS;
                }
            }
        }
        if ($echo === true) {
            echo $output;
        }
        return $output;
    }
    public static function PropertyMinPrice($echo = true)
    {
        $property = static::GetCurrentViewModel();
        $value = null;
        //        echo "<pre>";
        //        var_dump($property);
        //        die("</pre>");
        if ($property !== null && !$property->GetHidePrices()) {
            if ($property->GetRentalAmount() !== null && strtolower($property->GetPropertyStatus()) === 'available for rental') {
                $value = static::FormatCurrency(floatval($property->GetRentalAmount()));
            } else {
                if ($property->GetMinPrice() !== null && $property->GetMinPrice() > 0) {
                    $value = static::FormatCurrency(floatval($property->GetMinPrice()));
                }
            }
        }
        $output = static::CheckValue($value, !$property->GetHidePrices() ? "No price available" : "");
        if ($echo === true) {
            echo $output;
        }
        return $value;
    }
    public static function PropertyMaxPrice($echo = true)
    {
        $property = static::GetCurrentViewModel();
        $value = null;
        if ($property !== null && !$property->GetHidePrices()) {
            if ($property->GetRentalAmount() !== null && strtolower($property->GetPropertyStatus()) === 'available for rental') {
                $value = static::FormatCurrency(floatval($property->GetRentalAmount()));
            } else {
                if ($property->GetMaxPrice() !== null && $property->GetMaxPrice() > 0) {
                    $value = static::FormatCurrency(floatval($property->GetMaxPrice()));
                }
            }
        }
        $output = static::CheckValue($value, !$property->GetHidePrices() ? "No price available" : "");
        if ($echo === true) {
            echo $output;
        }
        return $value;
    }
    public static function PropertyAgentName()
    {
        $property = static::GetCurrentViewModel();
        $value = null;
        if ($property !== null) {
            $value = $property->GetAgentName();
        }
        echo static::CheckValue($value, "N/A");
    }
    public static function PropertyAgentContactNumber()
    {
        $property = static::GetCurrentViewModel();
        $value = null;
        if ($property !== null) {
            $value = $property->GetAgentContactNumber();
        }
        echo static::CheckValue($value, "N/A");
    }
    public static function PropertyEmailAddress($linkOnly = false)
    {
        $property = static::GetCurrentViewModel();
        $value = null;
        if ($property !== null) {
            $value = $property->GetAgentEmailAddress();
        }
        if ($linkOnly === true) {
            echo $value;
        } else {
            echo static::CheckValue($value, "N/A");
        }
    }
    public static function AgencyImage()
    {
    }
    public static function PropertyStatus()
    {
        $property = static::GetCurrentViewModel();
        $value = null;
        $class = "";
        // <span>Residential</span><span class="commercial">Commercial</span><span class="available">Available</span><span class="sold">Sold Out</span>
        if ($property !== null) {
            $value = $property->GetPropertyStatus();
            if (strpos($value, "Sold") != -1) {
                $class = "sold";
            } else {
                $class = "available";
            }
        }
        echo "<span class=\"{$class}\">" . static::CheckValue($value) . "</span>";
    }
    public static function PropertyType()
    {
        $property = static::GetCurrentViewModel();
        $value = null;
        if ($property !== null) {
            $value = $property->GetPropertyType();
        }
        echo static::CheckValue($value);
    }
    public static function PropertyBathrooms()
    {
        $property = static::GetCurrentViewModel();
        $value = null;
        if ($property !== null) {
            $value = $property->GetBathrooms();
        }
        echo static::CheckValue($value);
    }
    public static function PropertyBedrooms()
    {
        $property = static::GetCurrentViewModel();
        $value = null;
        if ($property !== null) {
            $value = $property->GetBedrooms();
        }
        echo static::CheckValue($value);
    }
    public static function PropertyGarages()
    {
        $property = static::GetCurrentViewModel();
        $value = null;
        if ($property !== null) {
            $value = $property->GetGarages();
        }
        echo static::CheckValue($value);
    }
    public static function PropertyParking()
    {
        $property = static::GetCurrentViewModel();
        $value = null;
        if ($property !== null) {
            $value = $property->GetParking();
        }
        echo static::CheckValue($value);
    }
    public static function PropertySize()
    {
        $property = static::GetCurrentViewModel();
        $value = null;
        if ($property !== null) {
            $value = $property->GetSize();
        }
        echo static::CheckValue($value);
    }
    public static function PropertyDescription($echo = true)
    {
        $property = static::GetCurrentViewModel();
        $value = null;
        if ($property !== null) {
            $value = $property->GetDescription();
        }
        $html = static::CheckValue($value, "No property description is currently available.");
        //        while(strpos($html, "\r") !== false) {
        //
        //            $html = str_replace("\r", "", $html);
        //        }
        //
        //        while(strpos($html, "\n\n") !== false) {
        //
        //            $html = str_replace("\n\n", "\n", $html);
        //        }
        //
        //        $html = trim($html);
        //
        //        while(strpos($html, "\n") !== false) {
        //
        //            $html = str_replace("\n", "*", $html);
        //        }
        $html = preg_replace('/\\r+/', '', $html);
        $html = preg_replace('/\\s*\\n+\\s*/', "\n", $html);
        $html = trim($html);
        $html = str_replace("\n", '<br />', $html);
        if ($echo) {
            echo '<span id="agent-description">' . $html . '</span>';
        }
        return $html;
    }
    public static function PropertyShowTimes()
    {
        $property = static::GetCurrentViewModel();
        $value = null;
        if ($property !== null) {
            $value = $property->GetShowTimes();
            if (count($property->GetShowTimes()) > 0) {
                echo "<h6>Show Times</h6>";
                foreach ($value as $showTime) {
                    $startTime = date("D H:i", $showTime->GetStartTime());
                    $endTime = date("D H:i", $showTime->GetEndTime());
                    echo $startTime . " - " . $endTime . "<br />";
                }
            }
        }
        if ($value === null) {
            echo static::CheckValue($value);
        }
    }
    public static function HasPropertyMapLink()
    {
        $estateName = WP::GetOption("redi-feed-estate");
        if ($estateName !== null) {
            $state = new State($estateName);
            if ($state->IsForUs()) {
                //if ($state->GetDevelopment() !== null) {
                if ($state->IsProperty()) {
                    $property = static::GetCurrentViewModel();
                    $value = null;
                    if ($property !== null) {
                        $value = $property->GetMapLink();
                    }
                    return $value !== null;
                } else {
                    if ($state->IsPropertyIndex()) {
                        return true;
                    }
                }
                //} else {
                //    if ($state->IsPropertyIndex()) {
                //        return true;
                //    }
                //}
            }
        }
        return false;
    }
    public static function PropertyMapLink($echo = true)
    {
        $estateName = WP::GetOption("redi-feed-estate");
        $value = '';
        if ($estateName !== null) {
            $state = new State($estateName);
            if ($state->IsForUs()) {
                if ($state->GetDevelopment() !== null) {
                    if ($state->IsProperty()) {
                        $property = static::GetCurrentViewModel();
                        $value = null;
                        if ($property !== null) {
                            $value = $property->GetMapLink();
                        }
                    } else {
                        if ($state->IsPropertyIndex()) {
                            $value = WP::GetOption("redi-feed-base-uri");
                            $value .= "/" . $estateName . "/#/estate/" . $state->GetDevelopment();
                        }
                    }
                } else {
                    if ($state->IsProperty()) {
                        $property = static::GetCurrentViewModel();
                        $value = null;
                        if ($property !== null) {
                            $value = $property->GetMapLink();
                        }
                    } else {
                        if ($state->IsPropertyIndex()) {
                            //https://www.salesmap.co.za/waterfall/#/estate/waterfall
                            $value = WP::GetOption("redi-feed-base-uri");
                            $value .= "/" . $estateName . "/#/estate/" . $estateName;
                        }
                    }
                }
            }
        }
        if ($echo === true) {
            echo $value;
        }
        return $value;
    }
    public static function PropertyFeatures($includeType = true, $includeBedrooms = true, $includeBathrooms = true, $includeGarages = true, $includeSize = true, $includeParking = false)
    {
        //                    <span><i class="fa fa-bed"></i> 4</span>
        //                    <span><i class="fa fa-bath"></i> 3</span>
        //                    <span><i class="fa fa-car"></i> 2</span>
        //                    <span><i class="fa fa-arrows-alt"></i>123 </span>
        $property = static::GetCurrentViewModel();
        $output = "";
        if ($property !== null) {
            $template = ["fa-home" => $includeType === true ? static::CheckValue($property->GetPropertyType(), null) : null, "fa-bed" => $includeBedrooms === true ? static::CheckValue($property->GetBedrooms(), null) : null, "fa-bath" => $includeBathrooms === true ? static::CheckValue($property->GetBathrooms(), null) : null, "fa-car" => $includeGarages === true ? static::CheckValue($property->GetGarages(), null) : null, "fa-arrows-alt" => $includeSize === true ? static::CheckValue($property->GetSize(), null) . " m<sup>2</sup>" : null, "fa-motorcycle" => $includeParking === true ? static::CheckValue($property->GetParking(), null) : null];
            $output .= "<div class=\"features\">";
            $joinedValue = 0;
            foreach ($template as $key => $value) {
                if ($value !== null && $key === "fa-car") {
                    $joinedValue += PHP::toInt($value);
                } else {
                    if ($value !== null && $key === "fa-motorcycle") {
                        $joinedValue += PHP::toInt($value);
                    }
                }
            }
            // echo $joinedValue;
            //
            foreach ($template as $key => $value) {
                if (PHP::isEmpty($value)) {
                    continue;
                }
                if ($value !== null && $key !== "fa-car" && $key !== "fa-motorcycle") {
                    $output .= "<span class=\"feature\"><i class=\"fa {$key}\"></i> <span>{$value}</span></span>";
                } else {
                    if ($includeGarages === true && $key == "fa-car") {
                        $output .= "<span class=\"feature\"><i class=\"fa {$key}\"></i> <span>{$joinedValue}</span></span>";
                    }
                }
            }
            $output .= "</div>";
        } else {
            $output .= static::GetInvalidImage();
        }
        echo $output;
    }
    public static function NextPropertyLink()
    {
        $property = static::GetCurrentViewModel();
        if ($property !== null) {
            $estateName = WP::GetOption("redi-feed-estate");
            if ($estateName !== null) {
                $state = new State($estateName);
                if ($state->IsForUs()) {
                    if ($state->IsProperty()) {
                        $feedSettings = RedIFeedPlugIn::GetInstance()->GetFeedSettings();
                        $data = Marshal::GetInstance()->Fetch($feedSettings, $state, true)['properties'];
                        $returnFilter = filter_input(INPUT_GET, "return-filter", FILTER_DEFAULT, FILTER_NULL_ON_FAILURE);
                        if ($data !== null) {
                            if ($returnFilter !== null) {
                                $filter = Filter::Parse($returnFilter);
                                $data = $filter->ApplyTo($data);
                            }
                            $index = Model::FindIndex("label", $property->GetLabel(), $data);
                            $nextIndex = null;
                            if ($index + 1 < count($data)) {
                                $nextIndex = $index + 1;
                            } else {
                                $nextIndex = 0;
                            }
                            $nextLabel = null;
                            if (count($data) > $nextIndex) {
                                if ($data[$nextIndex] !== null) {
                                    $nextLabel = $data[$nextIndex]->GetLabel();
                                    $returnPage = filter_input(INPUT_GET, "return-page", FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
                                    echo State::BuildSingleLink($state, $nextLabel, ["return-page" => $returnPage, "return-filter" => $returnFilter]);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    public static function HasNextPropertyLink()
    {
        $property = static::GetCurrentViewModel();
        if ($property !== null) {
            $estateName = WP::GetOption("redi-feed-estate");
            if ($estateName !== null) {
                $state = new State($estateName);
                if ($state->IsForUs()) {
                    if ($state->IsProperty()) {
                        $feedSettings = RedIFeedPlugIn::GetInstance()->GetFeedSettings();
                        $data = Marshal::GetInstance()->Fetch($feedSettings, $state, true)['properties'];
                        $returnFilter = filter_input(INPUT_GET, "return-filter", FILTER_DEFAULT, FILTER_NULL_ON_FAILURE);
                        if ($data !== null) {
                            if ($returnFilter !== null) {
                                $filter = Filter::Parse($returnFilter);
                                $data = $filter->ApplyTo($data);
                            }
                            $index = Model::FindIndex("label", $property->GetLabel(), $data);
                            $nextIndex = null;
                            if ($index + 1 < count($data)) {
                                $nextIndex = $index + 1;
                            } else {
                                $nextIndex = 0;
                            }
                            if (PHP::count($data) > $nextIndex) {
                                if ($data[$nextIndex] !== null) {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }
    public static function HasPreviousPropertyLink()
    {
        $property = static::GetCurrentViewModel();
        if ($property !== null) {
            $estateName = WP::GetOption("redi-feed-estate");
            if ($estateName !== null) {
                $state = new State($estateName);
                if ($state->IsForUs()) {
                    if ($state->IsProperty()) {
                        $feedSettings = RedIFeedPlugIn::GetInstance()->GetFeedSettings();
                        $data = Marshal::GetInstance()->Fetch($feedSettings, $state, true)['properties'];
                        $returnFilter = filter_input(INPUT_GET, "return-filter", FILTER_DEFAULT, FILTER_NULL_ON_FAILURE);
                        if ($data !== null) {
                            if ($returnFilter !== null) {
                                $filter = Filter::Parse($returnFilter);
                                $data = $filter->ApplyTo($data);
                            }
                            $index = Model::FindIndex("label", $property->GetLabel(), $data);
                            $nextIndex = null;
                            if ($index - 1 > 0) {
                                $nextIndex = $index - 1;
                            } else {
                                $nextIndex = 0;
                            }
                            if (PHP::count($data) > 0) {
                                if ($data[$nextIndex] !== null) {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }
    public static function PreviousPropertyLink()
    {
        $property = static::GetCurrentViewModel();
        if ($property !== null) {
            $estateName = WP::GetOption("redi-feed-estate");
            if ($estateName !== null) {
                $state = new State($estateName);
                if ($state->IsForUs()) {
                    if ($state->IsProperty()) {
                        $feedSettings = RedIFeedPlugIn::GetInstance()->GetFeedSettings();
                        $data = Marshal::GetInstance()->Fetch($feedSettings, $state, true)['properties'];
                        $returnFilter = filter_input(INPUT_GET, "return-filter", FILTER_DEFAULT, FILTER_NULL_ON_FAILURE);
                        if ($data !== null) {
                            if ($returnFilter !== null) {
                                $filter = Filter::Parse($returnFilter);
                                $data = $filter->ApplyTo($data);
                            }
                            $index = Model::FindIndex("label", $property->GetLabel(), $data);
                            $nextIndex = null;
                            if ($index - 1 >= 0) {
                                $nextIndex = $index - 1;
                            } else {
                                $nextIndex = count($data) - 1;
                            }
                            $prevLabel = $data[$nextIndex]->GetLabel();
                            $returnPage = filter_input(INPUT_GET, "return-page", FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
                            echo State::BuildSingleLink($state, $prevLabel, ["return-page" => $returnPage, "return-filter" => $returnFilter]);
                        }
                    }
                }
            }
        }
    }
    public static function PropertyIndexLink()
    {
        $property = static::GetCurrentViewModel();
        if ($property !== null) {
            $estateName = WP::GetOption("redi-feed-estate");
            if ($estateName !== null) {
                $state = new State($estateName);
                if ($state->IsForUs()) {
                    if ($state->IsProperty()) {
                        $returnPage = filter_input(INPUT_GET, "return-page", FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
                        $returnFilter = filter_input(INPUT_GET, "return-filter", FILTER_DEFAULT, FILTER_NULL_ON_FAILURE);
                        $link = State::BuildIndexLink($state, $returnPage, 0, false, $state->GetShow(), $state->GetOrderBy(), null, null);
                        if ($returnFilter !== null && !empty($returnFilter)) {
                            echo $link . "filter?parameters=" . rawurlencode($returnFilter);
                        } else {
                            echo $link;
                        }
                    }
                }
            }
        }
    }
    public static function PropertyLink($echo = true)
    {
        $property = static::GetCurrentViewModel();
        if ($property !== null) {
            $estateName = WP::GetOption("redi-feed-estate");
            if ($estateName !== null) {
                $state = new State($estateName);
                if ($state->IsForUs()) {
                    if ($state->IsProperty()) {
                        //echo "PROPERTY<br />";
                        echo State::BuildSingleLink($state, $property->GetLabel(), ["stored-filter" => $state->getStoredFilterName()]);
                    } else {
                        if ($state->IsPropertyIndex() || $state->IsFront()) {
                            //echo "INDEX<br />";
                            $filter = null;
                            //                        echo "<pre>";
                            //                        var_dump($state);
                            //                        echo "</pre>";
                            if ($state->GetFilter() !== null) {
                                $filter = rawurlencode($state->GetFilter()->ToQueryString());
                            }
                            //var_dump($property->GetDevelopment());
                            //exit;
                            $development = $property->GetDevelopment() !== null ? $property->GetDevelopment()->GetName() : null;
                            $link = State::BuildSingleLink($state, $property->GetLabel(), ["return-page" => $state->GetPage(), "return-filter" => $filter, "stored-filter" => $state->getStoredFilterName()], $development);
                            if ($echo === true) {
                                echo $link;
                            }
                            return $link;
                        }
                    }
                }
            }
        }
        return null;
    }
    public static function PropertyHasPlans()
    {
        $property = static::GetCurrentViewModel();
        //echo "<pre>";
        //print_r($property);
        //echo "</pre>";
        //exit;
        $cnt = 0;
        if ($property !== null) {
            if (count($property->GetPlans()) > 0) {
                foreach ($property->GetPlans() as $plan) {
                    $cnt += count($plan->GetImages());
                }
                return $cnt > 0;
            }
        }
        return false;
    }
    public static function PropertyPlanImages($element = null, $echo = true, $forPlanImageGallery = true, $num = null)
    {
        //<img src="img/plan1.gif" alt=""> <img src="img/plan1.gif" alt=""> <img src="img/plan1.gif" alt="">
        $prefix = "";
        $suffix = "";
        if ($element !== null) {
            $prefix = "<{$element}>";
            $suffix = "</{$element}>";
        }
        $output = "";
        $property = static::GetCurrentViewModel();
        if ($property !== null) {
            //            echo "<pre>";
            //            print_r($property->GetPlans());
            //            echo "</pre>";
            //            exit;
            if ($property->GetPlans() !== null) {
                if (is_array($property->GetPlans()) && count($property->GetPlans()) > 0) {
                    $class = '';
                    if ($forPlanImageGallery === true) {
                        $class = 'RedI plan-thumb';
                    }
                    $cnt = 0;
                    foreach ($property->GetPlans() as $plan) {
                        $label = $plan->GetLabel();
                        $src = "";
                        $caption = "";
                        $img = $plan->GetPrimaryImage();
                        if ($img !== null) {
                            //var_dump($img);
                            $thumbSrc = $img->GetThumb();
                            $imgSrc = $img->GetUrl();
                            if ($thumbSrc === null) {
                                $thumbSrc = $imgSrc;
                            }
                            if ($imgSrc !== null) {
                                $caption = $img->GetCaption();
                                //$src = $plan->GetUrl();
                                //$caption = $plan->GetCaption();
                                //$output .= "$prefix<img onclick=\"$click\" src=\"$src\" alt=\"$caption\" />$suffix";
                                if ($forPlanImageGallery === true) {
                                    $output .= "<li class=\"{$class}\"><a class=\"{$class} with-caption\" href=\"{$imgSrc}\" planType=\"{$label}\" title=\"{$caption}\"><img class=\"{$class}\" src=\"{$thumbSrc}\" alt=\"{$caption}\" /></a></li>\n";
                                } else {
                                    $output .= "<img class=\"{$class}\" src=\"{$thumbSrc}\" alt=\"{$caption}\" />\n";
                                }
                            } else {
                                if (RedIFeedPlugIn::IsDebugMode()) {
                                    $output .= "<li  class=\"{$class}\">" . static::GetInvalidImage("100px") . "</li>\n";
                                }
                            }
                            $cnt++;
                        } else {
                            if (RedIFeedPlugIn::IsDebugMode()) {
                                $output .= "<li  class=\"{$class}\">" . static::GetInvalidImage("100px") . "</li>\n";
                            }
                        }
                        if ($num !== null && $cnt === $num) {
                            break;
                        }
                    }
                    if (strlen($output) > 0 && $forPlanImageGallery === true) {
                        $output = "{$prefix}<ul class=\"{$class}\">\n{$output}</ul>{$suffix}";
                    }
                } else {
                    if (RedIFeedPlugIn::IsDebugMode()) {
                        $output .= $prefix . static::GetInvalidImage() . $suffix;
                    }
                }
            } else {
                if (RedIFeedPlugIn::IsDebugMode()) {
                    $output .= $prefix . static::GetInvalidImage() . $suffix;
                }
            }
        } else {
            if (RedIFeedPlugIn::IsDebugMode()) {
                $output .= $prefix . static::GetInvalidImage() . $suffix;
            }
        }
        $output = trim($output);
        if ($echo === true) {
            echo $output;
        }
        return $output;
    }
    public static function PropertyPrimaryImage($withLink = false, $link = null, $thumb = null)
    {
        $output = "";
        if ($thumb === null) {
            $thumb = false;
            $estateName = WP::GetOption("redi-feed-estate");
            if ($estateName !== null) {
                $state = new State($estateName);
                if ($state->IsForUs()) {
                    if ($state->IsPropertyIndex() || $state->IsFront()) {
                        $thumb = true;
                    }
                }
            }
        }
        $property = static::GetCurrentViewModel();
        if ($property !== null) {
            if ($property->GetImages() !== null) {
                $src = null;
                $img = null;
                if ($thumb) {
                    $img = $property->GetIndexImage();
                } else {
                    if (PHP::count($property->GetOverridedImages()) > 0) {
                        $img = $property->GetOverridedImages()[0];
                    }
                }
                if ($img !== null) {
                    $src = $thumb === false ? $img->GetUrl() : $img->GetThumb();
                    $caption = $img->GetCaption();
                    $output = "<img src=\"{$src}\" alt=\"{$caption}\" />";
                }
                if (PHP::isEmpty($img)) {
                    $output = static::PropertyPlanImages(null, false, false, 1);
                    if (strlen($output) === 0) {
                        $output = static::GetInvalidImage();
                    }
                }
                if ($withLink === true) {
                    if ($link === null) {
                        $link = $src;
                    }
                    $output = "<a href=\"{$link}\">{$output}</a>";
                }
            } else {
                //if (RedIFeedPlugIn::IsDebugMode()) {
                $output = static::GetInvalidImage();
                //}
            }
        } else {
            //if (RedIFeedPlugIn::IsDebugMode()) {
            $output = static::GetInvalidImage();
            //}
        }
        echo $output;
    }
    public static function PropertyImageCount()
    {
        $property = static::GetCurrentViewModel();
        if ($property !== null) {
            if (PHP::isCountable($property->GetImages())) {
                return count($property->GetImages());
            }
        }
        return 0;
    }
    public static function PropertyImages($element = null)
    {
        $prefix = "";
        $suffix = "";
        if ($element !== null) {
            $prefix = "<{$element}>";
            $suffix = "</{$element}>";
        }
        $images = [];
        $output = "";
        $property = static::GetCurrentViewModel();
        if ($property !== null) {
            if ($property->GetImages() !== null) {
                if (count($property->GetImages()) > 0) {
                    $imgs = [];
                    //
                    //var_dump($property->getAgencyOverrides());
                    $imgs = $property->GetImages();
                    foreach ($imgs as $img) {
                        $src = $img->GetUrl(false);
                        $thumb = $img->GetThumb(true);
                        $caption = $img->GetCaption();
                        if (PHP::strEndsWith(strtolower($src), '.png') || PHP::strEndsWith(strtolower($src), '.jpg') || PHP::strEndsWith(strtolower($src), '.gif')) {
                            $images[] = <<<HTML
{$prefix}
<a href="{$src}" class="with-caption" title="{$caption}">
    <span class="zoom">+</span>
    <img src="{$thumb}" alt="{$caption}" />
</a>
{$suffix}
HTML;
                            continue;
                        }
                        $images[] = <<<HTML
{$prefix}
<a href="{$src}" class="with-caption MPvideo" title="{$caption}">
    <span class="zoom">+</span>
    <!-- <iframe src="{$src}" class="mfp-iframe" frameborder="0" allowfullscreen="allowfullscreen" allow="fullscreen;vr"></iframe> -->
    <img src="{$thumb}" alt="{$caption}" />
</a>
{$suffix}
HTML;
                    }
                } else {
                    //if (!static::PropertyHasPlans()) {
                    $images[] = $prefix . static::GetInvalidImage() . $suffix;
                    //}
                }
            } else {
                //if (!static::PropertyHasPlans()) {
                $images[] = $prefix . static::GetInvalidImage() . $suffix;
                //}
            }
        } else {
            //if (!static::PropertyHasPlans()) {
            $images[] = $prefix . static::GetInvalidImage() . $suffix;
            //}
        }
        //        if ($property !== null) {
        //            if ($property->GetPlans() !== null) {
        //
        //                if (is_array($property->GetPlans()) && count($property->GetPlans()) > 0) {
        //
        //                    foreach ($property->GetPlans() as $plan) {
        //
        //                        foreach ($plan->GetImages() as $img) {
        //
        //                            $src = $img->GetUrl();
        //                            $caption = $img->GetCaption();
        //
        //                            $images[] = "$prefix<a href=\"$src\" class=\"with-caption\" title=\"$caption\"><span class=\"zoom\">+</span><img src=\"$src\" alt=\"$caption\" /></a>$suffix";
        //                        }
        //                    }
        //                }
        //            }
        //        }
        echo join("\n", $images);
        //            echo "<pre>";
        //            var_Dump($property->GetAgencyOverrides());
        //            echo "</pre>";
    }
    public static function PropertyThumbs($element = null)
    {
        $prefix = "";
        $suffix = "";
        if ($element !== null) {
            $prefix = "<{$element}>";
            $suffix = "</{$element}>";
        }
        $output = "";
        $property = static::GetCurrentViewModel();
        if ($property !== null) {
            if ($property->GetImages() !== null) {
                if (count($property->GetImages()) > 0) {
                    foreach ($property->GetImages() as $img) {
                        $prefix = "<{$element}>";
                        $src = $img->GetThumb();
                        $caption = $img->GetCaption();
                        if ($element !== null && $img->getType() !== 'image') {
                            $prefix = "<{$element} class=\"MPvideo\">";
                        }
                        $output .= "{$prefix}<img src=\"{$src}\" alt=\"{$caption}\" />{$suffix}";
                    }
                } else {
                    if (!static::PropertyHasPlans()) {
                        $output .= $prefix . static::GetInvalidImage() . $suffix;
                    }
                }
            } else {
                if (!static::PropertyHasPlans()) {
                    $output .= $prefix . static::GetInvalidImage() . $suffix;
                }
            }
        } else {
            if (!static::PropertyHasPlans()) {
                $output .= $prefix . static::GetInvalidImage() . $suffix;
            }
        }
        echo $output;
    }
    public static function NextPropertyPageLink()
    {
        $estateName = WP::GetOption("redi-feed-estate");
        if ($estateName !== null) {
            $state = new State($estateName);
            if ($state->IsForUs()) {
                if ($state->IsPropertyIndex() || $state->IsFront()) {
                    $nextPage = $state->GetPage() + 1;
                    if ($nextPage > static::GetTotalCount()) {
                        $nextPage = 1;
                    }
                    //if(WP::IsFrontPage()) {
                    //    echo State::BuildIndexLink($state, $nextPage, intval( WP::GetOption("redi-feed-front-page-items", 4)));
                    //} else {
                    echo State::BuildIndexLink($state, $nextPage, 0, true, $state->GetShow(), $state->GetOrderBy());
                    //}
                }
            }
        }
    }
    public static function PreviousPropertyPageLink()
    {
        $estateName = WP::GetOption("redi-feed-estate");
        if ($estateName !== null) {
            $state = new State($estateName);
            if ($state->IsForUs()) {
                if ($state->IsPropertyIndex()) {
                    $prevPage = $state->GetPage() - 1;
                    if ($prevPage <= 0) {
                        $prevPage = 1;
                    }
                    echo State::BuildIndexLink($state, $prevPage, 0, true, $state->GetShow(), $state->GetOrderBy());
                }
            }
        }
    }
    public static function HasNextPropertyPage()
    {
        $estateName = WP::GetOption("redi-feed-estate");
        if ($estateName !== null) {
            $state = new State($estateName);
            if ($state->IsForUs()) {
                if ($state->IsPropertyIndex()) {
                    $pageSize = intval(WP::GetOption("redi-feed-index-page-items", 9));
                    if ($pageSize === 0) {
                        return false;
                    }
                    $totalPages = ceil((double) static::GetTotalCount() / $pageSize);
                    if ($state->GetPage() < $totalPages) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    public static function HasPreviousPropertyPage()
    {
        $estateName = WP::GetOption("redi-feed-estate");
        if ($estateName !== null) {
            $state = new State($estateName);
            if ($state->IsForUs()) {
                if ($state->IsPropertyIndex()) {
                    if ($state->GetPage() > 1) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    public static function PropertyPageLink($pageNum = null)
    {
        // used by PropertyPages()
        $estateName = WP::GetOption("redi-feed-estate");
        if ($estateName !== null) {
            $state = new State($estateName);
            if ($state->IsForUs()) {
                if ($state->IsPropertyIndex()) {
                    if ($pageNum === null) {
                        $pageNum = $state->GetPage();
                    }
                    echo State::BuildIndexLink($state, $pageNum, 0, true, $state->GetShow(), $state->GetOrderBy());
                }
            }
        }
    }
    public static function PropertyPages()
    {
        /*
         <a href="#" class="current">1</a>
         <a href="#">2</a>
         <a href="#">3</a>
        */
        $estateName = WP::GetOption("redi-feed-estate");
        if ($estateName !== null) {
            $state = new State($estateName);
            if ($state->IsForUs()) {
                if ($state->IsPropertyIndex()) {
                    $pageSize = intval(WP::GetOption("redi-feed-index-page-items", 9));
                    $totalPages = ceil((double) static::GetTotalCount() / $pageSize);
                    $start = 1;
                    $end = $totalPages + 1;
                    if ($totalPages > 9) {
                        if ($state->GetPage() < 5) {
                            $start = 1;
                            $end = 10;
                        } else {
                            if ($state->GetPage() < $totalPages - 4) {
                                $start = $state->GetPage() - 4;
                                $end = $state->GetPage() + 5;
                            } else {
                                //$end = $state->GetPage() + 6;
                                $start = $totalPages - 9;
                                $end = $totalPages + 1;
                            }
                        }
                        if ($end > $totalPages) {
                            $end = $totalPages + 1;
                        }
                        if ($state->GetPage() >= 5) {
                            $start = $end - 9;
                        }
                    }
                    for ($i = $start; $i < $end; $i++) {
                        $current = "";
                        if ($i == $state->GetPage()) {
                            $current = " class=\"current\"";
                        }
                        echo "<a href=\"";
                        static::PropertyPageLink($i);
                        echo "\"{$current}>" . $i . "</a>";
                    }
                }
            }
        }
    }
    public static function PropertyRecordsCurrentPage($rangeTemplate = "{start} - {end}", $singleTemplate = "{value}")
    {
        // 21 - 40
        $estateName = WP::GetOption("redi-feed-estate");
        if ($estateName !== null) {
            $state = static::getInstance()->getState();
            if ($state->IsForUs()) {
                if ($state->IsPropertyIndex()) {
                    $pageSize = PHP::toInt(WP::GetOption("redi-feed-index-page-items", 9));
                    $output = "";
                    if (static::GetTotalCount() > 0) {
                        $start = ($state->GetPage() - 1) * $pageSize + 1;
                        $end = $start + $pageSize - 1;
                        if ($end > static::GetTotalCount()) {
                            $end = static::GetTotalCount();
                        }
                        if ($pageSize === 1 || $start === $end) {
                            $end = $start;
                            $output = $singleTemplate;
                            $output = str_replace("{value}", $end, $output);
                        } else {
                            $output = $rangeTemplate;
                            $output = str_replace("{start}", $start, $output);
                            $output = str_replace("{end}", $end, $output);
                        }
                    }
                    echo $output;
                }
            }
        }
    }
    public static function PropertyRecordsTotal()
    {
        echo static::GetTotalCount();
    }
    public static function HasDevelopment()
    {
        $estateName = WP::GetOption("redi-feed-estate");
        if ($estateName !== null) {
            $state = new State($estateName);
            if ($state->IsForUs()) {
                //return($state->GetDevelopment() === null);
                return true;
            }
        }
        return false;
    }
    private static function GetDevelopmentName($estateName, $in)
    {
        $name = null;
        foreach (static::GetDevelopments() as $dev) {
            if ($name === null && $dev->GetCode() === $in) {
                $name = $dev->GetLabel();
                break;
            }
        }
        if ($name === null) {
            foreach (static::GetDevelopments() as $dev) {
                if ($name === null && $dev->GetCode() === $estateName) {
                    $name = $dev->GetLabel();
                    break;
                }
            }
        }
        return $name;
    }
    public static function DevelopmentName($echo = true, $estateOnly = false)
    {
        //FIXME
        $estateName = WP::GetOption("redi-feed-estate");
        if ($estateName !== null) {
            $state = new State($estateName);
            if ($state->IsForUs()) {
                $title = null;
                if ($estateOnly) {
                    $title = static::GetDevelopmentName($estateName, $state->GetDevelopment());
                }
                if ($title === null) {
                    $title = static::GetCurrentViewModel() !== null && static::getCurrentViewModel()->getDevelopment() !== null ? static::getCurrentViewModel()->getDevelopment()->getLabel() : static::GetDevelopmentName($estateName, $state->GetDevelopment());
                }
                if (PHP::isEmpty($title)) {
                    $tmp = explode('-', $estateName);
                    foreach ($tmp as $word) {
                        $title .= ucfirst($word) . ' ';
                    }
                    $title = trim($title);
                }
                if ($echo === true) {
                    echo $title;
                }
                return $title;
            }
        }
        return null;
    }
    public static function DisplaySelection()
    {
        $estateName = WP::GetOption("redi-feed-estate");
        if ($estateName !== null) {
            $state = new State($estateName);
            if ($state->IsForUs()) {
                $listLink = State::BuildIndexLink($state, $state->GetPage(), 0, true, 1);
                $tileLink = State::BuildIndexLink($state, $state->GetPage(), 0, true, 2);
                $currentTiles = "";
                $currentList = "";
                //                echo "<!-- ";
                //                var_Dump($state->GetShow());
                //                echo "-->\n";
                if ($state->GetShow() === 1 || $state->GetShow() === null || $state->GetShow() === false) {
                    $currentList = " current";
                } else {
                    $currentTiles = " current";
                }
                $mapMarkup = '';
                if (static::HasPropertyMapLink()) {
                    $mapLink = static::PropertyMapLink(false);
                    $mapMarkup = '<a href="' . $mapLink . '" class="map" target="_blank"><i class="fa fa-fw fa-map-marker"></i></a>';
                }
                echo <<<TEMPLATE
        
        <div class="display">
            <a href="{$listLink}" class="list{$currentList}"><i class="fa fa-fw fa-reorder"></i></a>
            <a href="{$tileLink}" class="tiles{$currentTiles}"><i class="fa fa-fw fa-th"></i></a>
            {$mapMarkup}
        </div>
        
TEMPLATE;
            }
        }
    }
    public static function DisplayContactForm()
    {
        $estateName = WP::GetOption("redi-feed-estate");
        if ($estateName !== null) {
            $property = static::GetCurrentViewModel();
            if ($property !== null) {
                //                if(defined('WP_DEBUG')) {
                //
                //                    echo $property->CreateForm($property->getLabel(), $property->GetDevelopment()->GetCode());
                //                    return;
                //                }
                if (!PHP::isEmpty($property->GetContactForm())) {
                    echo $property->GetContactForm();
                    return;
                }
                if (!array_key_exists("__send_mail", $_POST)) {
                    echo $property->RenderForm($_SERVER['REQUEST_URI'], [new EnquiryFormField(['name' => '__send_mail', 'label' => null, 'value' => 'true', 'required' => true, 'type' => 'hidden']), new EnquiryFormField(['name' => 'label', 'label' => null, 'value' => $property->GetLabel(), 'required' => true, 'type' => 'hidden']), new EnquiryFormField(['name' => 'name', 'label' => 'Your name', 'required' => true, 'type' => 'text']), new EnquiryFormField(['name' => 'email', 'label' => 'Your email address', 'required' => true, 'type' => 'email'])], 'Contact');
                    return;
                }
                $label = $_POST['label'];
                $name = $_POST['name'];
                $email = $_POST['email'];
                $to = WP::getOption('redi-contact-email');
                $subject = 'General enquiry';
                $message = <<<MESSAGE

Hi!

There has been a general enquiry by '{$name}' on 
property '{$label}' for {$estateName}.

He/she can be reacehd at: {$email}                                

Kind regards

MESSAGE;
                wp_mail($to, $subject, $message);
                echo '<div class="email-form"><h6>Thanks ' . $name . ' for your enquiry!</h6></div>';
            }
        }
        return;
    }
    public static function DisplayPageTitle()
    {
        static::PageTitle(true);
    }
    // Renamed to cater for non-waterfall themes
    public static function PageTitle($echo = true)
    {
        $estateName = WP::GetOption("redi-feed-estate");
        ob_start();
        if ($estateName !== null) {
            $state = new State($estateName);
            if ($state->IsForUs()) {
                if ($state->IsPropertyIndex()) {
                    echo "Properties";
                    if ($state->GetDevelopment() !== null) {
                        echo " - " . static::DevelopmentName(false);
                    }
                    if ($state->IsPaged() === true && $state->GetPage() > 1) {
                        echo " (Page " . $state->GetPage() . ")";
                    }
                    echo ' | ';
                } else {
                    if ($state->IsFront()) {
                        echo "";
                    } else {
                        echo "Property " . ($state->GetLabel() !== null ? $state->GetLabel() : null);
                        echo ' | ';
                    }
                }
            } else {
                WP::title(true);
                echo ' | ';
            }
            bloginfo('name');
            static::$titleDisplayed = true;
        }
        $output = ob_get_clean();
        if ($echo === true) {
            echo $output;
        }
        return $output;
    }
    public static function BreadCrumbs()
    {
        //<a href="">Home</a> / <a href="">Waterfall Country Estate</a> / Houses for sale /
        $estateName = WP::GetOption("redi-feed-estate");
        if ($estateName !== null) {
            $state = new State($estateName);
            if ($state->IsForUs() && $state->IsProperty()) {
                $property = static::GetCurrentViewModel();
                if ($property !== null) {
                    $links['Home'] = '/';
                    if ($state->GetDevelopment() !== null) {
                        $links[static::GetDevelopmentName($estateName, $state->GetDevelopment())] = State::BuildIndexLink($state, 1, 0, false);
                    }
                    $links['Property'] = State::BuildSingleLink($state, $property->GetLabel());
                    $crumbs = [];
                    foreach ($links as $label => $link) {
                        $crumbs[] = "<a href=\"{$link}\">{$label}</a>";
                    }
                    echo join(' / ', $crumbs);
                }
            }
        }
    }
    public static function Favourite()
    {
    }
    public static function ShowTitle()
    {
        $showTitle = (bool) WP::getOption("redi-h1-titles", false);
        return $showTitle;
    }
    public static function themeHeader($echo = true, $include = null)
    {
        static::startDebugLogEntry('Theme Header');
        $header = PHP::obGet(function () use($include) {
            if ($include !== null) {
                include_once $include;
                return;
            }
            get_header();
        });
        if (WP::getOption('redi-override-title')) {
            $header = preg_replace('/<\\s*title\\s*>[^<]+<\\s*\\/\\s*title\\s*>/', '<title>' . static::PageTitle(false) . '</title>', $header);
        }
        if ($echo) {
            echo $header;
        }
        static::endDebugLogEntry('Theme Header');
        return $header;
    }
    public static function themeFooter($echo = true, $include = null)
    {
        $footer = PHP::obGet(function () use($include) {
            if ($include !== null) {
                include_once $include;
                return;
            }
            get_footer();
        });
        if ($echo) {
            echo $footer;
        }
        return $footer;
    }
}