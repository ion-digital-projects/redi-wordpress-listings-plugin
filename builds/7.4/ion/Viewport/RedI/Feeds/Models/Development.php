<?php
namespace ion\Viewport\RedI\Feeds\Models;

/**
 * Description of Development
 *
 * @author Justus
 */
use ion\Viewport\RedI\SalesMapFeed;
use ion\Viewport\RedI\Feeds\GalleryFeed;
use ion\Viewport\RedI\Feeds\PropertiesFeed;
use ion\Viewport\RedI\Feeds\PlanTypesFeed;
use ion\Viewport\RedI\Feeds\AgentsFeed;
use ion\Viewport\RedI\Feeds\AgencyFeed;
use ion\Viewport\RedI\Feeds\EnquiriesFeed;
use ion\Viewport\RedI\FeedSettings;
use ion\Viewport\RedI\Model;
use ion\Viewport\RedI\Feeds\EstateFeed;
use ion\Viewport\RedI\Feeds\Models\Property;
use ion\Viewport\RedI\Feeds\Models\PlanType;
use ion\Viewport\RedI\Feeds\Models\Image;
use ion\Viewport\RedI\Feeds\Models\Enquiries;
class Development extends Model
{
    private $estate;
    private $gallery;
    private $planTypes;
    private $properties;
    private $agents;
    private $agencies;
    private $name;
    private $enquiries;
    public function __construct(FeedSettings $feedSettings, $estateName, $developmentName, array $data = null, $fetchNow = false, $noFetch = false)
    {
        parent::__construct($data);
        $this->name = $developmentName;
        $this->estate = $estateName;
        if (!$noFetch) {
            $planTypesFeed = new PlanTypesFeed($feedSettings, $estateName, $developmentName);
            $agentsFeed = new AgentsFeed($feedSettings, $estateName, $developmentName);
            $galleryFeed = new GalleryFeed($feedSettings, $estateName, $developmentName);
            $enquiryFeed = new EnquiriesFeed($feedSettings, $this);
            $agencyFeed = new AgencyFeed($feedSettings, $estateName, $developmentName);
            $this->agents = $agentsFeed->Fetch($fetchNow);
            $this->agencies = $agencyFeed->Fetch($fetchNow);
            $this->planTypes = $planTypesFeed->Fetch($fetchNow);
            $this->gallery = $galleryFeed->Fetch($fetchNow);
            $this->enquiries = $enquiryFeed->Fetch($fetchNow);
            $propertiesFeed = new PropertiesFeed($feedSettings, $estateName, $developmentName, $this->planTypes, $this->agents, $this->agencies);
            $this->properties = $propertiesFeed->Fetch($fetchNow);
        }
    }
    public function GetGallery()
    {
        return $this->gallery;
    }
    public function GetPlanTypes()
    {
        return $this->planTypes;
    }
    public function GetProperties()
    {
        return $this->properties;
    }
    public function GetAgents()
    {
        return $this->agents;
    }
    public function GetAgencies()
    {
        return $this->agencies;
    }
    public function GetName()
    {
        return $this->name;
    }
    public function GetEstate()
    {
        return $this->estate;
    }
    public function GetEnquiries()
    {
        return $this->enquiries;
    }
    // string
    public function GetCode()
    {
        return $this->Get("code");
    }
    // string
    public function GetLabel()
    {
        return $this->Get("label");
    }
    // string
    public function GetDescriptionHeading()
    {
        return $this->Get("descriptionHeading");
    }
    // string
    public function GetDescriptionBody()
    {
        return $this->Get("descriptionBody");
    }
    // string
    public function GetSuburb()
    {
        return $this->Get("suburb");
    }
    // string
    public function GetCity()
    {
        return $this->Get("city");
    }
    // string
    public function GetProvince()
    {
        return $this->Get("province");
    }
    // string
    public function GetCountry()
    {
        return $this->Get("country");
    }
    // string
    public function GetLogoUrl()
    {
        return $this->Get("logoUrl");
    }
    // string
    public function GetCurrencyCode()
    {
        return $this->Get("currencyCode");
    }
    public function GetHidePrices()
    {
        return $this->Get("hidePrices");
    }
    public function GetHidePlanPrices()
    {
        return $this->Get("hidePlanPrices");
    }
}