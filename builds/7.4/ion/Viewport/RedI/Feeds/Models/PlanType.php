<?php
namespace ion\Viewport\RedI\Feeds\Models;

/**
 * Description of PlanTypes
 *
 * @author Justus
 */
use ion\Viewport\RedI\Model;
class PlanType extends Model
{
    public function __construct(array $data)
    {
        parent::__construct($data);
        $images = [];
        if (array_key_exists('media', $data)) {
            foreach ($data["media"] as $image) {
                $images[] = new Image($image);
            }
        }
        $this->Set("media", $images);
    }
    // string
    public function GetLabel()
    {
        return $this->Get("label");
    }
    // numeric
    public function GetPlanSize()
    {
        return $this->Get("planSize");
    }
    // numeric
    public function GetPlanPrice()
    {
        return $this->Get("planPrice");
    }
    // numeric
    public function GetBedrooms()
    {
        return $this->Get("bedrooms");
    }
    // numeric
    public function GetBathrooms()
    {
        return $this->Get("bathrooms");
    }
    // numeric
    public function GetGarages()
    {
        return $this->Get("garages");
    }
    // numeric
    public function GetParking()
    {
        return $this->Get("parking");
    }
    // boolean
    public function GetCustom()
    {
        return $this->Get("custom");
    }
    // array -> ?
    public function GetMedia()
    {
        return $this->Get("media");
    }
}