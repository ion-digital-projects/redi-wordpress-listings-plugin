<?php
namespace ion\Viewport\RedI\Feeds;

/**
 * Description of FeedPlanTypes
 *
 * @author Justus
 */
use ion\WordPress\WordPressHelper as WP;
use ion\WordPress\Helper\LogLevel;
use ion\PhpHelper as PHP;
use ion\Viewport\RedI\SalesMapFeed;
use ion\Viewport\RedI\Feed;
use ion\Viewport\RedI\FeedSettings;
use ion\Viewport\RedI\Feeds\Models\Enquiries;
use ion\Viewport\RedI\Feeds\Models\EnquiryFormField;
class EnquiryFormFieldsFeed extends Feed
{
    public function __construct(FeedSettings $feedSettings, $url, $fetchNow = false)
    {
        parent::__construct($feedSettings, $url);
    }
    protected function Process(array $json, $now = false)
    {
        $result = [];
        // Justus Meyer, 2021/7/5: Support for Property Engine
        if (array_key_exists('formConfig', $json)) {
            $json = $json['formConfig'];
        }
        //echo "<pre>"; var_dump($json); die("</pre>");
        foreach ($json as $jsonItem) {
            $result[] = new EnquiryFormField($jsonItem);
        }
        return $result;
    }
}