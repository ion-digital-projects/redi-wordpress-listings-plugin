<?php
namespace ion\Viewport\RedI;

/**
 * Description of FeedSettings
 *
 * @author Justus
 */
class FeedSettings
{
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public static function ApplyTemplate($template, array $tags)
    {
        foreach (array_keys($tags) as $tag) {
            $template = preg_replace("/{\\s*{$tag}\\s*}/", $tags[$tag] !== null ? $tags[$tag] : '', $template);
        }
        return $template;
    }
    private $delay;
    private $cacheDirectory;
    private $username;
    private $password;
    private $baseUri;
    private $uriTemplates;
    private $fakeFeed;
    private $import;
    private $db;
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct($baseUri, $delay, array $uriTemplates, $cacheDirectory = null, $fakeFeed = false, $username = null, $password = null, $import = false, $db = false)
    {
        $this->delay = $delay;
        $this->cacheDirectory = $cacheDirectory;
        $this->username = $username;
        $this->password = $password;
        $this->baseUri = $baseUri;
        $this->uriTemplates = $uriTemplates;
        $this->fakeFeed = $fakeFeed;
        $this->import = $import;
        $this->db = $db;
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function GetDelay()
    {
        return (int) $this->delay;
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function GetCacheDirectory()
    {
        return $this->cacheDirectory;
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function GetUsername()
    {
        return $this->username;
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function GetPassword()
    {
        return $this->password;
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function GetBaseUri()
    {
        return $this->baseUri;
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function GetUriTemplates()
    {
        return $this->uriTemplates;
    }
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function GetUriTemplate($name)
    {
        if (array_key_exists($name, $this->uriTemplates)) {
            return $this->uriTemplates[$name];
        }
        return null;
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function GetFakeFeed()
    {
        return $this->fakeFeed;
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function GetImport()
    {
        return $this->import;
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function GetDb()
    {
        return $this->db;
    }
}