<?php
namespace ion\Viewport\RedI;

/**
 * Description of RedIFeedFilterWidget
 *
 * @author Justus
 */
use ion\WordPress\WordPressHelper as WP;
use ion\WordPress\Helper\WordPressWidget;
use ion\Viewport\RedI\RedIFeedPlugIn as RedI;
class RedIFeedOrderingWidget extends RedIWidget
{
    /**
     * method
     * 
     * @return mixed
     */
    public function __construct()
    {
        parent::__construct("Red-I Ordering Widget");
    }
    /**
     * method
     * 
     * 
     * @return mixed
     */
    protected function RenderFrontEnd(array $values = null)
    {
        $view = WP::getContext('redi/redi-plugin')->getView("ordering-widget-front");
        $view();
    }
}