<?php
namespace ion\Viewport\RedI\Db\Models;

/**
 * Description of DbProperties
 *
 * @author Justus
 */
use ion\Viewport\RedI\Model;
use ion\Viewport\RedI\Db\DbModel;
use ion\WordPress\WordPressHelper as WP;
use ion\Viewport\RedI\ViewModels\PropertyViewModel;
use ion\PhpHelper as PHP;
class DbPropertyModel extends DbModel
{
    /**
     * method
     * 
     * @return mixed
     */
    protected static function GetDefaultTableName()
    {
        global $wpdb;
        return $wpdb->prefix . 'redi_properties';
    }
    /**
     * method
     * 
     * @return mixed
     */
    public static function GetSchema()
    {
        return [
            'import_id' => '%s',
            'development' => '%s',
            'label' => '%s',
            'title' => '%s',
            'subtitle' => '%s',
            'unitnumber' => '%d',
            //'price' => ['type' => 'decimal', 'null' => true],
            'maxprice' => '%f',
            'minprice' => '%f',
            'plotprice' => '%d',
            'rentalamount' => '%d',
            //'agentContactNumber' => ['type' => 'varchar(1000)', 'null' => true],
            'propertystatus' => '%s',
            'propertytype' => '%s',
            'listingCategory' => '%s',
            'phase' => '%s',
            'size' => '%d',
            'description' => '%s',
            'maplink' => '%s',
            'bathrooms' => '%d',
            'bedrooms' => '%d',
            'garages' => '%d',
            'parking' => '%d',
            //'plantypes' => '%s',
            'plans' => '%s',
            'images' => '%s',
            'forms' => '%s',
            'agencies' => '%s',
            'agency_lookup' => '%s',
            'propertyagents' => '%s',
            'showtimes' => '%s',
            'agencyoverrides' => '%s',
            'numimages' => '%d',
            'numplans' => '%d',
            'numplanimages' => '%d',
            'plan_required' => '%d',
            'selected_plan_type' => '%s',
            'time_imported' => '%s',
        ];
    }
    private $viewModel;
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct(PropertyViewModel $viewModel, $tableSuffix = null)
    {
        //var_dump($data);
        global $wpdb;
        parent::__construct($wpdb->prefix . 'redi_properties' . ($tableSuffix === null ? '' : '_' . $tableSuffix), $viewModel->ToArray(), 'id');
        $this->viewModel = $viewModel;
        //$this->Set('time_imported', );
        //var_dump($this->ToArray());
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function GetInsertSql()
    {
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function GetViewModel()
    {
        return $this->viewModel;
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function Insert()
    {
        $viewModel = $this->GetViewModel();
        $this->Set('import_id', md5($this->GetTableName() . '_' . $viewModel->GetDevelopment()->GetName() . '_' . $viewModel->GetLabel()));
        $this->Set('development', $viewModel->GetDevelopment()->GetName());
        $this->Set('label', $viewModel->GetLabel());
        $this->Set('title', $viewModel->GetTitle());
        $this->Set('subtitle', $viewModel->GetSubTitle());
        $this->Set('unitnumber', $viewModel->GetUnitNumber());
        $this->Set('maxprice', $viewModel->GetMaxPrice());
        $this->Set('minprice', $viewModel->GetMinPrice());
        $this->Set('plotprice', $viewModel->GetPlotPrice());
        $this->Set('rentalamount', $viewModel->GetRentalAmount());
        $this->Set('propertystatus', $viewModel->GetPropertyStatus());
        $this->Set('propertytype', $viewModel->GetPropertyType());
        $this->Set('listingCategory', $viewModel->GetListingCategory());
        $this->Set('phase', $viewModel->GetPhase());
        $this->Set('size', $viewModel->GetSize());
        $this->Set('description', $viewModel->GetDescription());
        $this->Set('maplink', $viewModel->GetMapLink());
        $this->Set('bathrooms', $viewModel->GetBathrooms());
        $this->Set('bedrooms', $viewModel->GetBedrooms());
        $this->Set('garages', $viewModel->GetGarages());
        $this->Set('parking', $viewModel->GetParking());
        $this->Set('forms', base64_encode($viewModel->GetContactForm()));
        //$this->Set('plantypes', base64_encode(serialize($viewModel->GetPlanTypes())));
        $this->Set('agencies', base64_encode(serialize($viewModel->GetAgencies())));
        $agencies = [];
        if (PHP::isArray($viewModel->GetAgencies())) {
            foreach ($viewModel->GetAgencies() as $agency) {
                $agencies[] = $agency->getName();
            }
        }
        $this->Set('agency_lookup', PHP::count($agencies) === 0 ? null : implode('; ', $agencies));
        $this->Set('plans', base64_encode(serialize($viewModel->GetPlans())));
        $this->Set('images', base64_encode(serialize($viewModel->GetImages())));
        $this->Set('propertyagents', base64_encode(serialize($viewModel->GetAgents())));
        $this->Set('showtimes', base64_encode(serialize($viewModel->GetShowTimes())));
        $this->Set('agencyoverrides', base64_encode(serialize($viewModel->GetAgencyOverrides())));
        $cnt = 0;
        $this->Set('numplans', null);
        $this->Set('numimages', null);
        if (PHP::isArray($viewModel->GetPlans())) {
            if (count($viewModel->GetPlans()) > 0) {
                foreach ($viewModel->GetPlans() as $plan) {
                    foreach ($plan->GetImages() as $img) {
                        $cnt++;
                    }
                }
            }
            $this->Set('numplans', count($viewModel->GetPlans()));
        }
        if (PHP::isArray($viewModel->GetImages())) {
            $this->Set('numimages', count($viewModel->GetImages()));
        }
        $this->Set('selected_plan_type', $viewModel->GetSelectedPlanType());
        $this->Set('numplanimages', $cnt);
        $this->Set('time_imported', date('Y-m-d H:i:s'));
        return parent::Insert();
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function GetId()
    {
        return $this->Get('id');
    }
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function SetId($id)
    {
        $this->Set('id', $id);
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function GetImportId()
    {
        //return $this->Get('import_id');
        return (string) md5($this->GetTableName() . '_' . $this->GetDevelopment() . ($this->GetLabel() !== null ? '_' . $this->GetLabel() : ""));
    }
    //    public function SetImportId(/* int */ $importId) {
    //        $this->Set('import_id', $importId);
    //    }
    /**
     * method
     * 
     * @return mixed
     */
    public function GetDevelopment()
    {
        return $this->Get('development');
    }
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function SetDevelopment($development)
    {
        $this->Set('development', $development);
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function GetLabel()
    {
        return $this->Get('label');
    }
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function SetLabel($label)
    {
        $this->Set('label', $label);
    }
}