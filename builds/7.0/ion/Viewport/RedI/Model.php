<?php
namespace ion\Viewport\RedI;

/**
 * Description of FeedObject
 *
 * @author Justus
 */
use ion\WordPress\WordPressHelper as WP;
abstract class Model
{
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public static function Deserialize($data)
    {
        return deserialize($data);
    }
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public static function FindIndex($field, $value, array $models)
    {
        $found = false;
        $i = 0;
        foreach ($models as $item) {
            if ($item->Get($field) === $value) {
                $found = true;
                break;
            }
            $i++;
        }
        if ($found !== true) {
            return -1;
        }
        return $i;
    }
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public static function Find($field, $value, array $models)
    {
        $result = null;
        $index = static::FindIndex($field, $value, $models);
        if ($index === -1) {
            return null;
        }
        return $models[$index];
    }
    private $data;
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct(array &$data = null)
    {
        $this->data = $data;
    }
    /**
     * method
     * 
     * 
     * @return mixed
     */
    protected function Set($propertyName, $value = null)
    {
        if ($this->data === null) {
            $this->data = [];
        }
        $this->data[$propertyName] = $value;
        return $value;
    }
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function Get($propertyName)
    {
        if ($this->data !== null) {
            if (array_key_exists($propertyName, $this->data)) {
                return $this->data[$propertyName];
            }
        }
        return null;
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function Serialize()
    {
        return serialize($this->ToArray());
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function &ToArray()
    {
        return $this->data;
    }
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function IsPropertyEqualTo($propertyName, $expectedValue = null)
    {
        return $this->Get($propertyName) == $expectedValue;
        // Note: only two == - NOT exact matching
    }
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function IsPropertyGreaterThan($propertyName, $expectedValue = null)
    {
        if ($expectedValue === null) {
            return false;
        }
        return $this->Get($propertyName) > $expectedValue;
    }
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function IsPropertyGreaterThanOrEqualTo($propertyName, $expectedValue = null)
    {
        if ($expectedValue === null) {
            return false;
        }
        return $this->Get($propertyName) >= $expectedValue;
    }
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function IsPropertyLessThan($propertyName, $expectedValue = null)
    {
        if ($expectedValue === null) {
            return false;
        }
        return $this->Get($propertyName) < $expectedValue;
    }
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function IsPropertyLessThanOrEqualTo($propertyName, $expectedValue = null)
    {
        if ($expectedValue === null) {
            return false;
        }
        return $this->Get($propertyName) <= $expectedValue;
    }
}