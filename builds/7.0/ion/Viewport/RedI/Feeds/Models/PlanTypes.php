<?php
namespace ion\Viewport\RedI\Feeds\Models;

/**
 * Description of Gallery
 *
 * @author Justus
 */
use ion\Viewport\RedI\Model;
use ion\Viewport\RedI\Feeds\Models\PlanType;
use ion\Viewport\RedI\FeedSettings;
class PlanTypes extends Model
{
    private $items;
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct(FeedSettings $feedSettings, array $data, $fetchNow = false)
    {
        parent::__construct($data);
        $this->items = [];
        foreach ($data as $obj) {
            $this->items[] = new PlanType($obj);
        }
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function GetItems()
    {
        return $this->items;
    }
}