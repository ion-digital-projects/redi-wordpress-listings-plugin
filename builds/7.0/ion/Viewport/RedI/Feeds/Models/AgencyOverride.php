<?php
namespace ion\Viewport\RedI\Feeds\Models;

/**
 * Description of Agent
 *
 * @author Justus
 */
use ion\Viewport\RedI\Model;
class AgencyOverride extends Model
{
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct(array $data)
    {
        parent::__construct($data);
        $images = [];
        foreach ($data["media"] as $image) {
            $images[] = new Image($image);
        }
        $this->Set("media", $images);
    }
    // string
    /**
     * method
     * 
     * @return mixed
     */
    public function GetAgency()
    {
        return $this->Get("agency");
    }
    // string
    /**
     * method
     * 
     * @return mixed
     */
    public function GetDescription()
    {
        return $this->Get("description");
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function GetMedia()
    {
        return $this->Get("media");
    }
}