<?php
namespace ion\Viewport\RedI\Feeds\Models;

/**
 * Description of Enquiry
 *
 * @author Justus
 */
use ion\Viewport\RedI\Model;
class EnquiryFormField extends Model
{
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct(array $data)
    {
        parent::__construct($data);
    }
    // string
    /**
     * method
     * 
     * @return mixed
     */
    public function GetName()
    {
        return $this->Get("name");
    }
    // string
    /**
     * method
     * 
     * @return mixed
     */
    public function GetLabel()
    {
        return $this->Get("label");
    }
    // string
    /**
     * method
     * 
     * @return mixed
     */
    public function GetType()
    {
        return $this->Get("type");
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function GetUrl()
    {
        return $this->Get("url");
    }
    // string
    /**
     * method
     * 
     * @return mixed
     */
    public function GetValue()
    {
        return $this->Get("value");
    }
    // int
    /**
     * method
     * 
     * @return mixed
     */
    public function GetMaxLength()
    {
        return $this->Get("maxLength");
    }
    // bool
    /**
     * method
     * 
     * @return mixed
     */
    public function GetRequired()
    {
        return $this->Get("required");
    }
    // bool
    /**
     * method
     * 
     * @return mixed
     */
    public function GetChecked()
    {
        return $this->Get("checked");
    }
    // array
    /**
     * method
     * 
     * @return mixed
     */
    public function GetOptions()
    {
        return $this->Get("options");
    }
}