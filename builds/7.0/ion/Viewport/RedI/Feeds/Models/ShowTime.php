<?php
namespace ion\Viewport\RedI\Feeds\Models;

/**
 * Description of ShowTime
 *
 * @author Justus
 */
use ion\Viewport\RedI\Model;
class ShowTime extends Model
{
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct(array $data)
    {
        parent::__construct($data);
    }
    // string
    /**
     * method
     * 
     * @return mixed
     */
    public function GetAgency()
    {
        return $this->Get("agency");
    }
    // string
    /**
     * method
     * 
     * @return mixed
     */
    public function GetAgent()
    {
        return $this->Get("agent");
    }
    // int
    /**
     * method
     * 
     * @return mixed
     */
    public function GetStartTime()
    {
        return $this->Get("startTime");
    }
    // int
    /**
     * method
     * 
     * @return mixed
     */
    public function GetEndTime()
    {
        return $this->Get("endTime");
    }
}