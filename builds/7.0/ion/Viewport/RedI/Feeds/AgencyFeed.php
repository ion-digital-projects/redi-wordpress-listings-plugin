<?php
namespace ion\Viewport\RedI\Feeds;

/**
 * Description of FeedPlanTypes
 *
 * @author Justus
 */
use ion\Viewport\RedI\SalesMapFeed;
use ion\Viewport\RedI\FeedSettings;
use ion\Viewport\RedI\Feeds\Models\Agents;
use ion\Viewport\RedI\Feeds\Models\Agencies;
class AgencyFeed extends SalesMapFeed
{
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct(FeedSettings $feedSettings, $estateName, $developmentName, $fetchNow = false)
    {
        parent::__construct($feedSettings, "agencies", ["estate" => $estateName, "development" => $developmentName]);
    }
    /**
     * method
     * 
     * 
     * @return mixed
     */
    protected function Process(array $json, $now = false)
    {
        return new Agencies($this->GetSettings(), $json);
    }
}