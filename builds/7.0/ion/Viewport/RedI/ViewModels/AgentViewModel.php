<?php
namespace ion\Viewport\RedI\ViewModels;

/**
 * Description of AgentViewModel
 *
 * @author Justus
 *  */
use ion\Viewport\RedI\ViewModel;
use ion\Viewport\RedI\Feeds\Models\Agent;
use ion\Viewport\RedI\Feeds\Models\Agencies;
use ion\PhpHelper as PHP;
class AgentViewModel extends ViewModel
{
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct(Agent $agent, Agencies $agencies = null)
    {
        parent::__construct();
        $this->Set("firstName", $agent->GetFirstName());
        $this->Set("lastName", $agent->GetLastName());
        $this->Set("emailAddress", $agent->GetEmailAddress());
        $this->Set("mobileNumber", $agent->GetMobileNumber());
        $this->Set("photoUrl", $agent->GetPhotoUrl());
        $this->Set("agencyName", null);
        if (PHP::count($agent->GetAgencies()) > 0) {
            $this->Set("agencyName", $agent->GetAgencies()[0]);
        }
        $this->Set("agencies", $agent->GetAgencies());
    }
    // string
    /**
     * method
     * 
     * @return mixed
     */
    public function GetFirstName()
    {
        return $this->Get("firstName");
    }
    // string
    /**
     * method
     * 
     * @return mixed
     */
    public function GetLastName()
    {
        return $this->Get("lastName");
    }
    // string
    /**
     * method
     * 
     * @return mixed
     */
    public function GetEmailAddress()
    {
        return $this->Get("emailAddress");
    }
    // string
    /**
     * method
     * 
     * @return mixed
     */
    public function GetMobileNumber()
    {
        return $this->Get("mobileNumber");
    }
    // string
    /**
     * method
     * 
     * @return mixed
     */
    public function GetPhotoUrl()
    {
        return $this->Get("photoUrl");
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function GetAgencies()
    {
        return $this->Get("agencies");
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function GetAgencyName()
    {
        return $this->Get("agencyName");
    }
}