<?php
namespace ion\Types;

// This is a polyfill for the actual ion\Types\StringObject class, to fix
// Waterfall
use ion\PhpHelper as PHP;
class StringObject
{
    private $string;
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public static function create($string = null)
    {
        return new static($string);
    }
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct($string = null)
    {
        $this->string = $string;
    }
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function replace($search, $replacement, $recursive = false)
    {
        return static::create(str_replace($search, $replacement, $this->string));
    }
    /**
     * method
     * 
     * 
     * @return mixed
     */
    function stripWhiteSpace($replacement = ' ')
    {
        return static::create(PHP::strStripWhiteSpace($this->string, $replacement));
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function __toString()
    {
        return (string) $this->string;
    }
}