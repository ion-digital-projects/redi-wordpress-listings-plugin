<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Viewport\RedI\Db\Models;

/**
 * Description of DbTempDevelopmentModel
 *
 * @author Justus
 */
class DbTempDevelopmentModel extends DbDevelopmentModel
{
    /**
     * method
     * 
     * @return mixed
     */
    protected static function GetDefaultTableName()
    {
        return parent::GetDefaultTableName() . '_import';
    }
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct($development)
    {
        parent::__construct($development, 'import');
    }
}