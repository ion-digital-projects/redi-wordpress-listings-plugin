<?php
namespace ion\Viewport\RedI\Db\Models;

/**
 * Description of DbProperties
 *
 * @author Justus
 */
use ion\Viewport\RedI\Model;
use ion\Viewport\RedI\Db\DbModel;
use ion\WordPress\WordPressHelper as WP;
use ion\Viewport\RedI\ViewModels\PropertyViewModel;
class DbBatchModel extends DbModel
{
    /**
     * method
     * 
     * @return mixed
     */
    public static function GetSchema()
    {
        return ['import_id' => '%s', 'development' => '%s', 'label' => '%s', 'import_start_time' => '%s', 'import_end_time' => '%s', 'errors' => '%s'];
    }
    private $development;
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct($development)
    {
        //var_dump($data);
        global $wpdb;
        parent::__construct($wpdb->prefix . 'redi_developments', $development->ToArray(), 'id');
        $this->development = $development;
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function GetDevelopmentModel()
    {
        return $this->development;
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function Insert()
    {
        $this->Set('import_id', md5($this->GetTableName() . '_' . $this->GetDevelopmentModel()->GetName()));
        $this->Set('development', $this->GetDevelopmentModel()->GetName());
        $this->Set('label', $this->GetDevelopmentModel()->GetLabel());
        $this->Set('import_start_time', date('Y-m-d H:i:s'));
        $this->Set('import_end_time', null);
        $this->Set('errors', null);
        $tableName = static::GetTableName();
        $parms = [];
        $values = [];
        foreach (static::GetSchema() as $field => $parm) {
            //$parms[] = ($value === null ? 'NULL' : (array_key_exists($field, $schema) ? $schema[$field] : '%s') );
            if (array_key_exists(strtolower($field), array_change_key_case($this->ToArray(), CASE_LOWER))) {
                $val = $this->ToArray()[$field];
                if ($val === null) {
                    $parms[] = 'NULL';
                    $values[] = 'NULL';
                } else {
                    $parms[] = $parm;
                    if ($parm == '%s') {
                        $values[] = "'{$val}'";
                    } else {
                        $values[] = $val;
                    }
                }
            } else {
                //$parms[] = 'NULL';
                //$values[] = null;
            }
        }
        return join(',', $values);
        //        $sql = <<<SQL
        //INSERT INTO $tableName (
        //    $columns
        //) VALUES (
        //    $placeholders
        //);
        //SQL;
        //
        //
        //        WP::DbQuery($sql, $values);
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function GetId()
    {
        return $this->Get('id');
    }
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function SetId($id)
    {
        $this->Set('id', $id);
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function GetImportId()
    {
        //return $this->Get('import_id');
        return (string) md5($this->GetTableName() . '_' . $this->GetDevelopment() . ($this->GetLabel() !== null ? '_' . $this->GetLabel() : ""));
    }
    //    public function SetImportId(/* int */ $importId) {
    //        $this->Set('import_id', $importId);
    //    }
    /**
     * method
     * 
     * @return mixed
     */
    public function GetDevelopment()
    {
        return $this->Get('development');
    }
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function SetDevelopment($development)
    {
        $this->Set('development', $development);
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function GetLabel()
    {
        return $this->Get('label');
    }
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function SetLabel($label)
    {
        $this->Set('label', $label);
    }
}