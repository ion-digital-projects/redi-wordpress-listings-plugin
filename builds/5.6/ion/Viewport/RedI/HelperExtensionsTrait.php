<?php
namespace ion\Viewport\RedI;

use ion\PhpHelper as PHP;
use ion\WordPress\WordPressHelper as WP;
use ion\PhpHelperException;
use ion\WordPress\Helper\Wrappers\OptionMetaType;
trait HelperExtensionsTrait
{
    /**
     * method
     * 
     * @return void
     */
    private static function extendHelper()
    {
        WP::extend("removeOption", function ($key, $id = null, OptionMetaType $type = null) {
            if ($id === null) {
                return delete_option($key);
            }
            if ($type === null) {
                $type = new OptionMetaType(OptionMetaType::POST);
            }
            switch ($type->toValue()) {
                case OptionMetaType::TERM:
                    return delete_term_meta($id, $key);
                case OptionMetaType::USER:
                    return delete_user_meta($id, $key);
                case OptionMetaType::POST:
                    return delete_post_meta($id, $key);
                case OptionMetaType::COMMENT:
                    return delete_comment_meta($id, $key);
            }
        });
        WP::extend("hasOption", function ($key, $id = null, OptionMetaType $type = null) {
            if ($id === null) {
                return WP::hasSiteOption($key);
            }
            if ($type === null) {
                $type = new OptionMetaType(OptionMetaType::POST);
            }
            switch ($type->toValue()) {
                case OptionMetaType::TERM:
                    return WP::hasTermOption($key, $id);
                case OptionMetaType::USER:
                    return WP::hasUserOption($key, $id);
                case OptionMetaType::COMMENT:
                    return WP::hasCommentOption($key, $id);
                case OptionMetaType::POST:
                    return WP::hasPostOption($key, $id);
            }
            return WP::hasSiteOption($key);
        });
        WP::extend("setOption", function ($key, $value = null, $id = null, OptionMetaType $type = null, $raw = false, $autoLoad = false) {
            if ($raw === false) {
                $value = PHP::serialize($value);
            } else {
                $value = $value === null ? '' : $value;
            }
            //($id === null ? update_option($key, $value, $autoLoad) : ($term ?  : ));
            if ($id === null) {
                return update_option($key, $value, $autoLoad);
            } else {
                if ($type === null) {
                    $type = new OptionMetaType(OptionMetaType::POST);
                }
                switch ($type->toValue()) {
                    case OptionMetaType::TERM:
                        return update_term_meta($id, $key, $value);
                    case OptionMetaType::USER:
                        return update_user_meta($id, $key, $value);
                    case OptionMetaType::COMMENT:
                        return update_comment_meta($id, $key, $value);
                    case OptionMetaType::POST:
                        return update_post_meta($id, $key, $value);
                }
            }
            return false;
        });
        WP::extend("getOption", function ($key, $default = null, $id = null, OptionMetaType $type = null, $raw = false) {
            if (WP::hasOption($key, $id, $type) === false) {
                return $default;
            }
            $value = null;
            //        var_dump($type);
            if ($id === null) {
                $value = get_option($key, null);
            } else {
                if ($type === null) {
                    $type = new OptionMetaType(OptionMetaType::POST);
                }
                //            var_dump($type->toValue());
                switch ($type->toValue()) {
                    case OptionMetaType::TERM:
                        $value = get_term_meta($id, $key, true);
                        break;
                    case OptionMetaType::USER:
                        $value = get_user_meta($id, $key, true);
                        break;
                    case OptionMetaType::COMMENT:
                        $value = get_comment_meta($id, $key, true);
                        break;
                    case OptionMetaType::POST:
                        $value = get_post_meta($id, $key, true);
                        break;
                }
            }
            if ($value === null || $value !== null && $value === '') {
                return $default;
            }
            if ($raw === true) {
                return $value;
            }
            $tmp = null;
            try {
                $tmp = PHP::unserialize($value);
            } catch (PhpHelperException $ex) {
                $tmp = $value;
            }
            return $tmp;
        });
        WP::extend("getRawOption", function ($key, $default = null, $id = null, OptionMetaType $type = null) {
            return WP::getOption($key, $default, $id, $type, true);
        });
        WP::extend("setRawOption", function ($key, $value = null, $id = null, OptionMetaType $type = null, $autoLoad = false) {
            return WP::setOption($key, $value, $id, $type, true, $autoLoad);
        });
        //        WP::extend("getSiteOption", function(string $name, /* mixed */ $default = null) {
        //
        //            return WP::getOption($name, $default, null, null, false);
        //        });
        //
        //        WP::extend("setSiteOption", function(string $name, /* mixed */ $value = null, bool $autoLoad = false) {
        //
        //            return WP::setOption($name, $value, null, null, false, $autoLoad);
        //        });
        WP::extend("addRediAdminForm", function ($name, $slug) {
            $form = WP::addAdminForm($name, $slug);
            $form->onRead(function ($record = null, $key = null, $id = null, $type = null, array $keys = []) {
                $data = [];
                foreach ($keys as $key) {
                    $data[$key] = WP::getOption($key, null);
                }
                return $data;
            })->onUpdate(function ($index, $newValues, $oldValues, $key = null, $metaId = null, $type = null) {
                foreach ($newValues as $key => $value) {
                    WP::setOption($key, $value);
                }
            });
            return $form;
        });
    }
    /**
     * method
     * 
     * @return void
     */
    private static function extendHelperContext()
    {
        $context = WP::getContext("redi/redi-plugin");
        $context->extend("getViewDirectory", function () use($context) {
            $dirs = ['views/', 'source/views/', 'includes/views/'];
            foreach ($dirs as $subDir) {
                $dir = $context->getWorkingDirectory() . $subDir;
                if (is_dir($dir)) {
                    return $dir;
                }
            }
            return $context->getWorkingDirectory();
        });
        $context->extend("getView", function ($viewSlug) use($context) {
            return function () use($viewSlug, $context) {
                $path = $context->getViewDirectory() . $viewSlug . ".php";
                if (file_exists($path)) {
                    // Load the PHP view and strip the PHP tags before
                    $view = file_get_contents($path);
                    $view = preg_replace("/^(<\\s*\\?(\\s*php)?\\s+)/", "", $view);
                    $view = preg_replace("/(\\s*\\?>\\s*)\$/", "", $view);
                    //FIXME: Use includes here instead
                    ob_start();
                    eval($view);
                    echo ob_get_clean();
                }
            };
        });
        $context->extend("template", function ($name, $echo = false) use($context) {
            if (substr_compare($name, '.php', -strlen('.php')) !== 0) {
                $name = $name . '.php';
            }
            ob_start();
            if ($overriddenTemplate = locate_template($name)) {
                load_template($overriddenTemplate);
            } else {
                load_template($context->getWorkingDirectory() . "templates/{$name}");
            }
            $output = ob_get_clean();
            if ($echo === true) {
                http_response_code(200);
                echo $output;
            }
            return $output;
        });
    }
}