<?php
namespace ion\Viewport\RedI\ViewModels;

/**
 * Description of AgentViewModel
 *
 * @author Justus
 *  */
use ion\Viewport\RedI\ViewModel;
use ion\Viewport\RedI\Feeds\Models\Agency;
class AgencyViewModel extends ViewModel
{
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct(Agency $agency)
    {
        parent::__construct();
        $this->Set("name", $agency->GetName());
        $this->Set("description", $agency->GetDescription());
        $this->Set("emailAddress", $agency->GetEmailAddress());
        $this->Set("officeNumber", $agency->GetOfficeNumber());
        $this->Set("photoUrl", $agency->GetPhotoUrl());
    }
    // string
    /**
     * method
     * 
     * @return mixed
     */
    public function GetName()
    {
        return $this->Get("name");
    }
    // string
    /**
     * method
     * 
     * @return mixed
     */
    public function GetDescription()
    {
        return $this->Get("description");
    }
    // string
    /**
     * method
     * 
     * @return mixed
     */
    public function GetEmailAddress()
    {
        return $this->Get("emailAddress");
    }
    // string
    /**
     * method
     * 
     * @return mixed
     */
    public function GetOfficeNumber()
    {
        return $this->Get("officeNumber");
    }
    // string
    /**
     * method
     * 
     * @return mixed
     */
    public function GetPhotoUrl()
    {
        return $this->Get("photoUrl");
    }
}