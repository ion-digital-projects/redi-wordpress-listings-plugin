<?php
namespace ion\Viewport\RedI\Models;

/**
 * Description of StoredFilterModel
 *
 * @author Justus
 */
use ion\Viewport\RedI\Model;
use ion\WordPress\WordPressHelper as WP;
use ion\Viewport\RedI\Db\Models\DbPropertyModel;
use ion\PhpHelper as PHP;
class StoredFilterModel extends Model
{
    protected static $statusMap = null;
    protected static $typeMap = null;
    protected static $phaseMap = null;
    protected static $agencyMap = null;
    protected static $listingCategoryMap = null;
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public static function GetWidgetStatusArray($storedFilter)
    {
        $propertyStatusMap = static::getPropertyStatusMap($storedFilter);
        $tmp = [];
        foreach (array_values($propertyStatusMap) as $value) {
            if ($value['enabled'] === true) {
                $tmp[$value['dbValue']] = $value['label'];
            }
        }
        return $tmp;
    }
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public static function GetWidgetTypeArray($storedFilter)
    {
        $propertyTypeMap = static::getPropertyTypeMap($storedFilter);
        $tmp = [];
        foreach (array_values($propertyTypeMap) as $value) {
            if ($value['enabled'] === true) {
                $tmp[$value['dbValue']] = $value['label'];
            }
        }
        return $tmp;
    }
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public static function GetWidgetPhaseArray($storedFilter)
    {
        $propertyPhaseMap = static::getPropertyPhaseMap($storedFilter);
        $tmp = [];
        foreach (array_values($propertyPhaseMap) as $value) {
            if ($value['enabled'] === true) {
                $tmp[$value['dbValue']] = $value['label'];
            }
        }
        return $tmp;
    }
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public static function GetDevelopmentStatusArray($storedFilter)
    {
        if ($storedFilter === null) {
            return null;
        }
        return $storedFilter->GetDevelopments();
    }
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct(array $data)
    {
        parent::__construct($data);
    }
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public static function getPropertyStatusMap($storedFilter = null)
    {
        if (static::$statusMap !== null) {
            return static::$statusMap;
        }
        static::$statusMap = [];
        $result = WP::dbQuery('SELECT DISTINCT `propertystatus` FROM `' . DbPropertyModel::GetTableName() . '`;');
        if (!empty($result) && count($result) > 0) {
            foreach ($result as $item) {
                if ($item['propertystatus'] !== null) {
                    static::$statusMap[WP::slugify($item['propertystatus'])] = ['dbValue' => $item['propertystatus'], 'label' => $item['propertystatus'], 'enabled' => $storedFilter === null ? true : $storedFilter->Get(WP::slugify($item['propertystatus']))];
                }
            }
        }
        return static::$statusMap;
    }
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public static function getPropertyTypeMap($storedFilter = null)
    {
        if (static::$typeMap !== null) {
            return static::$typeMap;
        }
        static::$typeMap = [];
        $result = WP::dbQuery('SELECT DISTINCT `propertytype` FROM `' . DbPropertyModel::GetTableName() . '`;');
        if (!empty($result) && count($result) > 0) {
            foreach ($result as $item) {
                if ($item['propertytype'] !== null) {
                    static::$typeMap[WP::slugify($item['propertytype'])] = ['dbValue' => $item['propertytype'], 'label' => $item['propertytype'], 'enabled' => $storedFilter === null ? true : $storedFilter->Get(WP::slugify($item['propertytype']))];
                }
            }
        }
        return static::$typeMap;
    }
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public static function getPropertyPhaseMap($storedFilter = null)
    {
        if (static::$phaseMap !== null) {
            return static::$phaseMap;
        }
        static::$phaseMap = [];
        $result = WP::dbQuery('SELECT DISTINCT `phase` FROM `' . DbPropertyModel::GetTableName() . '`;');
        if (!empty($result) && count($result) > 0) {
            foreach ($result as $item) {
                if ($item['phase'] !== null) {
                    static::$phaseMap[WP::slugify($item['phase'])] = ['dbValue' => $item['phase'], 'label' => $item['phase'], 'enabled' => $storedFilter === null ? true : $storedFilter->Get(WP::slugify($item['phase']))];
                }
            }
        }
        return static::$phaseMap;
    }
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public static function getPropertyListingCategoryMap($storedFilter = null)
    {
        if (static::$listingCategoryMap !== null) {
            return static::$listingCategoryMap;
        }
        static::$listingCategoryMap = [];
        $result = WP::dbQuery('SELECT DISTINCT `listingCategory` FROM `' . DbPropertyModel::GetTableName() . '`;');
        if (!empty($result) && count($result) > 0) {
            foreach ($result as $item) {
                if ($item['listingCategory'] !== null) {
                    static::$listingCategoryMap[WP::slugify($item['listingCategory'])] = ['dbValue' => $item['listingCategory'], 'label' => $item['listingCategory'], 'enabled' => $storedFilter === null ? true : $storedFilter->Get(WP::slugify($item['listingCategory']))];
                }
            }
        }
        return static::$listingCategoryMap;
    }
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public static function getPropertyAgencyMap($storedFilter = null)
    {
        if (static::$agencyMap !== null) {
            return static::$agencyMap;
        }
        static::$agencyMap = [];
        $result = WP::dbQuery('SELECT DISTINCT `agencies` FROM `' . DbPropertyModel::GetTableName() . '`;');
        if (!empty($result) && count($result) > 0) {
            foreach ($result as $item) {
                if ($item['agencies'] !== null) {
                    $arr = unserialize(base64_decode($item['agencies']));
                    if (is_array($arr) && count($arr) > 0) {
                        foreach ($arr as $obj) {
                            if ($obj !== null && !array_key_exists($obj->getName(), static::$agencyMap)) {
                                static::$agencyMap[$obj->getName()] = ['dbValue' => $obj->getName(), 'label' => $obj->getName(), 'enabled' => true];
                                //                                static::$statusMap[WP::slugify($item['propertystatus'])] = [
                                //                                    'dbValue' => $item['propertystatus'],
                                //                                    'label' => $item['propertystatus'],
                                //                                    'enabled' => ($storedFilter === null ? true : $storedFilter->Get(WP::slugify($item['propertystatus'])))
                                //                                ];
                            }
                        }
                    }
                }
            }
        }
        return static::$agencyMap;
    }
    // string
    /**
     * method
     * 
     * @return mixed
     */
    public function getSlug()
    {
        return $this->Get("slug");
    }
    // string
    /**
     * method
     * 
     * @return mixed
     */
    public function getDescription()
    {
        return $this->Get("description");
    }
    // bool
    /**
     * method
     * 
     * @return mixed
     */
    public function getImagesOnly()
    {
        return $this->get('images-only');
    }
    // bool
    /**
     * method
     * 
     * @return mixed
     */
    public function getRented()
    {
        return $this->Get("rented");
    }
    // bool
    /**
     * method
     * 
     * @return mixed
     */
    public function getAvailableForRental()
    {
        return $this->Get("available-for-rental");
    }
    // bool
    /**
     * method
     * 
     * @return mixed
     */
    public function getSold()
    {
        return $this->Get("sold");
    }
    // bool
    /**
     * method
     * 
     * @return mixed
     */
    public function getAvailableForResale()
    {
        return $this->Get("available-for-resale");
    }
    // bool
    /**
     * method
     * 
     * @return mixed
     */
    public function getAvailableForSale()
    {
        return $this->Get("available");
    }
    // bool
    /**
     * method
     * 
     * @return mixed
     */
    public function getAvailable()
    {
        return $this->Get("available");
    }
    // bool
    /**
     * method
     * 
     * @return mixed
     */
    public function getUnreleased()
    {
        return $this->Get("unreleased");
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function getExclusiveStatuses()
    {
        return $this->Get("exclusive-statuses");
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function getDevelopments()
    {
        if (PHP::isArray($this->Get('developments'))) {
            return $this->Get('developments');
        }
        return unserialize($this->Get('developments'));
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function getAgencies()
    {
        if (PHP::isArray($this->Get('agencies'))) {
            return $this->Get('agencies');
        }
        return unserialize($this->get('agencies'));
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function toSql()
    {
        $statusMap = static::getPropertyStatusMap($this);
        $typeMap = static::getPropertyTypeMap($this);
        $phaseMap = static::getPropertyPhaseMap($this);
        $agencyMap = static::getPropertyAgencyMap($this);
        $categoryMap = static::getPropertyListingCategoryMap($this);
        $status = [];
        $types = [];
        $phases = [];
        $developments = [];
        $agencies = [];
        $categories = [];
        foreach (array_values($statusMap) as $value) {
            if ($value['enabled'] === true) {
                $status[] = "propertystatus LIKE ('" . $value['dbValue'] . "')";
            } else {
                if ($this->getExclusiveStatuses() === true) {
                    $status[] = "propertystatus NOT LIKE ('" . $value['dbValue'] . "')";
                }
            }
        }
        foreach (array_values($typeMap) as $value) {
            if ($value['enabled'] === true) {
                $types[] = "propertytype LIKE ('" . $value['dbValue'] . "')";
            }
        }
        foreach (array_values($phaseMap) as $value) {
            if ($value['enabled'] === true) {
                $phases[] = "phase LIKE ('" . $value['dbValue'] . "')";
            }
        }
        foreach (array_values($categoryMap) as $value) {
            if ($value['enabled'] === true) {
                $categories[] = "listingCategory LIKE ('" . $value['dbValue'] . "')";
            }
        }
        if (PHP::count($this->GetDevelopments()) > 0) {
            foreach ($this->GetDevelopments() as $dev) {
                $developments[] = "development LIKE ('" . $dev . "')";
            }
        }
        $sql = [];
        if (PHP::count($status) > 0) {
            $sql[] = '(' . join($this->getExclusiveStatuses() === true ? ' AND ' : ' OR ', $status) . ')';
        }
        if (PHP::count($types) > 0) {
            $sql[] = '(' . join(' OR ', $types) . ')';
        }
        if (PHP::count($phases) > 0) {
            $sql[] = '(' . join(' OR ', $phases) . ')';
        }
        if (PHP::count($developments) > 0) {
            $sql[] = '(' . join(' OR ', $developments) . ')';
        }
        if (PHP::count($categories) > 0) {
            $sql[] = '(' . join(' OR ', $categories) . ')';
        }
        if (PHP::count($sql) === 0) {
            return '';
        }
        return '(' . join(' AND ', $sql) . ')';
    }
}