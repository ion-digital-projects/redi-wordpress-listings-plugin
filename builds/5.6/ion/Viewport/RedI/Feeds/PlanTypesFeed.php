<?php
namespace ion\Viewport\RedI\Feeds;

/**
 * Description of FeedPlanTypes
 *
 * @author Justus
 */
use ion\Viewport\RedI\SalesMapFeed;
use ion\Viewport\RedI\FeedSettings;
use ion\Viewport\RedI\Feeds\Models\PlanTypes;
class PlanTypesFeed extends SalesMapFeed
{
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct(FeedSettings $feedSettings, $estateName, $developmentName, $custom = false)
    {
        parent::__construct($feedSettings, "plan-types", ["estate" => $estateName, "development" => $developmentName]);
    }
    /**
     * method
     * 
     * 
     * @return mixed
     */
    protected function Process(array $json, $now = false)
    {
        return new PlanTypes($this->GetSettings(), $json, $now);
    }
}