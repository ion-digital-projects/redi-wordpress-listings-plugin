<?php
namespace ion\Viewport\RedI\Feeds;

/**
 * Description of Estate
 *
 * @author Justus
 */
use Exception;
use ion\Viewport\RedI\SalesMapFeed;
use ion\Viewport\RedI\FeedSettings;
use ion\Viewport\RedI\Feeds\Models\Development;
use ion\Viewport\RedI\Feeds\Models\Estate;
// http://salesmap-demo.red-i.co.za/waterfall/ws/listing
class EstateFeed extends SalesMapFeed
{
    private $feedSettings;
    private $name;
    private $onlyDevelopments;
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct(FeedSettings $feedSettings, $name, $onlyDevelopments = false)
    {
        parent::__construct($feedSettings, "estate", ["estate" => $name]);
        $this->name = $name;
        $this->feedSettings = $feedSettings;
        $this->onlyDevelopments = $onlyDevelopments;
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function GetName()
    {
        return $this->name;
    }
    /**
     * method
     * 
     * 
     * @return mixed
     */
    protected function Process(array $json, $now = false)
    {
        return new Estate($this->GetSettings(), $this->GetName(), $json, $now, $this->onlyDevelopments);
    }
}