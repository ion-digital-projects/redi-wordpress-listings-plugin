<?php
namespace ion\Viewport\RedI\Feeds;

/**
 * Description of FeedPlanTypes
 *
 * @author Justus
 */
use ion\Viewport\RedI\SalesMapFeed;
use ion\Viewport\RedI\FeedSettings;
use ion\Viewport\RedI\Feeds\Models\Agents;
class AgentsFeed extends SalesMapFeed
{
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct(FeedSettings $feedSettings, $estateName, $developmentName, $fetchNow = false)
    {
        parent::__construct($feedSettings, "agents", ["estate" => $estateName, "development" => $developmentName]);
    }
    /**
     * method
     * 
     * 
     * @return mixed
     */
    protected function Process(array $json, $now = false)
    {
        return new Agents($this->GetSettings(), $json);
    }
}