<?php
namespace ion\Viewport\RedI\Feeds\Models;

/**
 * Description of PlanTypes
 *
 * @author Justus
 */
use ion\Viewport\RedI\Model;
class PlanType extends Model
{
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct(array $data)
    {
        parent::__construct($data);
        $images = [];
        if (array_key_exists('media', $data)) {
            foreach ($data["media"] as $image) {
                $images[] = new Image($image);
            }
        }
        $this->Set("media", $images);
    }
    // string
    /**
     * method
     * 
     * @return mixed
     */
    public function GetLabel()
    {
        return $this->Get("label");
    }
    // numeric
    /**
     * method
     * 
     * @return mixed
     */
    public function GetPlanSize()
    {
        return $this->Get("planSize");
    }
    // numeric
    /**
     * method
     * 
     * @return mixed
     */
    public function GetPlanPrice()
    {
        return $this->Get("planPrice");
    }
    // numeric
    /**
     * method
     * 
     * @return mixed
     */
    public function GetBedrooms()
    {
        return $this->Get("bedrooms");
    }
    // numeric
    /**
     * method
     * 
     * @return mixed
     */
    public function GetBathrooms()
    {
        return $this->Get("bathrooms");
    }
    // numeric
    /**
     * method
     * 
     * @return mixed
     */
    public function GetGarages()
    {
        return $this->Get("garages");
    }
    // numeric
    /**
     * method
     * 
     * @return mixed
     */
    public function GetParking()
    {
        return $this->Get("parking");
    }
    // boolean
    /**
     * method
     * 
     * @return mixed
     */
    public function GetCustom()
    {
        return $this->Get("custom");
    }
    // array -> ?
    /**
     * method
     * 
     * @return mixed
     */
    public function GetMedia()
    {
        return $this->Get("media");
    }
}