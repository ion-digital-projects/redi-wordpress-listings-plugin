<?php
namespace ion\Viewport\RedI;

/**
 * Description of FilterModel
 *
 * @author Justus
 */
use Exception;
class Filter extends Model
{
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public static function Parse($parameters)
    {
        $parmArray1 = explode("&", rawurldecode($parameters));
        $parmArray3 = [];
        $opArray = [];
        foreach ($parmArray1 as $parm) {
            $parmArray2 = null;
            $op = null;
            if (strpos($parm, ">=") !== false && $op === null) {
                $op = ">=";
            }
            if (strpos($parm, "<=") !== false && $op === null) {
                $op = "<=";
            }
            if (strpos($parm, ">") !== false && $op === null) {
                $op = ">";
            }
            if (strpos($parm, "<") !== false && $op === null) {
                $op = "<";
            }
            if (strpos($parm, "=") !== false && $op === null) {
                $op = "=";
            }
            if ($op !== null) {
                $parmArray2 = explode($op, $parm);
                if (count($parmArray2) > 1) {
                    $parmArray3[$parmArray2[0]] = $parmArray2[1];
                    $opArray[$parmArray2[0]] = $op;
                }
            }
        }
        //        echo $parameters . "<br />\n";
        //        print_r($parmArray1);
        //        die("X");
        return new static($parmArray3, $opArray);
    }
    private $operators;
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct(array $parameters = null, array $operators = null)
    {
        parent::__construct($parameters);
        $this->operators = $operators;
    }
    /**
     * method
     * 
     * 
     * @return mixed
     */
    protected function Check($inValue = null, $expectedValue = null, $operator = "=", $key = null)
    {
        //echo "$inValue $operator $expectedValue<br />\n";
        if (is_array($inValue)) {
            return false;
        }
        if ($operator === ">=") {
            return $inValue >= $expectedValue;
        }
        if ($operator === "<=") {
            return $inValue <= $expectedValue;
        }
        if ($operator === ">") {
            return $inValue > $expectedValue;
        }
        if ($operator === "<") {
            return $inValue < $expectedValue;
        }
        return $inValue === $expectedValue;
    }
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function ApplyTo(array $models)
    {
        $result = [];
        //print_r($this->ToArray());
        foreach ($models as $model) {
            $success = true;
            foreach (array_keys($this->ToArray()) as $key) {
                $operator = null;
                if ($this->operators !== null) {
                    if (array_key_exists($key, $this->operators)) {
                        $operator = $this->operators[$key];
                    }
                }
                //echo "[$key]<br />";
                $opResult = false;
                if ($operator === ">=") {
                    $opResult = $model->IsPropertyGreaterThanOrEqualTo($key, $this->Get($key));
                } else {
                    if ($operator === "<=") {
                        $opResult = $model->IsPropertyLessThanOrEqualTo($key, $this->Get($key));
                    } else {
                        if ($operator === ">") {
                            $opResult = $model->IsPropertyGreaterThan($key, $this->Get($key));
                        } else {
                            if ($operator === "<") {
                                $opResult = $model->IsPropertyLessThan($key, $this->Get($key));
                            } else {
                                $opResult = $model->IsPropertyEqualTo($key, $this->Get($key));
                            }
                        }
                    }
                }
                if ($opResult === false) {
                    $success = false;
                }
                if ($success === false) {
                    break;
                }
            }
            if ($success === true) {
                $result[] = $model;
            }
        }
        return $result;
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function ToQueryString()
    {
        $q = "";
        //print_r($this->ToArray());
        foreach (array_keys($this->ToArray()) as $key) {
            $q .= $key . $this->operators[$key] . $this->Get($key) . "&";
        }
        return $q;
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function ToSql()
    {
        $tmp = [];
        foreach (array_keys($this->ToArray()) as $key) {
            $operator = null;
            if ($this->operators !== null) {
                if (array_key_exists($key, $this->operators)) {
                    $operator = $this->operators[$key];
                }
            }
            if ($operator !== null) {
                $tmp[] = '(' . $key . ' ' . $operator . ' ' . (is_string($this->Get($key)) ? "'" . $this->Get($key) . "'" : $this->Get($key)) . ')';
            }
        }
        if (count($tmp) > 0) {
            return join(' AND ', $tmp);
        }
        return null;
    }
}