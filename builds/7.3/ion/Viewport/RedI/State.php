<?php
namespace ion\Viewport\RedI;

/**
 * Description of State
 *
 * @author Justus
 */
use ion\WordPress\WordPressHelper as WP;
use ion\PhpHelper as PHP;
use ion\Viewport\RedI\Models\StoredFilterModel;
use ion\Viewport\RedI\Analytics;
class State
{
    public static function GetPageSize(State $instance)
    {
        if ($instance->IsFront() === true) {
            return intval(WP::GetOption("redi-feed-front-page-items", 4));
        }
        return intval(WP::GetOption("redi-feed-index-page-items", 9));
    }
    public static function BuildSingleLink(State $state, $targetLabel, $parameters = null, $development = null)
    {
        if (!PHP::isEmpty($state->GetStoredFilterName()) && PHP::isArray($parameters) && !array_key_exists('stored-filter', $parameters)) {
            $parameters['stored-filter'] = $state->GetStoredFilterName();
        }
        if ($state->GetDevelopment() !== null) {
            return WP::SiteLink([WP::getOption('redi-feed-prefix', 'red-i'), "property", $state->GetDevelopment(), 'unit', $targetLabel], $parameters, true, false);
        }
        $parameters['root'] = true;
        if ($development !== null) {
            return WP::SiteLink([WP::getOption('redi-feed-prefix', 'red-i'), "property", $development, 'unit', $targetLabel], $parameters, true, false);
        }
        return WP::SiteLink([WP::getOption('redi-feed-prefix', 'red-i'), "property", 'unit', $targetLabel], $parameters, true, false);
    }
    public static function BuildIndexLink(State $state, $page = 1, $pageOffset = 0, $includeFilter = true, $show = null, $orderBy = null, $forceDevelopment = null, $storedFilter = null)
    {
        $parameters = [];
        if ($pageOffset > 0) {
            $parameters["page-offset"] = $pageOffset;
        }
        $function = null;
        if ($includeFilter) {
            if ($state->HasFilter() === true) {
                $function = "filter";
                //die($state->GetFilter()->ToQueryString());
                $parameters["parameters"] = rawurlencode($state->GetFilter()->ToQueryString());
            }
        }
        if ($storedFilter === null) {
            $storedFilter = $state->GetStoredFilterName();
            //die('<p>[' . $storedFilter . ']</p>');
        }
        if ($storedFilter !== null) {
            $parameters['stored-filter'] = $storedFilter;
        }
        if ($show === null) {
            if ($state->GetShow() !== null) {
                $parameters["show"] = $state->GetShow();
            }
        } else {
            $parameters["show"] = $show;
        }
        if ($orderBy === null) {
            if ($state->GetOrderBy() !== null) {
                $parameters["sort"] = $state->GetOrderBy();
            }
        } else {
            $parameters["sort"] = $orderBy;
        }
        if ($state->GetDevelopment() !== null || $forceDevelopment !== null) {
            $root = filter_input(INPUT_GET, 'root', FILTER_DEFAULT);
            if (!$root) {
                return WP::SiteLink([WP::getOption('redi-feed-prefix', 'red-i'), "properties", $forceDevelopment !== null ? $forceDevelopment : $state->GetDevelopment(), $page, $function], $parameters, true, false);
            }
            return WP::SiteLink([WP::getOption('redi-feed-prefix', 'red-i'), "properties", $page, $function], $parameters, true, false);
        }
        return WP::SiteLink([WP::getOption('redi-feed-prefix', 'red-i'), "properties", $page, $function], $parameters, true, false);
    }
    public static function GetStoredFilters($flat = false, $includeEmpty = true)
    {
        $records = WP::getOption('redi-filters', null);
        if ($records === null) {
            $records = [];
        }
        $tmp = [];
        if ($flat === true) {
            if ($includeEmpty === true) {
                $tmp['None'] = null;
            }
            foreach ($records as $rec) {
                $tmp[$rec['description']] = $rec['slug'];
            }
            return $tmp;
        }
        foreach ($records as $rec) {
            $tmp[] = new StoredFilterModel($rec);
        }
        return $tmp;
    }
    public static function GetStoredFilter($slug = null)
    {
        $records = WP::getOption('redi-filters');
        if ($records === null) {
            $records = [];
        }
        if ($slug === null) {
            $slug = WP::getOption("redi-default-filter");
        }
        $result = null;
        if ($slug != null) {
            foreach ($records as $record) {
                if ($record['slug'] === $slug) {
                    $result = $record;
                    break;
                }
            }
        }
        if ($result === null) {
            return null;
        }
        return new StoredFilterModel($result);
    }
    private $forUs = false;
    private $property = false;
    private $propertyIndex = false;
    private $notFound = false;
    private $front = false;
    private $filter = null;
    private $page = null;
    private $label = null;
    private $plan = null;
    private $function = null;
    private $estate = null;
    private $development = null;
    private $pageOffset = null;
    private $show = 1;
    // 1 = Default; 2 = List
    private $orderBy = null;
    private $storedFilter = null;
    private $frontFilter = null;
    public function __construct($estate = null, $development = null, $overrides = null)
    {
        if ($estate === null) {
            $estate = WP::GetOption("redi-feed-estate");
        }
        $this->estate = $estate;
        if ($overrides === null) {
            $overrides = [];
            $overrides['red-i'] = null;
            $overrides['load'] = null;
            $overrides['show'] = null;
        }
        if (!WP::isAdmin()) {
            $load = null;
            $redI = filter_input(INPUT_GET, "red-i");
            if (array_key_exists('red-i', $overrides) && $overrides['red-i'] !== null) {
                $redI = (string) $overrides['red-i'];
            }
            if ($redI !== null && $redI === "true") {
                $this->forUs = true;
                $load = filter_input(INPUT_GET, "load");
                if (array_key_exists('load', $overrides) && $overrides['load'] !== null) {
                    $load = (string) $overrides['load'];
                }
                if ($load !== null) {
                    //                        var_Dump($overrides);
                    //                        var_dump($load);
                    //                        die('x');
                    if ($load === "single") {
                        $this->property = true;
                        $label = filter_input(INPUT_GET, "label");
                        if ($label !== null) {
                            $this->label = $label;
                        }
                        $plan = filter_input(INPUT_GET, 'plan');
                        if ($plan !== null) {
                            $this->plan = $plan;
                        }
                    } else {
                        if ($load === "index") {
                            $this->propertyIndex = true;
                            $this->page = 1;
                            $this->pageOffset = 0;
                            $page = filter_input(INPUT_GET, "page", FILTER_VALIDATE_INT);
                            if ($page !== null) {
                                $this->page = PHP::toInt($page);
                            }
                            $pageOffset = filter_input(INPUT_GET, "page-offset");
                            if ($pageOffset !== null) {
                                $this->pageOffset = PHP::toInt($pageOffset);
                            }
                            $this->orderBy = filter_input(INPUT_GET, "sort", FILTER_DEFAULT);
                            $this->show = filter_input(INPUT_GET, "show", FILTER_VALIDATE_INT);
                            if (array_key_exists('show', $overrides) && $overrides['show'] !== null) {
                                $this->show = PHP::toInt($overrides['show']);
                            }
                            //                            var_dump($this->show);
                            //                            exit;
                        } else {
                            $this->notFound = true;
                        }
                    }
                    if ($load === "single" || $load === "index") {
                        if ($development === null) {
                            $development = filter_input(INPUT_GET, "development");
                            if ($development !== null) {
                                $this->development = $development;
                            }
                        } else {
                            $this->development = $development;
                        }
                        $function = filter_input(INPUT_GET, "function");
                        if ($function !== null) {
                            $this->function = $function;
                            if ($this->property === true) {
                                $newLabel = null;
                                if ($function === "next") {
                                    //TODO: Find the next property, or loop back
                                } else {
                                    if ($function === "previous") {
                                        // TODO: Find the previous property, or loop forward
                                    } else {
                                        $newLabel = $this->label;
                                    }
                                }
                                if ($newLabel !== null) {
                                    $this->filter = new Filter(["label" => $this->label]);
                                }
                            } else {
                                if ($this->propertyIndex === true) {
                                    if ($function === "filter") {
                                        $filter = filter_input(INPUT_GET, "parameters");
                                        if ($filter !== null) {
                                            $this->filter = Filter::Parse(rawurldecode($filter));
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        $this->notFound = true;
                    }
                    //                        $storedFilter = filter_input(INPUT_GET, 'stored-filter');
                    //
                    //                        if ($storedFilter === null) {
                    //                            $storedFilter = WP::getOption('redi-default-filter');
                    //                        }
                    //
                    //                        if ($storedFilter !== null) {
                    //                            $this->storedFilter = $storedFilter;
                    //                        }
                }
            } else {
                $this->front = WP::isFrontPage();
                $this->forUs = WP::isFrontPage();
            }
            if ($this->forUs === true) {
                $storedFilter = filter_input(INPUT_GET, 'stored-filter');
                if ($storedFilter === null) {
                    $storedFilter = WP::getOption('redi-default-filter', null);
                    if ($this->isFront()) {
                        $tmp = WP::getOption('redi-front-page-filter', null);
                        if ($tmp !== null) {
                            $storedFilter = $tmp;
                        }
                    }
                }
                if ($storedFilter !== null) {
                    $this->storedFilter = $storedFilter;
                }
            }
        }
    }
    public function ApplyPaging(array $models)
    {
        if ($this->IsPropertyIndex() === false && $this->IsFront() === false) {
            return $models;
        }
        $result = [];
        $pageSize = static::GetPageSize($this);
        $page = 1;
        if ($this->page !== null) {
            $page = $this->page;
        }
        if ($page < 1) {
            $page = 1;
        }
        $offset = ($page - 1) * $pageSize + $this->pageOffset;
        $newPageSize = $pageSize;
        if ($offset + $newPageSize > count($models)) {
            $newPageSize = count($models) - $offset;
        }
        if ($offset < count($models)) {
            for ($i = $offset; $i < $offset + $newPageSize; $i++) {
                $result[] = $models[$i];
            }
        }
        return $result;
    }
    public function ApplySorting(array $models)
    {
        if ($this->GetOrderBy() === null) {
            return $models;
        }
        $result = $models;
        /*
         for($m = 0; $m < count($models) - 1; $m++) {
        
         $model = $models[$m];
        
         $prevModel = null;
         if($m > 0) {
         $prevModel = $models[$m - 1];
         }
        
         $nextModel = null;
         if($m < count($models) - 1) {
         $nextModel = $models[$m + 1];
         }
         }
        */
        $self = $this;
        usort($result, function ($a, $b) use($self) {
            $key = null;
            if ($self->GetOrderBy() === "price-low-to-high") {
                $key = "maxprice";
                if ($a->IsPropertyGreaterThan($key, $b->Get($key))) {
                    return 1;
                } else {
                    return -1;
                }
            } else {
                if ($self->GetOrderBy() === "price-high-to-low") {
                    $key = "maxprice";
                    if ($a->IsPropertyLessThan($key, $b->Get($key))) {
                        return 1;
                    } else {
                        return -1;
                    }
                } else {
                    if ($self->GetOrderBy() === "most-recent") {
                        // Not provided by feed
                    } else {
                        if ($self->GetOrderBy() === "property-type") {
                            $key = "propertyType";
                            if ($a->IsPropertyGreaterThan($key, $b->Get($key))) {
                                return 1;
                            } else {
                                return -1;
                            }
                        } else {
                            if ($self->GetOrderBy() === "erf-size ") {
                                $key = "size";
                                if ($a->IsPropertyGreaterThan($key, $b->Get($key))) {
                                    return 1;
                                } else {
                                    return -1;
                                }
                            }
                        }
                    }
                }
            }
            return 0;
        });
        return $result;
    }
    public function IsForUs()
    {
        return $this->forUs;
    }
    public function IsPropertyIndex()
    {
        return $this->propertyIndex;
    }
    public function IsProperty()
    {
        return $this->property;
    }
    public function Is404()
    {
        return $this->notFound;
    }
    public function IsFront()
    {
        return $this->front;
    }
    public function isSingle()
    {
        return $this->single;
    }
    public function HasFilter()
    {
        return $this->filter !== null;
    }
    public function GetFilter()
    {
        return $this->filter;
    }
    public function GetLabel()
    {
        return $this->label;
    }
    public function GetPlan()
    {
        return $this->plan;
    }
    public function IsPaged()
    {
        return $this->page !== null;
    }
    public function GetPage()
    {
        return $this->page;
    }
    public function GetPageOffset()
    {
        return $this->pageOffset;
    }
    public function HasFunction()
    {
        return $this->function !== null;
    }
    public function GetFunction()
    {
        return $this->function;
    }
    public function GetEstate()
    {
        return $this->estate;
    }
    public function GetDevelopment()
    {
        return $this->development;
    }
    public function GetShow()
    {
        return $this->show;
    }
    public function GetOrderBy()
    {
        return $this->orderBy;
    }
    public function GetStoredFilterName()
    {
        return $this->storedFilter;
    }
}