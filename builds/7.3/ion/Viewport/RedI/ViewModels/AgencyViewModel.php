<?php
namespace ion\Viewport\RedI\ViewModels;

/**
 * Description of AgentViewModel
 *
 * @author Justus
 *  */
use ion\Viewport\RedI\ViewModel;
use ion\Viewport\RedI\Feeds\Models\Agency;
class AgencyViewModel extends ViewModel
{
    public function __construct(Agency $agency)
    {
        parent::__construct();
        $this->Set("name", $agency->GetName());
        $this->Set("description", $agency->GetDescription());
        $this->Set("emailAddress", $agency->GetEmailAddress());
        $this->Set("officeNumber", $agency->GetOfficeNumber());
        $this->Set("photoUrl", $agency->GetPhotoUrl());
    }
    // string
    public function GetName()
    {
        return $this->Get("name");
    }
    // string
    public function GetDescription()
    {
        return $this->Get("description");
    }
    // string
    public function GetEmailAddress()
    {
        return $this->Get("emailAddress");
    }
    // string
    public function GetOfficeNumber()
    {
        return $this->Get("officeNumber");
    }
    // string
    public function GetPhotoUrl()
    {
        return $this->Get("photoUrl");
    }
}