<?php
namespace ion\Viewport\RedI\ViewModels;

/**
 * Description of PropertyViewModel
 *
 * @author Justus
 */
use ion\Viewport\RedI\RedIFeedPlugIn as RedI;
use ion\PhpHelper as PHP;
use ion\Viewport\RedI\ViewModel;
use ion\Viewport\RedI\FeedSettings;
use ion\Viewport\RedI\Feeds\Models\Property;
use ion\Viewport\RedI\Feeds\Models\Gallery;
use ion\Viewport\RedI\Feeds\Models\PlanType;
use ion\Viewport\RedI\Feeds\Models\Development;
use ion\Viewport\RedI\Feeds\Models\EnquiryFormFields;
use ion\Viewport\RedI\Feeds\Models\Agent;
use ion\WordPress\WordPressHelper as WP;
use ion\Viewport\RedI\State;
use ion\Viewport\RedI\Filter;
use ion\Viewport\RedI\Models\Enquiry;
use ion\Viewport\RedI\Analytics;
use ion\Viewport\RedI\Feeds\EnquiriesFeed;
class PropertyViewModel extends ViewModel
{
    private $development;
    private $primaryAgency = null;
    public function __construct(Development $development = null, Property $property = null, $data = null, $developmentNameOverride = null)
    {
        parent::__construct($data);
        $this->development = $development;
        $estateName = WP::GetOption("redi-feed-estate");
        //                       echo "<pre>";
        //                var_dump($property);
        ////                var_dump($property->GetAgencyOverrides());
        //                echo "</pre>";
        //var_Dump($development);
        if ($development !== null && $property !== null) {
            $this->set("developmentName", $development->getLabel());
            if ($estateName !== null) {
                $state = new State($estateName);
                $this->Set("label", $property->GetLabel());
                $this->Set("title", null);
                $this->Set("subTitle", $property->GetAddress());
                $this->Set("unitNumber", $property->GetUnit());
                $rentalAmount = $property->GetRentalAmount();
                $this->Set("propertyStatus", $property->GetSaleStatus());
                // TODO: Process values here, if needed
                $this->Set("propertyType", $property->GetPropertyType());
                // TODO: Process values here, if needed
                if (strtolower($this->GetPropertyStatus()) == 'available for rental') {
                    $this->Set("maxPrice", $rentalAmount ?: $property->GetMaxTotalPrice());
                    $this->Set("minPrice", $rentalAmount ?: $property->GetMinTotalPrice());
                } else {
                    $this->Set("maxPrice", $property->GetMaxTotalPrice());
                    $this->Set("minPrice", $property->GetMinTotalPrice());
                }
                $this->Set("plotPrice", $property->GetPlotPrice());
                $this->Set("rentalAmount", $rentalAmount);
                $this->Set("price", $this->GetMinPrice() !== null ? $this->GetMinPrice() : $this->GetMaxPrice());
                // ??
                $this->Set("agentContactNumber", null);
                $this->Set("agentEmailAddress", null);
                $this->Set("agentAgency", null);
                //$agents = (count($property->GetAgencies()) > 0 ? $property->GetAgencies() : ( count($property->GetAgents()) > 0 ? $property->GetAgents() : null ));
                $agents = $property->GetAgents();
                $agentName = $agents !== null && PHP::count($agents) > 0 ? $agents[0] : null;
                $agent = null;
                $this->Set("agencies", null);
                if (PHP::count($property->GetPropertyAgents()) > 0) {
                    //$agent = $property->GetPropertyAgents()[rand(0, PHP::count($property->GetPropertyAgents()) - 1)];
                    $agents = $property->GetPropertyAgents();
                    $agencies = [];
                    $aCnt = [];
                    foreach ($property->GetPropertyAgents() as $a) {
                        if (PHP::count($a->GetAgencies()) > 0) {
                            foreach ($a->GetAgencies() as $tmpAgency) {
                                if (!in_array($tmpAgency, $property->GetAgencies())) {
                                    continue;
                                }
                                //                                echo "<pre>";
                                //                                var_dump($tmpAgency);
                                //                                echo "</pre>";
                                //var_dump($agencies);
                                $cnt = 0;
                                foreach ($agencies as $z) {
                                    if ($z->GetName() == $tmpAgency) {
                                        $cnt++;
                                    }
                                }
                                if ($cnt === 0 && $development->GetAgencies() !== null) {
                                    if (PHP::count($development->GetAgencies()->GetItems()) > 0) {
                                        $agencyFilter = new Filter(["name" => $tmpAgency]);
                                        $agencies[] = $agencyFilter->ApplyTo($development->GetAgencies()->GetItems())[0];
                                        if (!array_key_exists($tmpAgency, $aCnt)) {
                                            $aCnt[$tmpAgency] = 0;
                                        }
                                        $aCnt[$tmpAgency]++;
                                    }
                                }
                            }
                        }
                    }
                    if (PHP::count(array_keys($aCnt)) != PHP::count($property->GetAgencies())) {
                        //                        var_dump($aCnt);
                        //                        exit;
                        foreach ($property->GetAgencies() as $tmp) {
                            if (!array_key_exists($tmp, $aCnt)) {
                                $agencyFilter = new Filter(["name" => $tmp]);
                                $agency = $agencyFilter->ApplyTo($development->GetAgencies()->GetItems());
                                if (!PHP::isEmpty($agency)) {
                                    $agency = $agency[0];
                                    $agents[] = new Agent(['firstName' => $agency->GetName(), 'emailAddress' => $agency->GetEmailAddress(), 'mobileNumber' => $agency->GetOfficeNumber()]);
                                }
                            }
                            //                        echo '<pre>';
                            //                        var_dump($agents);
                            //                        die( '</pre>');
                        }
                    }
                    $this->Set("agents", $agents);
                    $this->Set("agencies", $agencies);
                    //                    //if($property->GetLabel() == 3961) {
                    //                    echo "ARGH";
                    //                    var_dump($agencies);
                    //                    //}
                } else {
                    if (PHP::count($property->GetAgencies()) > 0) {
                        $agencies = [];
                        foreach ($property->GetAgencies() as $tmpAgency) {
                            //var_dump($agencies);
                            $cnt = 0;
                            foreach ($agencies as $z) {
                                if ($z->GetName() == $tmpAgency) {
                                    $cnt++;
                                }
                            }
                            if ($cnt === 0 && $development->GetAgencies() !== null) {
                                if (PHP::count($development->GetAgencies()->GetItems()) > 0) {
                                    $agencyFilter = new Filter(["name" => $tmpAgency]);
                                    $agencies[] = $agencyFilter->ApplyTo($development->GetAgencies()->GetItems())[0];
                                }
                            }
                        }
                        $this->Set("agencies", $agencies);
                    }
                }
                //                if($property->GetLabel() == '4220') {
                //                    echo "<pre>";
                //                    var_dump($agents);
                //                    die("</pre>");
                //                }
                ////                if($property->GetLabel() == '4198') {
                //                    echo "<pre>";
                //    //                var_dump($property);
                //                    var_dump($property->GetAgencyOverrides());
                //                    echo "</pre>";
                //                    //die("X");
                ////                }
                if ($agent !== null) {
                    //echo "[$agent]<br />";
                    $this->Set("agentContactNumber", $agent->GetMobileNumber());
                    $this->Set("agentEmailAddress", $agent->GetEmailAddress());
                    $agency = null;
                    if (PHP::count($agent->GetAgencies()) > 0) {
                        // the initial agency to be displayed
                        $agency = $agent->GetAgencies()[0];
                    }
                    $this->Set("agentAgency", $agency);
                } else {
                    if (!empty($this->GetAgencies())) {
                        $this->Set("agentAgency", $this->GetAgencies()[0]);
                    }
                }
                $this->Set("agencyOverrides", null);
                if ($property->GetAgencyOverrides() !== null) {
                    if (PHP::count($property->GetAgencyOverrides()) > 0) {
                        $overrides = [];
                        foreach ($property->GetAgencyOverrides() as $tmpOverride) {
                            $overrides[] = $tmpOverride;
                        }
                        $this->Set("agencyOverrides", $overrides);
                    }
                }
                $this->Set("agentName", $agentName);
                $showTimeFilter = new Filter(["agent" => $agentName]);
                $this->Set("showTimes", $showTimeFilter->ApplyTo($property->GetShowTimes()));
                $this->Set("phase", $property->GetPhase());
                $this->Set("listingCategory", $property->getListingCategory());
                $this->Set("size", $property->GetPlotSize());
                $this->Set("description", $property->GetDescription());
                // https://www.salesmap.co.za/waterfall/#/estate/waterfall-country-estate/unit/3045
                $mapLink = null;
                if ($development !== null) {
                    $mapLink = WP::GetOption("redi-feed-base-uri");
                    $mapLink .= "/" . $development->GetEstate() . "/#/estate/" . $development->GetName() . "/unit/" . $property->GetLabel();
                }
                $this->Set("mapLink", $mapLink);
                $this->Set("bathrooms", null);
                $this->Set("bedrooms", null);
                $this->Set("garages", null);
                $this->Set("parking", null);
                $plans = [];
                if (!$property->GetPlanRequired()) {
                    $plans[] = new PlanTypeViewModel(new PlanType(['label' => 'Plot only', 'planPrice' => $property->GetPlotPrice()]));
                }
                if (PHP::count($property->GetPropertyPlanTypes()) > 0) {
                    //                    if ($property->GetSelectedPlanType() !== null) {
                    //                        foreach ($property->GetPropertyPlanTypes() as $plan) {
                    //                            if ($plan->GetLabel() == $property->GetSelectedPlanType()) {
                    //                                //$plans[] = new PlanTypeViewModel($this, $plan);
                    //
                    //                                $tmp[] = $property->GetSelectedPlanType();
                    //                            }
                    //                        }
                    //                    }
                    //                    else {
                    foreach ($property->GetPropertyPlanTypes() as &$plan) {
                        //$plans[] = new PlanTypeViewModel($this, $plan);
                        $plans[] = new PlanTypeViewModel($plan);
                        //echo '(property) ' . $this->GetLabel() . ' => (plan) ' . $plan->getLabel(). "\n";
                    }
                    //echo "\n\n";
                    //}
                    //var_dump($property->GetSelectedPlanType());
                    //echo $this->GetLabel() . "<br />";
                    //print_r($tmp);
                }
                $this->Set("selectedPlanType", $property->GetSelectedPlanType());
                $this->Set("plans", $plans);
                $this->Set("planPrice", null);
                if (PHP::count($plans) > 0) {
                    $tmp = $plans[0];
                    if (PHP::count($plans) > 1) {
                        if ($state->GetPlan() === null && $property->GetSelectedPlanType() === null) {
                        } else {
                            foreach ($property->GetPropertyPlanTypes() as &$plan) {
                                if ($state->GetPlan() === null && $property->GetSelectedPlanType() !== null) {
                                    if ($plan->GetLabel() == $property->GetSelectedPlanType()) {
                                        $tmp = $plan;
                                        break;
                                    }
                                } else {
                                    if ($plan->GetLabel() == $state->GetPlan()) {
                                        $tmp = $plan;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    $this->Set("bathrooms", $tmp->GetBathrooms());
                    $this->Set("bedrooms", $tmp->GetBedrooms());
                    $this->Set("garages", $tmp->GetGarages());
                    $this->Set("parking", $tmp->GetParking());
                    $this->Set('plan', $tmp);
                    $this->Set('planPrice', $tmp->GetPlanPrice());
                }
                //                var_dump($development->GetAgencies());
                //                exit;
                $this->Set("images", $property->GetMedia());
                //if(!PHP::isEmpty($property->GetAgencyOverrides())) {
                //                    echo "<pre>";
                //                    var_dump($property->GetAgencyOverrides());
                //                    die("</pre>");
                //}
                //        var_Dump($estateName);
                //        var_Dump($development === null);
                //        var_Dump($property === null);
                //        if($development !== null) {
                //            var_Dump($development->GetCode());
                //        }
                //die("Z");
                $this->Set("contactForm", $this->createForm($property->GetLabel(), $development->GetCode()));
                //$this->Set("contactForm", null);
            }
        }
        if (is_array($this->GetAgencies()) && PHP::count($this->GetAgencies()) > 0) {
            $this->primaryAgency = 0;
            if (PHP::count($this->GetAgencies()) > 0) {
                $this->primaryAgency = rand(0, PHP::count($this->GetAgencies()) - 1);
            }
        }
        //var_dump($this->ToArray());
        //die("X");
        //$this->Set("contactForm", 'THIS IS THE ARG FORM');
    }
    public function createForm($propertyLabel = null, $developmentName = null)
    {
        $forms = ['resales' => null, 'default' => null, 'embeddable' => null];
        $enquiries = $this->GetDevelopment()->GetEnquiries();
        if (defined("WP_DEBUG")) {
            //$this->development = new Development(, "", "", [], true);
            $state = new State();
            if ($state->IsForUs()) {
                $feedSettings = RedI::getInstance()->getFeedSettings();
                $enquiries = (new EnquiriesFeed($feedSettings, $this->GetDevelopment()))->Fetch(true);
            }
        }
        foreach ($enquiries->GetItems() as $enq) {
            if ($enq->GetType() == 'Resales') {
                $forms['resales']['action'] = $enq->GetPost();
                $forms['resales']['fields'] = $enq->GetFields();
                $forms['resales']['field_uri'] = $enq->GetGet();
            } else {
                if ($enq->GetType() == 'Default') {
                    $forms['default']['action'] = $enq->GetPost();
                    $forms['default']['fields'] = $enq->GetFields();
                    $forms['default']['field_uri'] = $enq->GetGet();
                } else {
                    if ($enq->GetType() == 'Embeddable') {
                        $forms['embeddable'] = null;
                        if ($enq->GetGet() !== null) {
                            $forms['embeddable'] = $enq->GetGet();
                        }
                    }
                }
            }
        }
        $form = null;
        if (strtolower($this->GetPropertyStatus()) == 'resale' || strtolower($this->GetPropertyStatus()) == 'available for resale') {
            $form = $forms['resales'];
        } else {
            if ($forms['embeddable'] === null) {
                $form = $forms['default'];
            }
            //FIXME
            $form = $forms['default'];
        }
        //        if($propertyLabel == 'PVG079') {
        //
        //            $output = PHP::obGet(function() use ($forms) { echo "<pre>"; var_dump($forms); echo "</pre>"; });
        //
        //            die($output);
        //
        //            RedI::startDebugLogEntry('form', $output);
        //            RedI::endDebugLogEntry('form');
        //        }
        return $this->RenderForm($form['action'], $form['fields'], null, $developmentName, $propertyLabel);
    }
    public function RenderForm($action, $fields, $caption = null, $developmentName = null, $propertyLabel = null)
    {
        $form = '';
        if ($caption === null) {
            $caption = 'Contact an Agent';
        }
        $formId = uniqid();
        if ($action !== null && $fields !== null) {
            $form = <<<TEMPLATE
    <div class="email-form">
        <h6>{$caption}</h6>
        <form id="{$formId}" class="enquiry-form" action="{$action}" method="post">            
TEMPLATE;
            foreach ($fields as $field) {
                $class = "";
                $name = $field->Get('name');
                $label = $field->Get('label');
                if ($field->Get('required') === true) {
                    $class .= ' required';
                }
                if (strlen(trim($class)) > 0) {
                    $class = ' class="' . trim($class) . '"';
                }
                $type = $field->Get('type');
                if ($field->Get('type') === 'hidden') {
                    $value = $field->Get('value');
                    // Overwriting 'enquirySource' at the request of Sean (Red-I)
                    if ($name === 'enquirySource') {
                        $value = 'WebsiteLiveListings';
                    }
                    $form .= "<input{$class} name=\"{$name}\" type=\"hidden\" value=\"{$value}\" />\n";
                } else {
                    if ($field->Get('type') === 'text' || $field->Get('type') === 'email') {
                        $maxLength = $field->Get('maxLength');
                        $type = $field->Get('type');
                        $value = $field->Get('value');
                        $form .= "<input{$class} name=\"{$name}\" maxlength=\"{$maxLength}\" type=\"{$type}\" value=\"{$value}\" placeholder=\"{$label}\" />\n";
                    } else {
                        if ($field->Get('type') === 'select') {
                            $form .= "<div class=\"{$type}\"><label for=\"{$name}\">{$label}</label></div>\n";
                            $form .= "<select{$class} name=\"{$name}\">\n";
                            $options = $field->Get('options');
                            foreach ($options as $option) {
                                $form .= "<option value=\"" . $option['value'] . "\">" . $option['label'] . "</option>\n";
                            }
                            $form .= "</select>\n";
                        } else {
                            if ($field->Get('type') === 'checkbox') {
                                $value = $field->Get('value');
                                $options = $field->Get('options');
                                $checked = $field->Get('checked') === true ? ' checked' : '';
                                $id = 'chkbox_' . WP::slugify($label) . '_' . $value;
                                if ($options === null) {
                                    $form .= "<div class=\"{$type}\"><input{$class} name=\"{$name}\" value=\"{$value}\" type=\"checkbox\" {$checked} /><label for=\"{$id}\">{$label}</label></div>\n";
                                } else {
                                    if ($options !== null && is_array($options)) {
                                        $form .= "<div class=\"{$type}\"><label>{$label}</label></div>\n";
                                        foreach ($options as $option) {
                                            $optionLabel = $option['label'];
                                            $optionValue = $option['value'];
                                            $optionId = $id . '_' . $optionValue;
                                            $form .= "<div class=\"{$type}\"><input{$class} name=\"{$name}\" value=\"{$optionValue}\" type=\"checkbox\" {$checked} /><label for=\"{$optionId}\">{$optionLabel}</label></div>\n";
                                        }
                                    }
                                }
                            } else {
                                if ($field->Get('type') === 'textarea') {
                                    $value = $field->Get('value');
                                    $form .= "<textarea{$class} name=\"{$name}\" placeholder=\"{$label}\"></textarea>\n";
                                } else {
                                    if ($field->Get('type') === 'document') {
                                        $req = $field->Get('required');
                                        $form .= "<div class=\"{$type}" . ($req ? " checkbox" : "") . "\">\n";
                                        if ($req === true) {
                                            $form .= "<input{$class} type=\"checkbox\" name=\"{$name}\" id=\"{$name}\" value=\"true\" />";
                                        }
                                        if (!empty($field->Get('url'))) {
                                            $form .= "<a href=\"{$field->Get('url')}\" target=\"_blank\">{$label}</a>";
                                        }
                                        $form .= "</div>\n";
                                    }
                                }
                            }
                        }
                    }
                }
            }
            //            $analyticsSession = 'null';
            //
            //            if(WP::getOption('redi-enable-analytics', false) === true) {
            //                if(Analytics::getSessionId() !== null) {
            //                    $analyticsSession = "'" . Analytics::getSessionId() . "'";
            //                }
            //            }
            $form .= <<<TEMPLATE
             <input type="submit" value="Submit" />
        </form>
        <script type="text/javascript">
                    
            jQuery('#{$formId} input[type="submit"]').click(function() {

                var form = jQuery('#{$formId}').get(0);    
                
                var development = '{$developmentName}';
                var property = '{$propertyLabel}';
                    
                if(form !== null) {                    
                    var action = jQuery(form).attr('action');
                    
                    //var action = 'http://proj.linux-dev.vm/endpoint.php';
                    
                    var agentNameField = jQuery('#agents option:selected');
                    var agentEmailField = jQuery('#emailaddress span.value');
                    
                    var agentName = null;
                    var agentEmail = null;                    
                    
                    if(agentNameField != null && agentEmailField != null) {
                    
                        var agentName = jQuery(agentNameField).val();
                    
                        if(agentName)
                            agentName = agentName.trim();
                        
                    
                        var agentEmail = jQuery(agentEmailField).text();
                    
                        if(agentEmail)
                            agentEmail = agentEmail.trim();

                        var submit = false;
                    
                        var data = jQuery(form).serializeArray();
                        var required = [];
                    
                        //console.dir(data);
                    
                        jQuery(form).find('.required').each(function(index, element) {
                            required.push(jQuery(element).attr('name'));
                        });
                        
                        //console.dir(required);

                        //if((agentName != null && agentName != 'undefined') && (agentEmail != null && agentEmail != 'undefined')) {
                                                                    
                        submit = true;

                        for(var i in required) {
                            var reqName = required[i];

                            for(var x in data) {
                                var field = data[x];                                                        

                                if(reqName == field.name) {

                                    //console.log(reqName, field.name, field.value);

                                    if(field.value === '' || field.value === null) {
                                        submit = false;
                                        break;
                                    }
                                }

                            }                    

                            if(submit === false) {
                                break;
                            }

                        }
                    
                        //}
                    
                        if(submit) {
                            //alert('Submitted');
                    
                            jQuery(form).append(jQuery('<input type="hidden" name="enquiryProperty" value="' + development + ':' + property + '" />'));
                
                            if(agentEmail != null)
                                jQuery(form).append(jQuery('<input type="hidden" name="enquiryContactEmail" value="' + agentEmail + '" />'));
                
                            if(agentName != null)
                                jQuery(form).append(jQuery('<input type="hidden" name="enquiryContactName" value="' + agentName + '" />'));

                            jQuery.post(action, jQuery(form).serialize(), function(result) {

                                //json = json.trim();

                                json = eval(result);

                                if(json) {
                                    var msg = null

                                    if(json.Message) {
                                        msg = jQuery(json.Message).text();
                                    } else {
                                        msg = 'Thanks for contacting us - we\\'ll get back to you as soon as we can.';
                                    }

                                    jQuery('#{$formId}').html(msg);                  
                                } else {
                                    console.log(result);
                                }

                            }).fail(function() {
                                alert( 'Something went wrong - please try again.' );
                            });              
                        
                        } else {
                            alert('Not all required fields have been filled out.');
                        }
                    }
                }
   
                return false;
            });
        </script>                    
    </div><!-- /email-form -->               
TEMPLATE;
        }
        return $form;
    }
    public function GetDevelopment()
    {
        return $this->development;
    }
    public function IsPropertyEqualTo($propertyName, $expectedValue = null)
    {
        if ($propertyName === "minPrice" || $propertyName === "maxPrice") {
            if (($this->GetPrice() === null ? 0 : $this->GetPrice()) === ($expectedValue === null ? 0 : $expectedValue)) {
                return true;
            }
            return false;
        }
        //        else if($propertyName === "bedrooms") {
        //
        //        } else if($propertyName === "bathrooms") {
        //
        //        } else if($propertyName === "garages") {
        //
        //        }
        return parent::IsPropertyEqualTo($propertyName, $expectedValue);
    }
    public function IsPropertyGreaterThan($propertyName, $expectedValue = null)
    {
        if ($propertyName === "minPrice" || $propertyName === "maxPrice") {
            //die($propertyName . " " . ($this->GetPrice() === null ? 0 : $this->GetPrice()) . " " . $expectedValue);
            if (($this->GetMinPrice() === null ? 0 : $this->GetMinPrice()) > ($expectedValue === null ? 0 : $expectedValue)) {
                return true;
            }
            //return false;
        }
        return parent::IsPropertyGreaterThan($propertyName, $expectedValue);
    }
    public function IsPropertyLessThan($propertyName, $expectedValue = null)
    {
        if ($propertyName === "minPrice" || $propertyName === "maxPrice") {
            if (($this->GetMaxPrice() === null ? 0 : $this->GetMaxPrice()) < ($expectedValue === null ? 0 : $expectedValue)) {
                return true;
            }
            return false;
        }
        return parent::IsPropertyLessThan($propertyName, $expectedValue);
    }
    public function IsPropertyGreaterThanOrEqualTo($propertyName, $expectedValue = null)
    {
        if ($propertyName === "minPrice" || $propertyName === "maxPrice") {
            //die($propertyName . " " . ($this->GetPrice() === null ? 0 : $this->GetPrice()) . " " . $expectedValue);
            if (($this->GetMinPrice() === null ? 0 : $this->GetMinPrice()) >= ($expectedValue === null ? 0 : $expectedValue)) {
                return true;
            }
            //return false;
        }
        return parent::IsPropertyGreaterThanOrEqualTo($propertyName, $expectedValue);
    }
    public function IsPropertyLessThanOrEqualTo($propertyName, $expectedValue = null)
    {
        if ($propertyName === "minPrice" || $propertyName === "maxPrice") {
            if (($this->GetMaxPrice() === null ? 0 : $this->GetMaxPrice()) <= ($expectedValue === null ? 0 : $expectedValue)) {
                return true;
            }
            return false;
        }
        return parent::IsPropertyLessThanOrEqualTo($propertyName, $expectedValue);
    }
    public function GetLabel()
    {
        return $this->Get("label");
    }
    public function GetTitle()
    {
        return $this->Get("title");
    }
    public function GetSubTitle()
    {
        return $this->Get("subTitle");
    }
    public function GetUnitNumber()
    {
        return $this->Get("unitNumber");
    }
    public function GetPlanPrice()
    {
        return $this->Get("planPrice");
    }
    public function GetPlotPrice()
    {
        return $this->Get("plotPrice");
    }
    public function GetPrice()
    {
        return $this->Get("price");
    }
    public function GetMaxPrice()
    {
        return $this->Get("maxPrice");
    }
    public function GetMinPrice()
    {
        return $this->Get("minPrice");
    }
    public function GetAgentName()
    {
        return $this->Get("agentName");
    }
    public function GetAgentAgency()
    {
        return $this->Get("agentAgency");
    }
    public function GetAgentContactNumber()
    {
        return $this->Get("agentContactNumber");
    }
    public function GetAgentEmailAddress()
    {
        return $this->Get("agentEmailAddress");
    }
    public function GetPropertyStatus()
    {
        return $this->Get("propertyStatus");
    }
    public function GetPropertyType()
    {
        return $this->Get("propertyType");
    }
    public function GetPhase()
    {
        return $this->Get("phase");
    }
    public function GetBathrooms()
    {
        return $this->Get("bathrooms");
    }
    public function GetBedrooms()
    {
        return $this->Get("bedrooms");
    }
    public function GetGarages()
    {
        return $this->Get("garages");
    }
    public function GetParking()
    {
        return $this->Get("parking");
    }
    public function GetSize()
    {
        return $this->Get("size");
    }
    public function GetDescription()
    {
        $description = null;
        if (is_array($this->GetAgencyOverrides()) && PHP::count($this->GetAgencyOverrides()) > 0 && (is_array($this->GetAgencies()) && PHP::count($this->GetAgencies()) > 0)) {
            $overrides = [];
            foreach ($this->GetAgencyOverrides() as $override) {
                if (PHP::isEmpty($override->GetDescription())) {
                    continue;
                }
                $overrides[] = $override;
            }
            $tmpAgency = $this->primaryAgency;
            if ($tmpAgency <= PHP::count($overrides)) {
                shuffle($overrides);
                $tmpAgency = 0;
            }
            if (PHP::count($overrides) > $tmpAgency) {
                $description = $overrides[$tmpAgency]->GetDescription();
            }
        }
        if (PHP::isEmpty($description) && !PHP::isEmpty($this->Get('description'))) {
            $description = $this->Get('description');
        }
        return $description;
    }
    public function GetShowTimes()
    {
        return $this->Get("showTimes");
    }
    public function GetMapLink()
    {
        return $this->Get("mapLink");
    }
    public function GetPlans()
    {
        return $this->Get("plans");
    }
    public function GetPlan()
    {
        return $this->Get("plan");
    }
    public function GetContactForm()
    {
        return $this->Get("contactForm");
    }
    public function GetIndexImage()
    {
        $media = [];
        if (is_array($this->GetAgencyOverrides()) && PHP::count($this->GetAgencyOverrides()) > 0 && (is_array($this->GetAgencies()) && PHP::count($this->GetAgencies()) > 0)) {
            $overrides = [];
            foreach ($this->GetAgencyOverrides() as $override) {
                if (PHP::count($override->getMedia()) === 0) {
                    continue;
                }
                $overrides[] = $override;
            }
            $tmpAgency = $this->primaryAgency;
            if ($tmpAgency >= PHP::count($overrides)) {
                shuffle($overrides);
                $tmpAgency = 0;
            }
            if (PHP::count($overrides) > 0) {
                $media = $overrides[$tmpAgency]->GetMedia();
            }
        }
        if (PHP::isEmpty($media) && PHP::count($this->Get("images")) > 0) {
            $media = $this->Get("images");
        }
        if (!PHP::isEmpty($media) && PHP::count($media) > 0) {
            //            shuffle($media);
            return $media[0];
        }
        return null;
    }
    public function GetImages()
    {
        return $this->Get("images");
    }
    public function GetOverridedImages()
    {
        $media = [];
        if (is_array($this->GetAgencyOverrides()) && PHP::count($this->GetAgencyOverrides()) > 0 && (is_array($this->GetAgencies()) && PHP::count($this->GetAgencies()) > 0)) {
            $overrides = [];
            foreach ($this->GetAgencyOverrides() as $override) {
                if (PHP::count($override->getMedia()) === 0) {
                    continue;
                }
                $overrides[] = $override;
            }
            if ($this->primaryAgency < PHP::count($overrides) && PHP::count($overrides) > 0) {
                $media = $overrides[$this->primaryAgency]->GetMedia();
            }
        }
        if (PHP::isEmpty($media) && PHP::count($this->Get("images")) > 0) {
            $media = $this->Get("images");
        }
        if (!PHP::isEmpty($media) && PHP::count($media) > 0) {
            return $media;
        }
        return null;
    }
    public function GetAgents()
    {
        return $this->Get("agents");
    }
    public function GetAgencies()
    {
        return $this->Get("agencies");
    }
    public function GetAgencyOverrides()
    {
        return $this->Get("agencyOverrides");
    }
    public function GetAgencyByName($name = null)
    {
        //var_dump($this->GetAgencies());
        $filter = new Filter(['name' => $name]);
        $arr = $filter->ApplyTo($this->GetAgencies());
        if (PHP::count($arr) > 0) {
            return $arr[0];
        }
        return null;
    }
    public function GetAgentImage()
    {
        $a = $this->GetAgencyByName(PHP::count($agent->GetAgencies()) > 0 ? $agent->GetAgencies()[0] : null);
        return $a !== null ? $a->GetPhotoUrl() : ($agent->GetPhotoUrl() !== null ? $agent->GetPhotoUrl() : null);
    }
    public function GetAgency()
    {
        return $this->Get("agency");
    }
    public function GetRentalAmount()
    {
        return $this->Get("rentalAmount");
    }
    public function GetSelectedPlanType()
    {
        return $this->Get("selectedPlanType");
    }
    public function GetPlanRequired()
    {
        return $this->Get("planRequired");
    }
    public function GetHidePrices()
    {
        return $this->Get("hidePrices");
    }
    public function GetHidePlanPrices()
    {
        return $this->Get("hidePlanPrices");
    }
    public function getDevelopmentName()
    {
        return $this->get("developmentName");
    }
    //    public function getParking() {
    //
    //        return $this->get('parking');
    //    }
    public function getListingCategory()
    {
        return $this->get('listingCategory');
    }
}