<?php
namespace ion\Viewport\RedI;

/**
 * Description of PlugIn
 *
 * @author Justus
 */
use ion\WordPress\WordPressHelperInterface;
use ion\Viewport\RedI\Feeds\Models\Estate;
use ion\Viewport\RedI\Feeds\EstateFeed;
use ion\Viewport\RedI\Feeds\Development;
use ion\WordPress\WordPressHelper as WP;
use ion\WordPress\Helper\WordPressHelperException;
use ion\PhpHelper as PHP;
use ion\Viewport\RedI\FeedSettings;
use Exception;
use ion\Viewport\RedI\RedIFeedFilterWidget as FilterWidget;
use ion\Viewport\RedI\RedIFeedOrderingWidget as OrderingWidget;
use ion\Viewport\RedI\RedIFeedAgentWidget as AgentWidget;
use ion\Viewport\RedI\Db\DbImporter;
use ion\Viewport\RedI\Db\DbMarshal;
use ion\Viewport\RedI\Marshal;
use ion\Viewport\RedI\Models\StoredFilterModel;
use ion\Viewport\RedI\State;
use ion\Viewport\RedI\Analytics;
use ion\WordPress\Helper\LogLevel;
use ion\WordPress\Helper\IContext;
use ion\WordPress\Helper\Context;
use ion\WordPress\Helper\HelperContextInterface;
use ion\WordPress\PlugIn;
use ion\Viewport\RedI\Db\Models\DbTempPropertyModel;
use ion\Viewport\RedI\Db\Models\DbTempDevelopmentModel;
use ion\Viewport\RedI\Db\Models\DbPropertyModel;
use ion\Viewport\RedI\Db\Models\DbDevelopmentModel;
//error_reporting(E_ALL);
function removeFileVersion($src)
{
    return preg_replace("/\\?ver=[0-9.]+/", "?ver=" . time(), $src);
}
class RedIFeedPlugIn extends Context
{
    protected static $currentViewModelIndex = null;
    protected static $developments = [];
    protected static $values = [];
    protected static $titleDisplayed = false;
    private static function removeFileVersion($src)
    {
        return preg_replace("/\\?ver=[0-9.]+/", "?ver=" . time(), $src);
    }
    public static function getSiteSpecificCss()
    {
        $result = ['None' => null];
        $context = WP::getContext('redi/redi-plugin');
        $dir = $context->getWorkingDirectory() . 'styles/';
        $files = glob($dir . 'SiteSpecific_*.css');
        if ($files !== false) {
            foreach ($files as $file) {
                $tmp = basename($file);
                $result[$tmp] = $tmp;
            }
        }
        return $result;
    }
    public static function FormatCurrency($value = null, $spanId = null)
    {
        $symbol = WP::getOption("redi-currency-symbol", 'R');
        if ($value === null) {
            $value = 0;
        }
        $value = (double) $value;
        $seperator = WP::getOption("redi-currency-thousands-seperator", ' ');
        $num = (string) number_format($value, 0, '.', $seperator);
        if ($spanId !== null) {
            $num = '<span id="' . $spanId . '">' . $num . '</span>';
        }
        if ((bool) WP::getOption("redi-currency-symbol-suffix")) {
            return $num . " {$symbol}";
        }
        return "{$symbol} " . $num;
    }
    public static function resetViewModelIndex()
    {
        static::$currentViewModelIndex = null;
    }
    public static function GetNextViewModel()
    {
        $model = null;
        if (PHP::count(static::GetViewModels()) > 0) {
            if (static::$currentViewModelIndex < count(static::GetViewModels()) - 1 || static::$currentViewModelIndex === null) {
                if (static::$currentViewModelIndex === null) {
                    static::$currentViewModelIndex = 0;
                } else {
                    static::$currentViewModelIndex++;
                }
                $model = static::GetViewModels()[static::$currentViewModelIndex];
            }
        }
        return $model;
    }
    public static function GetCurrentViewModel()
    {
        $model = null;
        if (count(static::GetViewModels()) > 0) {
            if (static::$currentViewModelIndex < count(static::GetViewModels()) || static::$currentViewModelIndex === null) {
                if (static::$currentViewModelIndex === null) {
                    static::$currentViewModelIndex = 0;
                }
                $model = static::GetViewModels()[static::$currentViewModelIndex];
            }
        }
        return $model;
    }
    public static function GetInstance()
    {
        return static::getContextInstance();
    }
    public static function GetViewModels()
    {
        return static::GetInstance()->GetData();
    }
    public static function GetTotalCount()
    {
        return static::GetInstance()->GetCount();
    }
    public static function GetDevelopments()
    {
        return static::$developments;
    }
    public static function GetValues()
    {
        return static::$values;
    }
    public static function areImportsDisabled()
    {
        return (bool) WP::getOption('redi-disable-imports', false);
    }
    use TemplateTagsTrait, HelperExtensionsTrait;
    private $feedSettings;
    private $state;
    private $data;
    private $count;
    //    protected function __construct(string $name, string $loadPath, array $wpHelperSettings = null, string $helperDir = null) {
    //        echo('WP Helper');
    //        parent::__construct($name, $loadPath, $wpHelperSettings, $helperDir);
    //    }
    //    protected function __construct(/* string */ $vendor, /* string */ $name, /* string */ $loadPath, /* array */ $wpHelperSettings = null, /* string */ $helperDir = null) {
    //        parent::__construct($vendor, $name, $loadPath, $wpHelperSettings, $helperDir);
    //    }
    public function __construct($pkg, $settings = null)
    {
        global $wpdb;
        //        echo "<pre>";
        //        var_dump($wpdb);
        //        die("</pre>");
        self::extendHelper();
        parent::__construct($pkg, $settings);
        self::extendHelperContext();
        WP::addAction("admin_init", function () {
            global $pagenow;
            if ($pagenow == "options-permalink.php") {
                WP::flushRewriteRules();
            }
        });
    }
    protected static $debugEntries = [];
    public static function IsDebugMode()
    {
        if (WP::getOption("redi-debug-mode") === true) {
            return true;
        }
        return false;
    }
    private static function removePathPrefix($string, HelperContextInterface $helperContext = null)
    {
        if ($helperContext === null) {
            return $string;
        }
        return str_replace($helperContext->getWorkingDirectory(), '', $string);
    }
    public static function traceToString(array $trace, HelperContextInterface $helperContext = null)
    {
        /*        return PHP::obGet(function() use ($trace) {
        
                  var_dump($trace);
                  }); */
        $output = "\n";
        foreach ($trace as $i => $traceItem) {
            $index = $i + 1;
            $file = $traceItem['file'];
            if ($helperContext !== null) {
                $file = static::removePathPrefix($file, $helperContext);
            }
            $output .= "{$index}. ";
            if (array_key_exists('class', $traceItem)) {
                $output .= "{$traceItem['class']}";
            }
            if (array_key_exists('type', $traceItem)) {
                $output .= "{$traceItem['type']}";
            }
            $output .= "{$traceItem['function']}()\n**{$file}** \n";
        }
        return $output;
    }
    public static function startDebugLogEntry($key, $entryMessage = null, $helperContext = null)
    {
        if (!static::isDebugMode()) {
            return;
        }
        if (WP::isAdmin()) {
            return;
        }
        if (array_key_exists($key, static::$debugEntries)) {
            WP::log("Debug timing entry for '{$key}' has already been started!", LogLevel::DEBUG, 'redi-debug');
            return;
        }
        static::$debugEntries[$key] = ['start' => microtime(true), 'helperContext' => $helperContext, 'startMessage' => $entryMessage];
        //        $trace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 2);
        //        $msg = "Started tracking for *'{$key}'*" . (PHP::isEmpty($entryMessage) ? "" : " - {$entryMessage}") . ".";
        //        WP::log($msg, LogLevel::DEBUG, 'redi-debug');
        return;
    }
    public static function endDebugLogEntry($key)
    {
        if (!static::isDebugMode()) {
            return;
        }
        if (WP::isAdmin()) {
            return;
        }
        if (!array_key_exists($key, static::$debugEntries)) {
            WP::log("Debug timing entry for '{$key}' has *not* been started - cannot determine timings!", LogLevel::DEBUG, 'redi-debug');
            return;
        }
        $duration = round(microtime(true) - static::$debugEntries[$key]['start'], 4);
        $trace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 3);
        array_shift($trace);
        $context = static::$debugEntries[$key]['helperContext'];
        $traceString = static::traceToString($trace, $context);
        $request = PHP::getServerRequestUri();
        $msg = "Completed tracking for *'{$key}'* - total processing time **{$duration} seconds**.\n\nLogged from: " . static::removePathPrefix(__FILE__, $context) . "\n\nRequest URI: {$request}\n\n{$traceString}\n\n" . static::$debugEntries[$key]['startMessage'];
        WP::log($msg, LogLevel::DEBUG, 'redi-debug');
        unset(static::$debugEntries[$key]);
        return;
    }
    protected function initialize() : void
    {
        $self = $this;
        $context = $this->getHelperContext();
        WP::registerLog('redi-cron', 'REDI Feed CRON');
        //        WP::registerLog('redi-imports', 'REDI Feed Imports');
        WP::registerLog('redi-analytics', 'REDI Analytics');
        if (static::isDebugMode()) {
            WP::registerLog('redi-debug', 'REDI Debugging');
        }
        static::startDebugLogEntry('Plugin Init', null, $this->getHelperContext());
        $this->feedSettings = new FeedSettings(WP::getOption("redi-feed-base-uri"), (int) WP::getOption("redi-feed-delay"), ["estate" => WP::GetOption("redi-feed-estate-path-template"), "development" => WP::GetOption("redi-feed-development-path-template"), "properties" => WP::GetOption("redi-feed-properties-path-template"), "plan-types" => WP::GetOption("redi-feed-plan-types-path-template"), "agents" => WP::GetOption("redi-feed-agents-path-template"), "agencies" => WP::GetOption("redi-feed-agencies-path-template"), "gallery" => WP::GetOption("redi-feed-gallery-path-template"), "enquiries" => WP::GetOption("redi-feed-enquiries-path-template")], WP::EnsureTemporaryFileDirectory("redi-listings-cache"), WP::getOption("redi-debug-feed") === "true", null, null, false, true);
        $prefix = WP::getOption('redi-feed-prefix', 'red-i');
        // /red-i/property/{label}
        WP::addRewriteRule("/?{$prefix}/property/unit/([^/]+)(/([^/]+))?/?\$", "?red-i=true&load=single&label=\$1&function=\$3");
        // /red-i/property/{development}/{label}
        WP::addRewriteRule("/?{$prefix}/property/([^/]+)/unit/([^/]+)(/([^/]+))?/?\$", "?red-i=true&load=single&development=\$1&label=\$2&function=\$4");
        // /red-i/properties
        WP::addRewriteRule("/?{$prefix}/properties/?\$", "?red-i=true&load=index&page=1");
        // /red-i/properties/{page}/?filter=value&filter=value
        WP::addRewriteRule("/?{$prefix}/properties/([0-9]+)(/([^/]+))?/?\$", "?red-i=true&load=index&page=\$1&function=\$3");
        // /red-i/properties/{development}/{page}/?filter=value&filter=value
        WP::addRewriteRule("/?{$prefix}/properties/([^/]+)/([0-9]+)(/([^/]+))?/?\$", "?red-i=true&load=index&development=\$1&page=\$2&function=\$4");
        // /red-i/properties/{development}
        WP::addRewriteRule("/?{$prefix}/properties/([^/]+)/?\$", "?red-i=true&load=index&development=\$1&page=1");
        // /red-i/something_else
        //WP::addRewriteRule("/?red-i/?(?!.*\\b(property|properties)\\b)", "?red-i=true");   //   \/red-i\/?(?!.*\b(property|properties)\b)
        WP::addRewriteRule("/?{$prefix}/click/?\$", "?red-i=true&click=true");
        //        $this->load($context, null);
        $filters = State::GetStoredFilters(false, false);
        //vaR_dump($filters);
        foreach ($filters as $filter) {
            WP::addRewriteRule("/?" . $filter->GetSlug() . "/?\$", "?red-i=true&load=index&page=1&stored-filter=" . $filter->GetSlug());
            WP::addRewriteRule("/?" . $filter->GetSlug() . "/([^/]+)/?\$", "?red-i=true&load=index&page=1&development=\$1&stored-filter=" . $filter->GetSlug());
        }
        WP::addWidget(new FilterWidget());
        $importTime = WP::getOption('redi-import-time', $context->getActivationTimeStamp());
        $importInterval = WP::getOption('redi-import-interval', 'daily');
        //if (!WP::getOption('__redi-import-processing', false)) {
        $tmpKey = '__redi-incremental-import-developments';
        $estate = WP::GetOption("redi-feed-estate");
        $useCron = false;
        register_shutdown_function(function () use(&$useCron) {
            $errfile = "unknown file";
            $errstr = "shutdown";
            $errno = E_CORE_ERROR;
            $errline = 0;
            $error = error_get_last();
            if ($error !== NULL && ($error["type"] !== E_NOTICE && $error["type"] !== E_WARNING && $error["type"] !== E_DEPRECATED && $error["type"] !== E_STRICT)) {
                $errno = $error["type"];
                $errfile = $error["file"];
                $errline = $error["line"];
                $errstr = $error["message"];
                //error_mail(format_error( $errno, $errstr, $errfile, $errline));
                WP::log("'{$errstr}' ({$errno}) @ {$errfile} on line {$errline}", LogLevel::ERROR, $useCron === true ? 'redi-cron' : null);
            }
        });
        //trigger_error("Fatal error", E_USER_ERROR);
        static::startDebugLogEntry('Plugin CRON Init', null, $this->getHelperContext());
        WP::addCronInterval('every-15-min', 60 * 15, 'Every 15 minutes');
        if (!WP::getOption('redi-incremental-downloads', false)) {
            //WP::removeCronJob('redi_db_import_developments');
            if (WP::hasOption($tmpKey . '_orig')) {
                $developments = WP::getOption($tmpKey, []);
                //                foreach ($developments as $development) {
                //
                //                    WP::removeCronJob('redi_db_import_development_' . $development);
                //                }
                WP::removeOption($tmpKey . '_orig');
                WP::removeOption($tmpKey);
            }
            //WP::log("Scheduled non-incremental fetch for '{$estate}.'", LogLevel::INFO(), 'redi-cron');
            WP::addCronJob('redi_db_import', $importTime, $importInterval, function () use($self, $estate, &$useCron) {
                //WP::setOption('__redi-import-processing', true);
                WP::log("Started non-incremental fetch for '{$estate}.'", LogLevel::INFO, 'redi-cron');
                if (static::areImportsDisabled()) {
                    WP::log("Did NOT complete non-incremental fetch for '{$estate}' - imports are disabled in the settings.", LogLevel::NOTICE, 'redi-cron');
                    return;
                }
                $useCron = true;
                DbImporter::TruncateTemporaryTables();
                $results = DbImporter::Fetch($self->feedSettings);
                //WP::setOption('__redi-import-processing', false);
                DbImporter::finalize();
                WP::log("Completed non-incremental fetch for '{$estate}' (" . $results . " records).", LogLevel::INFO, 'redi-cron');
            });
        } else {
            // Remove the non-incremental job
            //WP::removeCronJob('redi_db_import');
            if (!WP::hasOption($tmpKey)) {
                $t = time();
                WP::addCronJob('redi_db_import_developments', $importTime, $importInterval, function () use($self, $tmpKey, $importTime, $estate, &$useCron, $t) {
                    WP::log("Started incremental index fetch for '{$estate}.'", LogLevel::INFO, 'redi-cron');
                    if (static::areImportsDisabled()) {
                        WP::log("Did NOT complete incremental index fetch for '{$estate}' - imports are disabled in the settings.", LogLevel::NOTICE, 'redi-cron');
                        return;
                    }
                    $useCron = true;
                    WP::setOption('redi_last_import', $t);
                    $batch = DbImporter::CreateNewBatch($self->feedSettings);
                    WP::setOption($tmpKey, $batch);
                    WP::setOption($tmpKey . '_orig', $batch);
                    DbImporter::TruncateTemporaryTables();
                    WP::log("Completed incremental index fetch for '{$estate}.'", LogLevel::INFO, 'redi-cron');
                    //WP::setOption('__redi-import-processing', true);
                });
                //WP::log("Scheduled incremental index fetch for '{$estate}.'", LogLevel::INFO, 'redi-cron');
            } else {
                if (!static::areImportsDisabled()) {
                    $developments = WP::getOption($tmpKey, []);
                    if (WP::hasOption($tmpKey . '_orig')) {
                        $origDevelopments = WP::getOption($tmpKey . '_orig', []);
                        foreach ($origDevelopments as $origDevelopment) {
                            WP::removeCronJob('redi_db_import_development_' . $origDevelopment);
                        }
                    }
                    if (count($developments) > 0) {
                        $origDevelopments = WP::getOption($tmpKey . '_orig', []);
                        $development = $developments[0];
                        //die('['. count($origDevelopments) . ']['. count($developments) . ']');
                        //$time = strtotime('+' . ((count($origDevelopments) - count($developments)) * 5) + 2 . 'minutes', $importTime);
                        $time = WP::getOption('redi_last_import');
                        if ($time === null) {
                            WP::setOption('redi_last_import', strtotime('+5 minutes'));
                        }
                        //die("[" . strftime('%c', $time) . "]");
                        WP::addCronJob('redi_db_import_development_' . $development, $time, 'once', function () use($self, $tmpKey, $development, $developments, $origDevelopments, &$useCron) {
                            $useCron = true;
                            WP::log("Started incremental fetch for '{$development}.'", LogLevel::INFO, 'redi-cron');
                            $results = DbImporter::Fetch($self->feedSettings, $development);
                            //var_dump($developments);
                            array_shift($developments);
                            WP::setOption($tmpKey, $developments);
                            WP::setOption('redi_last_import', strtotime('+5 minutes'));
                            //var_dump($developments);
                            //var_dump(count($developments));
                            if (count($developments) === 0) {
                                foreach ($origDevelopments as $origDevelopment) {
                                    WP::removeCronJob('redi_db_import_development_' . $origDevelopment);
                                }
                                WP::removeOption($tmpKey . '_orig');
                                WP::removeOption($tmpKey);
                                WP::removeOption('redi_last_import');
                                DbImporter::finalize();
                            }
                            WP::log("Completed incremental fetch for '{$development}' (" . $results . " records).", LogLevel::INFO, 'redi-cron');
                        });
                        //WP::log("Scheduled incremental fetch for '{$development}.'", LogLevel::INFO, 'redi-cron');
                    }
                }
            }
        }
        static::endDebugLogEntry('Plugin CRON Init');
        if (!WP::isAdmin()) {
            static::startDebugLogEntry('Plugin Non-admin Init', null, $this->getHelperContext());
            WP::addShortCode("redi-search", function () {
                echo WP::Widget(new RedIFeedFilterWidget(), null, null, null, null, null, false);
            });
            WP::addShortCode("redi-index-list", function () use($context) {
                $this->load($context, ['red-i' => 'true', 'load' => 'index', 'show' => '1']);
                $dir = $context->getWorkingDirectory() . 'templates/' . WP::getOption('redi-shortcode-index-list-template');
                $output = PHP::obGet(function () use($dir) {
                    include_once $dir;
                });
                echo $output;
            });
            WP::addShortCode("redi-index-tiles", function () use($context) {
                $this->load($context, ['red-i' => 'true', 'load' => 'index', 'show' => '2']);
                $dir = $context->getWorkingDirectory() . 'templates/' . WP::getOption('redi-shortcode-index-tiles-template');
                $output = PHP::obGet(function () use($dir) {
                    include_once $dir;
                });
                echo $output;
            });
            WP::addShortCode("redi-index-front", function () use($context) {
                while ($model = static::GetNextViewModel() !== null) {
                    ?>
                    <div class="property">
                        <div class="image">
                            <span class="type"><?php 
                    static::PropertyStatus();
                    ?></span>
                    <?php 
                    static::PropertyPrimaryImage(true, static::PropertyLink(false));
                    ?>
                        </div>
                        <h3><a href="<?php 
                    static::PropertyLink();
                    ?>"><?php 
                    static::PropertyTitle();
                    ?></a></h3>
                        <div class=“price”><?php 
                    static::PropertyMinPrice();
                    ?></div>
                        <p><?php 
                    static::PropertyDescription();
                    ?></p>
                    <?php 
                    static::PropertyFeatures(true, true, true, true, false);
                    ?>
                    </div><!--/property -->                      
                <?php 
                }
                ?><p><a href="<?php 
                static::NextPropertyPageLink();
                ?>" class="btn outline">See more <i class="fa fa-angle-right"></i></a></p><?php 
            });
            remove_theme_support('title-tag');
            WP::addFilter('wp_head', function () {
                echo "<title>" . static::PageTitle(false) . "</title>\n";
            });
            add_action("wp", function () use($context) {
                static::startDebugLogEntry('Plugin Init - wp (hook)', null, $context);
                $state = new State();
                if ($state->isForUs() || $state->isFront()) {
                    //WP::addScript("jquery", $context->getWorkingUri() . "scripts/jquery-3.2.1.js", true, true);
                    if ($state->isForUs()) {
                        wp_deregister_script('jquery');
                        wp_deregister_script('jquery2');
                        //                            wp_register_script('jquery', "https://code.jquery.com/jquery-2.2.4.min.js");
                        //                            wp_enqueue_script('jquery');
                        wp_register_script('jquery', "https://code.jquery.com/jquery-3.5.1.min.js");
                        wp_enqueue_script('jquery');
                        wp_register_script('jquery2', "https://code.jquery.com/jquery-migrate-3.3.1.min.js", ['jquery']);
                        wp_enqueue_script('jquery2');
                    }
                    //WP::addScript("jquery", "https://code.jquery.com/jquery-2.2.4.min.js", false, true);
                    //WP::addScript("jquery-migrate", "https://code.jquery.com/jquery-migrate-3.3.1.min.js", false, true);
                    if (WP::getOption('redi-site-specific-css', null) !== null) {
                        WP::addStyle("site-specific", $context->getWorkingUri() . 'styles/' . WP::getOption('redi-site-specific-css'), false, true);
                    }
                    WP::addStyle("no-ui-slider", $context->getWorkingUri() . "styles/nouislider.css", false, true);
                    WP::addScript("w-numb", $context->getWorkingUri() . "scripts/wNumb.js", false, true);
                    WP::addScript("no-ui-slider", $context->getWorkingUri() . "scripts/nouislider.js", false, true);
                    WP::addStyle("RedIFeedPlugIn", $context->getWorkingUri() . "styles/RedIFeedPlugIn.css", false, true);
                    WP::addScript("RedIFeedPlugIn", $context->getWorkingUri() . "scripts/RedIFeedPlugIn.js", false, true);
                    add_action('pre_get_posts', function ($query) use($state) {
                        if (!$query->is_main_query() && $state->isForUs() && !is_admin()) {
                            $query->query_vars['is_404'] = false;
                        }
                        //
                        //                                if($state->isForUs()) {
                        //
                        //                                    //$query->query_vars['is_404'] = false;
                        //                                }
                    });
                }
                //                echo "<pre>{$context->getName()}</pre>";
                $exit = $this->template($context);
                static::endDebugLogEntry('Plugin Init - wp (hook)');
                if ($exit) {
                    exit;
                }
            });
            static::endDebugLogEntry('Plugin Non-admin Init');
        } else {
            static::startDebugLogEntry('Plugin Admin Init', null, $this->getHelperContext());
            WP::addPlugInAdminMenuPage("Feed Settings", $context->getView("feed-settings"), "REDi Listings", "feed-settings")->addSubMenuPage("Feed Settings", $context->getView("feed-settings"), "feed-settings")->addSubMenuPage("Visual Settings", $context->getView("visual-settings"), "visual-settings")->addSubMenuPage("Analytics Settings", $context->getView("analytics-settings"), "analytics-settings")->addSubMenuPage("Filters", $context->getView("filter-settings"), "filter-settings");
            WP::addStyle("RedIFeedPlugInAdmin", $context->getWorkingUri() . "styles/RedIFeedPlugInAdmin.css", true, false);
            WP::addScript("RedIFeedPlugInAdmin", $context->getWorkingUri() . "scripts/RedIFeedPlugInAdmin.js", true, false);
            static::endDebugLogEntry('Plugin Admin Init');
        }
        static::endDebugLogEntry('Plugin Init');
    }
    protected function deactivate() : void
    {
        WP::removeCronJob('redi_db_import');
    }
    protected function uninstall() : void
    {
        // Empty, for now...
    }
    protected function activate() : void
    {
        $developments = ['id' => ['type' => 'int(11)', 'null' => false, 'primary' => true, 'auto' => true], 'import_id' => ['type' => 'varchar(250)', 'null' => true], 'batch_id' => ['type' => 'int(11)', 'null' => true], 'development' => ['type' => 'varchar(5000)', 'null' => true], 'label' => ['type' => 'varchar(1000)', 'null' => true], 'import_start_time' => ['type' => 'datetime', 'null' => true], 'import_end_time' => ['type' => 'datetime', 'null' => true], 'hide_prices' => ['type' => 'int(11)', 'null' => true], 'hide_plan_prices' => ['type' => 'int(11)', 'null' => true], 'agencies' => ['type' => 'longtext', 'null' => true], 'errors' => ['type' => 'varchar(5000)', 'null' => true]];
        $properties = [
            'id' => ['type' => 'int(11)', 'null' => false, 'primary' => true, 'auto' => true],
            'import_id' => ['type' => 'varchar(250)', 'null' => true],
            'batch_id' => ['type' => 'int(11)', 'null' => true],
            'development' => ['type' => 'varchar(1000)', 'null' => true],
            'label' => ['type' => 'varchar(16)', 'null' => true],
            'title' => ['type' => 'varchar(1000)', 'null' => true],
            'subtitle' => ['type' => 'varchar(1000)', 'null' => true],
            'unitnumber' => ['type' => 'int(11)', 'null' => true],
            //'price' => ['type' => 'decimal', 'null' => true],
            'plotprice' => ['type' => 'decimal', 'null' => true],
            'maxprice' => ['type' => 'decimal', 'null' => true],
            'minprice' => ['type' => 'decimal', 'null' => true],
            'rentalamount' => ['type' => 'decimal', 'null' => true],
            //'agentContactNumber' => ['type' => 'varchar(1000)', 'null' => true],
            'propertystatus' => ['type' => 'varchar(250)', 'null' => true],
            'propertytype' => ['type' => 'varchar(250)', 'null' => true],
            'listingCategory' => ['type' => 'varchar(250)', 'null' => true],
            'phase' => ['type' => 'varchar(250)', 'null' => true],
            'size' => ['type' => 'int(11)', 'null' => true],
            'description' => ['type' => 'text', 'null' => true],
            'maplink' => ['type' => 'varchar(1000)', 'null' => true],
            'bathrooms' => ['type' => 'int(11)', 'null' => true],
            'bedrooms' => ['type' => 'int(11)', 'null' => true],
            'garages' => ['type' => 'int(11)', 'null' => true],
            'parking' => ['type' => 'int(11)', 'null' => true],
            'plans' => ['type' => 'longtext', 'null' => true],
            //'plantypes' => ['type' => 'longtext', 'null' => true],
            'images' => ['type' => 'longtext', 'null' => true],
            'forms' => ['type' => 'text', 'null' => true],
            'agencies' => ['type' => 'longtext', 'null' => true],
            'agency_lookup' => ['type' => 'varchar(5000)', 'null' => true],
            'propertyagents' => ['type' => 'longtext', 'null' => true],
            'agencyoverrides' => ['type' => 'longtext', 'null' => true],
            'showtimes' => ['type' => 'longtext', 'null' => true],
            'numimages' => ['type' => 'int(11)', 'null' => true],
            'numplans' => ['type' => 'int(11)', 'null' => true],
            'numplanimages' => ['type' => 'int(11)', 'null' => true],
            'plan_required' => ['type' => 'int(11)', 'null' => true],
            'selected_plan_type' => ['type' => 'varchar(1000)', 'null' => true],
            'time_imported' => ['type' => 'datetime', 'null' => true],
        ];
        WP::dbDeltaTable("redi_developments", $developments);
        WP::dbDeltaTable("redi_developments_import", $developments);
        WP::dbDeltaTable("redi_properties", $properties);
        WP::dbDeltaTable("redi_properties_import", $properties);
    }
    protected function template(HelperContextInterface $context, callable $onDone = null, $wpHelperSettings = null)
    {
        //        if(defined('SHORTCODE')) {
        //
        //            return false;
        //        }
        if (filter_input(INPUT_GET, 'click', FILTER_DEFAULT) !== null) {
            $event = filter_input(INPUT_POST, 'event', FILTER_DEFAULT);
            if ($event === null) {
                $event = filter_input(INPUT_GET, 'event', FILTER_DEFAULT);
            }
            if ($event !== null) {
                header("HTTP/1.1 200 OK");
                switch (urldecode($event)) {
                    case Analytics::LISTING_CLICK:
                        echo Analytics::recordClick();
                        break;
                    case Analytics::ENQUIRY:
                        echo Analytics::recordEnquiry();
                        break;
                    case Analytics::FAVOURITE:
                        echo Analytics::recordFavourite();
                        break;
                    default:
                        echo "Event '{$event}' not recognized.";
                }
            } else {
                header("HTTP/1.1 500 Internal Server Error");
                echo "No event supplied.";
            }
            exit;
        }
        return $this->load($context, null, $onDone);
    }
    private function load($context, array $overrides = null, callable $onDone = null)
    {
        $estateName = WP::GetOption("redi-feed-estate");
        if ($estateName !== null) {
            $this->state = new State($estateName, null, $overrides);
            if (!$this->state->IsForUs()) {
                return false;
            }
            http_response_code(200);
            DbMarshal::Create();
            $tmp = [];
            try {
                $tmp = Marshal::GetInstance()->Fetch($this->feedSettings, $this->state, false, false);
            } catch (Exception $ex) {
                WP::log($ex->getMessage(), LogLevel::ERROR, 'redi-cron');
                throw $ex;
            }
            $this->data = $tmp['properties'];
            static::$developments = $tmp['developments'];
            static::$values = $tmp['values'];
            $this->count = Marshal::GetTotalRecords();
            //echo '<pre>';
            //var_dump($_GET);
            //var_dump($overrides);
            //var_dump(count($this->data));
            //var_dump($this->state->IsProperty());
            //var_dump($this->state->IsPropertyIndex());
            //echo '</pre>';
            //die('HERE');
            Analytics::registerEvents($this->state);
            $template = null;
            if (count($this->data) > 0 && $this->state->IsProperty() || $this->state->IsPropertyIndex()) {
                if ($this->state->IsProperty()) {
                    $template = WP::GetOption("redi-single-template");
                } else {
                    if ($this->state->IsPropertyIndex()) {
                        if ($this->state->GetShow() === 2) {
                            $template = WP::GetOption("redi-index-template");
                        } else {
                            // default
                            $template = WP::GetOption("redi-list-template");
                        }
                    }
                }
            } else {
                if ($this->state->IsFront()) {
                    $template = WP::GetOption("redi-front-template");
                }
            }
            if (PHP::isEmpty($template)) {
                return false;
            }
            if (substr_compare($template, '.php', -strlen('.php')) !== 0) {
                $template = $template . '.php';
            }
            RediFeedPlugin::startDebugLogEntry('Template');
            load_template($context->getWorkingDirectory() . "templates/{$template}");
            RediFeedPlugin::endDebugLogEntry('Template');
            return true;
        }
        return false;
    }
    public function GetState()
    {
        return $this->state;
    }
    public function GetData()
    {
        return $this->data;
    }
    public function GetCount()
    {
        return $this->count;
    }
    public function GetFeedSettings()
    {
        return $this->feedSettings;
    }
}