<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Viewport\RedI\Db\Models;

/**
 * Description of DbTempDevelopmentModel
 *
 * @author Justus
 */
class DbTempDevelopmentModel extends DbDevelopmentModel
{
    protected static function GetDefaultTableName()
    {
        return parent::GetDefaultTableName() . '_import';
    }
    public function __construct($development)
    {
        parent::__construct($development, 'import');
    }
}