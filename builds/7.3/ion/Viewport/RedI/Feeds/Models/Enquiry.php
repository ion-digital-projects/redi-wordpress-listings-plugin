<?php
namespace ion\Viewport\RedI\Feeds\Models;

/**
 * Description of Enquiry
 *
 * @author Justus
 */
use ion\Viewport\RedI\FeedSettings;
use ion\Viewport\RedI\Model;
use ion\Viewport\RedI\Feeds\Models\Development;
use ion\Viewport\RedI\Feeds\EnquiryFormFieldsFeed;
class Enquiry extends Model
{
    private $fields;
    public function __construct(FeedSettings $feedSettings, array $data = null)
    {
        parent::__construct($data);
        $this->fields = null;
        if (strtolower($this->GetType()) == 'embeddable') {
            $this->embeddedFormUrl = $this->GetGet();
        } else {
            $formFeed = new EnquiryFormFieldsFeed($feedSettings, $this->GetGet());
            //            die("FORM");
            $this->fields = $formFeed->Fetch(true);
        }
    }
    // string
    public function GetType()
    {
        return $this->Get("type");
    }
    // string
    public function GetPost()
    {
        return $this->Get("post");
    }
    // string
    public function GetGet()
    {
        return $this->Get("get");
    }
    public function GetFields()
    {
        return $this->fields;
    }
}