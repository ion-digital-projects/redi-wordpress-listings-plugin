<?php
namespace ion\Viewport\RedI\Feeds\Models;

/**
 * Description of Enquiry
 *
 * @author Justus
 */
use ion\Viewport\RedI\Model;
class EnquiryFormField extends Model
{
    public function __construct(array $data)
    {
        parent::__construct($data);
    }
    // string
    public function GetName()
    {
        return $this->Get("name");
    }
    // string
    public function GetLabel()
    {
        return $this->Get("label");
    }
    // string
    public function GetType()
    {
        return $this->Get("type");
    }
    public function GetUrl()
    {
        return $this->Get("url");
    }
    // string
    public function GetValue()
    {
        return $this->Get("value");
    }
    // int
    public function GetMaxLength()
    {
        return $this->Get("maxLength");
    }
    // bool
    public function GetRequired()
    {
        return $this->Get("required");
    }
    // bool
    public function GetChecked()
    {
        return $this->Get("checked");
    }
    // array
    public function GetOptions()
    {
        return $this->Get("options");
    }
}