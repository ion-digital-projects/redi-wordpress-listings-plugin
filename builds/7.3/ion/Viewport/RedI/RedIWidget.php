<?php
namespace ion\Viewport\RedI;

use ion\WordPress\WordPressHelper as WP;
use ion\WordPress\Helper\WordPressWidget;
/**
 * Description of RedIWidget
 *
 * @author Justus
 */
abstract class RedIWidget extends WordPressWidget
{
    public function __construct($title)
    {
        parent::__construct($title);
    }
    public function widget($args, $instance)
    {
        // echo $args["before_widget"];
        if (!empty($instance["title"])) {
            echo $args["before_title"] . apply_filters("widget_title", $instance["title"]) . $args["after_title"];
        }
        echo $this->RenderFrontEnd($instance);
        //echo $args["after_widget"];
    }
}