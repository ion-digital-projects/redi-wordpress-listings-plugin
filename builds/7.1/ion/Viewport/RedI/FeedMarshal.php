<?php
namespace ion\Viewport\RedI;

/**
 * Description of FeedMarshal
 *
 * @author Justus
 */
use ion\Viewport\RedI\Feeds\Models\Development;
use ion\Viewport\RedI\Feeds\Models\Estate;
use ion\Viewport\RedI\Feeds\EstateFeed;
use ion\Viewport\RedI\ViewModels\PropertyViewModel;
use ion\Viewport\RedI\ViewModels\DevelopmentViewModel;
use ion\Viewport\RedI\Marshal;
class FeedMarshal extends Marshal
{
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function Fetch(FeedSettings $feedSettings, State $state, $ignoreFilters = false)
    {
        $result = ['developments' => [], 'properties' => []];
        static::$totalRecords = 0;
        if ($state->IsForUs()) {
            //$feedResult = null;
            if ($state->GetEstate() !== null) {
                // Developments
                $developments = [];
                $estateFeed = new EstateFeed($feedSettings, $state->GetEstate());
                $estate = $estateFeed->Fetch();
                if ($estate !== null) {
                    if ($state->GetDevelopment() !== null) {
                        // Only load the development to make things quicker - in case this is real time
                        //$filter = new Filter([ "code" => $state->GetDevelopment()]);
                        //$arr = $filter->ApplyTo($estate->GetDevelopments());
                        //if(count($arr) > 0) {
                        $developments[] = new Development($feedSettings, $state->GetEstate(), $state->GetDevelopment(), null);
                        //}
                    } else {
                        // We don't know which development to look for, so we have to load all estate data
                        foreach ($estate->GetDevelopments() as $development) {
                            $developments[] = $development;
                        }
                    }
                    $properties = [];
                    foreach ($developments as $development) {
                        if ($development->GetProperties() !== null) {
                            foreach ($development->GetProperties()->GetItems() as $property) {
                                $properties[] = new PropertyViewModel($development, $property);
                            }
                        }
                    }
                    if ($ignoreFilters === true) {
                        static::$totalRecords = count($properties);
                        $result['properties'] = $state->ApplySorting($properties);
                        return $result;
                    }
                    if ($state->GetFilter() !== null) {
                        $result['properties'] = $state->GetFilter()->ApplyTo($properties);
                    } else {
                        $result['properties'] = $properties;
                    }
                    static::$totalRecords = count($result['properties']);
                }
            }
        }
        $result['properties'] = $state->ApplyPaging($state->ApplySorting($result['properties']));
        return $result;
    }
}