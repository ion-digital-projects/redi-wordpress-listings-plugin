<?php
namespace ion\Viewport\RedI;

/**
 * Description of Marshal
 *
 * @author Justus
 */
abstract class Marshal
{
    //
    //    public static function FetchDevelopmentCodeForProperty($label, $developmentCode = null) {
    //
    //        if(static::GetInstance() === null) {
    //            return null;
    //        }
    //
    //        return static::GetInstance()->FetchDevelopment($label, $developmentCode);
    //    }
    protected static $instance = null;
    protected static $totalRecords = 0;
    /**
     * method
     * 
     * @return mixed
     */
    public static function GetInstance()
    {
        return static::$instance;
    }
    /**
     * method
     * 
     * @return mixed
     */
    public static function Create()
    {
        static::$instance = new static();
    }
    /**
     * method
     * 
     * @return mixed
     */
    public static function GetTotalRecords()
    {
        return static::$totalRecords;
    }
    /**
     * method
     * 
     * @return mixed
     */
    protected function __construct()
    {
        // empty!
    }
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public abstract function Fetch(FeedSettings $feedSettings, State $state, $ignoreFilters = false);
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public abstract function FetchDevelopmentForProperty(FeedSettings $feedSettings, $estateName, $label);
}