<?php
namespace ion\Viewport\RedI;

/**
 *
 * @author Justus
 */
interface IPersistentModel
{
    /**
     * method
     * 
     * @return mixed
     */
    static function GetTableFields();
    /**
     * method
     * 
     * @return mixed
     */
    static function GetTableName();
}