<?php
namespace ion\Viewport\RedI\ViewModels;

/**
 * Description of PropertyViewModel
 *
 * @author Justus
 */
use ion\Viewport\RedI\ViewModel;
/**
 * Description of PlanTypeViewModel
 *
 * @author Justus
 */
use ion\Viewport\RedI\Feeds\Models\PlanType;
class PlanTypeViewModel extends ViewModel
{
    //private $propertyViewModel;
    //
    //public function __construct(PropertyViewModel $propertyViewModel, PlanType $planType) {
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct(PlanType $planType)
    {
        parent::__construct();
        //$this->propertyViewModel = $propertyViewModel;
        $this->Set("label", $planType->GetLabel());
        $this->Set("bathrooms", $planType->GetBathrooms() == 0 || $planType->GetBathrooms() == 0 ? null : $planType->GetBathrooms());
        $this->Set("bedrooms", $planType->GetBedrooms() == 0 || $planType->GetBedrooms() == 0 ? null : $planType->GetBedrooms());
        $this->Set("garages", $planType->GetGarages() == 0 || $planType->GetGarages() == 0 ? null : $planType->GetGarages());
        $this->Set("parking", $planType->GetParking() == 0 || $planType->GetParking() == 0 ? null : $planType->GetParking());
        $this->Set("images", $planType->GetMedia());
        $this->Set("planPrice", $planType->GetPlanPrice());
        if (count($this->GetImages()) > 0) {
            $this->Set("primary-image", $this->GetImages()[0]);
        }
    }
    //public function GetPropertyViewModel() {
    //    return $this->propertyViewModel;
    //}
    /**
     * method
     * 
     * @return mixed
     */
    public function GetPlanPrice()
    {
        return $this->Get("planPrice");
    }
    // string
    /**
     * method
     * 
     * @return mixed
     */
    public function GetLabel()
    {
        return $this->Get("label");
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function GetBathrooms()
    {
        return $this->Get("bathrooms");
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function GetBedrooms()
    {
        return $this->Get("bedrooms");
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function GetGarages()
    {
        return $this->Get("garages");
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function GetParking()
    {
        return $this->Get("parking");
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function GetImages()
    {
        return $this->Get("images");
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function GetPrimaryImage()
    {
        return $this->Get("primary-image");
    }
}