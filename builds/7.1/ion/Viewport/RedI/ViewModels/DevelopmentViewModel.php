<?php
namespace ion\Viewport\RedI\ViewModels;

/**
 * Description of PropertyViewModel
 *
 * @author Justus
 */
use ion\Viewport\RedI\ViewModel;
use ion\Viewport\RedI\Feeds\Models\Property;
use ion\Viewport\RedI\Feeds\Models\Gallery;
use ion\Viewport\RedI\Feeds\Models\PlanType;
use ion\Viewport\RedI\Feeds\Models\Development;
use ion\Viewport\RedI\Feeds\Models\EnquiryFormFields;
use ion\WordPress\WordPressHelper as WP;
use ion\Viewport\RedI\State;
use ion\Viewport\RedI\Filter;
use ion\Viewport\RedI\Models\Enquiry;
class DevelopmentViewModel extends ViewModel
{
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct($data = null)
    {
        parent::__construct($data);
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function GetLabel()
    {
        return $this->Get("label");
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function GetCode()
    {
        return $this->Get("code");
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function GetHidePrices()
    {
        return $this->Get("hide_prices");
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function GetHidePlanPrices()
    {
        return $this->Get("hide_plan_prices");
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function getAgencies()
    {
        return $this->Get('agencies');
    }
}