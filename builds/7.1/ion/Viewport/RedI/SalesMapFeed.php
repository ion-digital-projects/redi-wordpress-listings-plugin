<?php
namespace ion\Viewport\RedI;

use Exception;
use ion\Viewport\RedI\FeedSettings;
use ion\WordPress\WordPressHelper as WP;
/**
 * Description of FeedParser
 *
 * @author Justus
 */
abstract class SalesMapFeed extends Feed
{
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct(FeedSettings $feedSettings, $templateName, array $uriTags, $suffix = null)
    {
        parent::__construct($feedSettings, trim($feedSettings->GetBaseUri(), "/") . "/" . trim(FeedSettings::ApplyTemplate($feedSettings->GetUriTemplate($templateName), $uriTags), "/") . ($suffix === null ? "" : "/" . trim($suffix, "/")));
        //echo $this->uri . "<br />\n";
    }
}