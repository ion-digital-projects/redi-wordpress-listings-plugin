<?php
namespace ion\Viewport\RedI;

/**
 * Description of ViewModel
 *
 * @author Justus
 */
abstract class ViewModel extends Model
{
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct(array $data = null)
    {
        parent::__construct($data);
    }
}