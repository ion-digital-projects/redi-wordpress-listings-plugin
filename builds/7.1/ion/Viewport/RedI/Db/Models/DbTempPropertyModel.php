<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Viewport\RedI\Db\Models;

/**
 * Description of DbTempPropertyModel
 *
 * @author Justus
 */
use ion\Viewport\RedI\ViewModels\PropertyViewModel;
class DbTempPropertyModel extends DbPropertyModel
{
    /**
     * method
     * 
     * @return mixed
     */
    protected static function GetDefaultTableName()
    {
        return parent::GetDefaultTableName() . '_import';
    }
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct(PropertyViewModel $viewModel)
    {
        parent::__construct($viewModel, 'import');
    }
}