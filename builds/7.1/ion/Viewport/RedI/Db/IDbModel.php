<?php
namespace ion\Viewport\RedI\Db;

/**
 *
 * @author Justus
 */
interface IDbmodel
{
    /**
     * method
     * 
     * @return mixed
     */
    static function GetSchema();
    /**
     * method
     * 
     * @return mixed
     */
    static function GetTableName();
    /**
     * method
     * 
     * @return mixed
     */
    static function GetInsertFieldSql();
    /**
     * method
     * 
     * @return mixed
     */
    function Exists();
    /**
     * method
     * 
     * @return mixed
     */
    function Insert();
}