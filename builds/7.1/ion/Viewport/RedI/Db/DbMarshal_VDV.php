<?php
namespace ion\Viewport\RedI\Db;

/**
 * Description of DbMarshal
 *
 * @author Justus
 */
use ion\PhpHelper as PHP;
use ion\WordPress\WordPressHelper as WP;
use ion\Viewport\RedI\FeedSettings;
use ion\Viewport\RedI\State;
use ion\Viewport\RedI\ViewModels\PropertyViewModel;
use ion\Viewport\RedI\ViewModels\DevelopmentViewModel;
use ion\Viewport\RedI\Marshal;
use ion\Viewport\RedI\Feeds\Models\Development;
class DbMarshal extends Marshal
{
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function Fetch(FeedSettings $feedSettings, State $state, $ignoreFilters = false)
    {
        $result = ['developments' => [], 'properties' => [], 'values' => []];
        static::$totalRecords = 0;
        $sql = "";
        //$developments = [];
        if ($state->IsForUs()) {
            //$feedResult = null;
            if ($state->GetEstate() !== null) {
                global $wpdb;
                $propTableName = $wpdb->prefix . 'redi_properties';
                $devTableName = $wpdb->prefix . 'redi_developments';
                $estate = WP::GetOption('redi-feed-estate', null);
                if ($estate !== null) {
                    // Values
                    foreach (['bathrooms', 'bedrooms', 'garages'] as $value) {
                        $tmp = WP::dbQuery("SELECT MAX({$value}) FROM {$propTableName}", null, false);
                        $result['values'][$value] = intval(count($tmp) === 0 ? null : (count($tmp[0]) > 0 ? $tmp[0][0] : null));
                    }
                    $tmp = WP::dbQuery("SELECT MIN(minPrice) FROM {$propTableName} WHERE minPrice > 0", null, false);
                    $result['values']['minPrice'] = intval(count($tmp) === 0 ? null : (count($tmp[0]) > 0 ? $tmp[0][0] : null));
                    $tmp = WP::dbQuery("SELECT MAX(maxPrice) FROM {$propTableName} WHERE maxPrice > 0", null, false);
                    $result['values']['maxPrice'] = intval(count($tmp) === 0 ? null : (count($tmp[0]) > 0 ? $tmp[0][0] : null));
                    // Developments
                    $tmp = null;
                    $sql = null;
                    // Reset variables.
                    $queryResult = WP::DbQuery("SELECT * FROM {$devTableName}");
                    if (count($queryResult) > 0) {
                        foreach ($queryResult as $row) {
                            //$developments[$row['development']] = new Development($feedSettings, $estate, $row['development'], null, false, true);
                            $devViewModel = new DevelopmentViewModel(['label' => $row['label'], 'code' => $row['development']]);
                            $result['developments'][] = $devViewModel;
                        }
                    }
                    // Properties
                    if ($state->GetDevelopment() !== null) {
                        $sql = "SELECT * FROM {$propTableName} WHERE development LIKE ('" . $state->GetDevelopment() . "')";
                    } else {
                        // We don't know which development to look for, so we have to load all estate data
                        $sql = "SELECT * FROM {$propTableName}";
                    }
                    if ($ignoreFilters === false) {
                        if ($state->GetFilter() !== null) {
                            $tmp = $state->GetFilter()->ToSql();
                            if ($tmp !== null) {
                                $sql = "SELECT * FROM ( {$sql} ) AS qd WHERE " . $tmp;
                            }
                        }
                    }
                    if ($state->GetStoredFilterName() !== null) {
                        $storedFilter = State::GetStoredFilter($state->GetStoredFilterName());
                        if ($storedFilter !== null) {
                            $sql = "SELECT * FROM ( {$sql} ) AS qd WHERE " . $storedFilter->ToSql();
                        }
                        //echo $storedFilter->ToSql(); exit;
                    }
                    //                    if(WP::getOption('redi-images-only', false) === true) {
                    //                        $b64 = base64_encode(serialize([]));
                    //                        $sql = "SELECT * FROM ( $sql ) AS qi WHERE `images` <> '$b64' AND `images` IS NOT NULL";
                    //                    }
                    if ($state->IsProperty() === false) {
                        if (WP::getOption('redi-images-only', false) === true) {
                            $sql = "SELECT * FROM ( {$sql} ) AS qi WHERE `numimages` > 0 AND `images` IS NOT NULL";
                        }
                        if (WP::getOption('redi-with-plans', false) === true) {
                            $sql = "SELECT * FROM ( {$sql} ) AS qe WHERE `numplans` > 1 AND `plans` IS NOT NULL";
                        }
                        if (WP::getOption('redi-with-plan-images', false) === true) {
                            $sql = "SELECT * FROM ( {$sql} ) AS qt WHERE `numplans` > 0 AND `numplanimages` > 0 AND `plans` IS NOT NULL";
                        }
                    }
                    if ($state->IsProperty() === true) {
                        //$sql = "SELECT * FROM ( $sql ) AS qa WHERE label LIKE ('" . $state->GetLabel() . "')";
                        $id = null;
                        $l = $state->GetLabel();
                        $d = $state->GetDevelopment() !== null ? $d = $state->GetDevelopment() : null;
                        $tmp = null;
                        if ($d === null) {
                            $tmp = WP::dbQuery("SELECT id FROM {$propTableName} WHERE label LIKE ('{$l}');");
                        } else {
                            $tmp = WP::dbQuery("SELECT id FROM {$propTableName} WHERE label LIKE ('{$l}') AND development LIKE ('{$d}');");
                        }
                        //var_dump($tmp);
                        //exit;
                        if (count($tmp) > 0) {
                            $id = intval($tmp[0]['id']);
                            $adjacentRecs = 1;
                            $currSql = <<<SQL
SELECT *
FROM ( SELECT * FROM ( {$sql} ) AS qx ) AS q
WHERE `id` = {$id}
LIMIT 1                                
SQL;
                            $tmp2 = WP::dbQuery($currSql);
                            //var_dump($tmp2);
                            if (is_array($tmp2) && count($tmp2) > 0) {
                                $sql = <<<SQL
SELECT *
FROM (

\tSELECT *
\tFROM ( SELECT * FROM ( {$sql} ) AS qx ) AS q
\tWHERE `id` < {$id}
\tORDER BY `id` DESC
\tLIMIT {$adjacentRecs}
\t
) AS prev

UNION

SELECT *
FROM (

{$currSql}
\t
) AS curr

UNION

SELECT *
FROM (

\tSELECT *
\tFROM ( SELECT * FROM ( {$sql} ) AS qx ) AS q
\tWHERE `id` > {$id}
\tORDER BY `id` ASC
\tLIMIT {$adjacentRecs}
\t
) AS next                
SQL;
                                static::$totalRecords = 1;
                            }
                        }
                    } else {
                        if ($state->IsFront() === true || $state->IsPropertyIndex()) {
                            $resCnt = WP::DbQuery("SELECT COUNT(*) AS cnt FROM ( {$sql} ) AS qc");
                            static::$totalRecords = intval($resCnt[0]['cnt']);
                            $pageSize = State::GetPageSize($state);
                            $page = 1;
                            if ($state->GetPage() !== null) {
                                $page = $state->GetPage();
                            }
                            if ($page < 1) {
                                $page = 1;
                            }
                            $offset = ($page - 1) * $pageSize;
                            $newPageSize = $pageSize;
                            if ($offset + $newPageSize > $resCnt) {
                                $newPageSize = $resCnt - $offset;
                            }
                            $sql = "SELECT * FROM ( {$sql} ) AS qb";
                            if ($state->GetOrderBy() === "price-low-to-high") {
                                $key = "minprice";
                                $sql .= " ORDER BY {$key} ASC";
                            } else {
                                if ($state->GetOrderBy() === "price-high-to-low") {
                                    $key = "minprice";
                                    $sql .= " ORDER BY {$key} DESC";
                                } else {
                                    if ($state->GetOrderBy() === "most-recent") {
                                        // Not provided by feed
                                    } else {
                                        if ($state->GetOrderBy() === "property-type") {
                                            $key = "propertyType";
                                            $sql .= " ORDER BY {$key} ASC";
                                        } else {
                                            if ($state->GetOrderBy() === "erf-size") {
                                                $key = "size";
                                                $sql .= " ORDER BY {$key} ASC";
                                            }
                                        }
                                    }
                                }
                            }
                            $sql .= " LIMIT {$pageSize} OFFSET {$offset}";
                        }
                    }
                    // echo "<pre>$sql</pre>"; exit;
                    $queryResult = WP::DbQuery($sql);
                    $properties = [];
                    if (count($queryResult) > 0) {
                        //                        echo "<pre>";
                        //                        echo $sql . "\n\n";
                        //                        var_dump($queryResult);
                        //                        echo "</pre>";
                        //                        exit;
                        foreach ($queryResult as $row) {
                            $dev = $this->FetchDevelopmentForProperty($feedSettings, $estate, $row['label'], $row['development']);
                            $property = new PropertyViewModel($dev, null, [
                                //$property = new PropertyViewModel(null, null, [
                                'label' => $row['label'],
                                'title' => $row['title'],
                                'subTitle' => $row['subtitle'],
                                'unitNumber' => $row['unitnumber'],
                                'maxPrice' => $row['maxprice'],
                                'minPrice' => $row['minprice'],
                                'plotPrice' => $row['plotprice'],
                                'propertyStatus' => $row['propertystatus'],
                                'propertyType' => $row['propertytype'],
                                'phase' => $row['phase'],
                                'size' => $row['size'],
                                'description' => $row['description'],
                                'mapLink' => $row['maplink'],
                                'bathrooms' => $row['bathrooms'],
                                'bedrooms' => $row['bedrooms'],
                                'garages' => $row['garages'],
                                'plans' => unserialize(base64_decode($row['plans'])),
                                'images' => unserialize(base64_decode($row['images'])),
                                'contactForm' => WP::getOption('redi-debug-mode', false) === false ? base64_decode($row['forms']) : null,
                                'agents' => unserialize(base64_decode($row['propertyagents'])),
                                'showTimes' => unserialize(base64_decode($row['showtimes'])),
                                'agencies' => unserialize(base64_decode($row['agencies'])),
                                'agencyOverrides' => unserialize(base64_decode($row['agencyoverrides'])),
                                'rentalAmount' => $row['rentalamount'] === null ? null : (double) floatval($row['rentalamount']),
                                'selectedPlanType' => $row['selected_plan_type'] === null ? null : $row['selected_plan_type'],
                                'planRequired' => $row['plan_required'] === null ? null : (bool) boolval($row['plan_required']),
                                'hidePrices' => $dev->Get('hide_prices') === null ? null : (bool) $dev->Get('hide_prices'),
                                'hidePlanPrices' => $dev->Get('hide_plan_prices') === null ? null : (bool) $dev->Get('hide_plan_prices'),
                            ]);
                            //                            echo "<pre>";
                            //                            var_dump($property);
                            //                            die("</pre>");
                            $properties[] = $property;
                        }
                        //echo "<pre>";
                        //foreach($properties as $p) {
                        //    echo $p->GetLabel() . ' ' . $p->GetDevelopment()->GetName() . "\n";
                        //    print_r($p);
                        //}
                        //echo "</pre>";
                        //exit;
                        if ($ignoreFilters === false) {
                            if ($state->GetFilter() !== null) {
                                $result['properties'] = $state->GetFilter()->ApplyTo($properties);
                            } else {
                                $result['properties'] = $properties;
                            }
                        } else {
                            $result['properties'] = $properties;
                        }
                        //static::$totalRecords = count($result['properties']);
                    }
                }
            }
        }
        //die("<pre>$sql</pre>");
        return $result;
    }
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function FetchDevelopmentForProperty(FeedSettings $feedSettings, $estateName, $label, $development = null)
    {
        global $wpdb;
        $propTableName = $wpdb->prefix . 'redi_properties';
        $devTableName = $wpdb->prefix . 'redi_developments';
        $propQueryResult = null;
        if ($development === null) {
            $propQueryResult = WP::DbQuery("SELECT * FROM {$propTableName} WHERE label LIKE ('{$label}') LIMIT 1;");
            $development = $propQueryResult[0]['development'];
        } else {
            $propQueryResult = WP::DbQuery("SELECT * FROM {$propTableName} WHERE label LIKE ('{$label}') AND development LIKE ('{$development}') LIMIT 1;");
        }
        $devQueryResult = WP::dbQuery("SELECT * FROM {$devTableName} WHERE development LIKE ('{$development}') LIMIT 1;");
        //die($queryResult[0]['development']);
        if (count($propQueryResult) > 0) {
            return new Development($feedSettings, $estateName, $development, count($devQueryResult) > 0 ? $devQueryResult[0] : null, false, true);
        }
        return null;
    }
}