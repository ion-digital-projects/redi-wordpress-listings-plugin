<?php
namespace ion\Viewport\RedI\Feeds\Models;

/**
 * Description of Agent
 *
 * @author Justus
 */
use ion\Viewport\RedI\Model;
class Agent extends Model
{
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct(array $data)
    {
        parent::__construct($data);
    }
    // string
    /**
     * method
     * 
     * @return mixed
     */
    public function GetFirstName()
    {
        return $this->Get("firstName");
    }
    // string
    /**
     * method
     * 
     * @return mixed
     */
    public function GetLastName()
    {
        return $this->Get("lastName");
    }
    // string
    /**
     * method
     * 
     * @return mixed
     */
    public function GetEmailAddress()
    {
        return $this->Get("emailAddress");
    }
    // string
    /**
     * method
     * 
     * @return mixed
     */
    public function GetMobileNumber()
    {
        return $this->Get("mobileNumber");
    }
    // string
    /**
     * method
     * 
     * @return mixed
     */
    public function GetPhotoUrl()
    {
        return $this->Get("photoUrl");
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function GetAgencies()
    {
        return $this->Get("agencies");
    }
}