<?php
namespace ion\Viewport\RedI\Feeds\Models;

/**
 * Description of Gallery
 *
 * @author Justus
 */
use ion\Viewport\RedI\Model;
use ion\Viewport\RedI\FeedSettings;
use ion\Viewport\RedI\Feeds\Models\EnquiryFormFields;
use ion\Viewport\RedI\Feeds\EnquiryFormFieldsFeed;
class Enquiries extends Model
{
    private $items;
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct(FeedSettings $feedSettings, Development $development, array $data)
    {
        parent::__construct($data);
        $this->items = [];
        foreach ($data as $obj) {
            $enquiry = new Enquiry($feedSettings, $obj);
            $this->items[] = $enquiry;
        }
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function GetItems()
    {
        return $this->items;
    }
}