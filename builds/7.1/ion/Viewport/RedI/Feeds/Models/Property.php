<?php
namespace ion\Viewport\RedI\Feeds\Models;

/**
 * Description of Properties
 *
 * @author Justus
 */
use ion\Viewport\RedI\Model;
use ion\Viewport\RedI\Filter;
use ion\PhpHelper as PHP;
class Property extends Model
{
    private $planTypes;
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct(array &$data, PlanTypes $planTypes = null, Agents $propertyAgents = null)
    {
        parent::__construct($data);
        $this->planTypes = $planTypes;
        $this->agents = $propertyAgents;
        $images = [];
        foreach ($data["media"] as $image) {
            $images[] = new Image($image);
        }
        $this->Set("media", $images);
        $showTimes = [];
        foreach ($data["showTimes"] as $showTime) {
            $showTimes[] = new ShowTime($showTime);
        }
        $this->Set("showTimes", $showTimes);
        // Plan Types
        $propertyPlanTypes = [];
        if ($this->GetAvailablePlanTypes() !== null && $this->planTypes !== null) {
            foreach ($this->GetAvailablePlanTypes() as $planTypeLabel) {
                //$result[] = $planTypeLabel;
                //echo $planTypeLabel;
                $filter = new Filter(["label" => $planTypeLabel]);
                $arr = $filter->ApplyTo($this->planTypes->GetItems());
                if (count($arr) > 0) {
                    $propertyPlanTypes[] = $arr[0];
                }
            }
        }
        $this->Set('planTypes', $propertyPlanTypes);
        // Agents
        $propertyAgents = [];
        if ($this->GetAgents() !== null) {
            if (PHP::count($this->GetAgents()) > 0) {
                foreach ($this->GetAgents() as $agentName) {
                    $space = strpos($agentName, " ");
                    $agentFirstName = trim(substr($agentName, 0, $space));
                    $agentLastName = trim(substr($agentName, $space, strlen($agentName) - $space));
                    $filter = new Filter(["firstName" => $agentFirstName, "lastName" => $agentLastName]);
                    $arr = $filter->ApplyTo($this->agents->GetItems());
                    if (PHP::count($arr) > 0) {
                        if (PHP::count($arr) > 1) {
                        } else {
                            $propertyAgents[] = $arr[0];
                        }
                    }
                }
            }
        }
        $this->Set('propertyAgents', $propertyAgents);
        $agencyOverrides = [];
        foreach ($data["agencyOverrides"] as $override) {
            $agencyOverrides[] = new AgencyOverride($override);
        }
        $this->Set("agencyOverrides", $agencyOverrides);
        $agencies = $data['agencies'];
        $this->Set("agencies", $agencies);
        //        $this->Set("selectedPlanType", $);
        //        $this->Set("planRequired", $);
        //        if($this->GetLabel() === '4220' /* &&  $this->GetLabel() === '4214' */) {
        //            echo '<pre>';
        //            print_r($this->ToArray());
        //            echo '</pre>';
        //            exit;
        //        }
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function GetPropertyPlanTypes()
    {
        return $this->Get('planTypes');
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function GetPropertyAgents()
    {
        return $this->Get('propertyAgents');
    }
    // string
    /**
     * method
     * 
     * @return mixed
     */
    public function GetLabel()
    {
        return $this->Get("label");
    }
    // string
    /**
     * method
     * 
     * @return mixed
     */
    public function GetPhase()
    {
        return $this->Get("phase");
    }
    // ?
    /**
     * method
     * 
     * @return mixed
     */
    public function GetErf()
    {
        return $this->Get("erf");
    }
    // ?
    /**
     * method
     * 
     * @return mixed
     */
    public function GetUnit()
    {
        return $this->Get("unit");
    }
    // ?
    /**
     * method
     * 
     * @return mixed
     */
    public function GetAddress()
    {
        return $this->Get("address");
    }
    // string
    /**
     * method
     * 
     * @return mixed
     */
    public function GetDescription()
    {
        return $this->Get("description");
    }
    // string
    /**
     * method
     * 
     * @return mixed
     */
    public function GetPropertyType()
    {
        return $this->Get("propertyType");
    }
    // string
    /**
     * method
     * 
     * @return mixed
     */
    public function GetSaleStatus()
    {
        return $this->Get("saleStatus");
    }
    // numeric
    /**
     * method
     * 
     * @return mixed
     */
    public function GetPlotSize()
    {
        return $this->Get("plotSize");
    }
    // numeric
    /**
     * method
     * 
     * @return mixed
     */
    public function GetPlotPrice()
    {
        return $this->Get("plotPrice");
    }
    // numeric
    /**
     * method
     * 
     * @return mixed
     */
    public function GetMinTotalPrice()
    {
        return $this->Get("minTotalPrice");
    }
    // numeric
    /**
     * method
     * 
     * @return mixed
     */
    public function GetMaxTotalPrice()
    {
        return $this->Get("maxTotalPrice");
    }
    // boolean
    /**
     * method
     * 
     * @return mixed
     */
    public function GetPlanRequired()
    {
        return $this->Get("planRequired");
    }
    // string
    /**
     * method
     * 
     * @return mixed
     */
    public function GetSelectedPlanType()
    {
        return $this->Get("selectedPlanType");
    }
    // array -> string
    /**
     * method
     * 
     * @return mixed
     */
    public function GetAvailablePlanTypes()
    {
        return $this->Get("availablePlanTypes");
    }
    // array -> { thumb, url, caption }
    /**
     * method
     * 
     * @return mixed
     */
    public function GetMedia()
    {
        return $this->Get("media");
    }
    // array -> ?
    /**
     * method
     * 
     * @return mixed
     */
    public function GetAgents()
    {
        return $this->Get("agents");
    }
    // array -> ?
    /**
     * method
     * 
     * @return mixed
     */
    public function GetAgencies()
    {
        return $this->Get("agencies");
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function GetShowTimes()
    {
        return $this->Get("showTimes");
    }
    // array -> ?
    /**
     * method
     * 
     * @return mixed
     */
    public function GetAgencyOverrides()
    {
        return $this->Get("agencyOverrides");
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function GetRentalAmount()
    {
        return $this->Get("rentalAmount");
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function getListingCategory()
    {
        return $this->Get("listingCategory");
    }
}