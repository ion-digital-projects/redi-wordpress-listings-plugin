<?php
namespace ion\Viewport\RedI\Feeds\Models;

/**
 * Description of Enquiry
 *
 * @author Justus
 */
use ion\Viewport\RedI\FeedSettings;
use ion\Viewport\RedI\Model;
use ion\Viewport\RedI\Feeds\Models\Development;
use ion\Viewport\RedI\Feeds\EnquiryFormFieldsFeed;
class Enquiry extends Model
{
    private $fields;
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct(FeedSettings $feedSettings, array $data = null)
    {
        parent::__construct($data);
        $this->fields = null;
        if (strtolower($this->GetType()) == 'embeddable') {
            $this->embeddedFormUrl = $this->GetGet();
        } else {
            $formFeed = new EnquiryFormFieldsFeed($feedSettings, $this->GetGet());
            //            die("FORM");
            $this->fields = $formFeed->Fetch(true);
        }
    }
    // string
    /**
     * method
     * 
     * @return mixed
     */
    public function GetType()
    {
        return $this->Get("type");
    }
    // string
    /**
     * method
     * 
     * @return mixed
     */
    public function GetPost()
    {
        return $this->Get("post");
    }
    // string
    /**
     * method
     * 
     * @return mixed
     */
    public function GetGet()
    {
        return $this->Get("get");
    }
    /**
     * method
     * 
     * @return mixed
     */
    public function GetFields()
    {
        return $this->fields;
    }
}