<?php
namespace ion\Viewport\RedI\Feeds\Models;

/**
 * Description of Agent
 *
 * @author Justus
 */
use ion\Viewport\RedI\Model;
class Agency extends Model
{
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct(array $data)
    {
        parent::__construct($data);
    }
    // string
    /**
     * method
     * 
     * @return mixed
     */
    public function GetName()
    {
        return $this->Get("name");
    }
    // string
    /**
     * method
     * 
     * @return mixed
     */
    public function GetDescription()
    {
        return $this->Get("description");
    }
    // string
    /**
     * method
     * 
     * @return mixed
     */
    public function GetEmailAddress()
    {
        return $this->Get("emailAddress");
    }
    // string
    /**
     * method
     * 
     * @return mixed
     */
    public function GetOfficeNumber()
    {
        return $this->Get("officeNumber");
    }
    // string
    /**
     * method
     * 
     * @return mixed
     */
    public function GetPhotoUrl()
    {
        return $this->Get("photoUrl");
    }
}