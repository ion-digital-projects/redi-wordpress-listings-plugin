
-- wp_7mqjhd

CREATE TABLE `wp_7mqjhd_redi_properties_import` (
  `id` int(11) NOT NULL,
  `import_id` varchar(250) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `batch_id` int(11) DEFAULT NULL,
  `development` varchar(1000) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `label` varchar(16) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `title` varchar(1000) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `subtitle` varchar(1000) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `unitnumber` int(11) DEFAULT NULL,
  `plotprice` decimal(10,0) DEFAULT NULL,
  `maxprice` decimal(10,0) DEFAULT NULL,
  `minprice` decimal(10,0) DEFAULT NULL,
  `rentalamount` decimal(10,0) DEFAULT NULL,
  `propertystatus` varchar(250) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `propertytype` varchar(250) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `phase` varchar(250) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_520_ci,
  `maplink` varchar(1000) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `bathrooms` int(11) DEFAULT NULL,
  `bedrooms` int(11) DEFAULT NULL,
  `garages` int(11) DEFAULT NULL,
  `plans` longtext COLLATE utf8mb4_unicode_520_ci,
  `images` longtext COLLATE utf8mb4_unicode_520_ci,
  `forms` text COLLATE utf8mb4_unicode_520_ci,
  `agencies` longtext COLLATE utf8mb4_unicode_520_ci,
  `agency_lookup` varchar(5000) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `propertyagents` longtext COLLATE utf8mb4_unicode_520_ci,
  `agencyoverrides` longtext COLLATE utf8mb4_unicode_520_ci,
  `showtimes` longtext COLLATE utf8mb4_unicode_520_ci,
  `numimages` int(11) DEFAULT NULL,
  `numplans` int(11) DEFAULT NULL,
  `numplanimages` int(11) DEFAULT NULL,
  `plan_required` int(11) DEFAULT NULL,
  `selected_plan_type` varchar(1000) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `time_imported` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

ALTER TABLE `wp_7mqjhd_redi_properties_import`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `wp_7mqjhd_redi_properties_import`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- == --

CREATE TABLE `wp_7mqjhd_redi_developments` (
  `id` int(11) NOT NULL,
  `import_id` varchar(250) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `batch_id` int(11) DEFAULT NULL,
  `development` varchar(5000) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `label` varchar(1000) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `import_start_time` datetime DEFAULT NULL,
  `import_end_time` datetime DEFAULT NULL,
  `hide_prices` int(11) DEFAULT NULL,
  `hide_plan_prices` int(11) DEFAULT NULL,
  `agencies` longtext COLLATE utf8mb4_unicode_520_ci,
  `errors` varchar(5000) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

ALTER TABLE `wp_7mqjhd_redi_developments`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `wp_7mqjhd_redi_developments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- == --

CREATE TABLE `wp_7mqjhd_redi_developments_import` (
  `id` int(11) NOT NULL,
  `import_id` varchar(250) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `batch_id` int(11) DEFAULT NULL,
  `development` varchar(5000) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `label` varchar(1000) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `import_start_time` datetime DEFAULT NULL,
  `import_end_time` datetime DEFAULT NULL,
  `hide_prices` int(11) DEFAULT NULL,
  `hide_plan_prices` int(11) DEFAULT NULL,
  `agencies` longtext COLLATE utf8mb4_unicode_520_ci,
  `errors` varchar(5000) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

ALTER TABLE `wp_7mqjhd_redi_developments_import`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `wp_7mqjhd_redi_developments_import`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- == --

CREATE TABLE `wp_7mqjhd_redi_properties` (
  `id` int(11) NOT NULL,
  `import_id` varchar(250) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `batch_id` int(11) DEFAULT NULL,
  `development` varchar(1000) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `label` varchar(16) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `title` varchar(1000) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `subtitle` varchar(1000) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `unitnumber` int(11) DEFAULT NULL,
  `plotprice` decimal(10,0) DEFAULT NULL,
  `maxprice` decimal(10,0) DEFAULT NULL,
  `minprice` decimal(10,0) DEFAULT NULL,
  `rentalamount` decimal(10,0) DEFAULT NULL,
  `propertystatus` varchar(250) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `propertytype` varchar(250) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `phase` varchar(250) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_520_ci,
  `maplink` varchar(1000) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `bathrooms` int(11) DEFAULT NULL,
  `bedrooms` int(11) DEFAULT NULL,
  `garages` int(11) DEFAULT NULL,
  `plans` longtext COLLATE utf8mb4_unicode_520_ci,
  `images` longtext COLLATE utf8mb4_unicode_520_ci,
  `forms` text COLLATE utf8mb4_unicode_520_ci,
  `agencies` longtext COLLATE utf8mb4_unicode_520_ci,
  `agency_lookup` varchar(5000) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `propertyagents` longtext COLLATE utf8mb4_unicode_520_ci,
  `agencyoverrides` longtext COLLATE utf8mb4_unicode_520_ci,
  `showtimes` longtext COLLATE utf8mb4_unicode_520_ci,
  `numimages` int(11) DEFAULT NULL,
  `numplans` int(11) DEFAULT NULL,
  `numplanimages` int(11) DEFAULT NULL,
  `plan_required` int(11) DEFAULT NULL,
  `selected_plan_type` varchar(1000) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `time_imported` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

ALTER TABLE `wp_7mqjhd_redi_properties`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `wp_7mqjhd_redi_properties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;







