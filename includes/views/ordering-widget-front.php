<?php

use \ion\WordPress\WordPressHelper as WP;
use \ion\Viewport\RedI\RedIFeedPlugIn AS RedI;
use \ion\Viewport\RedI\State;

$redI = RedI::GetInstance();

?>

<div class="orderby">
    <span>Order by: </span>
    <select id="orderBy_widget_select">
<?php
$values = [      
    "Sort by" => "",
    "Price - low to high" => "price-low-to-high",
    "Price - high to low" => "price-high-to-low",
    "Property type" => "property-type",
    "Erf Size" => "erf-size"        
];

$selected = filter_input(INPUT_GET, "sort", FILTER_DEFAULT, FILTER_NULL_ON_FAILURE);
foreach($values as $key => $value) {
    echo "<option value=\"" . $value . "\"" . ($selected !== null && $selected === $value ? " selected" : "") . ">" . $key . "</option>";
}

?>
    </select>
</div>

<script type="text/javascript">
    jQuery("#orderBy_widget_select").change(function() {
        
        var url = window.location.href;
        var queryChar = "?";
        
        if(url.indexOf("?") > -1)
            queryChar = "&";
        
        var val = jQuery("#orderBy_widget_select").val();        
        
        var patt = /(\S+)[\?|&]?(sort=[^&]+)&?(\S*)/i;  
        
        if(url.search(patt) !== -1) {
        
            var res = patt.exec(url);
            
            //console.dir(res);
            
            if(res.length > 2) {
                url = res[1];
                
                if(val.length > 0) {
                    url +=  "sort=" + val;
                    
                    if(res.length > 2) url += queryChar + res[3];
                }
                else {
                    if(res.length > 2) url += res[3];
                }
            } else {
                
                url = res[1];
            }
            
        } else {
            if(val.length > 0)
                url = window.location.href + queryChar + "sort=" + val;
        }
        
        window.location.href = url;
    });
</script>