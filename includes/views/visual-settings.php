<?php


use \ion\WordPress\WordPressHelper as WP;
use \ion\Viewport\RedI\ViewModels\PropertyViewModel;
use \ion\Viewport\RedI\RedIFeedPlugIn as RedI;
use \ion\Viewport\RedI\State;

$templates = WP::getContext('redi/redi-plugin')->getTemplates(false, false, true, 'Theme default');

$form = WP::addRediAdminForm("Visual Settings", 'redi-visual-settings')        
        
        ->addGroup("Data")
        
            ->addField(WP::checkBoxInputField("Images only", "redi-images-only", null, null, "Show properties that have images only."))        
            ->addField(WP::checkBoxInputField("With multiple plans", "redi-with-plans", null, null, "Show properties that have multiple plans available only."))        
            ->addField(WP::checkBoxInputField("With plan images", "redi-with-plan-images", null, null, "Show properties that have plan images only."))        
        
            ->addField(WP::textInputField("Feed Front Page Items", "redi-feed-front-page-items", null, null, "The maximum amount of items to display on a feed front page."))
            ->addField(WP::textInputField("Feed Index Page Items", "redi-feed-index-page-items", null, null, "The maximum amount of items to display on a feed index pages."))
            ->addField(WP::dropDownListInputField("Default filter", State::GetStoredFilters(true, true) , "redi-default-filter", null, null, "The default filter to use if none has been specified ."))        
            ->addField(WP::dropDownListInputField("Front page filter", State::GetStoredFilters(true, true) , "redi-front-page-filter", null, null, "The filter to use for the front page - if none specified, the default filter will be used."))        
        
        ->addGroup("Templates")
        
            ->addField(WP::dropDownListInputField("Front page template", $templates, "redi-front-template", null, null, "The theme template to use when displaying the front page (overrides WordPress)."))
            ->addField(WP::dropDownListInputField("Property index template (tiles)", $templates, "redi-index-template", null, null, "The theme template to use when displaying multiple properties (in tile format)."))
            ->addField(WP::dropDownListInputField("Property index template (list)", $templates, "redi-list-template", null, null, "The theme template to use when displaying multiple properties (in list format)."))
            ->addField(WP::dropDownListInputField("Property template", $templates, "redi-single-template", null, null, "The theme template to use when displaying a single property."))        
            ->addField(WP::dropDownListInputField("Property index template (tiles/short-code)", $templates, "redi-shortcode-index-tiles-template", null, null, "The theme template to use when displaying multiple properties via short-code <strong>[redi-index-tiles]</strong> (in tile format)."))        
            ->addField(WP::dropDownListInputField("Property index template (list/short-code)", $templates, "redi-shortcode-index-list-template", null, null, "The theme template to use when displaying multiple properties via short-code <strong>[redi-index-list]</strong> (in list format)."))        
            ->addField(WP::dropDownListInputField("Site specific CSS", RedI::getSiteSpecificCss(), "redi-site-specific-css", null, null, "A site specific CSS file to load."))        

//        ->addGroup("SEO")

            ->addField(WP::checkBoxInputField("Show H1 development / filter titles", "redi-h1-titles", null, null, "Show H1 titles for developments and filters."))
        
            ->addField(WP::checkBoxInputField("Show development", "redi-show-development", null, null, "Show the development name for indexes and single listings."))
            ->addField(WP::checkBoxInputField("Show phase / suburb", 'redi-show-phase', null, null, "Show the phase / suburb for indexes and single listings."))                   
            ->addField(WP::checkBoxInputField("Show unit number", "redi-show-unit-number", null, null, "Show the unit number for indexes and single listings."))
        
            
        
            ->addField(WP::checkBoxInputField("Override page title", "redi-override-title", null, null, "Override the page &lt;title /&gt; tag in templates."))
        
            ->addField(WP::textInputField("Developments plural label", "redi-development-string-plural", null, null, "The plural word to use for developments - defaults to 'Developments'"))
            ->addField(WP::textInputField("Developments singular label", "redi-development-string-singular", null, null, "The singular word to use for developments - defaults to 'Development'"))
        
        ->addGroup("Filter Widget")
        
            ->addField(WP::checkBoxInputField("Show 'Bedrooms' filter", 'redi-filter-widget-bedrooms-enabled', null, null, "Show the 'bedrooms' filter on the filter widget."))
            ->addField(WP::checkBoxInputField("Show 'Bathrooms' filter", 'redi-filter-widget-bathrooms-enabled', null, null, "Show the 'bathrooms' filter on the filter widget."))
            ->addField(WP::checkBoxInputField("Show 'Garages' filter", 'redi-filter-widget-garages-enabled', null, null, "Show the 'garages' filter on the filter widget."))
            ->addField(WP::checkBoxInputField("Show 'Status' filter", 'redi-filter-widget-status-enabled', null, null, "Show the 'status' filter on the filter widget."))
            ->addField(WP::checkBoxInputField("Show 'Type' filter", 'redi-filter-widget-type-enabled', null, null, "Show the 'property type' filter on the filter widget."))
            ->addField(WP::checkBoxInputField("Show 'Phase' filter", 'redi-filter-widget-phase-enabled', null, null, "Show the 'property phase' filter on the filter widget."))
            ->addField(WP::checkBoxInputField("Show 'Developments' filter", 'redi-filter-widget-developments-enabled', null, null, "Show the 'developments' filter on the filter widget."))
        
        ->addGroup("Currency Format")
        
            ->addField(WP::textInputField("Currency symbol", "redi-currency-symbol", null, null, "The currency symbol to use."))
            ->addField(WP::checkBoxInputField("Currency symbol as suffix", "redi-currency-symbol-suffix", null, null, "Show the currency symbol after the amount."))
            ->addField(WP::textInputField("Thousands seperator", "redi-currency-thousands-seperator", null, null, "The symbol to use to seperate thousands."))
;


echo $form->render();

