<?php

use \ion\WordPress\WordPressHelper as WP;

WP::addAdminTable("Filter Settings", "filter-settings", "Filter", "Filters", "slug", WP::getContext('redi/redi-plugin')->getView('filter-settings-detail'), true, true, true, null, false)
        
        //->addColumn(WP::textTableColumn("ID", "id", "id"))
        
        ->addColumn(WP::textTableColumn("Description", "description", "description"))        
        ->addColumn(WP::textTableColumn("Slug", "slug", "slug"))                
        ->addColumn(WP::textTableColumn("URL", "url", "url"))
        
        ->onRead(function() {
            
            $records = WP::getOption('redi-filters');

            //echo "<pre>";
            //var_dump($records);
            //echo "</pre>";
            
            if($records === null) {
                $records = [];
            }
            
            foreach($records as &$record) {
                $url = WP::siteLink([ $record['slug'] ], null, true, false);
                $record['url'] = '<a href="' . $url . '" target="_blank">' . $url . 
                        ' </a><br /><br /><strong>or </strong><em>' . $url . 
                        '/{development slug e.g. waterfall-crescent}</em>';
            }
            
            return $records;
        })
        ->onDelete(function(array $indexes) {
            
            $records = WP::getOption('redi-filters');
            
            if($records === null) {
                $records = [];
            }
            
            foreach($indexes as $index) {
                if(array_key_exists($index, $records)) {
                    unset($records[$index]);
                }
            }
            
            WP::setOption('redi-filters', $records);            
            
        })
        ->render();

