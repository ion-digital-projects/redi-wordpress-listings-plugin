<?php

use \ion\WordPress\WordPressHelper as WP;


$baseUri = "http://salesmap-demo.red-i.co.za";
$estateSlug = "waterfall";
$estateUriTemplate = "/{estate}/ws/listing/developments/";
$developmentUriTemplate = "/{estate}/ws/listing/developments/{development}/";
$feedUriTemplate = "/{estate}/ws/listing/developments/{development}/";
$planTypesUriTemplate = "/{estate}/ws/listing/developments/{development}/planTypes/";
$galleriesUriTemplate = "/{estate}/ws/listing/developments/{development}/gallery/";
$propertiesUriTemplate = "/{estate}/ws/listing/developments/{development}/properties/";
$agentsUriTemplate = "/{estate}/ws/listing/developments/{development}/agents/";
$enquiriesUriTemplate = "/{estate}/ws/listing/developments/{development}/enquiries/";
$agenciesUriTemplate = "/{estate}/ws/listing/developments/{development}/agencies/";


//https://www.salesmap.co.za/waterfall/ws/listing/analytics/record?source=Waterfall%20Website&development=waterfall-country-village&property=834&event=Listing%20Click&session=76252
$analyticsUriTemplate = "/{estate}/ws/listing/analytics/record?source={site}&development={development}&property={property}&event={event}&session={session}";

$cronIntervals = WP::getCronIntervals(true);

$form = WP::addRediAdminForm("Feed Settings", 'redi-feed-settings')
        
        ->addField(WP::checkBoxInputField("Disable imports", "redi-disable-imports", null, null, "Turns on/off imports."))
        
        ->addField(WP::textInputField("Base Feed URI","redi-feed-base-uri", null, null, "The base URI of the JSON feed (e.g. <em>$baseUri</em>)."))
        
        ->addField(WP::textInputField("Estate Feed Path Template","redi-feed-estate-path-template", null, null, "The path template for the estate (e.g. <em>$estateUriTemplate</em>)."))
        ->addField(WP::textInputField("Development Feed Path Template","redi-feed-development-path-template", null, null, "The path template for estate developments (e.g. <em>$developmentUriTemplate</em>)."))        

        ->addField(WP::textInputField("Properties Feed Path Template","redi-feed-properties-path-template", null, null, "The path template for properties (e.g. <em>$propertiesUriTemplate</em>)."))
        ->addField(WP::textInputField("Plan Types Feed Path Template","redi-feed-plan-types-path-template", null, null, "The path template for plan types (e.g. <em>$planTypesUriTemplate</em>)."))
        ->addField(WP::textInputField("Gallery Feed Path Template","redi-feed-gallery-path-template", null, null, "The path template for galleries (e.g. <em>$galleriesUriTemplate</em>)."))
        ->addField(WP::textInputField("Agents Feed Path Template","redi-feed-agents-path-template", null, null, "The path template for agents (e.g. <em>$agentsUriTemplate</em>)."))
        ->addField(WP::textInputField("Agencies Feed Path Template","redi-feed-agencies-path-template", null, null, "The path template for agencies (e.g. <em>$agenciesUriTemplate</em>)."))
        ->addField(WP::textInputField("Enquiries Feed Path Template","redi-feed-enquiries-path-template", null, null, "The path template for enquiry forms (e.g. <em>$enquiriesUriTemplate</em>)."))
        
        //->addField(WP::textInputField("Analytics End-Point Path Template","redi-analytics-endpoint-path-template", null, null, "The path for registering analytics events (e.g. <em>$analyticsUriTemplate</em>)."))

        ->addField(WP::textInputField("Feed Estate Name","redi-feed-estate", null, null, "The name of the estate for use in the URI (e.g. <em>$estateSlug</em>)."))
        //->addField(WP::textInputField("Feed Delay","redi-feed-delay", null, null, "The minimum amount of <b>minutes</b> to wait before fetching the feed (after the last fetch). Use <b>0</b> for real-time."))        

        //->addField(WP::CheckBoxInputField("Use database","redi-feed-use-database", null, null, "Use the database as a lookup (this will enable a WP-Cron task to import data from the deed into lookup tables)."))        
        
        ->addField(WP::textInputField("General enquiries email","redi-contact-email", null, null, "The email address to use to send queries when no agents have been linked to a property."))
		
		->addField(WP::textInputField("Feed prefix","redi-feed-prefix", null, null, "The prefix to the feed URI (defaults to 'red-i')."))
        
        ->addField(WP::timeInputField("CRON import start","redi-import-time", null, null, "The time to import the feed (based on the interval)."))
        
        ->addField(WP::dropDownListInputField("CRON import interval", $cronIntervals, "redi-import-interval", null, null, "The frequency with which to import the feed."))
        
        ->addField(WP::checkBoxInputField("Incremental downloads", "redi-incremental-downloads", null, null, "Schedule feed data retrieval per development instead of retrieving all data in one session."))
        
        ->addField(WP::textInputField("Property import batch size","redi-property-import-batch-size", null, null, "The maximum amount of property records to import before clearing the SQL buffer (defaults to 50)."))
        
        //->addField(WP::checkBoxInputField("Parallel download", "redi-parallel-downloads", null, null, "Download feed data in parallel, instead of one at a time."))

        ->addField(WP::checkBoxInputField("Debug mode", "redi-debug-mode", null, null, "Turns on/off debug mode (make sure that WordPress is also in debug mode)."));
        
        //->addField(WP::CheckBoxInputField("Use debug feed", "redi-debug-feed", null, null, "Turns on/off a mock debugging \"feed.\""));
        
        //->AddField(WP::textInputField("Feed Delay","debug-field", null, null, "FIXME", true, true, true)) 
        
echo $form->render();

