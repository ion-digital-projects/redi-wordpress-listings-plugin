<?php

use \ion\WordPress\WordPressHelper as WP;
use \ion\Viewport\RedI\RedIFeedPlugIn as RedI;
use \ion\Viewport\RedI\Db\DbMarshal;
use \ion\Viewport\RedI\Marshal;
use \ion\Viewport\RedI\ViewModels\DevelopmentViewModel;
use \ion\Viewport\RedI\Models\StoredFilterModel;

//use \ion\Viewport\RedI\

$form = WP::addRediAdminForm('Filter Settings', 'redi-filter-settings')
        
        ->addGroup('Filter details', null, null, 3)
            ->addField(WP::textInputField('Slug', 'slug', null, null, 'The slug that will be used to reference this filter', false, false, true))
            ->addField(WP::textInputField('Description', 'description', null, null, 'A description that describes this filter.', true, false, true))
            ->addField(WP::checkBoxInputField("Images only", "images-only", null, null, "Show properties that have images only."))
            ->addField(WP::checkBoxInputField("Exclusive statuses", "exclusive-statuses", null, null, "Show only properties that have the selected statuses and ignore others."));

$estateName = WP::GetOption("redi-feed-estate");

if ($estateName !== null) {

    global $wpdb;
    $devTableName = $wpdb->prefix . 'redi_developments';    
    
    $queryResult = WP::DbQuery("SELECT * FROM $devTableName");

    //var_dump($queryResult);
    
    if (count($queryResult) > 0) {

//        $form = $form->addGroup('Developments', null, null, 1);
        
        $developments = [];
        
        foreach ($queryResult as $row) {

            $development = new DevelopmentViewModel([
                'label' => $row['label'],
                'code' => $row['development']
            ]);

            $developments[] = $development;
        }


        //$form = $form->addGroup('Developments', null, null, 1);

        $tmp = [];
        
        foreach($developments as $dev) {
            $tmp[$dev->GetLabel()] = $dev->GetCode();
        }

        $form->addField(WP::listInputField('Developments', $tmp, 'developments', null, null, "The developments to filter by."));
    }
}
        
//$form = $form->addGroup('Agencies', null, null, 3);

$tmp = [];

foreach(StoredFilterModel::getPropertyAgencyMap() as $key => $value) {
    
    $tmp[$key] = $value['dbValue'];
}

$form->addField(WP::listInputField('Agencies', $tmp, 'agencies', null, null, "Agencies to filter by (if none are selected, all agencies will be allowed)."));

$form = $form->addGroup('Property Statuses', null, null, 3);

foreach(StoredFilterModel::getPropertyStatusMap() as $key => $mapItem) {
    $form = $form->addField(WP::checkBoxInputField($mapItem['label'], $key));
}

$form = $form->addGroup('Property Types', null, null, 3);

foreach(StoredFilterModel::getPropertyTypeMap() as $key => $mapItem) {
    $form = $form->addField(WP::checkBoxInputField($mapItem['label'], $key));
}

$form = $form->addGroup('Property Phases', null, null, 3);

foreach(StoredFilterModel::getPropertyPhaseMap() as $key => $mapItem) {
    
    $modKey = $key;
//    if(is_numeric($key)) {
//        $modKey = '_' . $modKey;
//    }
    
    $form = $form->addField(WP::checkBoxInputField($mapItem['label'], $modKey));
}

$form = $form->addGroup('Listing Categories', null, null, 3);

foreach(StoredFilterModel::getPropertyListingCategoryMap() as $key => $mapItem) {
    
    $modKey = $key;
//    if(is_numeric($key)) {
//        $modKey = '_' . $modKey;
//    }
    
    $form = $form->addField(WP::checkBoxInputField($mapItem['label'], $modKey));
}

$form = $form
        
    ->onRead(function($index) {

        $records = WP::getOption('redi-filters');

        if ($records === null) {
            $records = [];
        }

        $result = null;

        foreach ($records as $record) {

            if ($record['slug'] === $index) {
                $result = $record;
                break;
            }
        }

        return $result;
    })
    ->onUpdate(function($index, $values) {

        $records = WP::getOption('redi-filters');

        if ($records === null) {
            
            $records = [];
        }

        $records[$index] = $values;

        WP::setOption('redi-filters', $records);
    })
    ->onCreate(function($values) {

    $records = WP::getOption('redi-filters');

    if ($records === null) {
        $records = [];
    }

    $values['slug'] = WP::slugify($values['slug']);

    $values['id'] = count($records) + 1;
    $records[$values['slug']] = $values;

    WP::setOption('redi-filters', $records);
});

echo $form->render();

