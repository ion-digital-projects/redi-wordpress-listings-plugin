<?php

use \ion\WordPress\WordPressHelper as WP;


$analyticsDevelopmentUriTemplate = "/{estate}/ws/listing/analytics/record?source={site}&development={development}&event={event}&session={session}";
$analyticsPropertyUriTemplate = "/{estate}/ws/listing/analytics/record?source={site}&development={development}&property={property}&event={event}&session={session}";

$form = WP::addRediAdminForm("Analytics Settings", 'redi-analytics-settings')
        ->addField(WP::checkBoxInputField("Enable Analytics", 'redi-enable-analytics', null, null, 'Enable or disable analytics.'))
        ->addField(WP::textInputField("Development End-point Template","redi-analytics-development-path-template", null, null, "The path for registering analytics events for development clicks (e.g. <em>$analyticsDevelopmentUriTemplate</em>)."))
        ->addField(WP::textInputField("Property End-point Template","redi-analytics-property-path-template", null, null, "The path for registering analytics events for property clicks (e.g. <em>$analyticsPropertyUriTemplate</em>)."))
;

echo $form->processAndRender();

