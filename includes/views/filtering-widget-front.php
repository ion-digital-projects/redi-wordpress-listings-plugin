<?php

use \ion\WordPress\WordPressHelper as WP;
use \ion\Viewport\RedI\RedIFeedPlugIn AS RedI;
use \ion\Viewport\RedI\State;
use \ion\Viewport\RedI\Models\StoredFilterModel;
?>

<?php
$estateName = WP::GetOption("redi-feed-estate");
if ($estateName !== null):
    ?>


    <div class="search-box">
        <div class="search-box-inner">
            <?php
            $state = new State($estateName);

            $redI = RedI::GetInstance();

            if ($redI !== null):

                $storedFilter = State::GetStoredFilter($state->GetStoredFilterName());

                $priceMin = null;
                $priceMax = null;

                $numbersSelected = [];



                if ($state->GetFilter() !== null) {

                    $priceMin = $state->GetFilter()->Get("minPrice");
                    $priceMax = $state->GetFilter()->Get("maxPrice");

                    foreach (array_keys($state->GetFilter()->ToArray()) as $keyId) {

                        if ($state->GetFilter()->Get($keyId) !== null) {
                            $numbersSelected[$keyId] = $state->GetFilter()->Get($keyId);
                        }
                    }
                }


                $max = [
                    'bedrooms' => RedI::GetValues()['bedrooms'],
                    'garages' => RedI::GetValues()['garages'],
                    'bathrooms' => RedI::GetValues()['bathrooms']
                ];


                //print_r($numbersSelected);

                $numbers = [];
                
                if (WP::getOption('redi-filter-widget-bedrooms-enabled', false)) {
                
                    $numbers["bedrooms"] = [
                        "null" => "Bedrooms",
                        "options" => [
    //                        "1" => "1",
    //                        "2" => "2",
    //                        "3" => "3",
    //                        "4" => "4",
    //                        "5" => "5+"
                        ]
                    ];
                }
                
                if (WP::getOption('redi-filter-widget-bathrooms-enabled', false)) {
                
                    $numbers["bathrooms"] = [
                        "null" => "Bathrooms",
                        "options" => [
    //                        "1" => "1",
    //                        "2" => "2",
    //                        "3" => "3",
    //                        "4" => "4",
    //                        "5" => "5+"
                        ]
                    ];
                }

                if (WP::getOption('redi-filter-widget-garages-enabled', false)) {

                    $numbers["garages"] = [
                        "null" => "Garages",
                        "options" => [
    //                        "1" => "1",
    //                        "2" => "2",
    //                        "3" => "3",
    //                        "4" => "4",
    //                        "5" => "5+"
                        ]
                    ];
                }

                if (WP::getOption('redi-filter-widget-status-enabled', false)) {

                    $numbers["propertyStatus"] = [
                        "null" => "Any Status",
                        "options" => StoredFilterModel::GetWidgetStatusArray($storedFilter)
                    ];
                }

                if (WP::getOption('redi-filter-widget-type-enabled', false)) {

                    $numbers["propertyType"] = [
                        "null" => "Any Type",
                        "options" => StoredFilterModel::GetWidgetTypeArray($storedFilter)
                    ];
                }

                if (WP::getOption('redi-filter-widget-phase-enabled', false)) {

                    $numbers["phase"] = [
                        "null" => "Any Phase",
                        "options" => StoredFilterModel::GetWidgetPhaseArray($storedFilter)
                    ];
                }

                foreach ($max as $maxItem => $maxValue) {

                    if(!array_key_exists($maxItem, $numbers)) {
                        
                        continue;
                    }
                    
                    if ($maxValue !== null) {
                        for ($tmp = 1; $tmp <= $maxValue; $tmp++) {
                            $s = (string) $tmp;

                            if ($tmp == $maxValue) {
                                $s .= '+';
                            }

                            $numbers[$maxItem]['options'][(string) $tmp] = $s;
                        }
                    } else {

                        $numbers[$maxItem]['options'][(string) $tmp] = '1+';
                    }
                }
                ?>

                <div class="slider">				
                    <div class="min"><?php echo Redi::FormatCurrency($priceMin === null ? 0 : (float) $priceMin, 'price-min'); ?></div>
                    <div class="max"><?php echo Redi::FormatCurrency($priceMax === null ? 160000000 : (float) $priceMax, 'price-max'); ?></div>

        <!--                <div class="min"><span id="price-min"><?php echo ($priceMin === null ? 0 : (float) $priceMin); ?></span></div>
        <div class="max"><span id="price-max"><?php echo ($priceMax === null ? 160000000 : (float) $priceMax); ?></span></div>                -->

                    <div id="price-slider" class="PriceSlider"></div>						
                </div>
                <script type="text/javascript">

        //jQuery(document).ready(function() {
        //
        //    jQuery('.PriceSlider').noUiSlider({
        //         range: [<?php echo (RedI::GetValues()['minPrice']); ?>, <?php echo (RedI::GetValues()['maxPrice']); ?>],
        //         start: [<?php echo ($priceMin === null ? 0 : (float) $priceMin); ?>, <?php echo ($priceMax === null ? 160000000 : (float) $priceMax); ?>],
        //         step: 50000,
        //         connect: true,
        //         serialization: {
        //             mark: ',',
        //             resolution: 1,
        //             to: [
        //                 [jQuery('#price-min'), 'html'],
        //                 [jQuery('#price-max'), 'html']
        //             ]
        //         }
        //     });
        //        
        //});           

                    jQuery(function () {

                        var slider = document.getElementById('price-slider');

                        noUiSlider.create(slider, {
                            range: {
                                'min': <?php echo (RedI::GetValues()['minPrice'] === 0 ? 0 : RedI::GetValues()['minPrice']); ?>,
                                'max': <?php echo (RedI::GetValues()['minPrice'] === 0 ? 16000000 : RedI::GetValues()['maxPrice']); ?>
                            },
                            start: [
        <?php echo ($priceMin === null ? 0 : (float) $priceMin); ?>,
        <?php echo ($priceMax === null ? 160000000 : (float) $priceMax); ?>
                            ],
                            step: 50000,
                            connect: true,
                            serialization: {
                                mark: ',',
                                resolution: 1,
                                to: [
                                    [document.getElementById('price-min'), 'html'],
                                    [document.getElementById('price-max'), 'html']
                                ]
                            },
                            format: wNumb({
                                decimals: 0,
                                thousand: '<?php echo WP::getOption("redi-currency-thousands-seperator", ' '); ?>',
                                suffix: ''
                            })

        //            format: {
        //                to: function ( value ) {
        //                    return value + ',-';
        //                },
        //                from: function ( value ) {
        //                    return value.replace(',-', '');
        //                }
        //            }            
                        });


                        var snapValues = [
                            document.getElementById('price-min'),
                            document.getElementById('price-max')
                        ];

                        slider.noUiSlider.on('update', function (values, handle) {
                            snapValues[handle].innerHTML = values[handle];
                        });



                    });


                </script>
                <div class="numbers">

        <?php
        
        $devs = StoredFilterModel::GetDevelopmentStatusArray($storedFilter);

//        if ($state->GetDevelopment() === null) {

//            if (is_array(RedI::GetDevelopments()) && count(RedI::GetDevelopments()) > 0 && is_array($devs) && (count($devs) > 1 || $devs === null)) {
            if (is_array(RedI::GetDevelopments()) && count(RedI::GetDevelopments()) > 0) {
                echo "<span>";
                echo "<select id=\"development\">";

                $options = [];

                $options[] = ['label' => RedI::DevelopmentLabel(true, false), 'value' => 'null'];

                if (is_array(RedI::GetDevelopments())) {
                    
                    foreach (RedI::GetDevelopments() as $development) {
                        
                        if ($devs !== null && in_array($development->Get('code'), $devs) || StoredFilterModel::GetDevelopmentStatusArray($storedFilter) === null) {
                            
                            $options[] = ['label' => $development->Get('label'), 'value' => $development->Get('code')];
                        }
                    }
                }

                foreach ($options as $option) {

                    $label = $option['label'];
                    $value = $option['value'];

                    $selected = ($value == $state->GetDevelopment() ? ' selected' : '');

                    echo "<option value=\"$value\"$selected>" . $label . "</option>";
                }

                echo "</select>";
                echo "</span>";
            }
//        } else {
//            echo "<input id=\"development\" type=\"hidden\" value=\"" . $state->GetDevelopment() . "\" />";
//        }        
        
        foreach ($numbers as $keyId => $select) {
            echo "<span>";
            echo "<select id=\"$keyId\">";

            echo "<option>" . $select["null"] . "</option>";

            foreach ($select["options"] as $value => $label) {

                if (array_key_exists($keyId, $numbersSelected) && $value == $numbersSelected[$keyId]) {
                    echo "<option value=\"$value\" selected>" . $label . "</option>";
                } else {
                    echo "<option value=\"$value\">" . $label . "</option>";
                }
            }

            echo "</select>";
            echo "</span>";
        }


        ?>
                    </span>

                </div>
                <script type="text/javascript">

                    function stripCurrencyFormatting(string) {
                        return string.replace(/[^0-9]/g, '');
                    }

                    function getFilterValues() {

                        var bedrooms = jQuery('#bedrooms option:selected').val();
                        var bathrooms = jQuery('#bathrooms option:selected').val();
                        var garages = jQuery('#garages option:selected').val();
                        var status = jQuery('#propertyStatus option:selected').val();

                        var phase = null;
                        if (jQuery('#phase option:selected').length) {
                            phase = jQuery('#phase option:selected').val();
                        }

                        var type = null;
                        if (jQuery('#propertyType option:selected').length) {
                            type = jQuery('#propertyType option:selected').val();
                        }

                        if (bedrooms === "Bedrooms")
                            bedrooms = null;

                        if (bathrooms === "Bathrooms")
                            bathrooms = null;

                        if (garages === "Garages")
                            garages = null;

                        if (status === "Any Status") // NOTE: case!
                            status = null;

                        if (phase === 'Any Phase') // NOTE: case!
                            phase = null;

                        if (type === 'Any Type') // NOTE: case!
                            type = null;


                        var o = {};

                        o.minPrice = {'value': stripCurrencyFormatting(jQuery('#price-min').text()), 'operator': '>='};
                        o.maxPrice = {'value': stripCurrencyFormatting(jQuery('#price-max').text()), 'operator': '<='};

                        o.bedrooms = {'value': bedrooms, 'operator': '='};
                        if (bedrooms > 4) {
                            o.bedrooms = {'value': bedrooms, 'operator': '>='};
                        }

                        o.bathrooms = {'value': bathrooms, 'operator': '='};
                        if (bathrooms > 4) {
                            o.bathrooms = {'value': bathrooms, 'operator': '>='};
                        }

                        o.garages = {'value': garages, 'operator': '='};
                        if (garages > 4) {
                            o.garages = {'value': garages, 'operator': '>='};
                        }

                        o.propertyStatus = {'value': status, 'operator': '='};

                        o.propertyType = {'value': type, 'operator': '='};

                        o.phase = {'value': phase, 'operator': '='};

                        return o;
                    }

                    function getFilterValueString(properties) {
                        var s = "";

                        for (var prop in properties) {

                            var filter = properties[prop];

                            var val = filter.value;
                            var op = filter.operator;

                            if (val != null && op != null) {
                                s += prop + op + val + "&";
                            }

                        }

                        return s;
                    }

                    function buildUrl() {

                        var url = null;

                        url = "<?php echo State::BuildIndexLink(new State($estateName), 1, 0, false, null, null, '{*}', null); ?>";


                        var devCtl = jQuery('select#development option:selected').get(0);

                        if (!devCtl) {
                            devCtl = jQuery('#development').get(0);
                        }


                        //alert(url);
                        //
                        var development = null;


                        if (devCtl) {

                            var tmp = jQuery(devCtl).val();

                            //console.log(tmp);

                            if (tmp !== 'null' && tmp !== null) {
                                development = tmp;

                                url = url.replace('{*}/', development + '/');
                            }


                        }

        //alert(development + ' ' + url);


                        if (development === null) {
                            url = url.replace('{*}/', '');
                        }


                        //window.location = url;

                        //alert(url);

                        var patt = /([^?]+)\/?\??(\S*)/;
                        var res = patt.exec(url);

                        //console.log(res);

                        var newUrl = "";

                        if (res.length > 1)
                            newUrl += res[1];

                        newUrl += "filter?parameters=" + escape(getFilterValueString(getFilterValues()));

                        if (res.length > 2)
                            newUrl += "&" + res[2];

                        //alert(newUrl);                    
                        //return;

                        window.location.href = newUrl;

                        return false;
                    }

                </script>   
                <p><a id="search-btn"  class="btn" onclick="buildUrl();">Search</a></p>

    <?php endif; ?>
        </div>
    </div><!--/search-box -->

<?php endif; 