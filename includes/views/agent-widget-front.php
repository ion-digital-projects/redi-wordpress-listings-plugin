<?php
use \ion\WordPress\WordPressHelper as WP;
use \ion\PhpHelper as PHP;
use \ion\Viewport\RedI\RedIFeedPlugIn AS RedI;
use \ion\Viewport\RedI\State;
use \ion\Viewport\RedI\ViewModels\AgentViewModel;
use \ion\Viewport\RedI\ViewModels\AgencyViewModel;
use \ion\Viewport\RedI\Feeds\Models\Agency;
use \ion\Types\StringObject;
use \ion\Viewport\RedI\Feeds\AgencyFeed;

$property = RedI::GetCurrentViewModel();

if($property !== null && (PHP::count($property->GetAgencies()) > 0 || PHP::count($property->GetAgents()) > 0)): ?>

<div class="agent">    

<?php
    
        $contacts = [];
        $agenciesToExclude = [];
       
        if(PHP::count($property->GetAgents()) > 0) {
        
            echo "<!-- Importing agents -->\n";
            
            foreach ($property->GetAgents() as $agent) {

                $tmp = new AgentViewModel($agent);
                
                if(!PHP::isEmpty($tmp->getAgencyName())) {
                    
                    $agenciesToExclude[] = $tmp->getAgencyName();
                }
                    
                $tmp2 = [];
                foreach($property->getAgencies() as $agency) {
                    
                    $tmp2[] = $agency->getName();
                }
                
//                if(!PHP::isArray($tmp->GetAgencies())) {
//                    
//                    echo "<!-- ";
//                    var_dump($tmp->GetAgencies());
//                    echo " -->";
//                }
                
                if(PHP::isArray($tmp->GetAgencies()) && array_intersect($tmp->getAgencies(), $tmp2)) {
                    
                    $agenciesToExclude = array_merge($agenciesToExclude, $tmp->getAgencies());
                }
                
                $contacts[] = $tmp;
            }
            
        } 
        
        if(PHP::count($property->GetAgencies()) > 0) {
          
            echo "<!-- Importing agencies -->\n";
            
            foreach ($property->GetAgencies() as $agency) {

                $tmp = new AgencyViewModel($agency);
                
                if(in_array($tmp->getName(), $agenciesToExclude)) {
                    
                    continue;
                }
                
                $contacts[] = $tmp;
            }            
        }
        
//        if(PHP::count($property->GetAgencies()) > 0) {
//        
//            if(PHP::isArray($property->GetAgencies())) {
//                
//                foreach ($property->GetAgencies() as $agency) {
//
//                    $contacts[] = new AgencyViewModel($agency);
//                }            
//            }
//        } else {
//        
//            if(PHP::isArray($property->GetAgents())) {
//                
//                foreach ($property->GetAgents() as $agent) {
//
//                    $contacts[] = new AgentViewModel($agent);
//                }
//            }
//        }
               
        

        if(PHP::count($contacts) > 0):
            
            shuffle($contacts);
    ?>
    <select id="agents">
        <?php        
        
        $i = 0;
        foreach ($contacts as $agent) {

            //var_Dump($agent);
            
            if($agent instanceof AgentViewModel) {
                
                $agency = '';

                if (PHP::count($agent->GetAgencies()) > 0) {

                    $agency = ' (' . $agent->GetAgencies()[0] . ')';
                }


                echo '<option data-index="' . $i . '" value="' . $agent->GetFirstName() . " " . $agent->GetLastName() . '">' . $agent->GetFirstName() . " " . $agent->GetLastName() . $agency . '</option>';
                
            } else if($agent instanceof AgencyViewModel) {
                
                echo '<option data-index="' . $i . '" value="' . $agent->GetName() . '">' . $agent->GetName() . '</option>';
            }
            
            
            $i++;
        }
        ?>    
    </select>
    <?php elseif(PHP::count($contacts) === 1): ?>
<?php    

//        $agent = $contacts[0];
//        
//        if($agent instanceof AgentViewModel) {
//
//            $agency = '';
//
//            if (PHP::count($agent->GetAgencies()) > 0) {
//
//                $agency = ' (' . $agent->GetAgencies()[0] . ')';
//            }
//
//
//            echo '<option data-index="' . $i . '" value="' . $agent->GetFirstName() . " " . $agent->GetLastName() . '">' . $agent->GetFirstName() . " " . $agent->GetLastName() . $agency . '</option>';
//
//        } else if($agent instanceof AgencyViewModel) {
//
//            echo '<option data-index="' . $i . '" value="' . $agent->GetName() . '">' . $agent->GetName() . '</option>';
//        }    
?>    
    <?php endif ?>
    <script type="text/javascript">
  
        var agents = [
<?php
$o = [];

if(PHP::isArray($property->getAgencyOverrides())) {
    
    foreach($property->getAgencyOverrides() as $tmp) {

        $o[$tmp->GetAgency()] = $tmp;
    }
}

$i = 0;
foreach ($contacts as $agent) {

    $a = null;
    
    if($agent instanceof AgentViewModel) {
    
        $a = $property->GetAgencyByName(PHP::count($agent->GetAgencies()) > 0 ? $agent->GetAgencies()[0] : null);
       
        
        if($a === null) {
            
//                var_dump($agent);
//                var_dump($a);            
//            
//            var_dump($property->GetDevelopment()->getAgencies());
//            
//            $agencies = new AgencyFeed(new FeedSettings(), $estateName, $developmentName)
//                
//            $a = $property->GetAgencyByName($agent->GetFirstName());
            
            $agencies = json_decode($property->GetDevelopment()->toArray()['agencies'], true);
            
            foreach($agencies as $agency) {
                
                if($agency['name'] == $agent->GetFirstName() || $agency['name'] == $agent->GetLastName()) {
                    
                    $a = new AgencyViewModel(new Agency($agency));
                    break;
                }
            }
        }
       

        
//        echo "<pre>";
//        var_dump($agent);
//        echo "</pre>";
        
    } else if($agent instanceof AgencyViewModel) {
        
        $a = $property->GetAgencyByName($agent->GetName());
    }

    $desc = null;
    
    $media = [];
    
    if($a !== null && array_key_exists($a->GetName(), $o)) {

        $desc = $o[$a->GetName()]->GetDescription();

//        echo "<pre>";
//        var_dump($o[$a->GetName()]);
//        echo "</pre>";
        
        foreach($o[$a->GetName()]->getMedia() as $imageObj) {
            
            $media[] = "{ \"url\": \"{$imageObj->getUrl()}\", \"thumb\": \"{$imageObj->getThumb()}\" }";
        }
    } 
    
    
    if(PHP::isEmpty($media)) {
        
        foreach($property->getImages() as $imageObj) {
            
            $media[] = "{ \"url\": \"{$imageObj->getUrl()}\", \"thumb\": \"{$imageObj->getThumb()}\" }";
        }
    }

    if($desc !== null) {

        $desc = str_replace('"', '\\"', $desc);
        $desc = str_replace("\n", '<br />', $desc);
        $desc = PHP::strStripWhiteSpace($desc);
    }
    
//    $mediaStr = '';
    
    $mediaStr = '[' . implode(",", $media) . ']';
   
    if($agent instanceof AgentViewModel) {
    
        echo 
        '{ ' .
        '"name": "' . $agent->GetFirstName() . " " . $agent->GetLastName() .
        '", "phone": "' . $agent->GetMobileNumber() .
        '", "email": "' . $agent->GetEmailAddress() .
        '", "agency": "' . ($a !== null ? $a->GetName() : '') .
        '", "image": "' . ($a !== null ? $a->GetPhotoUrl() : ($agent->GetPhotoUrl() !== null ? $agent->GetPhotoUrl() : '')) .
        '", "media": ' . $mediaStr .
        ', "description": ' . ( $desc !== null ? '"' . $desc . '"' : 'null') .         
        ' }';
        
    } else if($agent instanceof AgencyViewModel) {
        
        echo 
        '{ ' .
        '"name": "' . $agent->GetName() .
        '", "phone": "' . $agent->GetOfficeNumber() .
        '", "email": "' . $agent->GetEmailAddress() .
        '", "agency": "' . ($a !== null ? $a->GetName() : '') .
        '", "image": "' . ($a !== null ? $a->GetPhotoUrl() : ($agent->GetPhotoUrl() !== null ? $agent->GetPhotoUrl() : '')) .        
        '", "media": ' . $mediaStr .                 
        ', "description": ' . ( $desc !== null ? '"' . $desc . '"' : 'null') .
        ' }';
    }    
    


    if ($i < PHP::count($contacts) - 1) {
        echo ",\n";
    }

    $i++;
}
?>
        ];

        function changeAgent(index) {
            //$("#name > span.value").html(agents[index].name);

            //console.log("AGENT CHANGE INDEX: " + index);

            if (agents[index].phone) {
                jQuery("#phonenumber > span.value > a").html(agents[index].phone);
                jQuery("#phonenumber > span.value > a").attr("href", "tel:" + agents[index].phone);
            } else {
                jQuery("#phonenumber > span.value > a").html('Not provided');
            }

            if (agents[index].email) {
                jQuery("#emailaddress > span.value > a").html(agents[index].email);
                jQuery("#emailaddress > span.value > a").attr("href", "mailto:" + agents[index].email);
            } else {
                jQuery("#emailaddress > span.value > a").html('Not provided');
            }

            if (agents[index].image != null && agents[index].image != '') {
                jQuery("#agent-image").empty();
                jQuery("#agent-image").html('<img src="' + agents[index].image + '" />');
            } else {
                jQuery("#agent-image").html("<span id=\"agent-name\"></span><span id=\"agent-agency\"></span>");
            }

            if (agents[index].description != null) {
                jQuery("#agent-description").empty();
                jQuery("#agent-description").html(agents[index].description);
            } else {
                jQuery("#agent-description").empty();
                jQuery("#agent-description").html('No description available.');
            }
            
            if(agents[index].media) {
                

                var thumbs = '';
                var images = '';                

                //http://demo.ion.digital/redi/wp/wp-content/plugins/redi-wordpress-listings-plugin//images/placeholder.gif

                if(agents[index].media.length > 0) {

                    for(var i in agents[index].media) {
                    
                        var img = agents[index].media[i];
                    
                        if(img.url) {
                            
                            if(img.caption) {
                            
                                images += '<li><a href="' + img.url + '" class="with-caption" title="' + img.caption + '"><span class="zoom">+</span><img src="' + img.url + '" alt="' + img.caption + '" /></a></li>';
                                
                            } else {
                                
                                images += '<li><a href="' + img.url + '" class="with-caption"><span class="zoom">+</span><img src="' + img.url + '" /></a></li>';
                            }   
                        }
                        
                        if(img.thumb) {
                        
                            thumbs += '<li><img src="' + img.url + '" /></li>';
                        }
                    }
                }      
                
                for(var c in jQuery('.flexslider')) {

                    if( jQuery.hasData( jQuery('.flexslider')[c] ) ){

                        jQuery.removeData( jQuery('.flexslider')[c] );
                    }                
                }                      

                if(images.length > 0) {

                    jQuery("#slider.flexslider ul.slides").html(images);
                    jQuery("#carousel.flexslider ul.slides").html(thumbs);

                } else {
                
                    jQuery("#slider.flexslider ul.slides").html('<li><a><?php echo RedI::GetInvalidImage(); ?></a></li>');
                    jQuery("#carousel.flexslider ul.slides").html('<li><a><?php echo RedI::GetInvalidImage(); ?></a></li>');                
                    
                }
                
                jQuery('#carousel').flexslider({
                    animation: "slide",
                    controlNav: false,
                    animationLoop: false,
                    slideshow: false,
                    itemWidth: 120,
                    itemMargin: 10,
                    asNavFor: '#slider'
                });

                jQuery('#slider').flexslider({
                    animation: "fade",
                    animationLoop: false,
                    slideshow: false,
                    sync: "#carousel",
                    start: function (slider) {
                        jQuery('body').removeClass('loading');
                        console.log('Starting slider');
                    }
                });    
                
                jQuery('.with-caption').magnificPopup({
                    type: 'image',
                    closeOnContentClick: true,
                    closeBtnInside: false,
                    mainClass: 'mfp-with-zoom mfp-img-mobile',
                    image: {
                        verticalFit: true
                    },
                    gallery: {
                        enabled: true
                    },
                    zoom: {
                        enabled: true
                    }
                });                
            }
            
        }
//}
        jQuery(document).ready(function () {
            jQuery("#agents").change(function () {

                var option = jQuery(this).find(":selected").get(0);

                var index = jQuery(option).attr('data-index') * 1;

                changeAgent(index);
            });

            changeAgent(0);
        });
    </script>        
            <!--<p id="name"><span class="value"><?php //echo $agent->GetFirstName() . " " . $agent->GetLastName();   ?></span></p> -->

    <div id="agent-image">
    </div>

    <div id="phonenumber">
        <i class="fa fa-fw fa-mobile-phone"></i> 
        <span>Show Contact Number</span>
        <span class="value" style="display:none;">
            <a href="tel:<?php //echo $agent->GetEmailAddress();   ?>"><?php //echo $agent->GetEmailAddress();   ?></a>
        </span>
    </div>
    <div id="emailaddress">
        <i class="fa fa-fw fa-envelope-o"></i> 
        <span>Show Email Address</span>
        <span class="value" style="display:none;">
            <a href="mailto:<?php //echo $agent->GetEmailAddress();   ?>"><?php //echo $agent->GetEmailAddress();   ?></a>
        </span>
    </div>    
</div>
<?php

endif;


RedI::DisplayContactForm();
