<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Viewport\RedI;

/**
 * Description of Analytics
 *
 * @author Justus
 */

use \ion\WordPress\WordPressHelper as WP;
use \ion\Viewport\RedI\FeedSettings;
use \ion\Viewport\RedI\State;
use \ion\WordPress\Helper\LogLevel;

class Analytics {
    
    const LISTING_CLICK = 'Listing Click';
    const ENQUIRY = 'Enquiry';
    const FAVOURITE = 'Favourite';
    const ANALYTICS_COOKIE_NAME = 'redi-analytics';
    const FAVOURITES_COOKIE_NAME = 'redi-favourites';
    
    public static $sessionId = null;
    
    private static function filterInput($name) {
        $result = filter_input(INPUT_POST, $name, FILTER_DEFAULT);

        if($result !== null) {
            return $result;
        }

        $result = filter_input(INPUT_GET, $name, FILTER_DEFAULT);
        
        return $result;
    }
    
    public static function registerEvents(State $state, $session = null) {

        if(WP::getOption('redi-enable-analytics', false) === false) {
            return;
        }
        
        if($state->GetDevelopment() === null) {
            return;
        }
        
        if($session === null) {
            $session = static::getSessionId();
        }

        $uri = null;
        
        $tags = [
            'estate' => $state->GetEstate(),
            'site' => get_bloginfo('name'),
            'development' => ($state->GetDevelopment() !== null ? $state->GetDevelopment() : null),
            'session' => ($session === null ? 0 : $session),
            'property' => ($state->IsProperty() === true ? $state->GetLabel() : null)              
        ];

        
        //$uri = WP::GetOption("redi-feed-base-uri") . '/' . $uri;;
        
        $clickData = new \stdClass();
        
        $clickData->estate = $tags['estate'];
        $clickData->site = $tags['site'];
        $clickData->development = $tags['development'];
        $clickData->event = self::LISTING_CLICK;
        $clickData->session = $tags['session'];
        $clickData->property = $tags['property'];
        
        
        $favData = new \stdClass();
        
        $favData->estate = $tags['estate'];
        $favData->site = $tags['site'];
        $favData->development = $tags['development'];
        $favData->event = self::FAVOURITE;
        $favData->session = $tags['session'];
        $favData->property = $tags['property'];        
        
        $enqData = new \stdClass();
        
        $enqData->estate = $tags['estate'];
        $enqData->site = $tags['site'];
        $enqData->development = $tags['development'];
        $enqData->event = self::ENQUIRY;
        $enqData->session = $tags['session'];
        $enqData->property = $tags['property'];        
                
        
        $uri = '/red-i/click/';


        
        $clickData = json_encode($clickData);        
        $favData = json_encode($favData);        
        $enqData = json_encode($enqData);        
        
        WP::log("Analytics::registerEvent($uri): $clickData", LogLevel::INFO, 'redi-analytics');
        
        $js = <<<JS

                             
               
jQuery(document).ready(function() {   
    
    // Click action
                
    jQuery.post('$uri', $clickData, function(data) {
        //console.log('Analytics click submission: ', '$uri', $clickData, 'Result: ', data);
                
        console.groupCollapsed('Analytics click submission');
        console.log('Request');
        console.dir($clickData);
        console.log('Response');
        console.dir(data);
        console.groupEnd();                
    }, 'json');                
               
    // Favourite setup
    
    //TODO
                
    // Favourite action
        
    jQuery('.redi-favourite-button').each(function(i, e) {

        jQuery(e).click(function() {
            jQuery.post('$uri', $favData, function(data) {
                //console.log('Analytics favourite submission: ', '$uri', $favData, 'Result: ', data);  
                
                console.groupCollapsed('Analytics favourite submission');
                console.log('Request');
                console.dir($favData);
                console.log('Response');
                console.dir(data);
                console.groupEnd();       
                    
                jQuery(e).addClass('redi-favourite');                
            }, 'json');
        });
    });
            
   // Enquiry action
                
    jQuery('.email-form input[type="submit"]').each(function(i, e) {    
                
        jQuery(e).click(function() {

            jQuery.post('$uri', $enqData, function(data) {
                
                console.groupCollapsed('Analytics enquiry submission');
                console.log('Request');
                console.dir($enqData);
                console.log('Response');
                console.dir(data);
                console.groupEnd();
                
            }, 'json');                
                
        });
    });
                
    // Enquiry form modification
                
    jQuery('.email-form form').prepend(jQuery('<input type="hidden" name="ismSessionId" value="{$tags['session']}" />'));
                
});
JS;
        
        WP::addScript('redi-analytics', $js, false, true, true, true);
        
    }
    
    public static function record($event) {
        
        $uri = null;
        
        $tags = [
            'estate' => urlencode(static::filterInput('estate')),
            'site' => urlencode(static::filterInput('site')),
            'development' => urlencode(static::filterInput('development')),
            'event' => urlencode($event),
            'session' => urlencode(static::filterInput('session')),
            'property' => urlencode(static::filterInput('property') )             
        ];
        
        if(empty($tags['session'])) {
            $tags['session'] = 0;
        }
        
//        if($event === static::ENQUIRY) {
//            $tags['ismSessionId'] = $tags['session'];
//        }
        
        if($tags['property'] !== null) {
            $uri = FeedSettings::ApplyTemplate(WP::getOption("redi-analytics-property-path-template"), $tags);
        } else {            
            $uri = FeedSettings::ApplyTemplate(WP::getOption("redi-analytics-development-path-template"), $tags);                        
        }       
        
        $uri = WP::GetOption("redi-feed-base-uri") . '/' . trim($uri, '/');
        
        WP::log("Server submit started: $uri", LogLevel::INFO, 'redi-analytics');
        
        //FIXME:
        //$uri = 'http://proj.linux-dev.vm/endpoint2.php' . '/' . trim($uri, '/');
                        
        //die($uri);
        
        $handle = curl_init($uri);
        
        $resultObj = new \stdClass();
        
        $resultObj->uri = $uri;
        $resultObj->session = null;
        $resultObj->error = null;
        $resultObj->errorCode = null;
        $resultObj->response = null;
        
        if ($handle !== false) {

            $opts = array(
                CURLOPT_AUTOREFERER => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_FAILONERROR => false,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HEADER => false,
                CURLOPT_MAXREDIRS => 3,
                CURLOPT_TIMEOUT => 28800, // set this to 8 hours so we dont timeout on big files
                CURLOPT_PROTOCOLS => CURLPROTO_HTTP | CURLPROTO_HTTPS,
                CURLOPT_HTTPAUTH => CURLAUTH_ANY
            );

            curl_setopt_array($handle, $opts);

            $result = curl_exec($handle);

            if ($result !== false && intval($result) !== 0) {
                
                $result = intval($result);
                
                if(static::getSessionId() !== $result) {
                    

                    setcookie(self::ANALYTICS_COOKIE_NAME, $result, 0, '/', filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_NULL_ON_FAILURE), false, false);

                    static::$sessionId = $result;
                    $resultObj->session = $result;
                }
                                
            } 
            
            $errno = curl_errno($handle);
            
            if($errno) {
                $resultObj->errorCode = $errno;
                $resultObj->error = curl_strerror($errno);
                
                WP::log("Server submit failed: $uri ({$resultObj->error})", LogLevel::ERROR, 'redi-analytics');
            } else {
                WP::log("Server submit completed: $uri", LogLevel::INFO, 'redi-analytics');
            } 
            
            $resultObj->response = $result;
            
            curl_close($handle); 
        }        

        $json = json_encode($resultObj);
        
        echo $json;
        
        return $json;
    }
    
    public static function recordClick() {
        static::record(self::LISTING_CLICK);
    }
    
    public static function recordFavourite() {
        static::record(self::FAVOURITE);

    }
    
    public static function recordEnquiry() {
        static::record(self::ENQUIRY);
  
    }
    
    public static function getSessionId() {
         
        if(static::$sessionId !== null) {
            return (string) static::$sessionId;
        }
        
        if(filter_input(INPUT_COOKIE, self::ANALYTICS_COOKIE_NAME, FILTER_DEFAULT) !== false) {
            return (string) filter_input(INPUT_COOKIE, self::ANALYTICS_COOKIE_NAME, FILTER_DEFAULT);
        }
        
        return null;                
    }
    
}
