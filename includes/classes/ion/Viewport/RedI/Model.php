<?php

namespace ion\Viewport\RedI;

/**
 * Description of FeedObject
 *
 * @author Justus
 */

use \ion\WordPress\WordPressHelper as WP;

abstract class Model {

    public static function Deserialize(/* string */ $data) {
        return deserialize($data);
    }
    
    public static function FindIndex(/* string */ $field, /* string */ $value, array $models) {
        
        $found = false;

        $i = 0;
        foreach($models as $item) {
            if($item->Get($field) === $value) {
                $found = true;
                break;
            }
            $i++;
        }

        if($found !== true) {
            return -1;
        }        
        
        return $i;
    }
    
    public static function Find(/* string */ $field, /* string */ $value, array $models) {
        $result = null;
        
        $index = static::FindIndex($field, $value, $models);
        
        if($index === -1) {
            return null;
        }
        
        return $models[$index];
    }
    
    private $data;
    
    public function __construct(array &$data = null) {
       $this->data = $data;
    }
    
    protected function Set(/* string */ $propertyName, $value = null) {
        if($this->data === null) {
            $this->data = [];
        }
        
        $this->data[$propertyName] = $value;
        
        return $value;
    }

    public function Get( /* string */ $propertyName) {
        if($this->data !== null) {
            if(array_key_exists($propertyName, $this->data)) {
                return $this->data[$propertyName];
            }
        }
        return null;
    }
    
    public function Serialize() {
        return serialize($this->ToArray());
    }

    
    public function &ToArray() {
        return $this->data;
    }
    
    public function IsPropertyEqualTo($propertyName, $expectedValue = null) {
        
        return ($this->Get($propertyName) == $expectedValue); // Note: only two == - NOT exact matching     
    }
    
    public function IsPropertyGreaterThan($propertyName, $expectedValue = null) {
        
        if($expectedValue === null) {
            return false;
        }        
        
        return ($this->Get($propertyName) > $expectedValue);        
    }
    
    public function IsPropertyGreaterThanOrEqualTo($propertyName, $expectedValue = null) {
        
        if($expectedValue === null) {
            return false;
        }        
        
        return ($this->Get($propertyName) >= $expectedValue);        
    }    

    public function IsPropertyLessThan($propertyName, $expectedValue = null) {
        
        if($expectedValue === null) {
            return false;
        }
        
        return ($this->Get($propertyName) < $expectedValue);      
    }
    
    public function IsPropertyLessThanOrEqualTo($propertyName, $expectedValue = null) {
        
        if($expectedValue === null) {
            return false;
        }
        
        return ($this->Get($propertyName) <= $expectedValue);      
    }    

    
    
}
