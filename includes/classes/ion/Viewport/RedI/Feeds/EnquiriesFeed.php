<?php

namespace ion\Viewport\RedI\Feeds;

/**
 * Description of FeedPlanTypes
 *
 * @author Justus
 */
use \ion\Viewport\RedI\SalesMapFeed;
use \ion\Viewport\RedI\FeedSettings;
use \ion\Viewport\RedI\Feeds\Models\Enquiries;
use \ion\Viewport\RedI\Feeds\Models\Development;

class EnquiriesFeed extends SalesMapFeed {

    private $development;
    
    public function __construct(FeedSettings $feedSettings, Development $development, $fetchNow = false) {
        parent::__construct($feedSettings, "enquiries", [
            "estate" => $development->GetEstate(),
            "development" => $development->GetName()
        ]);
        
        $this->development = $development;
    }

    protected function Process(array $json, $now = false) {
        return new Enquiries($this->GetSettings(), $this->development, $json);
    }    
    
}
