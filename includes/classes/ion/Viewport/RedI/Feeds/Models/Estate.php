<?php

namespace ion\Viewport\RedI\Feeds\Models;

/**
 * Description of Estate
 *
 * @author Justus
 */

use \ion\Viewport\RedI\SalesMapFeed;
use \ion\Viewport\RedI\Feeds\GalleryFeed;
use \ion\Viewport\RedI\Feeds\PropertiesFeed;
use \ion\Viewport\RedI\Feeds\PlanTypesFeed;
use \ion\Viewport\RedI\FeedSettings;
use \ion\Viewport\RedI\Model;
use \ion\Viewport\RedI\Feeds\EstateFeed;


class Estate extends Model {
    
    
    private $developments;
    private $name;
    
    public function __construct(FeedSettings $feedSettings, /* string */$name, array $data, $fetchNow = false, $noFetch = false) {        
        parent::__construct($data);
        
        $this->developments = [];
        $this->name = $name;
        
        foreach($data as $obj) {
            $this->developments[$obj["code"]] = new Development($feedSettings, $name, $obj["code"], $obj, $fetchNow, $noFetch);
        }
        
    }
    
    public function GetName() {
        return $this->name;
    }
    
    public function GetDevelopments() {
        return $this->developments;
    }
    
    public function GetDevelopment( /* string */ $name) {
        return $this->GetDevelopments()[$name];
    }
    
    public function Filter(array $filter) {
        
    }    
    
}
