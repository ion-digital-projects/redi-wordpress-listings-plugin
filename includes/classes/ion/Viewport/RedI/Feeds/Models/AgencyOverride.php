<?php

namespace ion\Viewport\RedI\Feeds\Models;

/**
 * Description of Agent
 *
 * @author Justus
 */

use \ion\Viewport\RedI\Model;

class AgencyOverride extends Model {
    
    
    public function __construct(array $data) {
        parent::__construct($data);
  
        $images = [];
        foreach($data["media"] as $image) {
            $images[] = new Image($image);
        }               
        $this->Set("media", $images);        
        
    }
    
    // string
    public function GetAgency() {
        return $this->Get("agency");
    }
    
    // string
    public function GetDescription() {
        return $this->Get("description");
    }

    public function GetMedia() {
        return $this->Get("media");
    }
    
}
