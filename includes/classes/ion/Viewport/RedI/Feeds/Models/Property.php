<?php

namespace ion\Viewport\RedI\Feeds\Models;

/**
 * Description of Properties
 *
 * @author Justus
 */

use \ion\Viewport\RedI\Model;
use \ion\Viewport\RedI\Filter;
use \ion\PhpHelper as PHP;

class Property extends Model {
   
    
    private $planTypes;
    
    public function __construct(array &$data, PlanTypes $planTypes = null, Agents $propertyAgents = null) {
        parent::__construct($data);
        
        
        $this->planTypes = $planTypes;
        $this->agents = $propertyAgents;        
        
        $images = [];
        foreach($data["media"] as $image) {
            $images[] = new Image($image);
        }               
        $this->Set("media", $images);
        
        $showTimes = [];
        foreach($data["showTimes"] as $showTime) {
            $showTimes[] = new ShowTime($showTime);
        }               
        $this->Set("showTimes", $showTimes);                
        
        // Plan Types
        
        $propertyPlanTypes = [];                
        
        if($this->GetAvailablePlanTypes() !== null && $this->planTypes !== null) {
        
            foreach($this->GetAvailablePlanTypes() as $planTypeLabel) {

                //$result[] = $planTypeLabel;
                
                //echo $planTypeLabel;
                
                $filter = new Filter([ "label" => $planTypeLabel]);

                $arr = $filter->ApplyTo($this->planTypes->GetItems());
                
                if(count($arr) > 0) {
                    $propertyPlanTypes[] = $arr[0];
                }
            }
        }
        
        $this->Set('planTypes', $propertyPlanTypes);
                       
        // Agents
        
        $propertyAgents = [];
 
        if($this->GetAgents() !== null) {                    
            
            if(PHP::count($this->GetAgents()) > 0) {
            
                foreach($this->GetAgents()  as $agentName) {
                
                    $space = strpos($agentName, " ");
                    
                    $agentFirstName = trim(substr($agentName, 0, $space));
                    $agentLastName = trim(substr($agentName, $space, strlen($agentName) - $space));                    
                    
                    $filter = new Filter([ "firstName" => $agentFirstName, "lastName" => $agentLastName]);

                    $arr = $filter->ApplyTo($this->agents->GetItems());
                    if(PHP::count($arr) > 0) {
                        
                        if(PHP::count($arr) > 1) {
                            
                            
                            
                        } else {
                        
                            $propertyAgents[] = $arr[0];
                        }
                    }
                }
            }
        }        
        
        $this->Set('propertyAgents', $propertyAgents);
        
        $agencyOverrides = [];
        
        foreach($data["agencyOverrides"] as $override) {
            $agencyOverrides[] = new AgencyOverride($override);
        }               
        
        $this->Set("agencyOverrides", $agencyOverrides); 
        
        $agencies = $data['agencies'];
        
        $this->Set("agencies", $agencies);
        
//        $this->Set("selectedPlanType", $);
//        $this->Set("planRequired", $);
        
//        if($this->GetLabel() === '4220' /* &&  $this->GetLabel() === '4214' */) {
//            echo '<pre>';
//            print_r($this->ToArray());
//            echo '</pre>';
//            exit;
//        }
    }    

    public function GetPropertyPlanTypes() {
        return $this->Get('planTypes');

    }
    
    public function GetPropertyAgents() {
        return $this->Get('propertyAgents');
    }    
    
    // string
    public function GetLabel() {
        return $this->Get("label");
    }

    // string
    public function GetPhase() {
        return $this->Get("phase");
    }    
    
    // ?
    public function GetErf() {
        return $this->Get("erf");
    }
    
    // ?
    public function GetUnit() {
        return $this->Get("unit");
    }
    
    // ?
    public function GetAddress() {
        return $this->Get("address");
    }
    
    // string
    public function GetDescription() {
        return $this->Get("description");
    }
    
    // string
    public function GetPropertyType() {
        return $this->Get("propertyType");
    }
    
    // string
    public function GetSaleStatus() {
        return $this->Get("saleStatus");
    }
    
    // numeric
    public function GetPlotSize() {
        return $this->Get("plotSize");
    }
    
    // numeric
    public function GetPlotPrice() {
        return $this->Get("plotPrice");
    }
    
    // numeric
    public function GetMinTotalPrice() {
        return $this->Get("minTotalPrice");
    }
    
    // numeric
    public function GetMaxTotalPrice() {
        return $this->Get("maxTotalPrice");
    }
    
    // boolean
    public function GetPlanRequired() {
        return $this->Get("planRequired");
    }
    
    // string
    public function GetSelectedPlanType() {
        return $this->Get("selectedPlanType");
    }
    
    // array -> string
    public function GetAvailablePlanTypes() {
        return $this->Get("availablePlanTypes");
    }
    
    // array -> { thumb, url, caption }
    public function GetMedia() {
        return $this->Get("media");
    }

    // array -> ?
    public function GetAgents() {
        return $this->Get("agents");
    }
    
    // array -> ?
    public function GetAgencies() {
        return $this->Get("agencies");
    }
    
    public function GetShowTimes() {
        return $this->Get("showTimes");
    }
    
    // array -> ?
    public function GetAgencyOverrides() {
        return $this->Get("agencyOverrides");
    }
    
    public function GetRentalAmount() {
        return $this->Get("rentalAmount");
    }
    
    
    public function getListingCategory() {
        return $this->Get("listingCategory");
    }
}
