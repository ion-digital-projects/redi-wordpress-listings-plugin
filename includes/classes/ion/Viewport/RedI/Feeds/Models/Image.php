<?php

namespace ion\Viewport\RedI\Feeds\Models;

/**
 * Description of Galleries
 *
 * @author Justus
 */

use \ion\Viewport\RedI\Model;
use \ion\PhpHelper as PHP;

class Image extends Model {


    
    public function __construct(array $data) {
        parent::__construct($data);
    }    
    
    // string
    public function GetCategory() {
        return $this->Get("category");
    }
    
    // url
    public function GetThumb(bool $large = false) {

  
        if($this->getType() === 'matterport') {

            return preg_replace(
                    "/^(http.?:\/\/my\.matterport\.com\/show\/\?m=)([0-9A-Za-z-]{11})/", 
                    'https://my.matterport.com/api/v1/player/models/\2/thumb?width=' . ($large ? '900' : '120') . '&dpr=1.125&disable=upscale', 
                    $this->GetUrl(false));  
        }

        if($this->getType() === 'youtube') {


            return preg_replace(
                    "/^(http.?:\/\/www\.youtube\.com\/watch\?v=|http.?:\/\/www\.youtube\.com\/embed\/|http.?:\/\/youtu.be\/)([0-9A-Za-z-]{11})/", 
                    'https://img.youtube.com/vi/\2/' . ($large ? 'hq' : '') . 'default.jpg', 
                    $this->GetUrl(false));           
        }        


        return $this->Get("thumb");
    }
    
    // url
    public function GetUrl(bool $modify = false) {
        
        if(!$modify || $this->getType() == 'image') {
        
            return $this->Get("url");
        }
        
        if($this->getType() === 'youtube') {
        
            //<iframe width="560" height="315" src="https://www.youtube.com/embed/dQw4w9WgXcQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            
            return preg_replace(

                "/^(http.?:\/\/www\.youtube\.com\/watch\?v=|http.?:\/\/www\.youtube\.com\/embed\/|http.?:\/\/youtu.be\/)([0-9A-Za-z-]{11})/", 
                'https://www.youtube.com/embed/\2?autoplay=1&rel=0&showinfo=0&enablejsapi=1', 
                $this->GetUrl(false)); 
        }
        
        if($this->getType() === 'matterport') {
        
            return preg_replace(
                    
                "/^(http.?:\/\/my\.matterport\.com\/show\/\?m=)([0-9A-Za-z-]{11})/", 
                'https://my.matterport.com/show/?m=\2&play=1', 
                $this->GetUrl(false)); 
        }
    
        return $this->Get("url");
    }
    
    // string
    public function GetCaption() {
        return $this->Get("caption");
    }
    
    // string
    public function getType() {
        
        if(PHP::strEndsWith(strtolower($this->GetUrl()), '.png') || PHP::strEndsWith(strtolower($this->GetUrl()), '.jpg') || PHP::strEndsWith(strtolower($this->GetUrl()), '.gif')) {
           
            return 'image';
        }
        
        return preg_replace('/([^.]*)\.?([^.]+)\.?(.*)/', '\2', parse_url($this->GetUrl(), PHP_URL_HOST));
        
    }
    
}
