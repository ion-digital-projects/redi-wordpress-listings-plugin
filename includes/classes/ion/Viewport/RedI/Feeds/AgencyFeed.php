<?php

namespace ion\Viewport\RedI\Feeds;

/**
 * Description of FeedPlanTypes
 *
 * @author Justus
 */
use \ion\Viewport\RedI\SalesMapFeed;
use \ion\Viewport\RedI\FeedSettings;
use \ion\Viewport\RedI\Feeds\Models\Agents;
use \ion\Viewport\RedI\Feeds\Models\Agencies;

class AgencyFeed extends SalesMapFeed {

    
    public function __construct(FeedSettings $feedSettings,/* string */$estateName,/* string */$developmentName, $fetchNow = false) {
        parent::__construct($feedSettings, "agencies", [
            "estate" => $estateName,
            "development" => $developmentName
        ]);
    }

    protected function Process(array $json, $now = false) {        
        return new Agencies($this->GetSettings(), $json);
    }    
    
}
