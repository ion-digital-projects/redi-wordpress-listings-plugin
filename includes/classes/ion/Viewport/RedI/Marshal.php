<?php


namespace ion\Viewport\RedI;

/**
 * Description of Marshal
 *
 * @author Justus
 */


abstract class Marshal {
//    
//    public static function FetchDevelopmentCodeForProperty($label, $developmentCode = null) {
//        
//        if(static::GetInstance() === null) {
//            return null;
//        }
//        
//        return static::GetInstance()->FetchDevelopment($label, $developmentCode);
//    }
    
    protected static $instance = null;
    protected static $totalRecords = 0;
    
    public static function GetInstance() {
        return static::$instance;
    }
    
    public static function Create() {
        static::$instance = new static();
    }        

    public static function GetTotalRecords() {
        return static::$totalRecords;
    }

    protected function __construct() {
       // empty!
    }
    
    public abstract function Fetch(FeedSettings $feedSettings, State $state, $ignoreFilters = false);
    
    public abstract function FetchDevelopmentForProperty(FeedSettings $feedSettings, /* string */$estateName, $label);
    
}
