<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Viewport\RedI\Db\Models;

/**
 * Description of DbTempPropertyModel
 *
 * @author Justus
 */

use \ion\Viewport\RedI\ViewModels\PropertyViewModel;

class DbTempPropertyModel extends DbPropertyModel {
    
    protected static function GetDefaultTableName() {        
        return parent::GetDefaultTableName() . '_import';
    }        
    
    public function __construct(PropertyViewModel $viewModel) {
        parent::__construct($viewModel, 'import');
    }
    
//    public abstract static function finalize($targetTableName, array $targetSchema) {
//        
//    }    
    
}
