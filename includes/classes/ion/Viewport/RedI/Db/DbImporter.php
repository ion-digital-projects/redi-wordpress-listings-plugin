<?php

namespace ion\Viewport\RedI\Db;

/**
 * Description of DBImporter
 *
 * @author Justus
 */
use \Exception;
use \ion\PhpHelper as PHP;
use \ion\WordPress\WordPressHelper as WP;
use \ion\Viewport\RedI\Feeds\Models\Development;
use \ion\Viewport\RedI\Feeds\Models\Estate;
use \ion\Viewport\RedI\ViewModels\PropertyViewModel;
use \ion\Viewport\RedI\FeedSettings;
use \ion\Viewport\RedI\Feeds\AgentsFeed;
use \ion\Viewport\RedI\Feeds\EnquiriesFeed;
use \ion\Viewport\RedI\Feeds\EnquiryFormFieldsFeed;
use \ion\Viewport\RedI\Feeds\EstateFeed;
use \ion\Viewport\RedI\Feeds\GalleryFeed;
use \ion\Viewport\RedI\Feeds\PlanTypesFeed;
use \ion\Viewport\RedI\Feeds\PropertiesFeed;
use \ion\Viewport\RedI\Db\Models\DbPropertyModel;
use \ion\Viewport\RedI\Db\Models\DbDevelopmentModel;
use \ion\Viewport\RedI\Db\Models\DbTempPropertyModel;
use \ion\Viewport\RedI\Db\Models\DbTempDevelopmentModel;


//use \ion\Viewport\RedI\Db\DbImporter;


class DbImporter {

//    public static function GetCurrentBatchId() {        
//        return (int) WP::getOption('red-import-batch-id', 0);        
//    }
//    
//    public static function ClearBatch() {
//        WP::deleteOption('redi-import-batch-id');
//    }
//    
//    public static function HasNewBatchToProcess() {
//       
//        $devDbTable = DbDevelopmentModel::GetTableName();
//        
//        $sql = "SELECT * FROM `$devDbTable` WHERE ";
//       
//    }

    public static function TruncateTemporaryTables() {
        DbTempPropertyModel::Truncate();
        DbTempDevelopmentModel::Truncate();         
    }
    
    public static function CreateNewBatch(FeedSettings $feedSettings) {

        $developments = [];
        $estateName = WP::GetOption('redi-feed-estate', null);

        if ($estateName !== null) {
            
            $estateFeed = new EstateFeed($feedSettings, $estateName);
            $estate = $estateFeed->Fetch(false);
            
            foreach($estate->GetDevelopments() as $development) {
                $developments[] = $development->GetName();
            }
             
        }        
        
        return $developments;
    }
    
    public static function Fetch(FeedSettings $feedSettings, $developmentName = null) {

        $result = 0;

        $estateName = WP::getOption('redi-feed-estate', null);

        if ($estateName !== null) {

            $developments = [];

            $estateFeed = null;
            
            if ($developmentName === null) {
                $estateFeed = new EstateFeed($feedSettings, $estateName, false);
            } else {
                $estateFeed = new EstateFeed($feedSettings, $estateName, true);
            }
            
//            DbTempPropertyModel::Truncate();
//            DbTempDevelopmentModel::Truncate();            
            
            $estate = $estateFeed->Fetch(true);

            if ($estate !== null) {

                foreach ($estate->GetDevelopments() as $development) {
                    if ($developmentName === null) {
                        $developments[] = $development;
                    } else {
                        if($development->GetCode() === $developmentName) {
                            $developments[] = new Development($feedSettings, $estateName, $developmentName, $development->ToArray(), true, false);
                            

                            
                            break;
                        }
                    }
                }

            } else {
                throw new Exception('Could not fetch estate.');
            }

            if (PHP::count($developments) > 0) {

                $sql = '';

                foreach ($developments as $development) {

//                    echo "<pre>";
//                    var_dump($development->GetAgencies());
//                    echo "</pre>";
                    
                    $devDbModel = new DbTempDevelopmentModel($development);
                    $devDbTable = DbTempDevelopmentModel::GetTableName();

                    $sql = 'INSERT INTO ' .
                            $devDbTable .
                            ' ( ' . DbTempDevelopmentModel::GetInsertFieldSql() . ' ) VALUES ( ' .
                            $devDbModel->Insert() .
                            ' );' . "\n";

//                    var_dump($sql);
//                    
                    WP::DbQuery($sql);

                    
                    $batchSize = WP::getOption('redi-property-import-batch-size', 50);

                    if(empty($batchSize)) {
                        
                        $batchSize = 50;
                    }                    
                    
                    $batchSize = 50;
                    
                    if ($development->GetProperties() !== null) {

                        $tmp1 = [];
                        $sql = '';
                        $b = 0;                        					
						
                        foreach ($development->GetProperties()->GetItems() as $property) {

                            //var_dump($property->GetMinTotalPrice());

                            $prop = new PropertyViewModel($development, $property);

                            $propDbModel = new DbTempPropertyModel($prop);
                            //$propDbModel->Insert();

//                            if($prop->getDevelopmentName() == 'Waterfall Crescent') { // && $property->GetPropertyStatus() == 'Available for Resale') {
//                                
//                                print_r($prop->ToArray());
//                            }
                            
                            $result++;

                            $tmp1[] = '( ' . $propDbModel->Insert() . ' )';

                            
                            $b++;
                            
                            if($b >= $batchSize) {
                                
                                if (count($tmp1) > 0) {

                                    $sql = 'INSERT INTO ' .
                                            DbTempPropertyModel::GetTableName() .
                                            ' ( ' . DbTempPropertyModel::GetInsertFieldSql() . ' ) VALUES ' . 
                                            join(",\n", $tmp1) . ';' . "\n";

									//global $wpdb;
									//$wpdb->query($sql);
                                    WP::DbQuery($sql);
                                    
									WP::log("Imported: " . count($tmp1) . " records.");
									
                                    $tmp1 = [];
                                }                                  
                                
                                $sql = '';
                                $b = 0;
                            }
                            
                        }

                        if (count($tmp1) > 0) {

                                $sql = 'INSERT INTO ' .
                                                DbTempPropertyModel::GetTableName() .
                                                ' ( ' . DbTempPropertyModel::GetInsertFieldSql() . ' ) VALUES ' . 
                                                join(",\n", $tmp1) . ';' . "\n";

                                WP::DbQuery($sql);													
                                //global $wpdb;
                                //$wpdb->query($sql);

                                WP::log("Imported: " . count($tmp1) . " records.");
                        } 

                        $sql = "UPDATE `{$devDbTable}` SET `import_end_time` = '" . date('Y-m-d H:i:s') . "' WHERE `development` = '{$development->GetName()}';\n";                        
                        WP::DbQuery($sql);                        

                        WP::log("Feed data: {$development->getName()} - " . count($development->GetProperties()->GetItems()));
                    }
                }
            }
            //die($sql);
        }

        return $result;
    }
    
    public static function finalize() {
        
//        var_dump([
//            DbTempPropertyModel::GetTableName(),
//            DbTempDevelopmentModel::GetTableName(),
//            DbPropertyModel::GetTableName(),
//            DbDevelopmentModel::GetTableName()
//        ]);        
        
        DbPropertyModel::Truncate();
        DbDevelopmentModel::Truncate();
        
        DbModel::copy(DbTempPropertyModel::GetTableName(), DbPropertyModel::GetTableName(), DbPropertyModel::GetSchema());
        DbModel::copy(DbTempDevelopmentModel::GetTableName(), DbDevelopmentModel::GetTableName(), DbDevelopmentModel::GetSchema());
        
        
    }

}
