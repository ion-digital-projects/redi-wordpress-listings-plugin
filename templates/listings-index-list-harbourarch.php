<?php
/*

  Template Name: RedI Listings (list - HarbourArch)

 */

include_once(get_template_directory() . "/header.php");

use \ion\WordPress\WordPressHelper as WP;
use \ion\Viewport\RedI\RedIFeedPlugIn AS RedI;
use \ion\Viewport\RedI\RedIFeedFilterWidget as FilterWidget;
use \ion\Viewport\RedI\RedIFeedOrderingWidget as OrderingWidget;
?>

<span class="red-i">

<div class="spacer"></div>

<?php if (RedI::HasDevelopment() && RedI::ShowTitle()): ?>
    <div class="banner">	
        <div class="wrapper">
            <h1><?php RedI::DevelopmentName(); ?></h1>
        </div><!-- /wrapper -->	
    </div><!-- /banner -->
<?php endif; ?>


<div class="main-content">

    <div class="wrapper">

        <div class="property-listings list">

            <div class="results">

                <?php WP::Widget(new OrderingWidget()); ?>

                <div class="showing">
                    Showing:                     
                    <?php if(Redi::GetTotalCount() > 0): ?>
                    <strong><?php RedI::PropertyRecordsCurrentPage(); ?></strong> of <?php Redi::PropertyRecordsTotal(); ?>
                    <?php else: ?>
                    <strong>No properties</strong>
                    <?php endif; ?>
                </div><!--/showing -->

                <?php RedI::DisplaySelection(); ?>

            </div><!--/results -->


            <?php WP::DoShortCode("search"); ?>
            

            <?php while ($model = RedI::GetNextViewModel() !== null): ?>

                <!-- <?php RedI::PropertyLabel(); ?> -->
                <div class="property">
                    <div class="image">
                        <span class="type"><?php RedI::PropertyStatus(); ?></span>
                        <?php RedI::PropertyPrimaryImage(true, RedI::PropertyLink(false), true); ?>
                    </div>
                    <div class="copy">
                        <h3><a href="<?php RedI::PropertyLink(); ?>"><?php RedI::PropertyTitle(); ?></a></h3>
                        <div class="price"><?php RedI::PropertyMinPrice(); ?></div>
                        <p><?php RedI::PropertyDescription(); ?></p>
                        <?php RedI::PropertyFeatures(true, true, true, true, false); ?>
                    </div>
                </div><!--/property -->   

            <?php endwhile; ?>


            <?php if (RedI::HasPreviousPropertyPage() || RedI::HasNextPropertyPage()): ?>
                <div class="pagination">	
                    <?php if (RedI::HasPreviousPropertyPage()): ?>
                        <a href="<?php RedI::PreviousPropertyPageLink(); ?>" class="btn outline prev"> <i class="fa fa-fw fa-angle-left"></i> Back</a>
                    <?php endif; ?>
                    <?php if (RedI::HasNextPropertyPage()): ?>
                        <a href="<?php RedI::NextPropertyPageLink(); ?>#" class="btn outline next">Next <i class="fa fa-fw fa-angle-right"></i></a>
                    <?php endif; ?>
                    <?php RedI::PropertyPages(); ?>
                </div><!--/pagination -->
            <?php endif; ?>

        </div><!--/property-listings -->


    </div><!--/wrapper -->


</div><!--/main-content -->

</span>

<?php include_once(get_template_directory() . "/footer.php"); ?>