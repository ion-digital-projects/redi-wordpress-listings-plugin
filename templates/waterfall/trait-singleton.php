<?php 
namespace Traits;

trait Singleton {
	protected static $_instance = null;

	public static function instance() {
		if ( ! self::$_instance ) {
			self::$_instance = new self;
		}
		return self::$_instance;
	}
}
