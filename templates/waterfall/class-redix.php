<?php 
use \PHPHtmlParser\Dom;
use \ion\Viewport\RedI\RedIFeedPlugIn AS RedI;
use \ion\WordPress\WordPressHelper as WP;
use \ion\Viewport\RedI\RedIFeedAgentWidget as AgentWidget;
use \ion\Viewport\RedI\State;
use \ion\Viewport\RedI\Models\StoredFilterModel;
use \ion\Viewport\RedI\ViewModels\AgentViewModel;
use \ion\Viewport\RedI\ViewModels\AgencyViewModel;
use \ion\Viewport\RedI\Feeds\Models\Agency;
use \ion\Types\StringObject;
use \ion\PhpHelper as PHP;
use \RD\Acf\Fields\Text;
use \RD\Acf\Fields\Select;
use \RD\Acf\Fields\RepeaterInline;

class RedIX {
	use \Traits\Singleton;

	/**
	 * Get list of filter buttons
	 * 
	 * @return array
	 */
	public static function get_filter_list(){
		$btns = [];
		$records = WP::getOption('redi-filters');
		foreach( $records as $record ) {
			$btn = [
				'label' => $record['description'],
				'slug' 	=> $record['slug'],
				'url'	=> $url = WP::siteLink([ $record['slug'] ], null, true, false),
			];
			$btns[$record['slug']] = $btn;
		}

		return $btns;
	}

	/**
	 * Get list of plan images
	 *
	 * @return array
	 */
	public static function GetPlanImages(): array {
		$plans = [];		
		$property = RedI::GetCurrentViewModel();
		if ($property->GetPlans() !== null) {
			if (is_array($property->GetPlans()) && count($property->GetPlans()) > 0) {
				foreach ($property->GetPlans() as $plan) {
					$label = $plan->GetLabel();
					$img = $plan->GetPrimaryImage();
					if ($img !== null) {
						$thumbSrc = $img->GetThumb();
						$imgSrc = $img->GetUrl();
						if ($thumbSrc === null) {
							$thumbSrc = $imgSrc;
						}
						if ($imgSrc !== null) {
							$caption = $img->GetCaption();
						}
						$plans[] = [
							'label' 	=> $label,
							'caption' 	=> $caption,
							'url' 		=> $imgSrc,
							'thumb' 	=> $thumbSrc,
						];
					}

				}
			}
		}
		return $plans;
	}

	/**
	 * Get agency overrides
	 *
	 * @return void
	 */
	public static function GetAgentData(){
		$property = RedI::GetCurrentViewModel();
		$o = [];
		if(PHP::isArray($property->getAgencyOverrides())) {    
			foreach($property->getAgencyOverrides() as $tmp) {		
				$o[$tmp->GetAgency()] = $tmp;
			}
		}
		$contacts = [];
		if(PHP::count($property->GetAgents()) > 0) {
			foreach ($property->GetAgents() as $agent) {
				$contacts[] = new AgentViewModel($agent);
			}
		}
		$agents_list = [];
		foreach ($contacts as $agent) {
			$agent_info = [];
			$a = null;
			if($agent instanceof AgentViewModel) {
				$a = $property->GetAgencyByName(PHP::count($agent->GetAgencies()) > 0 ? $agent->GetAgencies()[0] : null);
				if($a === null) {
					$agencies = json_decode($property->GetDevelopment()->toArray()['agencies'], true);
					foreach($agencies as $agency) {                
						if($agency['name'] == $agent->GetFirstName() || $agency['name'] == $agent->GetLastName()) {							
							$a = new AgencyViewModel(new Agency($agency));
							break;
						}
					}
				} else if($agent instanceof AgencyViewModel) {        
					$a = $property->GetAgencyByName($agent->GetName());
				}
			}
			
			$desc = null;
			$media = [];
			if($a !== null && array_key_exists($a->GetName(), $o)) {
				$desc = $o[$a->GetName()]->GetDescription();
				foreach($o[$a->GetName()]->getMedia() as $imageObj) {            
					$media[] = [
						'url' => $imageObj->getUrl(),
						'thumb' => $imageObj->getThumb(),
					];
				}
			}

			if(PHP::isEmpty($media)) {        
				foreach($property->getImages() as $imageObj) {					
					$media[] = [
						'url' => $imageObj->getUrl(),
						'thumb' => $imageObj->getThumb(),
					];
				}
			}

			if( $desc !== null ) {
				$desc = StringObject::create( nl2br( $desc ) );
				$desc = $desc->replace('"', '\\"');
				$desc = $desc->replace("\n\n", '<br />');
				$desc = $desc->stripWhiteSpace();
				$desc = Helper::content( $desc );
			}

			if ( $agent instanceof AgentViewModel ) {
				$agent_info['name'] 		= $agent->GetFirstName() . " " . $agent->GetLastName();
				$agent_info['phone'] 		= $agent->GetMobileNumber();
				$agent_info['email'] 		= $agent->GetEmailAddress();
				$agent_info['agency']		= $a !== null ? $a->GetName() : '';
				$agent_info['image'] 		= $a !== null ? $a->GetPhotoUrl() : ( $agent->GetPhotoUrl() !== null ? $agent->GetPhotoUrl() : '');
				$agent_info['media'] 		= $media;
				$agent_info['description'] 	= $desc !== null ? $desc : null;				
			} 
			else if($agent instanceof AgencyViewModel) {				
				$agent_info['name'] 		= $agent->GetName();
				$agent_info['phone'] 		= $agent->GetOfficeNumber();
				$agent_info['email'] 		= $agent->GetEmailAddress();
				$agent_info['agency'] 		= $a !== null ? $a->GetName() : '';
				$agent_info['image'] 		= $a !== null ? $a->GetPhotoUrl() : ( $agent->GetPhotoUrl() !== null ? $agent->GetPhotoUrl() : '');
				$agent_info['media'] 		= $media;
				$agent_info['description'] 	= $desc !== null ? $desc : null;
			}
			
			$agents_list[] = $agent_info;
		}

		return $agents_list;
	}


	/**
	 * Get list of options
	 *
	 * @return array
	 */
	public static function OrderOpts(): array {
		$options = [];
		$values = [      
			"Default" => "",
			"Price - low to high" => "price-low-to-high",
			"Price - high to low" => "price-high-to-low",
			"Property type" => "property-type",
			"Erf Size" => "erf-size"        
		];
		$selected = filter_input(INPUT_GET, "sort", FILTER_DEFAULT, FILTER_NULL_ON_FAILURE);
		foreach($values as $label => $value) {
			$isSelected = false;
			if ( $selected !== null && $selected === $value ) {
				$isSelected = true;
			}
			$options[] = (object) [
				'label' 	=> $label,
				'value' 	=> $value,
				'selected' 	=> $isSelected,
			];
		}
		return $options;
	}

	/**
	 * Extract excerpt from description
	 *
	 * @return string
	 */
	public static function GetExcerpt(): ?string {
		$str = '';
		ob_start();
		RedI::PropertyDescription();
		$str = ob_get_clean();	

		$str = nl2br( mb_strimwidth( wp_strip_all_tags( $str ), 0, 120, "...") );	
		$str = preg_replace("/(<br\s*\/>\s*)+/", "<br />", $str);
		return $str;
	}

	/**
	 * Check value between 2
	 *
	 * @param mixed $value
	 * @param mixed $default
	 * @return mixed
	 */
	public static function CheckValue( $value, $default = null ) {
		return ($value === null || $value === "") ? $default : $value;
	}

	/**
	 * Get list of features
	 *
	 * @return array|null
	 */
	public static function GetFeaturesList(): ?array {
		$list = [];
		$property = RedI::GetCurrentViewModel();
		if ( $val = $property->GetPropertyType() ) {
			$list[] =  [
				'name' 	=> 'home',
				'value' => $val,
			];
		}

		if ( $val = $property->GetBedrooms() ) {
			$list[] =  [
				'name' 	=> 'bed',
				'value' => $val,
			];
		}

		if ( $val = $property->GetBathrooms() ) {
			$list[] =  [
				'name' 	=> 'bath',
				'value' => $val,
			];
		}

		if ( $val = $property->GetGarages() ) {
			$list[] =  [
				'name' 	=> 'garage',
				'value' => $val,
			];
		}

		if ( true != true &&  $val = $property->GetSize() ) {
			$list[] =  [
				'name' 	=> 'size',
				'value' => $val ? $val . " m<sup>2</sup>" : '0' ,
			];
		}

		return $list;
	}

	/**
	 * Get page pagination
	 *
	 * @return void
	 */
	public static function PropertyPages()  {
		$page_num = get_query_var( 'page' );
		$list = [];
		$str = '';
		ob_start();
		RedI::PropertyPages();
		$str = ob_get_clean();
		if ( $str ) {
			$dom = new Dom;
			$dom->loadStr("<div>{$str}</div>");
			$links = $dom->find('a');
			foreach ( $links as $content ) {
				$link = $content->getAttribute('href');
				$label = $content->innerHtml;
				$list[] = (object) [
					'label' => $label,
					'url' => $link,
					'active' => $page_num == $label ? true : false,
				];
			}
		}
		return $list;
	}

	/**
	 * Get current layout
	 *
	 * @return void
	 */
	public static function GetCurrentLayout(): ?string {
		$layout = filter_input(INPUT_GET, "show", FILTER_DEFAULT, FILTER_NULL_ON_FAILURE)?: 1;
		if ( $layout == 1 ) {
			return 'list';
		}

		if ( $layout == 2 ) {
			return 'grid';
		}

		return '';
	}

	/**
	 * Get layout links
	 *
	 * @return void
	 */
	public static function GetLayoutLinks() {
		$str = '';
		$list = [];
		ob_start();
		RedI::DisplaySelection();
		$str = ob_get_clean();
		
		if ( $str ) {
			$dom = new Dom;
			$dom->loadStr($str);
			$links = $dom->find('a');	
			$layout = self::GetCurrentLayout();
			
			foreach ( $links as $a ) {
				if ( $a->find('i')[0]->getAttribute('class') == 'fa fa-fw fa-reorder' ) {
					$list['list'] = (object)[
						'url' => $a->getAttribute('href'),
						'active' => $layout == 'list'
					];
				}
				else if ( $a->find('i')[0]->getAttribute('class') == 'fa fa-fw fa-th' ) {
					$list['grid'] =  (object)[
						'url' => $a->getAttribute('href'),
						'active' => $layout == 'grid'
					];
				}
			}
		}
		return $list;
	}


	/**
	 * Get title
	 *
	 * @return void
	 */
	public static function GetSingleTitle() {
		$devName = RedI::DevelopmentName(false);
		$propTitle = RedI::PropertyTitle(false);
		
		$parts = [$propTitle];		
		if( $devName != $propTitle && (strpos($devName, $propTitle) !== false) ) {
			$parts[] =  $devName;
		}
		return implode(' - ', $parts);
	}

	/**
	 * Get sub title
	 *
	 * @return void
	 */
	public static function GetSingleSubTitle(){
		return RedI::PropertySubTitle();
	}

	/**
	 * Get plan selector
	 *
	 * @return void
	 */
	public static function GetPlanSelector(){
		// RedI::PlanSelection(); TODO
		// RedI::PlanSelection();
	}

	public static function GetPropertyStatus(){
		$property = RedI::GetCurrentViewModel();
        $value = null;
        $class = "";
        if ($property !== null) {
            $value = $property->GetPropertyStatus();

            if (strpos($value, "Sold") != -1) {
                $class = "sold";
            } else {
                $class = "available";
            }
        }

        return sprintf( '<span class="wf-redi-status wf-redi-status--%s">%s</span>', $class, self::CheckValue($value) );
	}

	/**
	 * Get property description
	 *
	 * @return string
	 */
	public static function PropertyDescription() {
		ob_start();
		RedI::PropertyDescription();
		return ob_get_clean();		
	}

	/**
	 * Get list of images
	 *
	 * @return void
	 */
	public static function PropertyImages() {
		$list = [];

		$property = RedI::GetCurrentViewModel();
		if ($property->GetImages() !== null) {
			$images = $property->GetOverridedImages();
			if ( ! empty( $images ) )  {
				foreach( $images as $img ) {
					$list[] = $img->GetUrl();
				}
			} else {
				$list[] = self::GetInvalidImage();
			}
		}

		return $list;
	}

	/**
	 * Get invalid image URL
	 *
	 * @return void
	 */
	public static function GetInvalidImage(){
		return WP::getContext('redi/redi-plugin')->getWorkingUri() . "/images/placeholder.gif";
	}

	/**
	 * Get list of agents
	 *
	 * @return array
	 */
	public static function GetAgents() {
		$property = RedI::GetCurrentViewModel();
		$agents = $property->GetAgents();
		$list = [];

		if ( $agents && count( $agents ) > 0 ) {
			foreach( $agents as $k => $agent ) {
				$name = sprintf('%s %s', $agent->GetFirstName(), $agent->GetLastName());
				$list[] = (object) [
					'id' => sprintf( '%d-%s', $k, sanitize_title_with_dashes( $name ) ),
					'name' => $name,
					'email' => $agent->GetEmailAddress(),
					'phone' => $agent->GetMobileNumber(),
				];
			}
		}
		return $list;
	}
	
	/**
	 * Get agent form
	 *
	 * @return void
	 */
	public static function GetAgentForm() {
		ob_start();
		WP::widget(new AgentWidget());
		$str = ob_get_clean();
		$data = [];


		if ( $str ) {
			$dom = new Dom;
			$dom->loadStr("<div>{$str}</div>");
			if ( count( $form = $dom->find('.enquiry-form') ) > 0 ) {
				$form = $form[0];
				$data['action'] = $form->getAttribute('action');

				foreach( $form->find('input, textarea, select') as $field ) {
					if ( $field->tag->name() == 'div' || $field->getAttribute('type') == 'submit' ) continue;
					$label = $field->getAttribute('placeholder');
					if ( $field->getAttribute('type') == 'checkbox' ) {
						$label = $field->nextSibling()->innerText;
					}
					
					if ( $field->tag->name() == 'select' ) {
						$prev = $field->previousSibling();
						if($prev->tag->name() == 'text') {
							$prev = $prev->previousSibling();
						}
						$label = $prev->find('label')[0]->innerText;
					}

					$data['fields'][] = (object)[
						'tag' => $field->tag->name(),
						'type' => $field->getAttribute('type'),
						'html' => $field->outerHtml,
						'label' =>  $label,
					];
				}

			}

		}
		return (object) $data;
	}


	/**
	 * Set custom page title
	 *
	 * @param [type] $title
	 * @return string
	 */
	public static function PageTitle ($header): ?string {
		$header = preg_replace('/<\s*title\s*>[^<]+<\s*\/\s*title\s*>/', '<title>' . RedI::PageTitle(false) . '</title>', $header);
		return $header;
	}

	/**
	 * Get filter inputs
	 *
	 * @return void
	 */
	public static function GetFilters() {		
		$filters = [];
		$estateName = WP::GetOption("redi-feed-estate");
		$state = new State($estateName);
		$redI = RedI::GetInstance();
		$numbers = [];

		if ($redI !== null) {	
			
			$parameters = null;
			if ($state->GetFilter() !== null) {
				$parameters = $state->GetFilter();
			}


			$storedFilter = State::GetStoredFilter($state->GetStoredFilterName());

			// Numbers
			$numbers = [
				"bedrooms" => [
					"null" => "Any",
					"options" => [
						// "1" => "1",
						// "2" => "2",
						// "3" => "3",
						// "4" => "4",
						// "5" => "5+"
					],
					"state" => $parameters ? $parameters->Get('bedrooms') : '',
				],
				"bathrooms" => [
					"null" => "Any",
					"options" => [
					// "1" => "1",
					// "2" => "2",
					// "3" => "3",
					// "4" => "4",
					// "5" => "5+"
					],
					"state" => $parameters ? $parameters->Get('bathrooms') : '',
				],
				"garages" => [
					"null" => "Any",
					"options" => [
						// "1" => "1",
						// "2" => "2",
						// "3" => "3",
						// "4" => "4",
						// "5" => "5+"
					],
					"state" => $parameters ? $parameters->Get('garages') : '',
				]
				,
				"propertyStatus" => [
					"null" => "Any",
					"options" => StoredFilterModel::GetWidgetStatusArray($storedFilter),
					"state" => $parameters ? $parameters->Get('propertyStatus') : '',
				]
			];

			if (WP::getOption('redi-filter-widget-type-enabled', false)) {
				$numbers["propertyType"] = [
					"null" => "Any",
					"options" => StoredFilterModel::GetWidgetTypeArray($storedFilter),
					"state" => $parameters ?  $parameters->Get('propertyType') : '',
				];
			}

			if (WP::getOption('redi-filter-widget-phase-enabled', false)) {
				$numbers["phase"] = [
					"null" => "Any",
					"options" => StoredFilterModel::GetWidgetPhaseArray($storedFilter),
					"state" => $parameters ? $parameters->Get('phase') : '',
				];
			}

			
			$max = [
				'bedrooms' => RedI::GetValues()['bedrooms'],
				'garages' => RedI::GetValues()['garages'],
				'bathrooms' => RedI::GetValues()['bathrooms']
			];
			foreach ($max as $maxItem => $maxValue) {
				if ($maxValue !== null) {
					for ($tmp = 1; $tmp <= $maxValue; $tmp++) {
						$s = (string) $tmp;
						if ($tmp == $maxValue) {
							$s .= '+';
						}
						$numbers[$maxItem]['options'][(string) $tmp] = $s;
					}
				} else {
					$numbers[$maxItem]['options'][(string) $tmp] = '1+';
				}
			}
			
			// Dev
			$devs = StoredFilterModel::GetDevelopmentStatusArray($storedFilter);
			if (is_array(RedI::GetDevelopments()) && count(RedI::GetDevelopments()) > 0) {
				$options = [];
				if (is_array(RedI::GetDevelopments())) {                    
					foreach (RedI::GetDevelopments() as $development) {                        
						if ($devs !== null && in_array($development->Get('code'), $devs) || StoredFilterModel::GetDevelopmentStatusArray($storedFilter) === null) {
							$options[] = ['label' => $development->Get('label'), 'value' => $development->Get('code')];
						}
					}
				}
				$numbers['dev'] = [
					"null" => "Any",
					"options" => [],
					'state' => $state->GetDevelopment(),
				];
				foreach( $options as $opt ) {
					$numbers['dev']['options'][$opt['value']] = $opt['label'];
				}
			}

			// Range
			$priceMin = null;
			$priceMax = null;
			$numbersSelected = [];
			if ($state->GetFilter() !== null) {
				$priceMin = $state->GetFilter()->Get("minPrice");
				$priceMax = $state->GetFilter()->Get("maxPrice");
				foreach (array_keys($state->GetFilter()->ToArray()) as $keyId) {
					if ($state->GetFilter()->Get($keyId) !== null) {
						$numbersSelected[$keyId] = $state->GetFilter()->Get($keyId);
					}
				}
			}
			$range = [];

			$range['min_html'] = Redi::FormatCurrency($priceMin === null ? 0 : (float) $priceMin, 'price-min');
			$range['max_html']  = Redi::FormatCurrency($priceMax === null ? 160000000 : (float) $priceMax, 'price-max');
	
			$range['min']  = RedI::GetValues()['minPrice'] === 0 ? 0 : RedI::GetValues()['minPrice'];			
			$range['max']  = RedI::GetValues()['maxPrice'] === 0 ? 16000000 : RedI::GetValues()['maxPrice'];		
			$range['stateMin']  = ($priceMin === null ? 0 : (float) $priceMin);
			$range['stateMax']  = ($priceMax === null ? 160000000 : (float) $priceMax);


			$numbers['range'] = $range;
		}

		$filters = [
			'url' => State::BuildIndexLink(new State($estateName), 1, 0, false, null, null, '{*}', null),
			'fields' => $numbers,
		];
		return $filters;
	}

	/**
	 * Get property price on single page
	 *
	 * @return void
	 */
	public static function GetPropertyPrice(){
		$price = RedI::PropertyPrice(false);
		echo $price;
	}

	/**
	 * Get list of filter buttons
	 * 
	 * @return array
	 */
	public static function get_filter_btns(){
		$btns_list = [];
		$estateName = WP::GetOption("redi-feed-estate");
		$filter_name = '';

		if ($estateName !== null) {
			$state = new State($estateName);
			$filter_name = $state->getStoredFilterName();

			$url_format = State::BuildIndexLink($state, 1, 0, true, null, null, null, 'FILTER_SLUG');

			$filters = self::get_filter_list();
			$btns_settings  = Option('redi/filter-btns');
			if (is_array($btns_settings)){
				foreach( $btns_settings as $item ) {
					if ( isset( $filters[$item['slug']] ) ) {
						$btns_list[] = [
							'label' => $item['label'],
							'url'	=> str_replace( 'FILTER_SLUG', $item['slug'], $url_format),
							// 'url'	=> $filters[$item['slug']]['url'],
							'is_active' => !! ( $filter_name == $item['slug'] ),
						];
					}
				}
			}
		}

		return $btns_list;
	}


	/**
	 * Init
	 */
	public function init() {
		add_action('acf/init', [$this, 'setup_settings']);
	}

	/**
	 * Setup settings options
	 * 
	 * @return void
	 */
	public function setup_settings(){
		$btns = self::get_filter_list();
		$choices = [];
		foreach( $btns as $btn ) {
			$choices[$btn['slug']] = $btn['label'];
		}
		Settings::instance()->register_module_settings('redi', 'RedI', [
			RepeaterInline::make('Filter Buttons','redi/filter-btns')->buttonLabel('Add filter')->fields([
				Select::make('slug')->choices($choices),
				Text::make('Label'),
			]),
		]);
	}



	/**
	 * Check if its RedI page
	 *
	 * @return boolean
	 */
	public static function is_redi_page(): bool {
		$estateName = WP::GetOption("redi-feed-estate");
		$state = new State($estateName);
		$redI = RedI::GetInstance();
		return ( $state->IsProperty() || $state->IsPropertyIndex() );
	}
}