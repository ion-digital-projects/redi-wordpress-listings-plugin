<?php 
/**
 * Template name: WF - RedI Tiles
 */
require_once(__DIR__ . "/waterfall/trait-singleton.php"); 
require_once(__DIR__ . "/waterfall/class-redix.php");

use \ion\WordPress\WordPressHelper as WP;
use \ion\Viewport\RedI\RedIFeedPlugIn AS RedI;
use \ion\Viewport\RedI\RedIFeedFilterWidget as FilterWidget;
use \ion\Viewport\RedI\RedIFeedOrderingWidget as OrderingWidget;


$params = PropertyListing::instance()->params();
$layout = $params->show == 2 ? 'tiles' : 'list';

?>

<?= RedIX::PageTitle( Helper::template('global/header') ) ?>
	<div class="wf-page-screen" data-id="<?= get_the_ID() ?>" data-screen="ScreenPage" style="<?= Helper::css([]) ?>">
		<div class="wf-page-body wf-redi-body">
			<?= Helper::template('navbar/navbar') ?>
			<div class="wf-redi">
				<div class="container">
					<?= Helper::template('redi/hero') ?>
					<?= Helper::template('redi/grid') ?>
				</div>
			</div>		
			<div class="wf-redi-grid__sidebar">	
				<div class="wf-redi-grid__sidebar-item" data-sidebar-id="filter">
					<a class="wf-redi-grid__sidebar-item-close">
						<?= Helper::svg('property-listing-sidebar-close') ?>
					</a>
					<?= Helper::template('redi/filters'); ?>
				</div>
				<div class="wf-redi-grid__sidebar-item" data-sidebar-id="contact">
					<a class="wf-redi-grid__sidebar-item-close">
						<?= Helper::svg('property-listing-sidebar-close') ?>
					</a>
					<?= Helper::template('redi/agent-contact') ?>
				</div>
			</div>	
			
			<?= Helper::template('redi/pagination') ?>
			<?= Helper::template('footer/footer') ?>
		</div>		
	</div>	
<?= Helper::template('global/footer') ?>