<?php
/*

  Template Name: RedI Listings (404)

 */

use \ion\WordPress\WordPressHelper as WP;

include_once(get_template_directory() . "/header.php");

use \ion\Viewport\RedI\RedIFeedPlugIn AS RedI;

?>

<span class="red-i">

<div class="spacer"></div>

<div class="banner">
    <div class="wrapper">
        <h1>404</h1>
    </div><!-- /wrapper -->	
</div><!-- /banner -->

<div class="main-content">
    <div class="wrapper">
        <div class="copy nosidebar">
            <p>
                We couldn't find anything here - would you like to <a href='/'>go home</a>?
            </p>
        </div><!--/copy -->
        <?php //include_once("sidebar.php"); ?>        
    </div><!--/wrapper -->
</div><!--/main-content -->


</span>

<?php include_once(get_template_directory() . "/footer.php"); ?>


