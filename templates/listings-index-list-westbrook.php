<?php
/*

  Template Name: REDi Listings (List - Westbrook)

 */

use \ion\WordPress\WordPressHelper as WP;
use \ion\Viewport\RedI\RedIFeedPlugIn AS RedI;
use \ion\Viewport\RedI\RedIFeedFilterWidget as FilterWidget;
use \ion\Viewport\RedI\RedIFeedOrderingWidget as OrderingWidget;

//include_once( __DIR__ . "/westbrook/dev_header.php");


include_once( __DIR__ . "/westbrook/list.php");


//include_once( __DIR__ . "/westbrook/dev_footer.php");