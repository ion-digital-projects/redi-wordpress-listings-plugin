<?php

/* 
 * See license information at the package root in LICENSE.md
 */


use \ion\WordPress\WordPressHelper as WP;

$dir = WP::getContentPath() . '/uploads/fusion-styles/';

$files = glob("{$dir}*.css");

if(count($files) > 0) {
    define('FUSION_FILENAME', pathinfo($files[0])['basename']);
}