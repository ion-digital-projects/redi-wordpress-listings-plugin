<?php
/*

  Template Name: RedI Listings (list - Avada)

 */

require_once(__DIR__ . '/avada/common.php');

use \ion\WordPress\WordPressHelper as WP;
use \ion\Viewport\RedI\RedIFeedPlugIn AS RedI;
use \ion\Viewport\RedI\RedIFeedFilterWidget as FilterWidget;
use \ion\Viewport\RedI\RedIFeedOrderingWidget as OrderingWidget;

$siteUrl = WP::siteLink(null, null, true, false);

WP::addStyle('infi-elegant-elements', "{$siteUrl}wp-content/plugins/elegant-elements-fusion-builder/assets/css/min/elegant-elements.min.css", false, true, false, 'screen', 1000);
WP::addStyle('infi-elegant-animations', "{$siteUrl}wp-content/plugins/elegant-elements-fusion-builder/assets/css/min/infi-css-animations.min.css", false, true, false, 'screen', 1000);
WP::addStyle('infi-elegant-combined-css', "{$siteUrl}wp-content/plugins/elegant-elements-fusion-builder/assets/css/min/elegant-elements-combined.min.css", false, true, false, 'screen', 1000);
WP::addStyle('elegant-google-fonts', "https://fonts.googleapis.com/css?family=Open+Sans%3Aregular%7COpen+Sans%3A300&#038;", false, true, false, 'screen', 1000);
WP::addStyle('fusion-dynamic-css', "{$siteUrl}wp-content/uploads/fusion-styles/" . FUSION_FILENAME, false, true, false, 'screen', 1000000, null, ['avada-stylesheet']);
WP::addStyle('', "{$siteUrl}wp-content/themes/Avada/assets/css/style.min.css?ver=5.8", false, true, false, 'screen', 1000);

WP::addStyle('redi-custom', <<<INLINE
.property-info .email-form form.enquiry-form input::placeholder {
    color: #3336 !important;
}
INLINE
        , false, true, true);

WP::addScript('avada-menu-vars', <<<INLINE
/* <![CDATA[ */
var avadaMenuVars = {"header_position":"Top","logo_alignment":"Left","header_sticky":"1","side_header_break_point":"1150","mobile_menu_design":"modern","dropdown_goto":"Go to...","mobile_nav_cart":"Shopping Cart","submenu_slideout":"1"};
/* ]]> */   
INLINE
        ,false, true, true, false, 1000
        );

WP::addScript('modernizr', "{$siteUrl}wp-content/themes/Avada/includes/lib/assets/min/js/library/modernizr.js", false, true, false, false, 1);

WP::addScript('hover-intent', "{$siteUrl}wp-content/themes/Avada/includes/lib/assets/min/js/library/jquery.hoverintent.js", false, true, false, true, 1000, null, ['modernizr']);

//RedI::startDebugLogEntry('Avada Index Template (List - Including header)');
?>
<!-- Header -->
<?php RedI::themeHeader(); ?>
<!-- /Header -->

<span class="red-i">

<div class="spacer"></div>

<?php if (RedI::HasDevelopment() && RedI::ShowTitle()): ?>
    <div class="banner">	
        <div class="wrapper">
		
            <h1><?php RedI::DevelopmentName(); ?></h1>
			
        </div><!-- /wrapper -->	
    </div><!-- /banner -->
<?php endif; ?>



<div class="main-content">

    <div class="wrapper">

        <div class="property-listings list">

            <div class="results">

                <?php WP::addWidget(new OrderingWidget()); ?>

                <div class="showing">
                    Showing:                     
                    <?php if(Redi::GetTotalCount() > 0): ?>
                    <strong><?php RedI::PropertyRecordsCurrentPage(); ?></strong> of <?php Redi::PropertyRecordsTotal(); ?>
                    <?php else: ?>
                    <strong>No properties</strong>
                    <?php endif; ?>
                </div><!--/showing -->

                <?php RedI::DisplaySelection(); ?>

            </div><!--/results -->


            <?php WP::DoShortCode("redi-search"); ?>
            

            <?php Redi::resetViewModelIndex(); while (RedI::GetNextViewModel() !== null): ?>

                <!-- <?php RedI::PropertyLabel(); ?> -->
                <div class="property">
                    <div class="image">
                        <span class="type"><?php RedI::PropertyStatus(); ?></span>
                        <?php RedI::PropertyPrimaryImage(true, RedI::PropertyLink(false), true); ?>
                    </div>
                    <div class="copy">
                        <h3><a href="<?php RedI::PropertyLink(); ?>"><?php RedI::PropertyTitle(); ?></a></h3>
                        <div class="price"><?php RedI::PropertyMinPrice(); ?></div>
                        <p><?php RedI::PropertyDescription(); ?></p>
                        <?php RedI::PropertyFeatures(true, true, true, true, false); ?>
                    </div>
                </div><!--/property -->   

            <?php endwhile; ?>


            <?php if (RedI::HasPreviousPropertyPage() || RedI::HasNextPropertyPage()): ?>
                <div class="pagination">	
                    <?php if (RedI::HasPreviousPropertyPage()): ?>
                        <a href="<?php RedI::PreviousPropertyPageLink(); ?>" class="btn outline prev"> <i class="fa fa-fw fa-angle-left"></i> Back</a>
                    <?php endif; ?>
                    <?php if (RedI::HasNextPropertyPage()): ?>
                        <a href="<?php RedI::NextPropertyPageLink(); ?>#" class="btn outline next">Next <i class="fa fa-fw fa-angle-right"></i></a>
                    <?php endif; ?>
                    <?php RedI::PropertyPages(); ?>
                </div><!--/pagination -->
            <?php endif; ?>

        </div><!--/property-listings -->


    </div><!--/wrapper -->


</div><!--/main-content -->

</span>

<?php 
//include_once(get_template_directory() . "/footer.php"); 

RedI::themeFooter();

//RedI::endDebugLogEntry('Avada Index Template (List - Including header)');
//RedI::endDebugLogEntry('Avada Index Template (List - EXcluding header)');
