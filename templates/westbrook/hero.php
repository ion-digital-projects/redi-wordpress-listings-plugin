<?php

/* 
 * See license information at the package root in LICENSE.md
 */

?>
                        
<!-- ==== Beginning Hero section ==== -->
<section class="red-i hero-section">
    <div class="large-container clear">
         
        <div class="hero-inner-wrap " style="background-image: url(http://westbrook.flywheelsites.com/wp-content/uploads/2020/05/the-ridge-gallery-image-4@x2.jpg)">
           <div class="hero-text">
               <h1>Units for Sale</h1>
           </div>          
        </div>
    </div>
</section>

<!-- ==== End Hero section ==== -->   
