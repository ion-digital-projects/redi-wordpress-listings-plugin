<?php

/* 
 * See license information at the package root in LICENSE.md
 */

use \ion\PhpHelper as PHP;
use \ion\WordPress\WordPressHelper as WP;
use \ion\Viewport\RedI\RedIFeedPlugIn AS RedI;

if(!PHP::strEndsWith(PHP::filterInput('HTTP_HOST', [ INPUT_SERVER ]),'ion.digital')) {
    
    RedI::themeHeader();
}
else {
    
    include_once( __DIR__ . "/dev_header.php" );
}
?>



<?php

$link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$pathPeace = "property";

if(!strpos($link, $pathPeace) === true){
    include_once(__DIR__ . "/hero.php"); 
    
}

 ?>
<section class="content-wrap">
    <span class="red-i">

    <?php

    // $link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    // $pathPeace = "property";
    
    if(!strpos($link, $pathPeace) === false){
        include_once(__DIR__ . "/pagination-header.php"); 
        
        include_once(__DIR__ . "/headerClassKeep.php");

    }
    
    
    ?>
        <span class="property-listings">
            <div class="md-container clear">