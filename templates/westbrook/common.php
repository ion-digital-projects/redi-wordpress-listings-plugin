<?php

use \ion\Viewport\RedI\RedIFeedPlugIn AS RedI;
use \ion\Viewport\RedI\State;

function swatch($echo = true, $development = null) {

    if($development === null) {
        
        $development = RedI::DevelopmentName(false);
    }
    
    if(empty($development)) {
        
        return "<!-- no swatch -->";
    }

    
    $colours = [
        'Hillcrest' => "#cfc3ad",
        'Hillside' => "#f2dbd3",
        'Hilltop' => "#ebc8c4",
        'Lake Side' => "#d7e2da",
        'River Side' => "#b4dce3",
        'River Dale' => "#f4e3c9",
        'The Ridge' => "#d9ddae",
        'Valley View' => "#cfd0c0",
        'Waterfall' => "#cfdaf0"
    ];
    
    $html = null;
    $css = null;
    
    if(array_key_exists($development, $colours)) {
        
        $css = "background-color: {$colours[$development]};";
        
    } else {
        
        $css = "border: solid 1px var(--grey-text-colour); background-color: var(--secondary);";
    }
    
    $html = "<!-- [{$development}] -->\n<span class=\"swatch\" style=\"{$css}\"></span>";

    if($echo) {

        echo $html;
    }

    return $html;
}