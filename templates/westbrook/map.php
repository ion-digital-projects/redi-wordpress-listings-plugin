<?php

/* 
 * See license information at the package root in LICENSE.md
 */

use \ion\Viewport\RedI\RedIFeedPlugIn AS RedI;

?>

<div class="map-link">
    <div>
        <h3>Use our interactive sales map to see available units.</h3>
        <span class="map-mobile">Explore our estate villages</span>
        <a target="_blank" href="<?php

if (RedI::HasPropertyMapLink()) { RedI::PropertyMapLink(); } else { echo "https://salesmap.co.za/westbrook/"; }
            
?>"><span>View sales map</span></a>
    </div>
</div>