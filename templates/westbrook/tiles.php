<?php
use \ion\PhpHelper as PHP;
use \ion\WordPress\WordPressHelper as WP;
use \ion\Viewport\RedI\RedIFeedPlugIn AS RedI;
use \ion\Viewport\RedI\RedIFeedAgentWidget as AgentWidget;
use \ion\Viewport\RedI\RedIFeedOrderingWidget as OrderingWidget;
?>

<?php include_once(__DIR__ . "/common.php"); ?>

<?php include_once(__DIR__ . "/header.php"); ?>

<?php include_once(__DIR__ . "/search.php"); ?>

<?php include_once(__DIR__ . "/results.php"); ?>

<span class="properties-tiles">
    <?php Redi::resetViewModelIndex(); 
    while ($model = RedI::GetNextViewModel() !== null): ?>

        <!-- <?php RedI::PropertyLabel(); ?> -->
        <div class="property">
            <div class="image">
                <!-- <span class="type"><?php RedI::PropertyStatus(); ?></span> -->
                <?php RedI::PropertyPrimaryImage(true, RedI::PropertyLink(false), true); ?>
            </div>
            <div class="copy">
                <div class="property-title">                    
                    <?php swatch(); ?>
                    <div class="development-title"><?php RedI::DevelopmentName(); ?></div>
                    

                    <?php if(WP::getOption('redi-show-unit-number', true)): ?>
                        <div class="title-divider"></div>
                        <div class="phase-title">Unit <?php RedI::PropertyUnitNumber(); ?></div><br />
                        <?php endif; ?>
                        <!-- <div class="phase-title">Phase <?php// RedI::PropertyPhase(); ?></div> -->
                </div>

                <div class="property-features">
                    <?php RedI::PropertyFeatures(false, true, false, false, false, false); ?>                                        
                    <!--<div class="divider"></div>-->
                    <?php RedI::PropertyFeatures(false, false, true, false, false, false); ?>
                    <!--<div class="divider"></div>-->
                    <?php RedI::PropertyFeatures(false, false, false, true, false, true); ?>
                    <!--<div class="divider"></div>-->
                   
                </div>

                <div class="property-price"><?php RedI::PropertyMinPrice(); ?></div>

                <a class="button" href="<?php RedI::PropertyLink(); ?>">
                    <span>View more</span>
                </a>
            </div>
        </div><!--/property -->   

    <?php endwhile;?>   
</span>



<?php include_once(__DIR__ . "/pagination.php"); ?>
<script>

    

 

</script>


<?php include_once(__DIR__ . "/footer.php");

