<?php 

use \ion\PhpHelper as PHP;
use \ion\WordPress\WordPressHelper as WP;
use \ion\Viewport\RedI\RedIFeedPlugIn AS RedI;

if(PHP::filterInput('HTTP_HOST', [ INPUT_SERVER ])!= 'demo.ion.digital'):
    
RedI::themeHeader();

include_once( __DIR__ . "/hero.php" );
    
else: ?><!DOCTYPE html>
<html land="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, initial-scale=1.0, user-scalable=no">
    <meta name="format-detection" content="telephone=no">
    <title> &raquo; Units for Sale</title>
    <link rel="icon" href="http://westbrook.flywheelsites.com/wp-content/themes/westbrook/img/favicon.png" type="image/png">
    <link rel="apple-touch-icon" href="http://westbrook.flywheelsites.com/wp-content/themes/westbrook/img/touch.png">
    <link rel="stylesheet" type="text/css" href="http://westbrook.flywheelsites.com/wp-content/themes/westbrook/normalize.min.css">
    <link rel="stylesheet" type="text/css" href="http://westbrook.flywheelsites.com/wp-content/themes/westbrook/css/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="http://westbrook.flywheelsites.com/wp-content/themes/westbrook/css/selectric.css">
    <link rel="stylesheet" type="text/css" href="http://westbrook.flywheelsites.com/wp-content/themes/westbrook/css/slick-theme.css">
    <link rel="stylesheet" type="text/css" href="http://westbrook.flywheelsites.com/wp-content/themes/westbrook/css/slick.css">
    <link rel="stylesheet" type="text/css" href="http://westbrook.flywheelsites.com/wp-content/themes/westbrook/css/jquery.fancybox.min.css">
    <link rel="stylesheet" type="text/css" href="http://westbrook.flywheelsites.com/wp-content/themes/westbrook/style.css">
	
    
<script type='text/javascript' src='https://code.jquery.com/jquery-2.2.4.min.js'></script>
<script type='text/javascript' src='http://demo.ion.digital/redi/wp/wp-content/plugins/redi-wordpress-listings-plugin/scripts/wNumb.js?ver=1592899704'></script>
<script type='text/javascript' src='http://demo.ion.digital/redi/wp/wp-content/plugins/redi-wordpress-listings-plugin/scripts/nouislider.js?ver=1592899704'></script>
<script type='text/javascript' src='http://demo.ion.digital/redi/wp/wp-content/plugins/redi-wordpress-listings-plugin/scripts/RedIFeedPlugIn.js?ver=1592899704'></script>
<link rel='stylesheet' id='no-ui-slider-css'  href='http://demo.ion.digital/redi/wp/wp-content/plugins/redi-wordpress-listings-plugin/styles/nouislider.css?ver=1592899704' type='text/css' media='screen' />
<link rel='stylesheet' id='RedIFeedPlugIn-css'  href='http://demo.ion.digital/redi/wp/wp-content/plugins/redi-wordpress-listings-plugin/styles/RedIFeedPlugIn.css?ver=1592899704' type='text/css' media='screen' />
<link rel='stylesheet' id='RedIFeedPlugIn-css'  href='http://demo.ion.digital/redi/wp/wp-content/plugins/redi-wordpress-listings-plugin/styles/SiteSpecific_Westbrook.css' type='text/css' media='screen' />

    <meta name='robots' content='noindex,nofollow' />
<link rel='dns-prefetch' href='//s.w.org' />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/westbrook.flywheelsites.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.4.2"}};
			/*! This file is auto-generated */
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
	<link rel='stylesheet' id='wp-block-library-css'  href='http://westbrook.flywheelsites.com/wp-includes/css/dist/block-library/style.min.css?ver=5.4.2' media='all' />
<link rel='stylesheet' id='addtoany-css'  href='http://westbrook.flywheelsites.com/wp-content/plugins/add-to-any/addtoany.min.css?ver=1.15' media='all' />
<script type='text/javascript' src='http://westbrook.flywheelsites.com/wp-content/themes/westbrook/js/lib/conditionizr-4.3.0.min.js?ver=4.3.0'></script>
<script type='text/javascript' src='http://westbrook.flywheelsites.com/wp-content/themes/westbrook/js/lib/modernizr-2.7.1.min.js?ver=2.7.1'></script>
<script type='text/javascript' src='http://westbrook.flywheelsites.com/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp'></script>
<script type='text/javascript' src='http://westbrook.flywheelsites.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<script type='text/javascript' src='http://westbrook.flywheelsites.com/wp-content/plugins/add-to-any/addtoany.min.js?ver=1.1'></script>
<link rel='https://api.w.org/' href='http://westbrook.flywheelsites.com/wp-json/' />
<link rel="alternate" type="application/json+oembed" href="http://westbrook.flywheelsites.com/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fwestbrook.flywheelsites.com%2Fsales%2Funits-for-sale%2F" />
<link rel="alternate" type="text/xml+oembed" href="http://westbrook.flywheelsites.com/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fwestbrook.flywheelsites.com%2Fsales%2Funits-for-sale%2F&#038;format=xml" />

<script data-cfasync="false">
window.a2a_config=window.a2a_config||{};a2a_config.callbacks=[];a2a_config.overlays=[];a2a_config.templates={};a2a_localize = {
	Share: "Share",
	Save: "Save",
	Subscribe: "Subscribe",
	Email: "Email",
	Bookmark: "Bookmark",
	ShowAll: "Show All",
	ShowLess: "Show less",
	FindServices: "Find service(s)",
	FindAnyServiceToAddTo: "Instantly find any service to add to",
	PoweredBy: "Powered by",
	ShareViaEmail: "Share via email",
	SubscribeViaEmail: "Subscribe via email",
	BookmarkInYourBrowser: "Bookmark in your browser",
	BookmarkInstructions: "Press Ctrl+D or \u2318+D to bookmark this page",
	AddToYourFavorites: "Add to your favourites",
	SendFromWebOrProgram: "Send from any email address or email program",
	EmailProgram: "Email program",
	More: "More&#8230;",
	ThanksForSharing: "Thanks for sharing!",
	ThanksForFollowing: "Thanks for following!"
};

(function(d,s,a,b){a=d.createElement(s);b=d.getElementsByTagName(s)[0];a.async=1;a.src="https://static.addtoany.com/menu/page.js";b.parentNode.insertBefore(a,b);})(document,"script");
</script>
</head>
<body class="page-template-default page page-id-19 page-child parent-pageid-17 units-for-sale">

    <main class="main-wrap">
        <!--  Beginning header section ======  -->
        <header class="main-header-section">
            <div class="common-wrap clear">
                <div class="logo-wrap">
                    <a href="http://westbrook.flywheelsites.com" class="main-logo desk">
                        <img class="black-logo" src="http://westbrook.flywheelsites.com/wp-content/themes/westbrook/svg/westbrook-logo.svg" alt="">
                        <img class="white-logo" src="http://westbrook.flywheelsites.com/wp-content/themes/westbrook/svg/westbrook-logo-white.svg" alt="">
                    </a>
                    <a href="http://westbrook.flywheelsites.com" class="main-logo mobi"><img src="http://westbrook.flywheelsites.com/wp-content/themes/westbrook/svg/westbrook-logo-white.svg" alt=""></a>
                    <div class="phone-link mobi">
                        <a href="tel:#"><img src="http://westbrook.flywheelsites.com/wp-content/themes/westbrook/svg/24px-whatsapp-white.svg" alt=""></a>
                    </div>
                    <div class="phone-nav">
                        <div></div>
                    </div>
                </div>             
                <div class="nav-wrap">
                <nav class="main-nav">
                    <ul>
                        <li class="mobi"><a href="http://westbrook.flywheelsites.com">Home</a></li>
                    </ul>
                        <ul id="menu-header-menu" class="menu"><li id="menu-item-212" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-212"><a href="http://westbrook.flywheelsites.com/the-estate/">The Estate</a>
<ul class="sub-menu">
	<li id="menu-item-213" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-213"><a href="http://westbrook.flywheelsites.com/the-estate/overview/">Overview</a></li>
	<li id="menu-item-242" class="menu-item menu-item-type-post_type menu-item-object-village menu-item-242"><a href="http://westbrook.flywheelsites.com/village/the-ridge/">The Ridge</a></li>
	<li id="menu-item-241" class="menu-item menu-item-type-post_type menu-item-object-village menu-item-241"><a href="http://westbrook.flywheelsites.com/village/riverdale/">Riverdale</a></li>
	<li id="menu-item-240" class="menu-item menu-item-type-post_type menu-item-object-village menu-item-240"><a href="http://westbrook.flywheelsites.com/village/river-side/">River Side</a></li>
	<li id="menu-item-239" class="menu-item menu-item-type-post_type menu-item-object-village menu-item-239"><a href="http://westbrook.flywheelsites.com/village/waterfall/">Waterfall</a></li>
	<li id="menu-item-238" class="menu-item menu-item-type-post_type menu-item-object-village menu-item-238"><a href="http://westbrook.flywheelsites.com/village/hillside/">Hillside</a></li>
	<li id="menu-item-237" class="menu-item menu-item-type-post_type menu-item-object-village menu-item-237"><a href="http://westbrook.flywheelsites.com/village/lake-side/">Lake Side</a></li>
	<li id="menu-item-236" class="menu-item menu-item-type-post_type menu-item-object-village menu-item-236"><a href="http://westbrook.flywheelsites.com/village/hill-crest/">Hill Crest</a></li>
	<li id="menu-item-235" class="menu-item menu-item-type-post_type menu-item-object-village menu-item-235"><a href="http://westbrook.flywheelsites.com/village/hill-top/">Hill Top</a></li>
	<li id="menu-item-234" class="menu-item menu-item-type-post_type menu-item-object-village menu-item-234"><a href="http://westbrook.flywheelsites.com/village/valley-view/">Valley View</a></li>
</ul>
</li>
<li id="menu-item-214" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-214"><a href="http://westbrook.flywheelsites.com/lifestyle/">Lifestyle</a>
<ul class="sub-menu">
	<li id="menu-item-215" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-215"><a href="http://westbrook.flywheelsites.com/lifestyle/the-suburb/">The Suburb</a></li>
	<li id="menu-item-216" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-216"><a href="http://westbrook.flywheelsites.com/lifestyle/the-lifestyle/">The Lifestyle</a></li>
	<li id="menu-item-217" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-217"><a href="http://westbrook.flywheelsites.com/lifestyle/security/">Security</a></li>
	<li id="menu-item-218" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-218"><a href="http://westbrook.flywheelsites.com/lifestyle/facilities/">Facilities</a></li>
	<li id="menu-item-219" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-219"><a href="http://westbrook.flywheelsites.com/lifestyle/curro-school/">Curro School</a></li>
</ul>
</li>
<li id="menu-item-220" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-220"><a href="http://westbrook.flywheelsites.com/gallery/">Gallery</a>
<ul class="sub-menu">
	<li id="menu-item-221" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-221"><a href="http://westbrook.flywheelsites.com/gallery/images/">Images</a></li>
	<li id="menu-item-222" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-222"><a href="http://westbrook.flywheelsites.com/gallery/videos/">Videos</a></li>
	<li id="menu-item-223" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-223"><a href="http://westbrook.flywheelsites.com/gallery/podcasts/">Podcasts</a></li>
</ul>
</li>
<li id="menu-item-224" class="menu-item menu-item-type-post_type menu-item-object-page current-page-ancestor current-menu-ancestor current-menu-parent current-page-parent current_page_parent current_page_ancestor menu-item-has-children menu-item-224"><a href="http://westbrook.flywheelsites.com/sales/">Sales</a>
<ul class="sub-menu">
	<li id="menu-item-225" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-225"><a href="http://westbrook.flywheelsites.com/sales/sales-map/">Sales Map</a></li>
	<li id="menu-item-226" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-19 current_page_item menu-item-226"><a href="http://westbrook.flywheelsites.com/sales/units-for-sale/" aria-current="page">Units for Sale</a></li>
</ul>
</li>
<li id="menu-item-227" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-227"><a href="http://westbrook.flywheelsites.com/blog/">Blog</a>
<ul class="sub-menu">
	<li id="menu-item-228" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-228"><a href="http://westbrook.flywheelsites.com/blog/blog/">Blog</a></li>
	<li id="menu-item-229" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-229"><a href="http://westbrook.flywheelsites.com/blog/press/">Press</a></li>
	<li id="menu-item-230" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-230"><a href="http://westbrook.flywheelsites.com/blog/construction-updates/">Construction Updates</a></li>
</ul>
</li>
<li id="menu-item-231" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-231"><a href="http://westbrook.flywheelsites.com/the-developer/">The Developer</a></li>
<li id="menu-item-232" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-232"><a href="http://westbrook.flywheelsites.com/contact-us/">Contact Us</a></li>
</ul>                </nav>
                <div class="interested-wrap">
                    <h4>Interested?</h4>
                    <a href="http://westbrook.flywheelsites.com/contact-us/" class="btn transparent">Request A Viewing</a>
                                            <div class="follow-us-wrap">
                            <h6>FOLLOW US:</h6>
                                                            <a href="#" target="_blank"><img src="http://westbrook.flywheelsites.com/wp-content/themes/westbrook/svg/social-facebook.svg" alt=""></a>
                                                            <a href="#" target="_blank"><img src="http://westbrook.flywheelsites.com/wp-content/themes/westbrook/svg/social-twitter.svg" alt=""></a>
                                                    </div>
                                    </div>
            </div>
            </div>
        </header>
        <!-- ==== End header section ==== -->
            

<!-- ==== Beginning main content section ==== -->
<section class="main-content-wrap">
            		<div class="gen-bg">
                        
<!-- ==== Beginning Hero section ==== -->
<section class="hero-section">
    <div class="large-container clear">
         
        <div class="hero-inner-wrap " style="background-image: url(http://westbrook.flywheelsites.com/wp-content/uploads/2020/05/the-ridge-gallery-image-4@x2.jpg)">
           <div class="hero-text">
               <h1>Units for Sale</h1>
                          </div>
            
        </div>
    </div>
</section>

<!-- ==== End Hero section ==== -->   
                        </div>
            
<!-- For Gallery Pages -->
	
</section>

<!-- //End main content section -->

<?php endif; ?>