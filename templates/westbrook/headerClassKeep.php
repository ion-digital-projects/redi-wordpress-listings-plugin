<script>

function checkWindowSize(){
    var w = document.documentElement.clientWidth;
    var elementWidth = document.getElementsByClassName("md-container");
    var elementPagination = document.getElementsByClassName("single-pagination");
    var elementSize = elementWidth[0].offsetWidth
    var paddingA = (w - elementSize)/2 +24;

if(!(w<=360))
{
    elementPagination[0].style.padding = "20px " + paddingA + "px";
}else{
    elementPagination[0].style.padding = "20px 0px";
}   
}
window.addEventListener("load", checkWindowSize);
window.addEventListener("resize", checkWindowSize);

</script>
