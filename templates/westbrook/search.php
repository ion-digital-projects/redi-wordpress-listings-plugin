<?php

/* 
 * See license information at the package root in LICENSE.md
 */

use \ion\PhpHelper as PHP;
use \ion\WordPress\WordPressHelper as WP;
use \ion\Viewport\RedI\RedIFeedPlugIn AS RedI;
use \ion\Viewport\RedI\RedIFeedAgentWidget as AgentWidget;
use \ion\Viewport\RedI\RedIFeedOrderingWidget as OrderingWidget;
?>

<div class="find-future-wrap">
    <h6>Find your future home</h6>
    <?php WP::doShortCode("redi-search"); ?>
</div>

<script>
    let slider = document.querySelector('.slider');  
    window.onload = function(){
        let sliderHandle = document.querySelector('.noUi-connects');
        if(sliderHandle === null){
        slider.style.cssText = "display: none !important;"
    }}
</script>