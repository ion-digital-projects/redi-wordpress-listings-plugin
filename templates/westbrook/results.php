<?php

/* 
 * See license information at the package root in LICENSE.md
 */

use \ion\PhpHelper as PHP;
use \ion\WordPress\WordPressHelper as WP;
use \ion\Viewport\RedI\RedIFeedPlugIn AS RedI;
use \ion\Viewport\RedI\RedIFeedAgentWidget as AgentWidget;
use \ion\Viewport\RedI\RedIFeedOrderingWidget as OrderingWidget;

?>
<div class="results">
    <div class="results-left">
    <span class="mobile-none">Showing                 
        <?php if(Redi::GetTotalCount() > 0): ?>
        <strong><?php RedI::PropertyRecordsCurrentPage(); ?></strong> of</span> <strong><?php Redi::PropertyRecordsTotal(); ?></strong>
       <span class="mobile-none">Search</span> results
        <?php else: ?>
        <strong>No properties</strong>
        <?php endif; ?>
    </div>
    <div class="results-right">
    
        <!-- Sort by: -->
        <?php WP::widget(new OrderingWidget()); ?>
        <div style="width: 2px;
  height: 30px;
  background-color: #ebeff1; margin: 0 20px 0 15px;" class="mobile-none"></div>
    <span class="mobile-none">
        View:
       <?php RedI::DisplaySelection(); ?>
    </span>
    </div>
</div>     

<script>
let dropDown = document.getElementById("orderBy_widget_select");
let elSize = (7.1 *dropDown.options[dropDown.selectedIndex].text.length)+30;
dropDown.style.width = "" + elSize + "px";
</script>