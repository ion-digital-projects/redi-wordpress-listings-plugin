<?php 

use \ion\PhpHelper as PHP;
use \ion\WordPress\WordPressHelper as WP;
use \ion\Viewport\RedI\RedIFeedPlugIn AS RedI;

if(PHP::filterInput('HTTP_HOST', [ INPUT_SERVER ])!= 'demo.ion.digital'):
    
RedI::themeFooter();
    
else: ?><!-- Beginning footer section
            ============================== -->
            <footer class="main-footer-section">
                <div class="common-wrap clear ">
                   <div class="footer-logo">
                       <a href="http://westbrook.flywheelsites.com"><img src="http://westbrook.flywheelsites.com/wp-content/themes/westbrook/svg/westbrook-logo-white.svg" alt=""></a>
                   </div>
                    <div class="widget-wrap">
                        <div class="widget-item">
                                                            <div class="widget-inner-item">
                                    <h7>Contact Number</h7>
                                    <a href="tel:041 001 0090">041 001 0090</a>
                                </div>
                                                            <div class="widget-inner-item">
                                    <h7>Email</h7>
                                    <a href="mailto:info@westbrooklife.co.za">info@westbrooklife.co.za</a>
                                </div>
                                                            <div class="widget-inner-item">
                                    <h7>Location</h7>
                                    <address>The Ridge, Summerville Avenue, Morningside, Port Elizabeth, 6025</address>
                                </div>
                                                    </div>
                        <div class="widget-item">
                            <div class="widget-inner-item">
                                <h7>Follow Us</h7>
                                <p>Stay in the know about our development by connecting with us on Social Media.</p>
                            </div>
                                                            <div class="widget-inner-item social-link">
                                    <h6>FOLLOW US:</h6>
                                                                            <a href="#" target="_blank"><img src="http://westbrook.flywheelsites.com/wp-content/themes/westbrook/svg/social-facebook.svg"></a>
                                                                            <a href="#" target="_blank"><img src="http://westbrook.flywheelsites.com/wp-content/themes/westbrook/svg/social-twitter.svg"></a>
                                                                    </div>
                                                    </div>
                    </div>
                </div>
                <div class="footer-bottom">
                    <div class="common-wrap clear">
                        <div class="footer-bottom-nav">
                            <ul>
                                                                    <li><a href="http://westbrook.flywheelsites.com/?page_id=3">Terms of Use</a></li>
                                                                    <li><a href="http://westbrook.flywheelsites.com/?page_id=3">Privacy Policy</a></li>
                                                                    <li><a href="http://westbrook.flywheelsites.com/?page_id=3">Sitemap</a></li>
                                                        </ul>
                        </div>
                        <p>&copy; Copyright 2020 Westbrook Lifestyle Villages. All Rights Reserved.</p>
                    </div>
                </div>
            </footer>
            <!-- //End main footer section -->
        </main>
        <script type='text/javascript' src='http://westbrook.flywheelsites.com/wp-content/themes/westbrook/js/jquery-3.4.1.min.js?ver=5.4.2'></script>
<script type='text/javascript' src='http://westbrook.flywheelsites.com/wp-content/themes/westbrook/js/jquery-ui.min.js?ver=5.4.2'></script>
<script type='text/javascript' src='http://westbrook.flywheelsites.com/wp-content/themes/westbrook/js/jquery.selectric.js?ver=5.4.2'></script>
<script type='text/javascript' src='http://westbrook.flywheelsites.com/wp-content/themes/westbrook/js/slick.min.js?ver=5.4.2'></script>
<script type='text/javascript' src='http://westbrook.flywheelsites.com/wp-content/themes/westbrook/js/jquery.fancybox.min.js?ver=5.4.2'></script>
<script type='text/javascript' src='http://westbrook.flywheelsites.com/wp-content/themes/westbrook/js/common-scripts.js?ver=1.0.1'></script>
<script type='text/javascript' src='http://westbrook.flywheelsites.com/wp-includes/js/wp-embed.min.js?ver=5.4.2'></script>
        <script>
            $( function() {
                var $slider = $("#slider-range");
                //Get min and max values
                var priceMin = $slider.attr("data-price-min"),
                    priceMax = $slider.attr("data-price-max");

                //Set min and max values where relevant
                $("#price-filter-min, #price-filter-max").map(function(){
                    $(this).attr({
                        "min": priceMin,
                        "max": priceMax
                    });
                });
                $("#price-filter-min").attr({
                    "placeholder": "min " + priceMin,
                    "value": priceMin
                });
                $("#price-filter-max").attr({
                    "placeholder": "max " + priceMax,
                    "value": priceMax
                });

                $slider.slider({
                    range: true,
                    min: Math.max(priceMin, 0),
                    max: priceMax,
                    values: [priceMin, priceMax],
                    slide: function(event, ui) {
                        // $("#amount").val("$" + ui.values[0] + " - $" + ui.values[1]);
                        $("#price-filter-min").val(ui.values[0]);
                        $("#price-filter-max").val(ui.values[1]);
                    }
                });
            } );
        </script>
    </body>
</html>

<?php endif; ?>