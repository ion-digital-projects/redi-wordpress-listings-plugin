<?php

/* 
 * See license information at the package root in LICENSE.md
 */

use \ion\PhpHelper as PHP;
use \ion\WordPress\WordPressHelper as WP;
use \ion\Viewport\RedI\RedIFeedPlugIn AS RedI;
use \ion\Viewport\RedI\RedIFeedAgentWidget as AgentWidget;
use \ion\Viewport\RedI\RedIFeedOrderingWidget as OrderingWidget;
?>


<?php if (RedI::HasPreviousPropertyPage() || RedI::HasNextPropertyPage()): ?>
    <div class="pagination">	
        <?php RedI::PropertyPages(); ?>
    </div>
<?php endif; ?>

<script>
    var pages = document.querySelectorAll(".pagination a");
    for(let i = 0; i < pages.length; i++){
        let existing = pages[i].innerHTML;
        if(existing <10){
            pages[i].innerHTML = "0" + existing + "";
        }
    } 
</script>