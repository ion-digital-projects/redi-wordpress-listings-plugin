<?php

use \ion\PhpHelper as PHP;
use \ion\WordPress\WordPressHelper as WP;
use \ion\Viewport\RedI\RedIFeedPlugIn AS RedI;
use \ion\Viewport\RedI\RedIFeedAgentWidget as AgentWidget;
use \ion\Viewport\RedI\RedIFeedOrderingWidget as OrderingWidget;
?>

<?php include_once(__DIR__ . "/common.php"); ?>

<?php include_once(__DIR__ . "/header.php"); ?>

<!-- <div class="single-pagination">
 
        <a href="<?php RedI::PropertyIndexLink(); ?>" class="btn outline back"><span></span>Back to Property Listings</a> 

        <span class="pagination-caption">Units for sale</span>
        <span class="pagination-divider"></span>
        <span class="pagination-development"><?php RedI::DevelopmentName(true); ?></span>

        <?php if(RedI::HasNextPropertyLink()): ?>
            <a href="<?php RedI::NextPropertyLink(); ?>" class="btn outline next">Next Property<span></span></a>
        <?php endif; ?>
        
    </div> -->

<span class="properties-single">

    <div class="property-development">
        <?php swatch(); ?>
        <?php RedI::DevelopmentName(true); ?>
        <!-- added from single listing -->
        <?php if(WP::getOption('redi-show-unit-number', true)): ?>
            <div class="divider"></div>
            <small>Unit <?php RedI::PropertyUnitNumber(); ?></small><br />
            <?php endif; ?>
    </div>
    <div class="property-price">

        <h2><?php RedI::PropertyPrice(); ?></h2>

    </div>    

    <div class="property-info">

        
        
        <div class="copy">

            <div class="gallery-contain">
                <div class="flexslider" id="slider">                   
                    <ul class="slides">
                        <?php RedI::PropertyImages("li"); ?>
                    </ul>
                </div>
                <?php if(RedI::PropertyImageCount() > 1): ?>
                <div class="flexslider" id="carousel">
                    <ul class="slides">
                        <?php RedI::PropertyThumbs("li"); ?>
                    </ul>
                </div>
                <?php endif; ?>
            </div><!-- /gallery-contain -->

            <div class="property-features">
                <?php RedI::PropertyFeatures(false, true, false, false, false, false); ?>                                        
                    <!--<div class="divider"></div>-->
                    <?php RedI::PropertyFeatures(false, false, true, false, false, false); ?>
                    <!--<div class="divider"></div>-->
                    <?php RedI::PropertyFeatures(false, false, false, true, false, true); ?>
                    <!--<div class="divider"></div>-->
                    <?php RedI::PropertyFeatures(false, false, false, false, true, false); ?>
            </div>

            <div class="property-description">
                <h6>Property Description</h6>

                <!--                
                <p>Pristine Modern Contemporary Home Nestled in the prestigious Waterfall Country Estate. This spectacular contemporary home offers the very best of Waterfall Country Estate Living! Located within walking distance of Reddam House School.</p>
                <p>The sleek Monolithic architecture of this spectacular home with the intent to create a home that fosters interaction not only between family members but also with the surrounding environment and community. The openness and space this horseshoe shaped home creates is awe inspiring.</p>
                -->

                <?php RedI::PropertyDescription(); ?>

            </div><!-- /description -->

            <?php if ((RedI::PropertyHasPlans() || Redi::IsDebugMode()) && strlen(RedI::PropertyPlanImages(null, false)) > 0): ?>
                <div class="plans">
                    <h6>Plan Images</h6>
                    <p><?php RedI::PropertyPlanImages(); ?></p>
                </div><!-- /plans -->
            <?php endif; ?>


        </div><!-- /copy -->

        <div class="sidebar">

            <?php WP::widget(new AgentWidget()); ?>

        </div><!-- /sidebar -->

        <script type="text/javascript" > 

        var textSelecter = document.querySelectorAll("textarea[name='note']");
         textSelecter[0].className ="required";
        var selector = document.getElementsByClassName("required");
        for(let i = 0; i < selector.length; i ++){

            var div = document.createElement("div");
            div.className = "field";

            var label = document.createElement("label");
            label.className = "field-label";

            let labelName = selector[i].getAttribute("placeholder");

            label.setAttribute("alt", labelName);
            label.setAttribute("placeholder", labelName);

            selector[i].setAttribute("placeholder", "");
            selector[i].setAttribute("required", '');

            var parent = this.selector[i].parentNode;
            parent.insertBefore(div, selector[i]);
            div.appendChild(selector[i]);
            div.appendChild(label);
        }
        let header = document.getElementsByClassName("main-header-section");
        let logoW = document.getElementsByClassName("white-logo");
        let logoB = document.getElementsByClassName("black-logo");
        header[0].style.backgroundColor = 'black';
        logoW[0].style.display = 'block';
        logoB[0].style.display = 'none';
        let labels = document.getElementById("menu-header-menu").children;
        for(let i = 0; i < labels.length; i++)
        {
            labels[i].children[0].style.color = 'rgba(255, 255, 255, .9)';
        } 
    </script>

    </div>
</span>

<?php include_once(__DIR__ . "/map.php"); ?>

<?php
include_once(__DIR__ . "/footer.php");

