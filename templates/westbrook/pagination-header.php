
<?php

/* 
 * See license information at the package root in LICENSE.md
 */

use \ion\PhpHelper as PHP;
use \ion\WordPress\WordPressHelper as WP;
use \ion\Viewport\RedI\RedIFeedPlugIn AS RedI;

if(!PHP::strEndsWith(PHP::filterInput('HTTP_HOST', [ INPUT_SERVER ]),'ion.digital')) {
    
    RedI::themeHeader();
}
else {
    
    include_once( __DIR__ . "/dev_header.php" );
}
?>



<div class="single-pagination">
 
        <a href="<?php RedI::PropertyIndexLink(); ?>" class="btn outline back"><span></span>Back to Property Listings</a> 

        <span class="pagination-caption">Units for sale</span>
        <span class="pagination-divider"></span>
        <span class="pagination-development"><?php RedI::DevelopmentName(true); ?></span>

        <?php if(RedI::HasNextPropertyLink()): ?>
            <a href="<?php RedI::NextPropertyLink(); ?>" class="btn outline next">Next Property<span></span></a>
        <?php endif; ?>
        
    </div>