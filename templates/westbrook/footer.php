<?php

/* 
 * See license information at the package root in LICENSE.md
 */

use \ion\PhpHelper as PHP;
use \ion\WordPress\WordPressHelper as WP;
use \ion\Viewport\RedI\RedIFeedPlugIn AS RedI;

?>

<?php include_once(__DIR__ . "/map.php"); ?>

            </div>
        </span>
    </span>
</section>


<?php

if(!PHP::strEndsWith(PHP::filterInput('HTTP_HOST', [ INPUT_SERVER ]),'ion.digital')) {
    
    RedI::themeFooter();
}
else {
    
    include_once( __DIR__ . "/dev_footer.php" );
}