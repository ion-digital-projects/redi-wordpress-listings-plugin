<?php
/*

  Template Name: RedI Single Listing (Avada)

 */

require_once(__DIR__ . '/avada/common.php');

use \ion\WordPress\WordPressHelper as WP;
use \ion\Viewport\RedI\RedIFeedPlugIn AS RedI;
use \ion\Viewport\RedI\RedIFeedAgentWidget as AgentWidget;
use \ion\SemVer;

//RedI::PrintDebugModels(RedI::GetViewModels());

$siteUrl = WP::siteLink(null, null, true, false);

WP::addStyle('infi-elegant-elements', "{$siteUrl}wp-content/plugins/elegant-elements-fusion-builder/assets/css/min/elegant-elements.min.css", false, true, false, 'screen', 1000);
WP::addStyle('infi-elegant-animations', "{$siteUrl}wp-content/plugins/elegant-elements-fusion-builder/assets/css/min/infi-css-animations.min.css", false, true, false, 'screen', 1000);
WP::addStyle('infi-elegant-combined-css', "{$siteUrl}wp-content/plugins/elegant-elements-fusion-builder/assets/css/min/elegant-elements-combined.min.css", false, true, false, 'screen', 1000);
WP::addStyle('elegant-google-fonts', "https://fonts.googleapis.com/css?family=Open+Sans%3Aregular%7COpen+Sans%3A300&#038;", false, true, false, 'screen', 1000);
//WP::addStyle('duplicate-post', "{$siteUrl}wp-content/plugins/duplicate-post/duplicate-post.css", false, true, false, 'screen', 1000);
//WP::addStyle('jquery-lazyloadxt-spinner-css', "{$siteUrl}wp-content/plugins/a3-lazy-load/assets/css/jquery.lazyloadxt.spinner.css", false, true, false, 'screen', 1000);

if(defined('FUSION_FILENAME')) {
    WP::addStyle('fusion-dynamic-css', "{$siteUrl}wp-content/uploads/fusion-styles/" . FUSION_FILENAME, false, true, false, 'screen', 1000000, null, ['avada-stylesheet']);
}

WP::addStyle('redi-custom', <<<INLINE
.property-info .email-form form.enquiry-form input::placeholder {
    color: #3336 !important;
}
INLINE
        , false, true, true);

WP::addScript('avada-menu-vars', <<<INLINE
/* <![CDATA[ */
var avadaMenuVars = {"header_position":"Top","logo_alignment":"Left","header_sticky":"1","side_header_break_point":"1150","mobile_menu_design":"modern","dropdown_goto":"Go to...","mobile_nav_cart":"Shopping Cart","submenu_slideout":"1"};
/* ]]> */   
INLINE
        ,false, true, true, 'screen', 1000
        );

WP::addScript('modernizr', "{$siteUrl}wp-content/themes/Avada/includes/lib/assets/min/js/library/modernizr.js", false, true, false, false, 1);

//WP::addScript('avada-menu', "{$siteUrl}wp-content/themes/Avada/assets/min/js/general/avada-menu.js", false, true, false, true, 1001);
//WP::addScript('avada-vertical-menu-widget', "{$siteUrl}wp-content/themes/Avada/assets/min/js/general/avada-vertical-menu-widget.js", false, true);

//WP::addScript('avada-sliding-bar', "{$siteUrl}wp-content/themes/Avada/assets/min/js/general/avada-sliding-bar.js", false, true, false, true, 1000, null, ['modernizr']);
//t' src='http://valdevie.co.za/testsite/wordpress/wordpress/?ver=5.5.2'></script>

//WP::addScript('fusion-general-global', "{$siteUrl}wp-content/themes/Avada/includes/lib/assets/min/js/general/fusion-general-global.js", false, true);
//WP::addScript('fusion-button', "{$siteUrl}wp-content/themes/Avada/includes/lib/assets/min/js/general/fusion-button.js", false, true);


WP::addScript('hover-intent', "{$siteUrl}wp-content/themes/Avada/includes/lib/assets/min/js/library/jquery.hoverintent.js", false, true, false, true, 1000, null, ['modernizr']);


//include_once(get_template_directory() . "/header.php");
//get_header();
RedI::themeHeader();
?>

<span class="red-i">

<div class="spacer"></div>
<div class="banner">
    <div class="wrapper">
        
        <div class="title">
            <h1><?php RedI::PropertyTitle(); ?></h1>
            <p><?php RedI::PropertySubTitle(); ?></p>
        </div>

        <div class="price">

            <?php if(WP::getOption('redi-show-unit-number', true)): ?>
            <small>Unit # <?php RedI::PropertyUnitNumber(); ?></small><br />
            <?php endif; ?>

            <?php RedI::PlanSelection(); ?>            
            <h2><?php RedI::PropertyPrice(); ?></h2>
            
        </div><!--/price -->

    </div><!-- /wrapper -->	
</div><!-- /banner -->
<div class="property-info">	
    <div class="wrapper">


        <?php //print_r(RedI::GetCurrentViewModel()->GetPlans()); ?>


        <div class="copy">

            <div class="gallery-contain">
                <div class="flexslider" id="slider">
                    <div class="status"><?php RedI::PropertyStatus(); ?></div>
                    <ul class="slides">
                        <?php RedI::PropertyImages("li"); ?>
                    </ul>
                </div>
                <div class="flexslider" id="carousel">
                    <ul class="slides">
                        <?php RedI::PropertyThumbs("li"); ?>
                    </ul>
                </div>
            </div><!-- /gallery-contain -->

            <?php RedI::PropertyFeatures(); ?>

            <div class="description">
                <h6>Property Description</h6>

                <!--                
                <p>Pristine Modern Contemporary Home Nestled in the prestigious Waterfall Country Estate. This spectacular contemporary home offers the very best of Waterfall Country Estate Living! Located within walking distance of Reddam House School.</p>
                <p>The sleek Monolithic architecture of this spectacular home with the intent to create a home that fosters interaction not only between family members but also with the surrounding environment and community. The openness and space this horseshoe shaped home creates is awe inspiring.</p>
                -->

                <?php RedI::PropertyDescription(); ?>

            </div><!-- /description -->

            <div class="show-times">
                <!--
                <h6>Show Times</h6>		
                <p>Monday - Friday  <br>
                    8am - 5pm</p>
                <p>Saturday  <br>
                    8am - 1pm</p>
                -->
                <?php RedI::PropertyShowTimes(); ?>

                <?php if (RedI::HasPropertyMapLink()): ?>
                    <div class="view-map">
                        <a target="_blank" href="<?php RedI::PropertyMapLink(); ?>" class="btn"><i class="fa fa-map-marker"></i>View property on map</a>
                    </div><!-- /view-map -->
                <?php endif; ?>

            </div><!-- /show-times -->       

            <?php //var_dump(RedI::PropertyHasPlans()); exit; ?>
            
            <?php if ((RedI::PropertyHasPlans() || Redi::IsDebugMode()) && strlen(RedI::PropertyPlanImages(null, false)) > 0): ?>
                <div class="plans">
                    <h6>Plan Images</h6>
                    <p><?php RedI::PropertyPlanImages(); ?></p>
                </div><!-- /plans -->
            <?php endif; ?>

            
        </div><!-- /copy -->

        <div class="sidebar">

            <?php WP::widget(new AgentWidget()); ?>

            <div class="breadcrumbs">
                <p><i class="fa fa-fw fa-sitemap"></i><?php RedI::BreadCrumbs(); ?></p>
            </div><!-- /breadcrumbs -->


            <?php if (function_exists('ADDTOANY_SHARE_SAVE_KIT')): ?>
            <div class="share-page">                
              <h6>Share this page</h6><p><?php ADDTOANY_SHARE_SAVE_KIT(); ?>
            </div>
            <?php endif; ?>

        </div><!-- /sidebar -->


        <div class="pagination">				       
            <a href="<?php RedI::PropertyIndexLink(); ?>" class="btn outline prev"> <i class="fa fa-fw fa-angle-left"></i> Back to property listings</a>
            <?php if(RedI::HasNextPropertyLink()): ?>
                <a href="<?php RedI::NextPropertyLink(); ?>" class="btn outline next">Next Property <i class="fa fa-fw fa-angle-right"></i></a>
            <?php endif; ?>
        </div><!--/pagination -->


    </div><!-- /wrapper -->
</div><!-- /property-info -->

</span>

<?php //include_once(get_template_directory() . "/footer.php");

RedI::themeFooter();
//exit;
//get_footer();