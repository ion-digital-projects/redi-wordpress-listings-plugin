
<div id="content" role="main">

    <div class="banner has-hover is-full-height" id="banner-854847059">
        <div class="banner-inner fill">
            <div class="banner-bg fill">
                <div class="bg fill bg-fill "></div>
            </div>
            <div class="banner-layers container">
                <div class="fill banner-link"></div>
                <div id="text-box-103851227" class="text-box banner-layer x50 md-x50 lg-x50 y50 md-y50 lg-y50 res-text">
                    <div class="text dark text-shadow-2">
                        <div class="text-inner text-center">
                            <h3><span style="font-size: 200%;"><strong>Live Listings</strong></span></h3>
                        </div>
                    </div>
                    <style scope="scope">

                        #text-box-103851227 {
                            width: 60%;
                        }
                        #text-box-103851227 .text {
                            font-size: 100%;
                        }
                    </style>
                </div>
            </div>
        </div>
        <style scope="scope">

            #banner-854847059 {
                padding-top: 100%;
            }
            #banner-854847059 .bg.bg-loaded {
                background-image: url(https://therest.co.za/wp-content/uploads/2019/02/LiveListings_01.jpg);
            }
        </style>
    </div>
    <div class="gap-element clearfix" style="display:block; height:auto; padding-top:5px"></div>
    <section class="section" id="section_1321143743">
        <div class="bg section-bg fill bg-fill  bg-loaded">
        </div>
        <div class="section-content relative">
            <div class="row" id="row-255391401">
                <div class="col small-12 large-12">
                    <div class="col-inner" style="padding:20px 20px 20px 20px;">
