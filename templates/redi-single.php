<?php 
/**
 * Template name: WF - RedI Single
 */
require_once(__DIR__ . "/waterfall/trait-singleton.php"); 
require_once(__DIR__ . "/waterfall/class-redix.php");

use \ion\WordPress\WordPressHelper as WP;
use \ion\Viewport\RedI\RedIFeedPlugIn AS RedI;
use \ion\Viewport\RedI\RedIFeedFilterWidget as FilterWidget;
use \ion\Viewport\RedI\RedIFeedOrderingWidget as OrderingWidget;



?>

<?= RedIX::PageTitle( Helper::template('global/header') ) ?>
	<div class="wf-page-screen" data-id="<?= get_the_ID() ?>" data-screen="ScreenPage" style="<?= Helper::css([]) ?>">
		<div class="wf-page-body wf-redi-body">
			<?= Helper::template('navbar/navbar') ?>
			<script>
				var wfRediAgentData = <?= json_encode( RedIX::GetAgentData() ) ?>;
			</script>
			<div class="wf-redi">
				<div class="container">
					<?= Helper::template('redi/hero-single') ?>
					<div class="wf-redi-single">
						<div class="wf-redi-single-content">
							<?= Helper::template('redi/gallery') ?>
							<?= Helper::template('redi/info') ?>
							<?= Helper::template('redi/plans') ?>
						</div>
						<div class="wf-redi-single-sidebar">
							<?php if( count( RedIX::GetAgents() ) > 0 ) : ?>
							<div class="wf-redi-single-sidebar__item wf-redi-single-sidebar__item--agentinfo">
								<?= Helper::template('redi/agent-info') ?>
							</div>
							<?php endif ?>
							
							<div class="wf-redi-single-sidebar__item wf-redi-single-sidebar__item--agentcontact">
								<?= Helper::template('redi/agent-contact') ?>
							</div>
						</div>
					</div>
				</div>
			</div>			
			<?= Helper::template('redi/pagination-single') ?>
			<?= Helper::template('footer/footer') ?>
		</div>		
	</div>	
<?= Helper::template('global/footer') ?>