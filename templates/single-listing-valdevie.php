<?php
/*

  Template Name: RedI Single Listing (Val de Vie)

 */

use \ion\WordPress\WordPressHelper as WP;
use \ion\Viewport\RedI\RedIFeedPlugIn AS RedI;
use \ion\Viewport\RedI\RedIFeedAgentWidget as AgentWidget;

//RedI::PrintDebugModels(RedI::GetViewModels());

//die(get_template_directory() . "/header.php");

//get_header();



//include_once(get_template_directory() . "/header.php");
RedI::themeHeader();
?>

<span class="red-i">

<div class="spacer"></div>
<div class="banner">
    <div class="wrapper">
        
        <div class="title">
            <h1><?php 

$devName = RedI::DevelopmentName(false);
$propTitle = RedI::PropertyTitle(false);

RedI::DevelopmentName(); 

if($devName != $propTitle) {
    echo " - "; 
    RedI::PropertyTitle();
}

          ?></h1>
            <p><?php RedI::PropertySubTitle(); ?></p>
        </div>

        <div class="price">

            <?php if(WP::getOption('redi-show-unit-number', true)): ?>
            <small>Unit # <?php RedI::PropertyUnitNumber(); ?></small><br />
            <?php endif; ?>
            
            <?php RedI::PlanSelection(); ?>            
            <h2><?php RedI::PropertyPrice(); ?></h2>
            
        </div><!--/price -->

    </div><!-- /wrapper -->	
</div><!-- /banner -->
<div class="property-info">	
    <div class="wrapper">


        <?php //print_r(RedI::GetCurrentViewModel()->GetPlans()); ?>


        <div class="copy">

            <div class="gallery-contain">
                <div class="flexslider" id="slider">
                    <div class="status"><?php RedI::PropertyStatus(); ?></div>
                    <ul class="slides">
                        <?php RedI::PropertyImages("li"); ?>
                    </ul>
                </div>
                <div class="flexslider" id="carousel">
                    <ul class="slides">
                        <?php RedI::PropertyThumbs("li"); ?>
                    </ul>
                </div>
            </div><!-- /gallery-contain -->

            <?php RedI::PropertyFeatures(); ?>

            <div class="description">
                <h6>Property Description</h6>

                <!--                
                <p>Pristine Modern Contemporary Home Nestled in the prestigious Waterfall Country Estate. This spectacular contemporary home offers the very best of Waterfall Country Estate Living! Located within walking distance of Reddam House School.</p>
                <p>The sleek Monolithic architecture of this spectacular home with the intent to create a home that fosters interaction not only between family members but also with the surrounding environment and community. The openness and space this horseshoe shaped home creates is awe inspiring.</p>
                -->

                <?php RedI::PropertyDescription(); ?>

            </div><!-- /description -->

            <div class="show-times">
                <!--
                <h6>Show Times</h6>		
                <p>Monday - Friday  <br>
                    8am - 5pm</p>
                <p>Saturday  <br>
                    8am - 1pm</p>
                -->
                <?php RedI::PropertyShowTimes(); ?>

                <?php if (RedI::HasPropertyMapLink()): ?>
                    <div class="view-map">
                        <a target="_blank" href="<?php RedI::PropertyMapLink(); ?>" class="btn"><i class="fa fa-map-marker"></i>View property on map</a>
                    </div><!-- /view-map -->
                <?php endif; ?>

            </div><!-- /show-times -->       

            <?php //var_dump(RedI::PropertyHasPlans()); exit; ?>
            
            <?php if ((RedI::PropertyHasPlans() || Redi::IsDebugMode()) && strlen(RedI::PropertyPlanImages(null, false)) > 0): ?>
                <div class="plans">
                    <h6>Plan Images</h6>
                    <p><?php RedI::PropertyPlanImages(); ?></p>
                </div><!-- /plans -->
            <?php endif; ?>

            
        </div><!-- /copy -->

        <div class="sidebar">

            <?php WP::widget(new AgentWidget()); ?>
            <?php /*
            <div class="breadcrumbs">
                <p><i class="fa fa-fw fa-sitemap"></i><?php RedI::BreadCrumbs(); ?></p>
            </div><!-- /breadcrumbs -->
            */ ?>
            
            <?php if (function_exists('ADDTOANY_SHARE_SAVE_KIT')): ?>
            <div class="share-page">                
              <h6>Share this page</h6><p><?php ADDTOANY_SHARE_SAVE_KIT(); ?>
            </div>
            <?php endif; ?>
            
        </div><!-- /sidebar -->

    </div><!-- /wrapper -->
</div><!-- /property-info -->

<div class="pagination">				       
    <a href="<?php RedI::PropertyIndexLink(); ?>" class="btn outline">Back to listings</a>
    <?php if(RedI::HasPreviousPropertyLink()): ?>
        <a href="<?php RedI::PreviousPropertyLink(); ?>" class="btn outline prev"><i class="fa fa-fw fa-angle-left"></i> Back</a>
    <?php endif; ?>    
    <?php if(RedI::HasNextPropertyLink()): ?>
        <a href="<?php RedI::NextPropertyLink(); ?>" class="btn outline next">Next <i class="fa fa-fw fa-angle-right"></i></a>
    <?php endif; ?>
</div><!--/pagination -->

</span>

<?php 

//include_once(get_template_directory() . "/footer.php");
RedI::themeFooter();
//exit;
